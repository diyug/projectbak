<?php


	function getStatus($status)
	{

		switch($status)
		{
			case 0:
				return  '未激活用户'; 
					
			case 1:
			  	 return '普通用户';
			
			case 2:
				 return '管理员';
			
			default :
				return '未知用户';
			break;
		}

	}

	/**
	*	识别前台专题的函数
	*/
	function showSpecial($special)
	{
		switch($special)
		{
			case 1:
				echo '热门品牌_big';
			break;
			case 2:
				echo '热门品牌_small';
			break;
			case 3:
				echo '运动馆';
			break;
			case 4:
				echo '女鞋馆';
			break;
			case 5:
				echo '户外馆';
			break;
			case 6:
				echo '首尔站';
			break;
			case 7:
				echo '男鞋馆';
			break;
			case 8:
				echo '服装馆';
			break;
			case 9:
				echo '孕婴童';
			break;
			case 10:
				echo '特卖专区';
			break;
			case 11:
				echo '导购专区';
			break;
		}
	}

	/**
	*	识别前台分支的函数
	*/
	function showBranch($branch)
	{
		switch($branch)
		{
			case 0:
				echo '小图';
			break;
			case 1:
				echo '中图';
			break;
			case 2:
				echo '大图';
			break;
		}
	}

	/**
	*	识别前台分支的函数
	*/
	function showOrderStatus($code)
	{
		switch($code)
		{
			case 0:
				echo '订单已取消';
			break;
			case 1:
				echo '未支付';
			break;
			case 2:
				echo '已发货';
			break;
			case 3:
				echo '已收货';
			break;
			case 4:
				echo '退货';
			break;
			case 5:
				echo '换货';
			break;
		}
	}

	/**
	*	识别送货时间的状态
	*/
	function showExpressStatus($code)
	{
		switch($code)
		{
			case 1:
				echo '不限时间';
			break;
			case 2:
				echo '周一至周五';
			break;
			case 3:
				echo '周六日/节假日';
			break;
		}
	}

	/**
	*	识别是否分类寄存的函数
	*/
	function showCateFalse($code)
	{
		switch($code)
		{
			case 0:
				echo '不寄存';
			break;
			case 1:
				echo '寄存';
			break;
		}
	}