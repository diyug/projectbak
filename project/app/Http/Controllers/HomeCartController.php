<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Session;
class HomeCartController extends Controller
{
   /**
	*	显示跳转页面
	*/
	public function getInsert(Request $request)
	{
		//获取数据
   
		$res = $request->except('_token');
		//把获取的数据存入session中
		// session::push('cart',$res);
      // session('id')   session('username')
	
    // session(['id'=>100]);
    // session(['username'=>'liyao']);
    // $res=['num'=>'1','color'=>'蓝色','size'=>'xl','sid'=>'7'];
    
    //判断用户是否登录
      if(session('id')){
    //登录的情况下向数据库添加数据
         $res['uid'] = session('id'); 
          DB::table('ug_cart')->insertGetId($res);
      }else{
    //未登录的情况下向session中添加数据
		$request->session()->push('cart',$res);
      }
    
		//显示模板  加入购物车成功的模板
		return view('/home/cartinsert');
	}
	
	/**
	*	显示购物车列表
	*/
   public function getIndex(Request $request)
   {
   	// session(['cart'=>null]);
    // session(['id'=>null]);
    // session(['username'=>null]);
    // var_dump(session('id'));
    // var_dump(session('username'));
    // var_dump(session('cart'));
   	//获取数据
     
      if(session('id')){
         //判断session中是否有商品 如果有存入数据库中
            if(session('cart')){
                $db = session('cart');
                foreach($db as $k=>$v){
                $db[$k]['uid'] = session('id');  
                }

               DB::table('ug_cart')->insert($db);
               session(['cart'=>null]);
            }
            //从数据库取符合条件的数据
         $object = DB::table('ug_cart')->where('uid',session('id'))->get();
        
         //将数组中的对象转化为数组中的数组
                $res = [];
            foreach($object as $k=>$v){
                $arr = [];
               foreach($v as $key=>$value){
                  //将对象转换成数组
                  $arr[$key] =$value;
               }
               $res[$k] = $arr;
            }


      }else{

        	$res = $request->session()->get('cart');
      }
   

	 //定义数组
   if(empty($res)){
      return view('/home/cart',['res'=>$res]);
   }
   	foreach($res as $k=>$v){
   		$res[$k]['into'] = DB::table('ug_goods_basic')->where('id',$v['sid'])->first();
   	}
   
    return view('/home/cart',['res'=>$res]);
   }

   /**
   *	删除数据
   */
   public function getDelete(Request $request)
   {  
    //接收数组中的下标
      $k = $request->input('sub');
      $sid = $request->input('sid');
    
    //判断用户是否登录
    if(session('id')){
    //登录的情况下删除数据
      $res = DB::table('ug_cart')->where('id',$sid)->delete();
      //登录的情况下
            if($res){
              return 0;
            }else{
              return 1;
            }

    }else{
    //未登录的情况下删除数据  
     $res=session('cart');
     unset($res[$k]);
     session(['cart'=>$res]); 
     //未登录的情况下  
         if(empty(session('id')) && empty(session('cart'))){
            return 0;
         }else{
            return 1;
         }
    }
    
        
    
   }
   /**
   *	清空购物车
   */
   public function getEmpty(Request $request)
   {
    //判断用户是否登录
    if(session('id')){
      //登陆的情况下清空数据
      $res = DB::table('ug_cart')->where('uid',session('id'))->delete();
    }else{
      //未登录的情况下清空数据
   	$res = session(['cart'=>null]);
    }
    // var_dump(session('cart'));
   	return 'true';
}
  
    /**
    *   从购物车登录
    */
    public function postLogin(Request $request)
    {
      //接收数据
      $account = $request->input('account');
      $password = md5($request->input('password'));
      //从数据库判断是否正确
      $res = DB::table('ug_user')
               ->where('account','=',$account)
               ->where('password','=',$password)
               ->first();
      if($res){
          $id = $res->id;
          $nickname = $res->nickname;
          $username=DB::table('ug_user_details')->where('uid',$id)->first()->username;
          if(empty($username))
                            {
                                session(['id'=>$id,'username'=>$nickname]);
                            }else{
                                session(['id'=>$id,'username'=>$username]);
                            }
            return redirect('/home/cart/index')->with('success','恭喜你登录成功');
      }else{
            return back()->with('error','用户名或者密码不正确');
      }

    }

    /**
    *     结算
    **/
    public function getPrice(Request $request)
    {
      if(session('id')){
        return 1;
      }else{
        return 0;
      }
    }

    /**
    *   点击加减向数据库添加数据
    */
    public function getChange(Request $request)
    {
      $id = $request->input('id');
      $num = $request->input('num');

      $res = DB::table('ug_cart')->where('id',$id)->update(['num'=>$num]);
      
    }

    
}
