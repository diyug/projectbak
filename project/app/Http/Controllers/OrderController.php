<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    /**
    *   处理订单列表显示界面的方法
    */

    public function getIndex()
    {	
    	// 从订单表获取数据
    	$orderData = DB::table('ug_order') -> get();

        // 渲染模板
        return view('admin/order/index',[
        		'orderData'=>$orderData,
        	]);
    }

    /**
    *	订单回收站列表显示方法
    */
    public function getRecycle()
    {
        // 渲染模板
        return view('admin/order/recycle');
    }

}
