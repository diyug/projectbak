<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// 引入数据库类
use DB;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CarouselController extends Controller
{
    /**
    *   这里是管理轮播图的界面
    */
    public function getPiclist(Request $request)
    {
        // 获取要搜索的内条件,若没有便设置默认值
        $num = $request -> input('num',6);
        $search = $request -> input('search');


        // 查询轮播图元素表数据
        $data = DB::table('ug_carousel') ->
        orderby('picline','desc') ->
        where('picline','like',$search.'%') ->
        paginate($num);

        // 定义图片存放文件夹
        $path = "/home/carousel/";

        // 遍历数组
        foreach($data as $key => $value) {
            $value -> describe = DB::table('ug_cate') -> where('id',$value->cid) -> value('name');
        }

        return view('admin/carousel/piclist',[
                'data' => $data,
                'path' => $path,
                'all'  => $request -> all(),
            ]);
    }

    /**
    *   显示添加轮播图界面的方法
    */
    public function getAddpic()
    {
        // 获取有效分类
        $cates = CateController::cates();
        // 渲染模板
        return view('admin/carousel/addpic',[
                'cates' => $cates,
            ]);
    }


    /**
    *   添加数据的方法
    */
    public function postInsertpic(Request $request)
    {
        // 检测是否上传文件
        $exists = $request -> hasFile('picname');

        if(!$exists) {
            return back() -> with('error','图片木有上传呀');
        }

        // 获取参数
        $data = $request -> except('_token');

        $picline = $request -> input('picline');

        // 指定路径
        $path = '/home/carousel/';

        // 随机图片名
        $picname = md5(rand(10000,99999));

        // 获取上传文件后缀名
        $suffix = $request ->file('picname') -> getClientOriginalExtension();

        // 拼接文件名
        $image = $picname.'.'.$suffix;

        // 移动文件
        $info = $request -> file('picname') -> move('.'.$path,$image);

        if(!$info) return back() -> with('error','上传失败');

        // 存入数组
        $data['picname'] = $image;

        // 查询数据库是否有该排位
        $found = DB::table('ug_carousel') -> where('picline',$picline) -> get();

        // 如果该排位存在
        if($found) {

            DB::table('ug_carousel') -> where('picline',$picline) -> update(['picline'=>'0']);

        }

        // 执行插入操作
        $result = DB::table('ug_carousel') -> insert($data);

        if($result) {
            return redirect('/admin/Carousel/piclist') -> with('success','添加成功!');
        } else {
            return back() -> with('error','添加失败!');
        }

    }

    /**
    *   删除轮播图数据的方法
    */
    public function getDel(Request $request)
    {
        // 获取参数
        $id = $request -> only('id');

        // 执行删除操作
        $info = DB::table('ug_carousel') -> where('id',$id) ->delete();

        // 判断返回结果
        if($info) {
            return redirect('/admin/Carousel/piclist') -> with('success','已删除');
        } else {
            return back() -> with('error','删除失败');
        }

    }

    /**
    *   编辑轮播图数据界面的显示方法
    */
     public function getEdit(Request $request)
    {

        // 调用方法
        $cates = CateController::cates();

        // 获取参数
        $id = $request -> only('id');

        //查询和数据
        $data = DB::table('ug_carousel') -> where('id',$id) -> first();

        // var_dump($data);
        // var_dump($cates);
        // die;

        // 渲染视图
        return view('admin/carousel/edit',[
                'data' => $data,
                'cates' => $cates,
            ]);
    }

    /**
    *   更改轮播图数据的方法
    */
     public function postUpdate(Request $request)
    {   

        // 获取参数
        $oldpicline = $request -> input('oldpicline');

        $picline = $request -> input('picline');

        $id = $request -> input('id');

        $data = $request -> only(['picline','cid']);


        if($picline != $oldpicline) {

            if($oldpicline == 0) {
                // 交换次序 不能有多个排位次序相同的
                DB::table('ug_carousel') -> where('picline',$picline) -> update(['picline'=>'0']);
            }
                // 查询该排位是否有元素占位
                $exists = DB::table('ug_carousel') -> where('picline',$picline) -> get();

                if($exists && $picline != '0') {

                     // 交换次序 不能有多个排位次序相同的
                    DB::table('ug_carousel') -> where('picline',$picline) -> update(['picline'=>$oldpicline]);
                }

                // 更改本身的数据
                $info = DB::table('ug_carousel') -> where('id',$id) -> update(['picline'=>$picline]);
        }

       

        // 判断操作返回结果
        if($info) {

            // 跳转界面
           return redirect('/admin/Carousel/piclist') -> with('success','修改了'.$info.'条数据');

        } else{
            // 返回错误信息
            return back() -> with('error','删除失败!');
        }
        
    }

    /**
    *   获取轮播图片元素的方法
    *
    *    @param $order string  图片排序方式
    */

    public function getPicname()
    {

        // 定义空数组
        $picname = [];

        // 查询数据库获取数据
        for ($i=0; $i < 7; $i++) { 
            $picname[$i] = DB::table('ug_carousel') -> where('picline',$i) -> value('picname');
        }

        // 把排位序列为0的删除掉
        array_shift($picname);

        // 返回数据
        return $picname;
    }

     /**
    *   获取轮播图片url地址的方法
    *
    */
     public function getPichref()
     {
        // 定义空数组
        $pichref = [];

        // 查询数据库获取数据
        for ($i=0; $i < 7; $i++) { 
            $pichref[$i] = DB::table('ug_carousel') -> where('picline',$i) -> value('cid');
        }

        // 把排位序列为0的删除掉
        array_shift($pichref);

        // 判断实参传来的排序方式

        // 返回数据
        return $pichref;
     }

    /**
    *   获取随机头部关键字的方法
    **/
    public function getRandkey()
    {
         // 获取有效分类
        $valid = CateController::valid();

        //显示几条关键字
        $keynum = 10;

        // 调用方法
        $success = $this -> unique($valid,$keynum);

        // 存入session
        session(['upkey'=>$success]);

        if($success) {
            return redirect('/admin/Carousel/piclist') -> with('success','更新成功!');
        }

    }
    

  /**
    *   保障关键字中没有重复值的方法
    *   
    *   @param $valid  array  存放有效分类id的数组
    *   @param $keynum  int    需要的关键字的条数
    */
    public function unique($valid,$count)
    {

            // 有效分类的条数
            $num = count($valid);

            //定义空数组
            $nuarr = [];
            $randkey = [];

            // 循环查询数据存入数组
            for($i=0;$i<$count;$i++) {

                // 获取随机数
                $rands = $valid[rand(0,$num-1)];

                // 存入数组名称
                $randkey[$i] = DB::table('ug_cate') -> where('id',$rands) -> value('name');

                // 存入数组
                $nuarr[] = $rands;

                // 删除数组最后一个元素
                array_pop($nuarr);

                // 数组中查找指定的值
                $liuxin = array_search($rands,$nuarr);

                // 判断搜索结果
                if($liuxin) {
                    unset($randkey[$i]);
                }

            }

            // 返回数据
            return $randkey;
    }

}
