<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// 引入DB类
use DB;

// 引入分类类
use App\Http\Controllers\CateController;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class HomeOrderController extends Controller
{

    /**
    *   前台购物车结算的显示界面
    */
    public function getPayment()
    {
        // 渲染模板
        return view('home/order/payment');
    }

    /**
    *	显示前台订单页面的方法
    */
    public function getIndex()
    {
        // 判断用户是否登录
        if(!session()->has('id')) {

            // 重定URL向到登录页
            return redirect('/home/User/login');
        }

        // 获取登录用户ID
        $uid = session() -> get('id');

        // 查询订单表
        $user_orderdata = DB::table('ug_order') -> where('uid',$uid) -> get();

        if(!$user_orderdata) return back();

        // 调用CateController类静态方法获取数据库内所有上架类信息
        $cates = CateController::getCates(0);
        
        // 将数组的最后一个元素删除
        array_pop($cates);

        // 遍历数组,添加商品属性
        foreach($user_orderdata as $key => $value) {
            $goodsData = DB::table('ug_goods_basic') -> where('id',$value -> gid) -> first();
            $order_details = DB::table('ug_order_details') -> where('oid',$value->id) -> first();
            $value -> uid = $uid;
            $value -> newUnlinePrice = $goodsData -> newprice;
            $value -> oldUnlinePrice = $goodsData -> oldprice;
            $value -> goods_describe = $goodsData -> goodsdes;
            $value -> goods_name     = $goodsData -> goodsname;
            $value -> goods_pic      = $goodsData -> goodspic;
            $value -> goods_size     = $order_details -> size;
            $value -> goods_color     = $order_details -> color;
        }

        // 存入6个不同iccon class属性的数组
        $icon = ['yund','huw','nvx','nanz','tongx','xiangb','star'];

        // 渲染模板 携带参数
        return view('home/order/order',[
            'user_orderdata' => $user_orderdata,
            'cates'          => $cates,
            'icon'           => $icon,
            ]);   
    }

    /**
    *   处理订单的方法
    */
    public function testDo()
    {

        // 获取Unix时间戳的微秒状态
        $time = time();

        // 订单状态(用户已下订单,未付款)
        $status = 1;

        // 订单号随机前缀存放数组
        $word = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];

        // 从参考订单号表获取数据
        $codes = DB::table('ug_plus_order_code') -> where('id',1) ->value('plus_code');

        // 对订单号进行++运算
        $codes++;

        // 存入数据库
        $info = DB::table('ug_plus_order_code') -> where('id',1) -> update(['plus_code'=>$codes]);

        // 判断是否存入
        if(!$info) die('订单回存反馈失败!');

        // 随机订单号
        $code = $word[rand(0,25)].$codes;

        $uname = session() -> get('username');

        // 渲染模板,附带参数
        return view('/home/order',[
            'uid'  => $uid,
            'code' => $code,
            ]);
    }

    /**
    *   显示订单详情页的界面
    */
    public function getOrderDetails(Request $request)
    {
        // 获取参数
        $uid = $request -> input('uid');
        $oid = $request -> input('oid');


        if(empty($uid) || empty($oid)) die('没有意义的请求');

        $orderdata = DB::table('ug_order_details') -> where('oid',$oid) -> where('oid',$oid) -> first();

        $ordermsg = DB::table('ug_order') -> where('uid',$uid) -> where('id',$oid) -> first();
        $orderdata -> username = DB::table('ug_user') -> where('id',$uid) -> value('nickname');

        // 获取值,截取字符串,替换字符串 目的是为了用户信息的保密性
        $oldnumber = $orderdata -> phone_number;
        $oldaddress= $orderdata -> address;
        $number =  substr($oldnumber,3,-4);
        $address =  mb_substr($oldaddress,6,9);
        $orderdata -> phone_number = str_replace($number,'****',$oldnumber);
        $orderdata -> address = str_replace($address,'****',$oldaddress);
        $goodsmsg = DB::table('ug_goods_basic') -> where('id',$orderdata->gid) -> first();

        // 调用CateController类静态方法获取数据库内所有上架类信息
        $cates = CateController::getCates(0);
        
        // 将数组的最后一个元素删除
        array_pop($cates);

        // 存入6个不同iccon class属性的数组
        $icon = ['yund','huw','nvx','nanz','tongx','xiangb','star'];

        // 渲染模板
        return view('home/order/order_detail',[
            'orderdata' => $orderdata,
            'cates'     => $cates,
            'icon'      => $icon,
            'ordermsg'  => $ordermsg,
            'goodsmsg'  => $goodsmsg,
            ]);

    }

    /**
    *   处理用户付款的方法
    */
    public function getUpOrderSta(Request $request)
    {   
        // 获取参数
        $oid = $request -> input('oid');
        $uid = $request -> input('uid');

        $info = DB::table('ug_order') -> where('id',$oid) -> where('uid',$uid) -> update(['order_status'=>2]);
        $gid = DB::table('ug_order') -> where('id',$oid) -> where('uid',$uid) -> value('gid');

        if($info) {
            $ptime = time();
            // 插入数据表交易时间
            DB::table('ug_order') -> where('id',$oid) -> where('uid',$uid) -> update(['order_payment_time'=>$ptime]);
            // 重定向
            return redirect('/home/order/order-details?uid='.$uid.'&oid='.$oid);
        }
    }

    /**
    *   下单一个小时未支付取消订单的方法
    */
    public function postCancelOrder(Request $request)
    {
        $id = $request -> input('id');

        $info = DB::table('ug_order') -> where('id',$id) -> update(['order_status'=>0]);

        if($info) {
            echo 1;
        } else {
            echo 0;
        }
    }
    /**
    *   改变倒计时时间的方法
    */
    public function postCancelSetout(Request $request)
    {

        $id = $request -> input('oid');
        $set = $request -> input('set');

        $info = DB::table('ug_order') -> where('id',$id) -> update(['order_setout'=>$set]);

        if($info) {
            echo 1;
        } else {
            echo 0;
        }

    }
}
