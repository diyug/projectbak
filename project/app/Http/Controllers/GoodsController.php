<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class GoodsController extends Controller
{
     /**
    * 这是商品添加的页面
    */

    public function getAdd()

    { 
        // 获取分类
        $data = DB::table('ug_cate') ->
        select(DB::raw('*,concat(path,id,",") as pathes')) ->
        groupby('pathes') ->
        get();

         // 遍历数组
        foreach($data as $key => $value) {

            // 把字符串分割为数组
            $exp = explode(",",$value -> path);

            // 计算数组的单元数目
            $num = count($exp) - 1;

            // 重复使用字符串
            $tag = str_repeat("|--",$num);

            $value -> name = $tag.$value -> name;
            // var_dump($value -> name);
            
        }

        // 渲染视图
        return view('admin/Goods/add',[
                'data'=>$data,
            ]);
    }

     /**
     *  商品添加的方法
     */ 

    public function postInsert(Request $request)
    {

        // 判断添加的数据
            //对用户提交数据进行验证
            $this->validate($request, [
            'goodsname' => 'required', //商品名唯一
            'goodspic' => 'required', //商品图片
            'goodsdes' => 'required', //商品的描述
            'newprice' => 'required', //商品价格  
            'oldprice' => 'required', //商品价格            
            'color' => 'required', //商品颜色
            'size' =>'required',  //商品尺寸
            ],[
            'goodsname.required'=>'商品名不能为空',
            'goodspic.required'=>'商品图片不能为空',
            'goodsdes.required'=>'商品描述不能为空',
            'newprice.required'=>'商品价格不能为空',
            'oldprice.required'=>'商品价格不能为空',
            'color.required'=>'商品颜色不能为空',
            'size.required'=>'商品尺寸不能为空',

            ]);

           
            // 获取数据
            $res = $request->except('_token');

             // 检测文件的上传
            if($request->hasFile('goodspic')) {

           // 随机名
            $name = md5(rand(100000,999999)).time();

            //  获取后缀名
            $suffix = $request ->file('goodspic')->getClientOriginalExtension();
            $request->file('goodspic')->move('./upload/',$name.'.'.$suffix);

            $res['goodspic'] ='/upload/'.$name.'.'.$suffix;

        }

            // 商品添加的时间
            $res['goodstime'] = time();
            //添加折扣

            $res['discount']=1-$res['newprice']/$res['oldprice'];

            // 把数据提交到数据库
     
            $pro = DB::table('ug_goods_basic')->insert($res);

            if($pro){

                return redirect('/admin/Goods/index')->with('success','添加成功');
            } else {

                return back()->with('error','添加失败');
            }
       }

       /**
       * 商品的显示页面
       */ 

       public function getIndex(Request $request)
       {

            // 获取数据库的数据

             $res = DB::table('ug_goods_basic')->
             where('status',1)->
            where('goodsname','like','%'.$request->input('search').'%')->
            paginate($request->input('num',10));

            // 数据显示的模板

            return view('./admin/Goods/index',['row'=>$res,'request'=>$request->all()]);
       }

        /**
        * 商品的编辑页面
        */

        public function getEdit(Request $request)
        {
            // 获取要编辑商品的id
              $id=$request->input('id');
              $res = DB::table('ug_goods_basic')->where('id',$id)->first();

            return view('/admin/Goods/edit',['row'=>$res,'id'=>$id]);
        }
        /**
        *   商品修改方法
        */
        public function postUpdate(Request $request)
        {
            $res = $request->except('_token');
            // dd($res);
             // 检测文件的上传
            if($request->hasFile('goodspic')) {

           // 随机名
            $name = md5(rand(100000,999999)).time();

            //  获取后缀名
            $suffix = $request ->file('goodspic')->getClientOriginalExtension();
            $request->file('goodspic')->move('./upload/',$name.'.'.$suffix);

            $res['goodspic'] ='/upload/'.$name.'.'.$suffix;

        }

            //把数据提交给数据库
            $pro = DB::table('ug_goods_basic')->where('id',$res['id'])->update($res);
            if($pro){
                return redirect('/admin/Goods/index')->with('success','编辑成功');
            }else{
                return back()->with('error','编辑失败');
            }
        }
        /**
        *   商品删除的方法
        */
        public function getDelete(Request $request)
        {
            $id = $request->input();
            $pro = DB::table('ug_goods_basic')->where('id',$id)->delete();
            if($pro){
               return redirect('/admin/Goods/index')->with('success','删除成功');
            }else{
                return back()->with('error','删除失败');
            }
        }
        /**
        * 商品下架的页面
        */ 

         public function getDisabled(Request $request)
         {
            // $res = DB::table('ug_goods_basic')->where('status','=',0)->get();
            $search = $request->input('search');
            $num = $request->input('num',10);
            // dd($request->input());
            //获取数据
             $data = DB::table('ug_goods_basic') ->
             where('status',0)
             ->where('goodsname','like','%'.$search.'%')
             ->paginate($num);
             
            // dd($data);
            return view('/admin/Goods/disabled',['row'=>$data,'request'=>$request->input()]);
           
         }
}
