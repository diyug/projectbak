<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
//添加
use DB;

class HomeListController extends Controller
{
    /**
    *   显示列表页
    */
    public function getIndex(Request $request)
    {

        //接收子类pid
        $pid = $request->input('id');
        // $pid = 23;
        //接收性别排序
        $sex = $request->input('sex');
        // $vsex = $request->input('vsex');
        //接收价格排列顺序
        $order = $request->input('order');
        //接收时间排列顺序值
        $time = $request->input('time');
         //接收折扣排列顺序值
        $discount = $request->input('discount');
        //接收价格区间排列顺序值
      
        $minPrice = $request->input('minPrice',0);
        $maxPrice = $request->input('maxPrice',0);
        
        //从session中接收值
        
        //  从数据库中提取数据
      
        if(!$sex && !$order && !$time && !$discount){
            $res = DB::table('ug_goods_basic')->
            where('pid',$pid)->
            where('status',1)->
            paginate(5);
        }
        //=========================性别排序===========================//
        // var_dump($nsex);
        // var_dump('男性');die;
        if(isset($sex)){
            $res = DB::table('ug_goods_basic')->
            where('pid',$pid)->
            where('status',1)->
            where('sex',$sex)->
            paginate(5);

         session(['sex'=>$sex]);
        }
      
        //==============================价格区间排序===========================//
        if($minPrice | $maxPrice){
            $res = DB::table('ug_goods_basic')->
            where('pid',$pid)->
            where('status',1)->
            where('newprice','>=',$minPrice)->
            where('newprice','<=',$maxPrice)->
            paginate(5);
        }
       //==================性别排序和价格区间排序=========================//
         // if(null!==(session('sex')) && ($minPrice | $maxPrice)){
         //    $res = DB::table('ug_goods_basic')->
         //    where('pid',$pid)->
         //    where('status',1)->
         //    where('sex',session('sex'))->
         //    where('newprice','>=',$minPrice)->
         //    where('newprice','<=',$maxPrice)->
         //    paginate(5);
         // }
         
        //========================价格排序=======================//
        //从数据库中提取数据
        if($order){
            $res = DB::table('ug_goods_basic')->
            where('pid',$pid)->
            where('status',1)->
            orderBy('newprice','asc')->
            paginate(5);
            // get();
        }
        
     //=========================时间排序======================//  
        //从数据库提取数据
        if($time){
        $res = DB::table('ug_goods_basic')->
        where('pid',$pid)->
        where('status',1)->
        orderBy('goodstime','desc')->
        paginate(5);
    }

    //============================折扣排序=============================//
   
    //从数据库提取数据
    if($discount){
        $res = DB::table('ug_goods_basic')->
        where('pid',$pid)->
        where('status',1)->
        orderBy('discount','desc')->
        paginate(5);   

    }

    //===========================总数==============================//
    //从数据库提取总数据
    $count = DB::table('ug_goods_basic')->
        where('pid',$pid)->
        where('status',1)->
        count();
    //男性总数据    
    $count1 = DB::table('ug_goods_basic')->
        where('pid',$pid)->
        where('status',1)->
        where('sex','男性')->
        count();
    //女性总数据
        $count2 = DB::table('ug_goods_basic')->
        where('pid',$pid)->
        where('status',1)->
        where('sex','女性')->
        count();
    //价格区间总数
        
        $count3 = DB::table('ug_goods_basic')->
        where('pid',$pid)->
        where('status',1)->
        where('newprice','>=',$minPrice)->
        where('newprice','<=',$maxPrice)->
        count();
       
    

        //视图模板
        return view('/home/list',['res'=>$res,'sex'=>$sex,'pid'=>$pid,'order'=>$order,'time'=>$time,'discount'=>$discount,'count'=>$count,'count1'=>$count1,'count2'=>$count2,'count3'=>$count3,'minPrice'=>$minPrice,'maxPrice'=>$maxPrice]);
    }
   
}
