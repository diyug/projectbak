<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class GoodsImagesController extends Controller
{
    
      /**
     * 这是商品图片添加的页面
     */ 

    public function getAdd()
    {


        // 返回模板
        return view('/admin/goodsimages/add');
    }


    /**
    *  这是商品图片添加的方法
    */ 

    public function postInsert(Request $request)

    {

       
       // 获取数据
            $res = $request->except('_token');

             // 检测第一张图片是否上传
            if($request->hasFile('images1')) {

           // 随机名
            $name = md5(rand(100000,999999)).time();

            //  获取后缀名
            $suffix = $request ->file('images1')->getClientOriginalExtension();
            $request->file('images1')->move('./upload/',$name.'.'.$suffix);

            $res['images1'] ='/upload/'.$name.'.'.$suffix;
       
        } 

          // 检测第二张图片是否上传
            if($request->hasFile('images2')) {

           // 随机名
            $name = md5(rand(100000,999999)).time();

            //  获取后缀名
            $suffix = $request ->file('images2')->getClientOriginalExtension();
            $request->file('images2')->move('./upload/',$name.'.'.$suffix);

            $res['images2'] ='/upload/'.$name.'.'.$suffix;
       
        } 

        // 检测第三张图片是否上传
            if($request->hasFile('images3')) {

           // 随机名
            $name = md5(rand(100000,999999)).time();

            //  获取后缀名
            $suffix = $request ->file('images3')->getClientOriginalExtension();
            $request->file('images3')->move('./upload/',$name.'.'.$suffix);

            $res['images3'] ='/upload/'.$name.'.'.$suffix;
       
        } 

        // 返回显示图片的模板

         $pro = DB::table('ug_goods_images')->insert($res);


         if($pro){

                return redirect('/admin/goodsimages/index')->with('success','添加成功');
            } else {

                return back()->with('error','添加失败');
            }
    
    }


    /**
    * 这是商品图片显示的页面
    */ 

       public function getIndex(Request $request)
       {

            // 获取数据库的数据

             $res = DB::table('ug_goods_images')->
             where('goods_id','like','%'.$request->input('search').'%')->
             paginate($request->input('num',10));

            // 数据显示的模板

            return view('./admin/goodsimages/index',['row'=>$res,'request'=>$request->all()]);
       }


       /**
       * 这是图片编辑的页面
       */ 

      public function getEdit(Request $request)
        {
            // 获取要编辑商品的id
              $id=$request->input('id');
              $res = DB::table('ug_goods_images')->where('id',$id)->first();

            return view('/admin/goodsimages/edit',['row'=>$res,'id'=>$id]);
        }


        // 这是商品图片编辑的方法

        public function postUpdate(Request $request)
        {

                $res = $request->except('_token');
                // dd($res);
              
                 //编辑第一张图片
                if($request->hasFile('images1')) {

               // 随机名
                $name = md5(rand(100000,999999)).time();

                //  获取后缀名
                $suffix = $request ->file('images1')->getClientOriginalExtension();
                $request->file('images1')->move('./upload/',$name.'.'.$suffix);

                $res['images1'] ='/upload/'.$name.'.'.$suffix;
           
            } 

              // 编辑第二张图片
                if($request->hasFile('images2')) {

               // 随机名
                $name = md5(rand(100000,999999)).time();

                //  获取后缀名
                $suffix = $request ->file('images2')->getClientOriginalExtension();
                $request->file('images2')->move('./upload/',$name.'.'.$suffix);

                $res['images2'] ='/upload/'.$name.'.'.$suffix;
           
            } 

              // 检测第三张图片是否上传
                if($request->hasFile('images3')) {

               // 随机名
                $name = md5(rand(100000,999999)).time();

                //  获取后缀名
                $suffix = $request ->file('images3')->getClientOriginalExtension();
                $request->file('images3')->move('./upload/',$name.'.'.$suffix);

                $res['images3'] ='/upload/'.$name.'.'.$suffix;
           
            } 

            //把数据提交给数据库
            $pro = DB::table('ug_goods_images')->where('id',$res['id'])->update($res);
            if($pro){
                return redirect('/admin/goodsimages/index')->with('success','编辑成功');
            }else{
                return back()->with('error','编辑失败');
            }
        }


        /**
        * 这是商品图片删除的方法
        */ 

        public function getDelete(Request $request)
        {

            $id = $request->input();
            $pro = DB::table('ug_goods_images')->where('id',$id)->delete();
            if($pro){
               return redirect('/admin/goodsimages/index')->with('success','删除成功');
            }else{
                return back()->with('error','删除失败');
            }
        }
}
