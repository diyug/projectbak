<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CateController extends Controller
{
    /**
    *   分类的显示界面
    */
    public function getIndex(Request $request)
    {
        // 获取参数
        $num = $request -> input('num',10);
        $search = $request -> input('search');

        // 查询所有数据,进行分组,模糊查询,分页
        $data = DB::table('ug_cate') ->
        select(DB::raw('*,concat(path,id,",") as pathes')) ->
        where('path','like',$search.'%')->
        where('status',1) ->
        orderby('pathes') ->
        paginate($num);
        // var_dump($data);

        // 遍历数组
        foreach($data as $key => $value) {

            // 把字符串分割为数组
            $exp = explode(",",$value -> path);

            // 计算数组的单元数目
            $num = count($exp) - 1;

            // 重复使用字符串
            $tag = str_repeat("<i class='icon-caret-right'></i>&nbsp;",$num);

            $value -> name = $tag.$value -> name;
            // var_dump($value -> name);
            
        }

        // 渲染模板
        return view("admin/cate/index",[
                'data' => $data,
                'all' => $request -> all(),
            ]);
    }

    /**
    *   分类的添加界面
    */
    public function getAdd()
    {
        // 获取分类
        $data = DB::table('ug_cate') ->
        select(DB::raw('*,concat(path,id,",") as pathes')) ->
        groupby('pathes') ->
        get();

         // 遍历数组
        foreach($data as $key => $value) {

            // 把字符串分割为数组
            $exp = explode(",",$value -> path);

            // 计算数组的单元数目
            $num = count($exp) - 1;

            // 重复使用字符串
            $tag = str_repeat("|--",$num);

            $value -> name = $tag.$value -> name;
            // var_dump($value -> name);
            
        }

        // 渲染视图
        return view('admin/cate/add',[
                'data'=>$data,
            ]);
    }

    /**
    *   添加分类的方法
    */
    public function postInsert(Request $request)
    {   
        // 获取参数
        $data = $request -> except('_token','false');
        $pid = $data['pid'];
        // dd($data);

        $false  = $request -> input('false');

        if(empty($false)) return back() -> with('error','您好像遗漏的一个重要的东西!');

        $data['false'] = $false[0];


        if($pid=='0') {

            // 拼接分类路径
            $data['path'] = $pid.',';
            // var_dump($data);

            $info = DB::table('ug_cate') -> insert($data);
            $msg = '顶级';

        } else {

            // $data['path'] = $pid
            $res = DB::table('ug_cate') -> where('id',$pid) -> first();

            // 拼接path
            $data['path'] = ($res -> path).($res -> id).',';
            
            $info = DB::table('ug_cate') -> insert($data);
            $msg = '子级';
        }

        // 如果添加成功执行跳转
        if($info) {
            return redirect('/admin/Cate/index') -> with('success','添加'.$msg.'分类成功');
        } else {
            return back() -> with('error','添加失败,请重新尝试');
        }
    }

    /**
    *   编辑分类的显示界面
    */
    public function getEdit(Request $request)
    {   
        // 获取参数
        $id = $request -> input('id');

        // 查询数据库
        $data = DB::table('ug_cate') -> where('id',$id) -> first();

        // 查询所有分类
        $all = DB::table('ug_cate') ->
        select(DB::raw('*,concat(path,id,",") as pathes')) ->
        groupby('pathes') ->
        get();

        foreach($all as $key => $value) {

            // 移动分类的下拉列表里不允许有本身
            if($value -> id == $id) {
                unset($all[$key]);
            }

            // 把字符串分割为数组
            $exp = explode(",",$value -> path);

            // 计算数组的单元数目
            $num = count($exp) - 1;

            // 重复使用字符串
            $str = str_repeat("|--",$num);

            // 拼接
            $value -> name = $str.$value -> name;
            // var_dump($value -> name);
        }

        // 渲染视图
        return view('admin/cate/edit',[
                'data' => $data,
                'all' => $all,
            ]);

    }

    /**
    *   更改分类的方法
    */
    public function postUpdate(Request $request)
    {   
        // 获取参数
        $id = $request -> only(['id']);
        // 排除获取
        $exc = $request -> except(['id','_token','false']);
        $false = $request -> input('false');

        // 子类的状态跟父类同步
        $status = $exc['status'];

        // 获取此分类的父级ID
        $opid = DB::table('ug_cate') -> where('id',$id) -> value('pid');
        $npid = $request -> input('pid');

        // 如果原有父级ID与表单传来的父级ID不一致
        if($opid != $npid) {

            // 获取要移动到的路径
            $npath = DB::table('ug_cate') -> where('id',$npid) -> value('path');

            // 拼接路径
            $npath .= $npid.',';
            // echo $npath;

            // 存入数组
            $exc['path'] = $npath;

            // 获取要模糊查找的路径
            $opath = DB::table('ug_cate') -> where('id',$id) -> value('path');

            // 拼接
            $opath .= current($id).',';

            // 模糊查找,查询是否有子类
            $num = DB::table('ug_cate') -> where('path','like',$opath.'%') -> get();

            if($num) {
                // 拼接新的路径
                $npath .=current($id).',';

                // 循环
                foreach($num as $key => $value) {
                // 获取子类路径
                $pak = $value -> path;

                // 字符串替换
                $newpak = str_replace($opath,$npath,$pak);

                // 修改子类
                $movec = DB::table('ug_cate') -> where('id',$value->id) -> update(['path'=>$newpak,'status'=>$status]);

                }
            }

            // 修改
             $info = DB::table('ug_cate') -> where('id',$id) -> update($exc);

        } else {

            // 修改分类数据
             $info = DB::table('ug_cate') -> where('id',$id) -> update($exc);

        }


            DB::table('ug_cate') -> where('id',$id) -> update(['false'=>$false[0]]);
            return redirect('/admin/Cate/index') -> with('success','修改分类成功');

    }

    /**
    *   删除分类的显示界面
    */
    public function getDelete(Request $request)
    {
        // 获取参数
        $id = $request -> input('id');

        // 获取单条数据的单个字段
        $path = DB::table('ug_cate') -> where('id',$id) -> value('path');

        // 拼接路径
        $path .= $id.',';

        // 模糊查询,删除子类和本身
        $inf = DB::table('ug_cate') -> where('path','like',$path.'%') -> delete();
        $res = DB::table('ug_cate') -> where('id',$id) -> delete();

        if($res || $inf) {
            // 执行跳转
            return redirect('/admin/Cate/index') -> with('success','操作成功,删除了此类的'.$inf.'个子类');
        } else {
            return back() -> with('error','删除失败,请重新尝试');
        }
    }

    /**
    *   下架分类的显示界面
    */
    public function getDisabled(Request $request)
    {
        // 获取参数
        $num = $request -> input('num',10);
        $search = $request -> input('search');

        // 查询所有数据,进行分组,模糊查询,分页
        $data = DB::table('ug_cate') ->
        select(DB::raw('*,concat(path,id,",") as pathes')) ->
        where('name','like','%'.$search.'%')->
        where('status',0) ->
        orderby('pathes') ->
        paginate($num);

        // 渲染模板
        return view('admin/cate/disabled',[
                'all' => $request -> all(),
                'data' => $data,
            ]);
    }

    /**
    *   无限分类递归获取数据
    */
    static private function getCateByMessage($pid)
    {
        //获取顶级信息
        $res = DB::table('ug_cate')->where('pid',$pid) -> where('status',1) ->get();
        $num = count($res);

        //定义空数组
        $arr = [];
        //遍历信息
        foreach($res as $k => $v){
            
            if($num<=7) {
                // 计算字符串的长度
                $len = strlen($v->name);
                // 检索字符串中是否有指定字符
                $exists = stripos($v->name,',');

                // 如果分类名字的长度符合条件,并且数组中含有指定字符串的话
                if($len>4 && $exists) {

                    // 把字符串分割到数字中
                    $liuxin = explode(',',$v->name);

                    // 添加属性,把处理好的数组存到属性中
                    $v -> liuxin = $liuxin;

                    $v -> maxnum = count($v->liuxin) - 1;
                }

                // 检测对象中是否存在该成员属性
                $v -> bol = property_exists($v,'liuxin');
            }

            //生成属性 存放子类信息
           $v->cate =  self::getCateByMessage($v->id);
           //存放数据进行遍历
           $arr[] = $v;
        }

        return $arr;
    }

    /**
    *   获取无限分类的方法
    */
    static public function getCates()
    {
        $row = self::getCateByMessage(0);

        return $row;
    }

    /**
    *   获取有效分类的id
    */

    static public function valid()
    {
        $num = DB::table('ug_cate') -> where('status',1) -> get();

        // 定义空数组
        $valids = array();

        // 遍历存入
        foreach($num as $key => $value) {
            $valids[] = $value -> id;
        }

        return $valids;
    }


    /**
    *   获得全部分类名称的静态方法
    */
    static public function cates()
    {
        // 获取分类表数据
        $cates = DB::table('ug_cate') ->
        select(DB::raw('*,concat(path,id,",") as pathes')) ->
        groupby('pathes') ->
        get();

         // 遍历数组
        foreach($cates as $key => $value) {

            // 把字符串分割为数组
            $exp = explode(",",$value -> path);

            // 计算数组的单元数目
            $num = count($exp) - 1;

            // 重复使用字符串
            $tag = str_repeat("|--",$num);

            $value -> name = $tag.$value -> name;
            // var_dump($value -> name);  
        }

        // 返回结果
        return $cates;
    }

}