<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
class HomeMyugController extends Controller
{
	//用户个人中心显示页面
    public function getUcenter(Request $request)
    {
    	$id=session('id');
    	$row=DB::table('ug_user')->select('account','email')->where('id',$id)->first();
        $row_d=DB::table('ug_user_details')->where('uid',$id)->first();
    	$account=$row->account;
    	$email=$row->email;
    	if(!empty($email))
    	{
    		$email=substr_replace($email,"****",3,4);
    		$res['email']=$email;
    	}
    	if(is_numeric($account))
    	{
    		$phone=substr_replace($account,"****",3,4);
    		$res['phone']=$phone;
    	} 	
    	
    	return view('/my_ug/ug_ucenter',['res'=>$res,'row_d'=>$row_d]);
    }
    //用户个人中心修改页面
    public function postDoucenter(Request $request)
    {
    	//获取用户提交的数据
    	$row=$request->except('_token');
    	$row['uid']=session('id');


    	$res=DB::table('ug_user_details')->where('uid',$row['uid'])->update($row);

        return redirect('/home/myug/ucenter');
    }

    //用户收货地址视图
    public function  getReceipt(Request $request)
    {
        $id=session('id');
        $row=DB::table('ug_address')->where('uid',$id)->get();
        return view('/my_ug/ug_receipt_address',['row'=>$row]);
    }

    //用户增加收货地址的方法
    public function postDoreceipt(Request $request)
    {
       
       $id=session('id');
       $num=DB::table('ug_address')->where('uid',$id)->get();
       $num=count($num);
       if($num>=3)
       {
        return back()->with('numerror','每位用户添加的地址不能超过三条 您可以更改或者删除已有地址');
       }
       $row=$request->except('_token');
       $row['uid']=$id;
       DB::table('ug_address')->insert($row);

       return redirect('/home/myug/receipt');
    }
    //用户删除收货地址的方法
    public function getDodelete(Request $request)
    {

        $id=$request->input('id');

        $res=DB::table('ug_address')->where('id',$id)->delete();

       return redirect('/home/myug/receipt');
    }
    //用户修改收货地址显示的方法
    public function getShowupdate(Request $request)
    {
        $id=$request->input('id');

        $row=DB::table('ug_address')->where('id',$id)->first();

        return view('/my_ug/ug_update_address',['row'=>$row]);
    }

    //处理用户修改的方法
    public function postDoupdate(Request $request)
    {
        $id=$request->input('id');
        $uid=session('id');
        $row=$request->except('id','_token');

       DB::table('ug_address')
       ->where('id',$id)
       ->where('uid',$uid)
       ->update($row);  
      return redirect('/home/myug/receipt');
    }

    //设置默认地址的方法
    public function getSetdefault(Request $request)
    {
        $id=$request->input('id');
        $uid=session('id');
        $num=count(DB::table('ug_address')->where('uid',$uid)->where('status',1)->get());
        if($num>=1)
        {
            return  back()->with('defaulterror','请先取消其他默认地址');
        }
        $row['status']=1;
        DB::table('ug_address')->where('uid',$uid)->where('id',$id)->update($row);

         return redirect('/home/myug/receipt');

    }
    //取消默认地址的方法
    public function getCanceldefault(Request $request)
    {
        $id=$request->input('id');
        $uid=session('id');
        $row['status']=0;
        DB::table('ug_address')->where('id',$id)
        ->where('uid',$uid)->update($row);

        return redirect('/home/myug/receipt');
    }
    //安全设置的显示模板的方法
    public function getSecurity(Request $request)
    {
        return view('/my_ug/ug_security_setting');
    }
    //安全设置第一步
    public function getSecurityone(Request $request)
    {
        return view('/my_ug/ug_security_setting_one');
    }
    //处理ajax请求图片验证码
    public function getAjax(Request $request)
    {
        $vcode=$request->input('vcode');
        if($vcode==session('vcode'))
           {
            return 'true';
           } else {
            return 'false';
           }
    }

    //修改登录密码的视图
    public function getDosecurity(Request $request)
    {

        return view('/my_ug/ug_security_setting_two');
    }
    //修改密码的处理方法
    public function getDosecuritytwo(Request $request)
    {
        $id=session('id');
        $oldpwd=md5($request->input('olduserpwd'));
        $pwd=DB::table('ug_user')->where('id',$id)->select('password')->first()->password;
       if($oldpwd!=$pwd)
       {
            return  back()->with('olderror','旧密码错误');
       }
       $userpwd=MD5($request->input('userpwd'));
       $row['password']=$userpwd;
       $res=DB::table('ug_user')->where('id',$id)->update($row);
       session(['id'=>null,'username'=>null]);
       return redirect('/home/User/login')->with('success','密码修改成功请用新密码登录');
     }
}
