<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Mail;
use App\sendEmail;
class HomeLoginController extends Controller
{
            
            /*
            *   用户登录视图
            */
            public function getLogin(Request $request)
            {
                // session(['url'=>null]);
                if(isset($_SERVER['HTTP_REFERER']))
                   {
                        if(session('url')==null){
                        $ori=$_SERVER['HTTP_HOST'];
                        $url=$_SERVER['HTTP_REFERER'];   
                        $url=explode($ori,$url)[1];
                           session(['url'=>$url]);
                        }
                   }elseif ($_SERVER['HTTP_REFERER']=='http://www.gama.com/home/User/login') {
                        if(session('url')==null){
                            session(['url'=>'/home/User/login']);
                        }
                   }else{ 
                              if(session('url')==null){
                                 session(['url'=>'/home/index']);   
                                } 
                   }                 
                   return view('/home/login');
            }
            /**
            * 处理用户登录提交数据
            */
            public function postDologin(Request $request)
            {

                //判断是否有验证码字段
                $code=$request->has('code');
                
                if($code){

                    if($request->input('code')!=session('vcode')){

                     return redirect('/home/User/login')->with('vcodeerror','验证码不正确');
                    }

                    session(['loginnum'=>0]);
                }
                $account=$request->input('account');
                $password=md5($request->input('password'));

                $res=DB::table('ug_user')->where('account',$account)->first();
                if($res)
                {
                    if($password!=$res->password)
                    {

                        return redirect('/home/User/login')->with('passworderror','密码错误');
                    }
                    if($res->status==0){

                    return redirect('/home/User/login')->with('noactivation','账户未激活');

                    }else{
                            $id=$res->id;
                            $nickname=$res->nickname;
                            $username=DB::table('ug_user_details')->where('uid',$id)->first()->username;

                            if(empty($username))
                            {

                                session(['id'=>$id,'username'=>$nickname]);

                            }else{

                                session(['id'=>$id,'username'=>$username]);
                            }

                           
                            // return redirect('/home/index');


                              $url=session('url');

                              session(['url'=>null]);
                              if ($url==null) {
                               
                               return  redirect('/home/index');

                              }else{

                              return redirect($url);

                              }

                            
                    }
                }else{

                            session(['loginnum'=>1]);

                    return redirect('/home/User/login')->with('noexiste','账户名不存在');
                }

            }
            /**
            *
            *   用户注册视图
            */
            public function getRegister()
            {
                return view('/home/register');
            }
            /**
            *   处理用户提交的注册信息
            */
            public function postDoregister(Request $request)
            {
                $type=$request->input('type');

                $row=$request->except('type','email','_token');
                if($row['ug_code']!=session('vcode'))
                {
                     return back()->with('vcodeerror','验证码不正确');
                }
                $row_s=[];
                $res=null;
                switch($type)
                {
                    case 'phone':
                           $res=DB::table('ug_user')->where('account','=',$row['mobile'])->get();
                           if($res)
                           {
                            return back()->with('accounterror','用户名已存在');
                           }else{

                            $row_s['account']=$row['mobile'];
                            $row_s['password']=md5($row['password1']);
                            $row_s['nickname']=rand(100000,999999);
                            $row_s['_token']=str_random(30);
                            $row_s['regtime']=time();
                                $res_id=DB::table('ug_user')->insertGetId($row_s);
                                if($res_id)
                                {
                                    $row_d['uid']=$res_id;
                                    DB::table('ug_user_details')->insert($row_d);
                                    return redirect('/home/User/login')->with('registersuccess','注册成功--请登陆');
                                }else{
                                    return back()->with('注册失败');
                                }
                           }
                              
                    break;
                    case  'email':
                            $row=$request->all();
                            $res=DB::table('ug_user')->where('account','=',$row['email'])->get();
                           if($res)
                           {
                            return back()->with('accounterror','用户名已存在');
                           }else{

                            $row_s['account']=$row['email'];
                            $row_s['password']=md5($row['password1']);
                            $row_s['nickname']=rand(100000,999999);
                            $row_s['_token']=str_random(30);
                            $row_s['regtime']=time();
                            $row_s['status']=0;
                            $row_s['email']=$row['email'];
                            $account=$row_s['account'];
                            $res_id=DB::table('ug_user')->insertGetId($row_s);
                            if($res_id)
                            {
                                $row_d['uid']=$res_id;
                                DB::table('ug_user_details')->insert($row_d);
                                $id=$res_id;
                                $_token=$row_s['_token'];
                                $account=$row_s['account'];

                                sendEmail::send($id,$_token,$account);
                                return view('/home/sendactivation',['account'=>$account,'id'=>$id,'_token'=>$_token]);
                                }else{
                                    return back()->with('fail','注册失败');
                                }
                           }
                    break;
                }                 
            }

            /**
            *  再次发送激活验证码email
            */
            public  function getAgainemail(Request $request)
            {
                $id=$request->input('id');
                $_token=$request->input('_token');
                $account=$request->input('account');

                sendEmail::send($id,$_token,$account);
                return  view('/home/sendagain',['account'=>$account]);                
            }

            /**
            *   激活账号的方法
            */
            public function getActivation(Request $request)
            {
                $id=$request->input('id');

                $_token=DB::table('ug_user')->where('id',$id)->select('_token')->first();
               
                if($_token->_token==$request->input('_token'))
                {

                    $res=DB::table('ug_user')->where('id',$id)->update(['status'=>1]);
                    if($res)
                    {
                         return  view('/home/perfect');
                    }else{
                         return  view('/home/sorry');
                    }
                    
                   
                 }else{

                    return  abort('404');
                  }
            }
            /**
            *
            *   找回密码的视图back_first
            */
            public function getBack()
            {
                return view('/home_back/back_first');
            }
            /**
            *
            *   找回密码第一步
            */
            public function postVcode(Request $request)
            {
                $vcode=$request->input('vcode');

                if($vcode!=session('vcode'))
                {

                    return back()->with('vcode_error','验证码错误');
                };

                $account=$request->input('account');

                $res=DB::table('ug_user')->where('account',$account)->get();
                
                 
                
                if(!$res)
                {
                    return back()->with('account_error','账户名不存在');
                }

                //将用户id存入信息中
                session([
                                'uId'=>$res[0]->id,

                                'uAccount'=>$res[0]->account,
                        'u_token'=>$res[0]->_token,
                                ]);
                        $account=session('uAccount');
                
               

                        if($request->input('type')=='phone')
                        { 
                            $account=substr_replace($account,'****',3,4);

                            return view('/home_back/mobile_second_one',['account'=>$account]);
                        }else{
                             $account=substr_replace($account,'****',3,4);
                            return view('/home_back/email_second',['account'=>$account]);
                        }
            } 
            /**
            *  手机找回密码第二步
            */ 
            public function getCheckout(Request $request)
            {
                
                $account=session('uAccount');
                $account=substr_replace($account,'****',3,4);
                return view('/home_back/mobile_second_two',['account'=>$account]);
            }
            
            /**
            *   手机找回第三步
            *
            */
            public function postThird(Request $request)
            {
                
                return view('/home_back/mobile_third');       
            }
            /**
            *   验证码ajax请求验证验证码
            */
            public function getAjax(Request $request)
            {
                $vcode=$request->input('vcode');

                if($vcode!=session('vcode'))
                {
                    return 'false'; 

                }else{

                    return 'true';
                }
                
            }

             /**
            *   手机找回第四步
            *   处理用户设置新密码
            */
             public function postRepassword(Request $request)
             {
                
                    $id=session('uId');
                    $row=[];
                    $row['password']=md5($request->input('newPassword'));
                   $res=DB::table('ug_user')->where('id',$id)->update($row);
                   if($res)
                   {
                        $request->session()->flush();
                      return view('/home_back/success');

                   }else{

                      $request->session()->flush();

                    return  abort('404');
                   }
             }
             /**
             *  邮箱找回的第二步请求
             */
             public function getEmailsecond()
             {
                    $account=session('uAccount');
                    $id=session('uId');
                    $_token=session('u_token');
                    session(['num'=>rand(100000,999999)]);
                    $num = session('num');
                    //发送邮件
                    sendEmail::sendback($id,$_token,$account,$num);
                   $account=substr_replace($account,'****',3,3);
                    return view('/home_back/email_third',['account'=>$account]);
             }
             /**
             * 用户email设置新密码视图
             */
             public  function getEmailback(Request $request)
             {
                    $id=$request->input('id');
                    $_token=$request->input('_token');
                    if($id==session('uId')&&$_token==session('u_token'))
                    {
                        return  view('/home_back/email_last');
                    } else {

                        return abort('404');
                    }  
             }
             /**
             * Ajax验证码方法
             */
             public function getEmailajax(Request $request)
             {
                    $num=$request->input('num');
                    if($num!=session('num'))
                    {
                        return 'false';
                    }else{

                        return  'true';
                    }

             }
             /**
             *  email 最后修改密码    
             *  
             */
             public function postLast(Request $request)
             {
                    $row['password']=MD5($request->input('newPassword'));
                    $id=session('uId');
                    $res=DB::table('ug_user')->where('id',$id)->update($row);
                    if($res)
                    {
                        $request->session()->flush();
                       return view('/home_back/success');
                    }else{
                        $request->session()->flush();
                        return abort('404');
                    }
             }

             /**
             *      用户退出操作
             *
             */
             public  function getLogout(Request $request)
             {

                session(['id'=>null,'username'=>null,'url'=>null]);

                return redirect('/home/index');
             }
}
