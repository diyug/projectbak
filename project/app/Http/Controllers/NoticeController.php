<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class NoticeController extends Controller
{
   /**
   *    公告的添加页面
   */
   public function getAdd(Request $request)
   {
   		
        return view('/admin/notice/add');
   }
}
