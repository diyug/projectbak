<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
class HomeCommentController extends Controller
{
    
    /**
    *   前台评论的显示视图
    *
    */
    public function getShow(Request $request)
    {
        $gid=1;
        $uid=session('id');
        $exist=DB::table('ug_comment')->where('gid',$gid)->where('uid',$uid)->get();

        $row=DB::table('ug_goods_basic')->where('id',$gid)->first();
        if($exist) {
          
            
             return view('/home/goods_comment_again',['row'=>$row]);
        }else{
       
             return view('/home/goods_comment',['row'=>$row]);

        }
        
    }
    /**
    *   处理用户提交的数据
    */
    public function  postDocomment(Request $request)
    {    
        $uid=session('id');
        $gid=$request->input('gid');
        $res=$request->hasFile('goodspic');
        $row=$request->except('_token','goodspic');
        if($res)
        {      
                $file=$request->file('goodspic');
                $fileAllow=array('jpep','png','gif','jpg');
                $filename = $file->getClientOriginalExtension();
                $file_res=in_array($filename,$fileAllow);
                if(!$file_res)
                {
                   return  back()->with('error','文件格式不正确');
                }
                $filename=$uid.time().'.'.$filename;
                $destinationPath = './comment';
                $request->file('goodspic')->move($destinationPath,$filename);
                $row['goodspic']='/comment/'.$filename;                       
        }
        $row['comment']=trim($row['comment']);
        $row['comment_time']=time();
        $row['uid']=$uid;
        $row['username']=session('username');
        $resolute=DB::table('ug_comment')->insertGetId($row);
        return redirect('/home/details?id='.$gid);
    }
    
    //追加评论的方法
    public function postDocommentagain(Request $request)
    {
       $uid=session('id');
       $gid=$request->input('gid');
       $comment=DB::table('ug_comment')->where('uid',$uid)->where('gid',$gid)->select('comment')->first()->comment;
       $comment=$comment.'$'.$request->input('comment');
       $row['comment']=$comment;
       $res=DB::table('ug_comment')->where('uid',$uid)->update($row);
      
      return redirect('/home/details?id='.$gid);
    }
}
