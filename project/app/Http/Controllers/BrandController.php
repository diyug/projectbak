<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// 引入数据库类
use DB;

// 引入商品分类控制器
use App\Http\Controllers\CateController;

// 引入Storage类
use Storage;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class BrandController extends Controller
{
    /**
    *   这是显示品牌列表的方法
    */
    public function getBrandlist(Request $request)
    {
        // 获取参数
    	$num = $request -> input('num',10);

        $search = $request -> input('search');

        $specialer = $request -> input('specialer');

        $specialer = isset($specialer) ? $specialer : 1;

        $branch = $request -> input('branch');

        $branch = isset($branch) ? $branch : 0;

        // 查询专区表获取专区名称
        $specialname = DB::table('ug_special') -> where('id',$specialer) -> value('specialname');

        //调用方法获取special表数据
        $specials = $this -> specials();

    	// 查询ug_brand表所有数据
    	$data = DB::table('ug_brand') ->
    	orderby('picline')->
        where('special',$specialer) ->
        where('branch',$branch) ->
        where('picline','like','%'.$search.'%') ->
    	paginate($num);

        // 渲染模板
        return view('admin/brand/brandlist',[
        		'data' => $data,
                'all'  => $request -> all(),
                'specials' => $specials,
                'specialname' => $specialname,
                'branch' => $branch,
                // 'branchs' => $branchs,
        	]);
    }

    /**
    *	添加品牌的界面
    */
    public function getAddbrand()
    {

        // 查询ug_special表所有数据
        $data = DB::table('ug_special') -> orderby('id') -> get();

    	// 渲染模板
    	return view('admin/brand/addbrand',[
                'data' => $data,
            ]);
    }

    /**
    *	处理添加品牌的方法
    */
    public function postInsertbrand(Request $request)
    {	
    	// 检测是否上传文件
        $exists = $request -> hasFile('img_path');

        // 如果没有上传文件
        if(!$exists) {
            return back() -> with('error','图片木有上传呀');
        }

        // 获取参数
        $data = $request -> except('_token');

        $special = $request -> input('special');

        $picline = $request -> input('picline');

        $branch = $request -> input('branch');

        if(!$branch) return back() -> with('error','分支还没有选好!');
        $branch = $branch[0];

        $data['branch'] = $branch;

        // 处理参数
        $picline = htmlspecialchars(trim($picline));

        if(strpos($picline,'-')) return back() -> with('error','排位号不允许为负');

        $name = $request -> input('brandname');

        // 查询数据库,此专题名称是否存在
        $nameexists = DB::table('ug_brand') -> where('brandname',$name) -> where('special',$special) -> where('branch',$branch) -> first();

        // 如果存在
        if($nameexists) {
            return back() -> with('error','此品牌名称已存在!');
        }

        // 定义数组
        $dir = ['hot-big','hot-small','sport','woman','outdoors','seoul','man','clothes','baby','sale','guide'];

        $diffdir = ['middle','big'];

        // 指定路径
        $path = '/home/special/'.$dir[$special-1].'/';

        // 判断传送分区
        if($branch != 0) {
            $path = '/home/special/'.$dir[$special-1].'/'.$diffdir[$branch-1].'/';
        }

        // 随机图片名
        $picname = md5(rand(10000,99999));

        // 获取上传文件后缀名
        $suffix = $request ->file('img_path') -> getClientOriginalExtension();

        // 拼接文件名
        $image = $picname.'.'.$suffix;

        // 移动文件
        $info = $request -> file('img_path') -> move('.'.$path,$image);

        if(!$info) return back() -> with('error','上传失败');

        // 拼接路径
        $img_path = '/home/special/'.$dir[$special-1].'/'.$image;

        if($branch != 0) {
            // 更改路径
            $img_path = '/home/special/'.$dir[$special-1].'/'.$diffdir[$branch-1].'/'.$image;
        }

        // 存入数组
        $data['img_path'] = $img_path;

        // 查询数据库是否有该排位
        $found = DB::table('ug_brand') -> where('picline',$picline) -> where('branch',$branch) -> where('special',$special) -> get();

        // 如果该排位存在
        if($found) {

            // DB::table('ug_brand') -> where('picline',$picline) -> where('branch',$branch) -> update(['picline'=>'0']);

            $id = DB::select('select id from ug_brand where picline='.$picline.' and branch='.$branch.' and special='.$special);

            DB::update('update ug_brand set picline=0 where id='.$id[0]->id);

        }

        // 执行插入操作
        $result = DB::table('ug_brand') -> insert($data);

        if($result) {

            return redirect('/admin/Brand/brandlist?specialer='.$special.'&branch='.$branch) -> with('success','添加成功!');
            // return redirect('/admin/Brand/addbrand') -> with('success','添加成功!');

        } else {

            return back() -> with('error','添加失败!');

        }

    }

    /**
    *	删除品牌的方法
    */
    public function getDeletebrand(Request $request)
    {
        // 获取参数
    	$id = $request -> input('id');

        // 获取分区和专题
        $data = DB::select('select special,branch from ug_brand where id='.$id);
        $special = $data[0] -> special;
        $branch = $data[0] -> branch;

        // 查询品牌图片详细信息
        $imgPath = DB::table('ug_brand') -> where('id',$id) -> value('img_path');
    	
        // 删除数据
        $info = DB::table('ug_brand') -> where('id',$id) -> delete();

         if($info) {

            // unlink($imgPath);
            $systemPath = 'D:/wamp/www/project/project/public';
            @unlink($systemPath.$imgPath);

            return redirect('/admin/Brand/brandlist?specialer='.$special.'&branch='.$branch) -> with('success','删除成功!');

        } else {

            return back() -> with('error','删除失败!');

        }

    }

    /**
    *	编辑品牌信息的显示界面
    */
    public function getEditbrand(Request $request)
    {	
    	// 获取参数
    	$id = $request -> input('id');

        // 查询ug_special表所有数据
        $special = DB::table('ug_special') -> orderby('id') -> get();

    	// 查询数据
    	$data = DB::table('ug_brand') -> where('id',$id) -> first();

    	// 渲染模板
    	return view('admin/brand/editbrand',[
    			'data' => $data,
                'special' => $special,
    	]);
    }

    /**
    *	处理更改品牌数据的方法
    */
    public function postUpdatebrand(Request $request)
    {
        // 获取参数
        $oldpicline = $request -> input('oldpicline');

        $picline = $request -> input('picline');

        $id = $request -> input('id');

        $data = $request -> only('special');

        $special = $request -> input('special');

        $oldspecial = $request -> input('oldspecial');

        $branch = $request -> input('branch');

        // 如果更换了专题的话
        if($oldspecial != $special) {

            // 包含文件夹名称的数组
            $dir = ['hot-big','hot-small','sport','woman','outdoors','seoul','man','clothes','baby','sale','guide'];

            $diffdir = ['middle','big'];

            // 从数据库中拿出laravel框架的绝对路径+图片名称
            $oldPath = DB::table('ug_brand') -> where('id',$id) -> value('img_path');

            // 使用php函数获取数据库查出的文件名称的后缀
            // $suffix = trim(strchr($oldPath,'.'),'.');

            // 随机文件名
            $newFileName = trim(strrchr($oldPath,'/'),'/');

            // 指定文件夹
            $savePath = '/home/special/'.$dir[$special-1].'/'.$newFileName;

            if($branch!=0) {
                // 更换存放文件夹
                $savePath = '/home/special/'.$dir[$special-1].'/'.$diffdir[$branch-1].'/'.$newFileName;
                
            }

            // var_dump($savePath);
            // echo 'D:/wamp/www/project/project/public'.$oldName;
            // die;

            // 获取指定字符从开始到结尾字符串

            // 移动文件
            // Storage::move('D:/wamp/www/project/project/public'.$oldName,$savePath);

            // 项目所存放路径
            $systemPath = 'D:/wamp/www/project/project/public';

            // 移动文件
            $res = @rename($systemPath.$oldPath,$systemPath.$savePath);

            // 如果移动失败就返回
            if(!$res) return back() -> with('error','文件移动失败!');

            $data['img_path'] = trim($savePath,'.');
        }

        // 如果原有的排位和传来的排位不一致的话
        if($picline != $oldpicline) {

            if($oldpicline == 0) {
                // 查询是否有排位占位
                $exis = DB::table('ug_brand') -> where('special',$special) -> where('picline',$picline) -> get();

                if($exis) {

                    // 获取ID
                    $id = DB::table('ug_brand') -> where('special',$special) -> where('picline',$oldpicline) -> where('branch',$branch) ->value('id');

                    // 把将要修改的的排位的享有者改为0
                    DB::update('update ug_brand set picline=0 where id='.$id);
                }
            }
                // 查询该排位是否有元素占位
                $exists = DB::table('ug_brand') -> where('special',$special) -> where('picline',$picline) -> where('branch',$branch) -> get();

                // 如果新的排位存在并且不是排位号0的话,就让两者替换
                if($exists && $picline != '0') {

                    // 交换次序 不能有多个排位次序相同的
                    $id = DB::table('ug_brand') -> where('special',$special) -> where('picline',$picline) -> where('branch',$branch) -> value('id');

                    // 修改本身
                    DB::update('update ug_brand set picline='.$oldpicline.' where id='.$id);
                }
        }

        $data['picline'] = $picline;
        $data['branch'] = $branch;

        // 更改本身的数据
        $info = DB::table('ug_brand') -> where('id',$id) -> update($data);

        // 判断操作返回结果
        if($info) {

            // 跳转界面
           return redirect('/admin/Brand/brandlist?specialer='.$special.'&branch='.$branch) -> with('success','修改了'.$info.'条数据');

        } else{
            // 返回错误信息
            return back() -> with('error','修改失败!');
        }
    }

    /**
    *   专题界面的显示方法
    */
    public function getSpeciallist()
    {
        // 查询数据
        $data = DB::table('ug_special') -> get();
         // 渲染视图
        return view('admin/Brand/speciallist',[
            'data' => $data,
            ]);
    }

    /**
    *   添加专题的界面显示方法
    */
    public function getAddspecial()
    {
        // 渲染视图
        return view('admin/Brand/addspecial');
    }

    /**
    *   处理添加专题的方法
    */
    public function postInsertspecial(Request $request)
    {   
        // 获取参数
        $data = $request -> only('specialname');

        $name = $data['specialname'];

        // 查询数据库,此专题名称是否存在
        $exists = DB::table('ug_special') -> where('specialname',$name) -> first();

        // 如果存在
        if($exists) {
            return back() -> with('error','此专题名称已存在!');
        }

        // 执行插入操作
        $info = DB::table('ug_special') -> insert($data);

         // 判断操作返回结果
        if($info) {

            // 跳转界面
           return redirect('/admin/Brand/speciallist') -> with('success','插入了'.$info.'条数据');

        } else{
            // 返回错误信息
            return back() -> with('error','删除失败!');
        }
    }

    /**
    *   修改专题信息的显示界面
    */
    public function getEditspecial(Request $request)
    {
        // 获取参数
        $id = $request -> input('id');

        // 查询数据
        $data = DB::table('ug_special') -> where('id',$id) -> first();

        // 渲染模板
        return view('admin/brand/editspecial',[
                'data' => $data,
        ]);
        
    }

    /**
    *   处理修改专题信息的方法
    */
    public function postUpdatespecial(Request $request)
    {
        // 获取参数
        $id = $request -> input('id');
        $data = $request -> only('specialname');

        // 修改数据
        $info = DB::table('ug_special') -> where('id',$id) -> update($data);

        if($info) {

            return redirect('/admin/Brand/speciallist') -> with('success','修改了'.$info.'条数据');

        } else {

            return back() -> with('error','修改数据失败!');

        }
    }

    /**
    *   处理删除专题的方法
    */
    public function getDeletespecial(Request $request)
    {
        // 获取参数
        $data = $request -> input('id');
        
        // 删除数据
        $info = DB::table('ug_special') -> where('id',$data) -> delete();

         if($info) {

            return redirect('/admin/Brand/speciallist') -> with('success','删除成功!');

        } else {

            return back() -> with('error','删除失败!');

        }
    }

    /**
    *   查询brand表获取品牌的方法
    */
    static public function brands($special)
    {   
        // 查询数据,以排位顺序
        $brands = DB::select('select * from ug_brand where special='.$special.' and branch=0 and picline not like "0%" order by picline');

        // 返回数据
        return $brands;
    }

    /**
    *   主页馆类大中小图获取方法
    */
    static public function branchs($special,$branch)
    {   
        // 查询数据,以排位顺序
        $branchs = DB::select("select * from ug_brand where special={$special} and branch={$branch} and picline not like '0%' order by picline");

        // 返回数据
        return $branchs;
    }

    /**
    *   针对主页两个li的分开查询方法
    */
    static public function half($special,$off)
    {
        if($off=='front') {
            $off = ' limit 0,20';
        } else if($off=='back') {
            $off = ' limit 20,40';
        }

        // 查询数据,以排位顺序
        $brands = DB::select('select * from ug_brand where special = '.$special.' and picline not like "0%" order by picline'.$off);

        // 返回数据
        return $brands;
    }

    /**
    *   查询special表获取所有数据的方法
    */
    public function specials()
    {   
        // 查询数据,以排位顺序
        $specials = DB::table('ug_special') -> orderby('id') -> get();

        // 返回数据
        return $specials;
    }

}
