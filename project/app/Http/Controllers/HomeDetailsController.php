<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
class HomeDetailsController extends Controller
{
   public function getIndex(Request $request)

    {
     
        //获取id
        $pid = $request->input('id');
        $id = $request->input('id');

        
       // 获取商品列表页传过来的id 来遍历数据
      
        $res = DB::table('ug_goods_basic')->where('id',$id)->first();


       // 通过id来获取商品分类的pid
        $row = DB::table('ug_goods_basic')->where('id',$pid)->first();
        $pid = $row->pid;
        // dd($pid);
        // 通过商品表的pid来获取分类表里的商品id
        $cate = DB::table('ug_cate')->where('id',$pid)->first();
        // dd($cate);
        //定义一个数组

       // 获取商品图片信息
        $pic = DB::table('ug_goods_images')->where('goods_id',$id)->get();
      
       //获取商品尺寸和颜色
       //定义空数组
        $data = [];
        //获取颜色
        $data['color'] = explode(',',$res->color);
        //获取尺寸
        $data['size'] = explode(',',$res->size);


        //把获取到的id存入到session中  通过id找相关的商品信息
       
       if(empty(Session::get('his')) || !in_array($id,Session::get('his')))
       {

        Session::push('his',$id);
       }   

        //通过id获取数据  获取的id
        $his = array_reverse(Session::get('his'));     
        if(empty($his)) {

            dd('获取id失败');
        }
         
        //声明数组
        $pro = [];
        foreach ($his as $k => $v) {
            
            $pro[]= DB::table('ug_goods_basic')->where('id',$v)->first();

        }

      // 模板显示
        return view('/home/details',
            [
            'res'=>$res,
            'row'=>$row,
            'pic'=>$pic,
            'data'=>$data,
            'pro'=>$pro,
            'cate'=>$cate,
            ]);
    }


}
