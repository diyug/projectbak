<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CommentController extends Controller
{
    /**
    *
    * 商品评论显示页面
    */
    public function getShow(Request $request)
    {

    	  //获取用户搜索的值
      $gid=$request->input('gid');

      //判断用户是否有进行搜索操作 返回相应的数据
      if($gid){
        $row=DB::table('ug_comment')
        ->where('status','=','1')
        ->where('gid','=',$gid)
        ->paginate($request->input('num',10));
        
      }else{
        $row=DB::table('ug_comment')
        ->where('status','=','1')
        ->paginate($request->input('num',10));
        
      }
      
       return view('/admin/comment/show',['row'=>$row,'request'=>$request]);

    }

    /**
    *禁止评论的显示页面
    *
    */
       public function getShowdisabled(Request $request)
    {

        //获取用户搜索的值
      $gid=$request->input('gid');

      //判断用户是否有进行搜索操作 返回相应的数据
      if($gid){
        $row=DB::table('ug_comment')
        ->where('status','=','0')
        ->where('gid','=',$gid)
        ->paginate($request->input('num',10));
        
      }else{
        $row=DB::table('ug_comment')
        ->where('status','=','0')
        ->paginate($request->input('num',10));
        
      }
      
       return view('/admin/comment/showdisabled',['row'=>$row,'request'=>$request]);

    }


    /**
    * 禁止商品评论展示
    *
    */
    public function getDisabled(Request $request)
    {
      $id=$request->input('id');
      $row['status']=0;
      $res=DB::table('ug_comment')->where('id',$id)->update($row);
        if ($res) {

            return  redirect('/admin/comment/showdisabled')->with('success','操作成功');
        } else {
            return redirect('/admin/comment/showdisabled')->with('error','操作失败');
        }

    }
    /**
    *
    * 商品评论启用
    *
    */
    public function getEnable(Request $request)
    {
        $id=$request->input('id');
        $row['status']=1;
        $res=DB::table('ug_comment')->where('id',$id)->update($row);
          if ($res) {

              return  redirect('/admin/comment/show')->with('success','操作成功');
          } else {
              return redirect('/admin/comment/show')->with('error','操作失败');
          }

    }
    /**
    *
    * 删除商品评论
    *
    */
    public function getDelete(Request $request)
    {
      $id=$request->input('id');
      $res=DB::table('ug_comment')->where('id',$id)->delete();
      if($res)
      {
        return back()->with('success','删除成功');
      }else{
        return back()->with('error','删除失败');
      }
    }
}
