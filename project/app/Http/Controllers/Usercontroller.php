<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class Usercontroller extends Controller
{
    /**
    *   后台用户的添加显示视图方法
    */
    public function getAdd(Request $request)
    {
        return  view('/admin/User/add');
    }

    /**
    *   后台用户添加的方法
    */
    public function postInsert(Request $request)
    {
        $account=$request->input('account');

        //判断用户输入是手机号码 或者 邮箱
        if(is_numeric($account))
        {
                 //对用户提交数据进行验证
            $this->validate($request, [
            'account' => 'required|unique:ug_user',

            'account' => array('regex:/^1[3|4|5|7|8]\d{9}$/'), //手机号码的正则

            'password' => 'required|between:5,16', //约束密码长度为6-12位

            'repassword' => 'required|same:password',  //两次密码必须相同
            ],[

            'account.required'=>'账户名不能为空',

            'account.unique'=>'账户名已存在',

            'account.regex'=>'手机号码不符合规范',

            'password.required'=>'密码不能为空',

            'password.between'=>'密码长度不符合要求',

            'repassword.required'=>'重复密码不能为空',

            'repassword.same'=>'两次密码输入不一致'
            ]);

             $row=$request->except('repassword');

        } else {
                 //对用户提交数据进行验证
            $this->validate($request, [
            'account' => 'required|unique:ug_user|email',

            'password' => 'required|between:5,16', //约束密码长度为6-12位

            'repassword' => 'required|same:password',  //两次密码必须相同
            ],[

            'account.required'=>'账户名不能为空',

            'account.unique'=>'账户名已存在',

            'account.email'=>'无效邮箱号码',

            'password.required'=>'密码不能为空',

            'password.between'=>'密码长度不符合要求',

            'repassword.required'=>'重复密码不能为空',

            'repassword.same'=>'两次密码输入不一致'
            ]);
            $row=$request->except('repassword');
            $row['email']=$request->input('account');   
        }
       

        $row['regtime']=time();
        //生成用户的随机昵称
        $row['nickname']=rand(100000,999999);

        //对密码进行加密操作
        $row['password']=md5($request->input('password'));

        $res_id=DB::table('ug_user')->insertGetId($row);

        if($res_id){
            $row_d['uid']=$res_id;
            DB::table('ug_user_details')->where('uid',$res_id)->insert($row_d);
            //插入成功 跳转到用户列表
            return redirect('/admin/User/index')->with('success','添加成功');
        }else{
            //插入失败 跳转到用户添加的页面
            return redirect('/admin/User/add')->with('error','添加失败');
        }
    }

    /**
    *   后台用户列表页面方法
    */
    public function  getIndex(Request $request)
    {   
        //获取用户搜索的值
      $account=$request->input('account');

      //判断用户是否有进行搜索操作 返回相应的数据
      if($account){
        $row=DB::table('ug_user')
        ->where('status','>','0')
        ->where('account','like','%'.$account.'%')
        ->paginate($request->input('num',10));
        
      }else{
        $row=DB::table('ug_user')
        ->where('status','>','0')
        ->paginate($request->input('num',10));
        
      }
       
       return view('/admin/User/index',['row'=>$row,'request'=>$request]);      
    }

    /**
    * 后台用户删除的方法
    */
    public function getDelete(Request $request)
    {
        //获取用户Id
        $id=$request->input('id');
        $url=$request->input('url');
        //从数据库删除用户
        $res=DB::table('ug_user')->where('id','=',$id)->delete();
        

        //判断操作结果 跳转页面分配不同的闪存信息
        
        if($res){

            return redirect($url)->with('success','删除成功!');
            
        }else{

            return redirect($url)->with('error','删除失败!');

        }
    }
    /**
    *   后天用户编辑页面方法
    */
    public function getEdit(Request $request)
    {
        //获取要编辑用户的id
        $id=$request->input('id');

        //查询要编辑用户的信息
        $row=DB::table('ug_user')->where('id','=',$id)->get();

        return  view('/admin/User/edit',['row'=>$row]);

    }
    /**
    *
    *   后台用户信息修改的方法
    */
    public function  postUpdate(Request $request)
    {
                  //对用户提交数据进行验证
                $this->validate($request, [
                'status'=>'between:0,2',

                'nickname'=>'digits_between:5,20',

                'email'=>'email', //约束邮箱符合约束

                'password' => 'between:5,12', //约束密码长度为6-12位
                'repassword' => 'same:password',  //两次密码必须相同
                ],[
                'nickname.digits_between'=>'用户昵称长度在5,20位之间',

                'status.not_in'=>'用户状态异常',

                'email.email'=>'邮箱格式不符合要求',
                
                'password.between'=>'密码长度在5,12位之间',

                'repassword.same'=>'两次密码输入不一致'

                ]);
                $row=$request->except('_token','id');

                $id=$request->input('id');     
                //判断用户是否对密码进行修改

                $password=$request->input('password');

                if(!empty($password)){
                    //去除数据中的某些字段
                    $row=$request->except('_token','id','repassword');

                    //对密码进行MD5加密
                    $row['password']=md5($request->input('password'));
                }

                $res=DB::table('ug_user')->where('id','=',$id)->update($row);

                if($res){

                    //返回来源网址附带参数
                    return redirect('/admin/User/edit?id='.$id)->with('success','修改成功');
                }else{

                    return redirect('/admin/User/edit?id='.$id)->with('error','修改失败');
                }
    }
    /**
    *   后台禁用用户显示模板方法
    */
    public function getDisabled(Request $request)
    {
          //获取用户搜索的值
          $account=$request->input('account');

          //判断用户是否有进行搜索操作 返回相应的数据
          if($account){
            $row=DB::table('ug_user')
            ->where('status','=','0')
            ->where('account','like','%'.$account.'%')
            ->paginate($request->input('num',10));
            
          }else{
            $row=DB::table('ug_user')
            ->where('status','0','0')
            ->paginate($request->input('num',10));
            
          }
           
           return view('/admin/User/disabled',['row'=>$row,'request'=>$request]);
    }


 }
