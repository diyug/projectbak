<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// 引入轮播图元素类
use App\Http\Controllers\CarouselController;

// 引入分类
use App\Http\Controllers\CateController;

use App\Http\Controllers\BrandController;

// 引入DB类
use DB;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
    *	显示优购网站主页的方法
    */
    public function index()
    {
    	// 调用CateController类静态方法获取数据库内所有上架类信息
    	$cates = CateController::getCates(0);
        // dd($cates);

        array_pop($cates);

        $footercates = CateController::getCates(0);

        // 调用CaronselController类静态方法获取轮播图片信息
        $picname = (new CarouselController) -> getPicname();

        $pichref = (new CarouselController) -> getPichref();

        $allpic = [];
        $i = 0;
        foreach($pichref as $key=> $value) {
            $allpic[$i++]['cid'] = $value;
        }
        $c = 0;
        foreach($picname as $key=> $value) {
            $allpic[$c++]['pic'] = $value;
        }

        // 定义空数组
        $brands = [];

        // 查品牌表获取不同的专区的品牌信息,
        $brands['hot-big'] = BrandController::brands(1);
        $brands['hot-small-front'] = BrandController::half(2,'front');
        $brands['hot-small-back'] = BrandController::half(2,'back');
        $brands['sport'] = BrandController::brands(3);
        $brands['woman'] = BrandController::brands(4);
        $brands['outdoors'] = BrandController::brands(5);
        $brands['seoul'] = BrandController::brands(6);
        $brands['man'] = BrandController::brands(7);
        $brands['clothes'] = BrandController::brands(8);
        $brands['baby'] = BrandController::brands(9);
        $brands['guide'] = BrandController::brands(11);

        $branchs = [];

        // 热门图片和推荐图片
        $branchs['sport-middle'] = BrandController::branchs(3,1);
        $branchs['sport-big'] = BrandController::branchs(3,2);
        $branchs['woman-middle'] = BrandController::branchs(4,1);
        $branchs['woman-big'] = BrandController::branchs(4,2);
        $branchs['outdoors-middle'] = BrandController::branchs(5,1);
        $branchs['outdoors-big'] = BrandController::branchs(5,2);
        $branchs['seoul-middle'] = BrandController::branchs(6,1);
        $branchs['seoul-big'] = BrandController::branchs(6,2);
        $branchs['man-middle'] = BrandController::branchs(7,1);
        $branchs['man-big'] = BrandController::branchs(7,2);
        $branchs['clothes-middle'] = BrandController::branchs(8,1);
        $branchs['clothes-big'] = BrandController::branchs(8,2);
        $branchs['baby-middle'] = BrandController::branchs(9,1);
        $branchs['baby-big'] = BrandController::branchs(9,2);

        // 定义空数组
        $all = [];

        // 添加进一个新的数组
        $all[0] = $brands;
        $all[1] = $branchs;

        // 存入6个不同iccon class属性的数组
        $icon = ['yund','huw','nvx','nanz','tongx','xiangb','star'];
        
    	// 渲染模板
    	return view('home/index',[
    			'cates'       => $cates,
                'footercates' => $footercates,
                'allpic'      => $allpic,
                // 'keyword' => $success,
                // 这是分类列表的css样式
                'icon'        => $icon,
                // 所有品牌列表
                'all'         => $all,
    		]);
    }

}
