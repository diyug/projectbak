<?php

namespace App\Http\Middleware;

use DB;
use Closure;

class LoginMiddleWare
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {        
        //判断请求用户是否登录 且 用户状态为管理员
        if(session('id') &&session ('status')==2){
              
            return $next($request);

        }else{
            
            return redirect('/admin');
        }
    }
}
