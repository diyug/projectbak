<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/**
*	访问后台的登录页面
*/
Route::get('/admin','LoginController@index');
/**
*	处理后台登录信息
*/
Route::post('/admin/doaction','LoginController@doaction');

/**
*
*	处理后台退出登录的信息
*/

Route::get('/admin/logout','LoginController@logout');

/**
*	后台生成验证码
*/
Route::get('/admin/vcode','LoginController@vcode');
/*
*
*	管理后台的路由组
*/

Route::group(['middleware' => 'login'],function(){

	/*
	*	这里是访问后台用户的隐式控制器
	*/	
	
	Route::controller('/admin/User','Usercontroller');

	/**
	*    这里是管理后台分类的隐式控制器
	*/
	Route::controller('/admin/Cate','CateController');
	
	/**
	*    这里是管理后台商品的隐式控制器
	*/
	Route::controller('/admin/Goods','GoodsController');

	/**
	*    这里是管理前台轮播图的隐式控制器
	*/
	Route::controller('/admin/Carousel','CarouselController');

	/**
	*	这里是管理前台分类、品牌列表的隐式控制器
	*/
	Route::controller('/admin/Brand','BrandController');

	/**
	*	后台订单管理隐式控制器
	*/
	Route::controller('/admin/Order','OrderController');
	
	/**
	*    这里是管理后台商品图片的隐式控制器
	*/
	Route::controller('/admin/goodsimages','GoodsImagesController');
	/**
	*
	*	这里是后台商品评论的管理
	*/
	Route::controller('/admin/comment','CommentController');

	/**
	*	这里是管理后台公告的隐身控制器
	*/
	Route::controller('/admin/notice','NoticeController');
});

/**
*
*	这里是管理用户登录  注册 退出 找回密码操作的隐式路由
*/
Route::controller('/home/User','HomeLoginController');

/**
*	显示前台主页的控制器
*/
Route::get('/home/index','HomeController@index');

/**
*	显示前台列表页的控制器
*/
Route::controller('/home/list','HomeListController');

/**
*	显示前台购物车列表的控制器
*/
Route::controller('/home/cart','HomeCartController');

/**
*	显示前台订单页面的控制器
*/

/**
*	前台我的优购控制器
*/
Route::controller('/home/myug','HomeMyugController');

/**
*	前台订单的控制器
*/

Route::controller('/home/order','HomeOrderController');

/**
* 这是显示前台详情页的控制器
*/ 

Route::controller('/home/details','HomeDetailsController');

/**
*   前台商品评论的控制器	
* 
*/
Route::controller('/home/comment','HomeCommentController');









/**
* 这是商品收藏的控制器
*/ 
Route::controller('/my_ug/collect','HomeCollectController');



