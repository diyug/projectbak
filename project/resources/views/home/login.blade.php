﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache,no-store, must-revalidate">
<meta http-equiv="expires" content="0">
<title>优购网_用户登录</title>
<meta name="description" content="二期项目_优购网"/>
<meta name="keywords"  content="二期项目_优购网"/>
<link rel="shortcut icon" href="/home/images/favicon.ico">
	<link href="/home/css/base-2.css" type="text/css" rel="stylesheet" />	
	<link href="/home/css/validator.css" type="text/css" rel="stylesheet" />
	<link href="/home/css/new_log_reg.v2.0.css" type="text/css" rel="stylesheet" />
	<script type="text/javascript" src="/home/js/jquery-1.4.2.min.js"></script>
	<script type="text/javascript" src="/home/js/yg.common.js"></script>
	<script type="text/javascript" src="/home/bootstrap/js/jquery-1.8.3.js"></script>
</head>
<!-- 添加的css样式	 -->
<style type="text/css">
	#ug_logo{
		width:300px;
		height:80px;
	}
	#ug_logo div
	{
		width: 150px;
		height:70px;
		float:left;
		text-align:center;
		line-height:60px;
	}
	#ug_logo div img{
		margin-top:20px;
	}
</style>
<body>
<div id="top_nav">
	@include('/home_public/header_login')
</div>
<!--view_area end -->
	<div id="ug_logo" class="cen clearfix rel">
			<div>
				<a href="/home/index">
				<img src="/home/picture/signin-register-logo.png" alt=""/>
				</a>
			</div>
			
			<div>
				<font size="9">|</font> <font size="5"> 用户登录</font>			 
			</div>
	</div>
@if(session('registersuccess'))
          <script type="text/javascript">
                alert('注册成功请登录');
          </script>
@endif
@if(session('success'))
		<script type="text/javascript">
				alert('密码修改成功 请用新密码登录');
		</script>
@endif
<div class="cen-new rel">
	
    <a id="ug_lunbo" href="/home/index" target="_blank" class="cen-new" style="background: url('/home/images/1.jpg') no-repeat top center;position: absolute;top: 0;left: 0;z-index: 0"></a>
 
    <div class="nreg_box mt15 clearfix" style="left: 50%;margin-left: 115px;position: absolute;top: 0;width: 378px;z-index: 1;">
        <div class="nreg_left fr">
            <ul class="tab_sig_reg clearfix">
                <li class="tab_sig tab_cur"><a href="/home/User/login">登录</a></li>
                <li class="tab_reg"><a href="/home/User/register">注册</a></li>
            </ul>
            <div class="nreg_form">
                <form  id="loginform" method="post" action="/home/User/dologin">
                    <dl class="nreg_item clearfix">
                        <dd>
                            <div class="nreg_input_bg">
                                <i class="reg_user_name"></i>
                                <input type="text" name="account" placeholder="手机号/会员名称/邮箱" id="email_" class="nreg_input" maxlength="50" autocomplele='off'>
                            </div>
                        </dd>
                        <dt>
                        <div id="login_email_tip" style="margin-bottom:0px;" class="errortips">
                        	@if(session('noactivation'))        	
								账户未激活
							@endif
							@if(session('noexiste'))        	
								账户不存在					
							@endif
                        </div>
                        </dt>
                    </dl>
                    <dl class="nreg_item clearfix">
                        <dd>
                            <div class="nreg_input_bg">
                                <i class="reg_password"></i>
                                <input type="password" name="password" placeholder="请输入密码" id="password_" class="nreg_input"  maxlength="50" />
                            </div>
                        </dd>
                        <dt>
                        <div id="login_password_tip" class="errortips">
                        	@if(session('passworderror'))        	
								密码错误
							@endif
                        </div>
                        </dt>
                    </dl>

                 {{csrf_field()}}
				<div id="code_container" style="display:none;">
		            <dl class="nreg_item clearfix">
		                <dd>
		                    <div class="nreg_input_bg fl" style="width:202px">
				                        <label class="lab1 ver_code" for="code2_">验证码</label>
				                        <input type="text" id="code2_" class="nreg_sinput"
				                        name="code" maxlength="5" style="width: 130px"/>
				                    </div>
				                    <div class="fl" style=" padding:0">
				                        <img src="{{URL('/admin/vcode')}}" alt="" onclick="this.src=this.src+'?s'" style="display:inline">
				                    </div>
		                </dd>
		                <dt><div id="code2_tip">
		        			@if(session('vcodeerror'))        	
								验证码错误
								<script type="text/javascript">
								$('#code2_tip').attr('class','errortips');
								</script>
							@endif
		                </div></dt>
		            </dl>
		        </div>
                    <p style="display: block;text-align: right;margin-right: 17px;">
                        <a href="/home/User/back" style="line-height: 26px; text-decoration:underline">忘记密码？</a>
                    </p>
                    <p style="text-align: center">
                        <input type="submit" class="nlog_submit" value="登录" title="登录"/>
                    </p>
                    <p class="f_black" style="color: #999;margin-top: 10px;text-align: center;">使用合作网站账号登录优购：</p>
                    <p class="cop_link">
                        <a class="we_chat f_blue" href="/member/toThirdPartLogin.jhtml?type=quick-weixin&amp;redirectURL=http://www.yougou.com/">&nbsp;</a> |
                        <a href="/api/alipay/sendToFastLogin.sc?redirectURL=http://www.yougou.com/" class="nreg_zpay f_blue">&nbsp;</a> |
                        <a href="/api/qq/toLogin.sc?redirectURL=http://www.yougou.com/" class="nreg_qq f_blue">&nbsp;</a> |
                        <a href="/api/sina/toLogin.sc?redirectURL=http://www.yougou.com/" class="nreg_sina f_blue">&nbsp;</a> |
                        <a href="/api/renren/toRenren.sc?redirectURL=http://www.yougou.com/" class="nreg_people f_blue">&nbsp;</a>
                    </p>
          
                </form>
            </div>
        </div>
    </div>
</div>
<div class="blank20" ></div>



<div class="footer Gray">
	<p class="tright">Copyright &copy; 2011-2014 Yougou Technology Co., Ltd.  <a href="http://www.miibeian.gov.cn" target="_blank">粤ICP备09070608号-4</a> 增值电信业务经营许可证：<a href="http://www.miibeian.gov.cn" target="_blank" style="padding-left:0" >粤 B2-20090203</a></p>
</div>
<script type="text/javascript">
//背景图片轮播事件
var j=1;
setInterval(function(){
	i=j%6+1;
	// $('#ug_lunbo').css('background','url("/home/images/'+(i+1)+'.jpg")')
	$('#ug_lunbo').attr('style','background: url("/home/images/'+i+'.jpg") no-repeat top center;position: absolute;top: 0;left: 0;z-index: 0">')

	j++;
},8000);



//错误框隐藏事件
	$('#ug_login_error').click(function() {

		$(this).slideUp(1000);
	});





	$('#ug_logo').click(function(){
		 $(this).animate({ 
			   marginRight:"65%",
			  }, 3000);
	})
	$('#ug_logo').trigger('click');


	var	eisok=false;

	var	pisok=false;

	//账户失去焦点的时候
	$('#email_').blur(function() 
	{
		//获取用户输得的账户值
	    account = $('#email_').val();

	   //匹配手机号的正则
	    reg_phone=/^1[3|4|5|7|8]\d{9}$/;
	    res_phone=reg_phone.test(account);
	    

	    //匹配邮箱的正则
	    reg_email=/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/; 
	    res_email=reg_email.test(account);

	    	
	    	//当账户名不符合标准的时候 
	    if (res_phone==false && res_email==false) 
	    {
	    	$('#login_email_tip').html('账户名不符合格式');
	    }else{
	    	
	    	eisok=true;
	    	$('#login_email_tip').html('');
			

	    	
	    }
	});

		$('#password_').blur(function(){

			var password=$(this).val();
			num=password.length;

			if(num>=6 && num<=12)
			{
				pisok=true;
				$('#login_password_tip').html('');
				
			}else{

			$('#login_password_tip').html('密码长度不符合要求');
				
			}
		});

		@if(session('loginnum')>=1)

			$('#code_container').css({display:"block",});
		
		@endif

		$('#loginform').submit(function(){
			$('input').trigger('blur');
			if (pisok==true && eisok==true){

				return true;
			}else{
				return false;
			}	
			return false;	
		});
</script>	
</body>
</html>
