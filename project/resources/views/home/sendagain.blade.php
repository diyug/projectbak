<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>优购网_用户注册</title>
<meta name="description" content="优购网">
<meta name="keywords" content="优购网">
<script src="%E4%BC%98%E8%B4%AD%E7%BD%91_%E7%94%A8%E6%88%B7%E6%B3%A8%E5%86%8C_files/ga.js" async="" type="text/javascript"></script><script>
	if(window.top !== window.self){ window.top.location = window.location;}
</script>
<script src="/home/getback/ga.js" async="" type="text/javascript"></script>
<script type="text/javascript" src="/home/getback/jquery-1.js"></script>
<link href="/home/getback/base-2.css" type="text/css" rel="stylesheet">
<link href="/home/getback/validator.css" type="text/css" rel="stylesheet">
<link href="/home/getback/new_log_reg.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="/home/getback/findpwd.js"></script>
</head>
<body>
<!-- top nav bar created time: 2014-11-28 18:34:02-->
@include('/home_back/public_header')
<!--top_nav end -->

<div class="uc_hd">
	<div class="cen clearfix rel">
		<h2>新用户注册</h2>
		<p class="link fl">
			<a href="/home/index" class="cblue">返回时尚商城</a>
			<!--
			|<a href="http://www.yougou.com/topics/1394617951051.html" class="cblue">OUTLETS 购划算</a>
			-->
		</p>
	</div>
</div>

<!--更换雅虎邮箱提示 start-->
<div class="uc_email_tip" id="uc_email_tip" style="display:none;">
	<i class="warn"></i><strong>由于雅虎邮箱即将停止服务</strong>，为了保障您以后能够通过邮箱找回密码、接收订单提醒等，建议尽快把账号完成绑定其他邮箱。<a class="Blue" href="javascript:void(0);" id="email_bind_modify">[立即绑定]</a><i class="close"></i>
</div>
<!--更换雅虎邮箱提示 end--><div class="cen sendEmailDiv">
  <div class="sendEmailDivCon">
    <h1 class="Size14">已经向您的邮箱
      <span class="Red">{{$account}}</span>发送了一封激活信！</h1>
    <p>  请收到后按照提示操作，需要在24小时内完成激活。</p>

    <p class="blank15">&nbsp;
    </p>
    <p>
      <span class="fl"> <a href="javascript:void(0)" onclick="toBindEmailBtn('{{$account}}')" class="regAutoBtn_2">
      <span>去邮箱激活账户</span>
      </a> </span>
      <span class="fl" style="padding-left:10px;"><a href="/home/User/register" class="f_blue" >没有收到???请重新注册</a></span>

    </p>
  </div>
  <dl class="sendEmailDivFooter clearfix">
    <dt>还没收到激活邮件？</dt>
    <dd>确认邮件是否被你提供的邮箱系统自动拦截，或被误认为垃圾邮件，请检查垃圾箱。</dd>
    <dd>如果再次发送激活邮件还是没有收到，请试试：<br>
      <a href="/home/login" target="_blank" class="f_blue">更换邮箱注册</a></dd>

    <dd class="last">如果还是一直无法收到激活邮箱，您可以：<br>
      <a href="/home/login" target="_blank" class="f_blue">改用手机注册</a></dd>
  </dl>
</div>
<div class="blank20">
  &nbsp;
</div>
<!--底部start-->
<div class="footer Gray">
	<p class="tright">Copyright © 2011-2014 Yougou Technology Co., Ltd. <a href="http://www.miibeian.gov.cn/" target="_blank">粤ICP备09070608号-4</a> 增值电信业务经营许可证：<a href="http://www.miibeian.gov.cn/" target="_blank" style="padding-left:0">粤 B2-20090203</a></p>

</div>
<!--底部end--> 
<script>
function toBindEmailBtn(email){	
	var dispacheUrl = getEmailLoginUrl(email);
	window.open(dispacheUrl);
}

function getEmailLoginUrl(email){
	var	suffUrl=[
		{id:"sina.com.cn",url:"http://mail.sina.com.cn/"},
		{id:"sina.com",url:"http://mail.sina.com.cn/"},
		{id:"sina.cn",url:"http://mail.sina.com.cn/"},
		{id:"vip.sina.com",url:"http://mail.sina.com.cn/"},
		{id:"2008.sina.com",url:"http://mail.sina.com.cn/"},
		{id:"163.com",url:"http://mail.163.com/"},
		{id:"126.com",url:"http://mail.126.com/"},
		{id:"popo.163.com",url:"http://popo.163.com/"},
		{id:"yeah.net",url:"http://email.163.com/"},
		{id:"vip.163.com",url:"http://vip.163.com/"},
		{id:"vip.126.com",url:"http://vip.126.com/"},
		{id:"188.com",url:"http://188.com/"},
		{id:"vip.188.com",url:"http://vip.188.com/"},
		{id:"tom.com",url:"http://mail.tom.com/"},
		{id:"yahoo.com",url:"http://mail.cn.yahoo.com/"},
		{id:"yahoo.com.cn",url:"http://mail.cn.yahoo.com/"},
		{id:"yahoo.cn",url:"http://mail.cn.yahoo.com/"},
		{id:"sohu.com",url:"http://mail.sohu.com/"},		
		{id:"hotmail.com",url:"https://login.live.com/"},
		{id:"139.com",url:"http://mail.10086.cn/"},
		{id:"gmail.com",url:"https://accounts.google.com"},
		{id:"msn.com",url:"https://login.live.com"},
		{id:"51.com",url:"http://passport.51.com/"},
		{id:"yougou.com",url:"http://mail.yougou.com/"},
		{id:"qq.com",url:"https://mail.qq.com"},
		{id:"foxmail.com",url:"http://mail.qq.com"},
		{id:"vip.qq.com",url:"http://mail.qq.com"}
	 ];
	var index = email.indexOf("@");
	var subStr = email.substring(index+1).replace(/\./g,"-");
	var suffIndext = subStr.indexOf(".");
	var exist = false;
	var loginUrl = "";	
	$.each(suffUrl,function(n,value) {
	  var emailId = value.id.replace(/\./g,"-");
	  if(subStr == emailId){
		loginUrl = value.url;
		exist = true;
	  }
	});
	 if(!exist){
		 loginUrl = "http://www."+ email.substring(index+1);
	 }
	
	 return loginUrl;
}
</script>
<script type="text/javascript">
//google
 var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23566531-1']);
  _gaq.push(['_setDomainName', '.yougou.com']);
  _gaq.push(['_addOrganic', 'baidu', 'word']);
  _gaq.push(['_addOrganic', 'soso', 'w']);
  _gaq.push(['_addOrganic', '3721', 'name']);
  _gaq.push(['_addOrganic', 'yodao', 'q']);
  _gaq.push(['_addOrganic', 'vnet', 'kw']);
  _gaq.push(['_addOrganic', 'sogou', 'query']);
  _gaq.push(['_addOrganic', '360', 'q']);
  _gaq.push(['_addOrganic', 'so', 'q']);
  _gaq.push(['_trackPageview']);
  _gaq.push(['_trackPageLoadTime']);


(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://analytic' : 'http://analytic') + '.yougou.com/ga.js?4.3.3';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script> 
<!--<script src="http://s1.ygimg.cn/template/common/js/mv.js?4.3.3" type="text/javascript"></script>-->

<!-- Google Code for &#27880;&#20876; Conversion Page --> 
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1016027598;
var google_conversion_language = "ar";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "EFaYCJrxqgIQzrO95AM";
var google_conversion_value = 0;
/* ]]> */
</script> 
<script type="text/javascript" src="%E4%BC%98%E8%B4%AD%E7%BD%91_%E7%94%A8%E6%88%B7%E6%B3%A8%E5%86%8C_files/conversion.js"></script><img alt="" src="%E4%BC%98%E8%B4%AD%E7%BD%91_%E7%94%A8%E6%88%B7%E6%B3%A8%E5%86%8C_files/a.gif" style="display:none" border="0" height="1" width="1">
<noscript>
	<div style="display:inline;">
	  <img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1016027598/?label=EFaYCJrxqgIQzrO95AM&amp;guid=ON&amp;script=0"/>
	</div>
</noscript>



</body></html>