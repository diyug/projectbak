<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache,no-store, must-revalidate">
<meta http-equiv="expires" content="0">
<title>商品评价</title>
<script type="text/javascript" src="/home/bootstrap/js/jquery-1.8.3.min.js"></script>
<link href="/home/my_ug/base-2.css" type="text/css" rel="stylesheet">
<link href="/home/my_ug/ucenter_v2.css" type="text/css" rel="stylesheet">
<link rel="shortcut icon" href="/home/images/favicon.ico">
<link href="/home/css/base.css" type="text/css" rel="stylesheet">
<link href="/home/css/channel.vs.css" type="text/css" rel="stylesheet">
<link href="/home/css/index.css" type="text/css" rel="stylesheet">
<script src="/home/my_ug/ga.js" async="" type="text/javascript"></script>
<script type="text/javascript" src="/home/my_ug/jquery-1.js"></script>
<script type="text/javascript" src="/home/my_ug/yg.js"></script>
<script type="text/javascript" src="/home/my_ug/ygdialog.js"></script>
<link href="/home/my_ug/ygdialog.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/home/my_ug/yg_002.js"></script>
<!-- 省市县三级联动 -->
 <script type="text/javascript" src="/home/my_ug/jquery.js"></script>
<script type="text/javascript" src="/home/my_ug/date_birthday.js"></script>
<script type="text/javascript" src="/home/my_ug/memberBasicinfo_editInfo.js"></script>
<script type="text/javascript" src="/home/my_ug/area.js"></script> 
<style type="text/css">
  .out{height:650px;background:#F8F8F8;}
  .main{width:90%;height:600px;margin:auto;background:#FFFFFF;}
  .line-left{float:left;width:44.3%;height:2px;background:#E3E3E5;margin-top:30px;}
  .line-right{float:right;width:44.5%;height:2px;background:#E3E3E5;margin-top:30px;}
  .goods_show{
              height:470px;
              border:1px solid #E3E3E5;
              
            }
  .blank{
             background:#F8F8F8;
             height:80px;}
  .order_num{
              height:50px;
              border-top:1px solid #E3E3E5;
              border:1px solid #E3E3E5;
              line-height:50px;
        }
  .goods{
              width:300px;
              height:470px;
              border-right:1px solid #E3E3E5;
               position:relative;
               float:left;
        } 
  .goods_pic{
               width:150px;
               margin:50px 75px;
               position:absolute;
               border-left:1px dotted #E3E3E5;
               border-right:1px dotted #E3E3E5;
            }
  .goods_des{
              position:absolute;
              top:200px;
              line-height:20px;
              text-align:center;
              padding:10px;
  }
  .goods_price{
              height: 20px;
              position:absolute;
              top:280px;
              left:42%; 
  }
  .goods_color{
              height:20px;
              position:absolute;
              top:310px;
              left:41%;
  }
  .goods_comment{
              height:470px;
              width:73.5%;
              float:left;
              position:relative;
  }
  .top{
              height:40px;
              width:100%;
              position:absolute;
              top:40px;
  }
  .top>div{
              float:left;
  }
  .top_left{
              margin-left:20px;
              height:40px;
              line-height:40px;
  }
  .top_right{
              margin-left:22px;
              height:40px;
              line-height:40px;
  }
  .score{
              margin-left:20px;
              height:40px;
              line-height:40px;
  }
  .expression{
              margin-top:20px;
              margin-left:128px;
              height:20px;
              width:150px;
              line-height:20px;
}
  .expression>img{
              float:left;
              margin-right:4px;
  }
  #impression{
              position:absolute;
              top:80px;
              width:100%;
            }
  #impression>.checkout{
              float:left;
              width:120px;
              margin-right:20px;
              height:40px;
              border:1px solid #E3E3E5;
              line-height:40px;
              text-align:center;
              position:relative;
  }
  #impression>.check{
              float:left;
              width:120px;
              margin-right:20px;
              height:40px;
              border:1px solid red;
              line-height:40px;
              text-align:center;
              position:relative;
  }
  #impression>.nocheckout{
              margin-left:20px;
              height:40px;
              line-height:40px;
  }
  #impression>.commentblank{
            float:left;
             width:120px;
  }
  .close{

      position:absolute;
      top:-10px;
      right:5px;
      color:red;
      display:none;
  }
  #comment{
      margin-top:170px;
      height:200px;
  }
  #comment .ug_comment{
       margin-left:20px;
      height:40px;
      line-height:40px;

  }
  .comment_pic{
      position:absolute;
      top:180px;
      height:40px;
  }
  .comment_pic>div{
    width:90px;
    height:20px;
    float:left;
    margin-left:20px;
    line-height:20px;
  }
</style>
</head>
<body class="myinfo">
@include('/home_public/header')
<!--//公共头部end-->
<div class="blank10"></div>
<div id="out">
    <div class="main">
    <div class="blank">
       <div class="line-left"></div><font size="6"><b>评价订单</b></font><div class="line-right"></div>                
    </div>
    <form action="/home/comment/docomment" method="post" enctype="multipart/form-data">
      <div class="order_num">
          <span style="margin-left:20px;">订单号:&nbsp;&nbsp;<b>20160920123</b></span>
          <span style="margin-left:20px;">时间:<b>2016-09-20&nbsp;&nbsp;02:00:05</b></span>
      </div>
      <div class="goods_show">
            <div class="goods">
                  <div class="goods_pic">
                     <a href="/home/details?id={{$row->id}}"> <img src="{{$row->goodspic}}" alt="{{$row->goodsname}}" width="100%" /></a>
                  </div>
                  <div class="goods_des">
                   <a href="/home/details?id="{{$row->id}}">{{$row->goodsdes}}</a> 
                  </div>
                  <div class="goods_price">
                    <b>&yen;{{$row->newprice}}</b>
                  </div>
                  <div class="goods_color">
                    {{$row->color}}
                  </div>
            </div>
            <div class="goods_comment">
                   <div class="expression">
                              <img src="/home/comment/1.png" style="display:none;" alt="" />
                              <img src="/home/comment/2.png" style="display:none;" alt="" />
                              <img src="/home/comment/3.png" style="display:none;" alt="" />
                              <img src="/home/comment/5.png" style="display:none;" alt="" />
                              <img src="/home/comment/4.png" style="display:none;" alt="" />
                   </div>
                   
                   <div class="top">
                        <div class="top_left">
                            <font size="3">商品满意度:</font>    
                        </div>
                        <div class="top_right">
                              <img src="/home/comment/7.png" alt="" />
                              <img src="/home/comment/7.png" alt="" />
                              <img src="/home/comment/7.png" alt="" />
                              <img src="/home/comment/7.png" alt="" />
                              <img src="/home/comment/7.png" alt="" />
                        </div>
                        <div class="score">
                            <span style="color:red;">0</span>&nbsp;&nbsp;分
                        </div>
                        <span style="color:red;line-height:45px;margin-left:30px;display:none;">
                        <img src="/home/comment/8.png" alt="" style="margin-left:20px;" />&nbsp;&nbsp;请您为商品评分</span>
                   </div>
                   <div id="impression">
                      <div class="nocheckout"><font size="3">买家印象　:</font><span><font color="red">&nbsp;&nbsp;至少选择一个印象</font></span></div>
                      <div class="commentblank">&nbsp;</div>
                      <div  class="checkout">
                         商品质量好  
                      </div>
                       <div  class="checkout">
                         店家服务好  
                      </div>
                       <div  class="checkout">
                         发货速度快  
                      </div>
                       <div  class="checkout">
                         配送员服务态度好 
                      </div>

                   </div>
                   <div class="comment_pic"> 
                          <div><font size="3">上传图片　:</font></div> 
                          <div><input type="file" name="goodspic"/></div>   
                   </div>
                   <div id="comment">
                        <div class="ug_comment">
                              <font size="3">评价晒单　:</font>
                              <span style="color:red;line-height:45px;display:none;">
                              <img src="/home/comment/8.png" alt=""  />&nbsp;&nbsp;请您为商品评价
                              </span>
                        </div>
                        <div>
                            <textarea name="comment" id="" cols="60" rows="5" style="margin-left:140px;resize:none;font-size:16px;"placeholder="商品是否给力??快分享你的心得吧!"></textarea>  
                        </div>
                   </div>
                   {{csrf_field()}}
                   <input type="hidden" value="" name="score" />
                   <input type="hidden" value="" name="impression" />
                   <input type="hidden" value="" name="comment" />
                   <input type="hidden" value="{{$row->id}}" name="gid" />
                  <input type="submit" value="发表评价" style="width:100px;height:30px;background:#FCAF4D;border:none;margin-left:30%; border-radius:3px; "/>
             </div>
      </div>

     </form>
    </div>
</div>
 
 <script type="text/javascript">
  
   //鼠标滑过的效果
  $('.top_right').find('img').mouseover(function() {
      this.src='/home/comment/6.png';
      $(this).prevAll('img').each(function() {
        this.src='/home/comment/6.png';
      });
       var i=5-$(this).nextAll('img').length;
       
      $('.score').find('span').eq(0).text(i);
      $('.expression').find('img').eq(i-1).css('display','block');
      $('.expression').find('img').eq(i-1).siblings().css('display','none');
      $('.expression').find('img').eq(i-1).css('marginLeft',(i-1)*20)
  });

//当属标离开top_right
  $('.top_right').mouseout(function() {
    
     $('.expression').find('img').css('display','none');

  });

   //鼠标离开的效果
  $('.top_right').find('img').mouseout(function() {
      this.src='/home/comment/7.png';
      $(this).prevAll('img').each(function() {
        this.src='/home/comment/7.png';
      });

       
      $('.score').find('span').eq(0).text(0);
  });
// 鼠标单击的效果
  $('.top_right').find('img').click(function() {
      this.src='/home/comment/6.png'; 
      $(this).prevAll('img').each(function() {
        this.src='/home/comment/6.png';
      });
        $(this).nextAll('img').each(function() {
        this.src='/home/comment/7.png';
      });    
      $('.top_right').find('img').each(function() {
         
         $(this).unbind('mouseover');
         $(this).unbind('mouseout');
       }); 
       $('.top_right').unbind('mouseout');

      var i=4-$(this).nextAll('img').length;
      $('.score').find('span').eq(0).text(i+1);
     $('.expression img:gt('+i+')').each(function() {
       
        $(this).css('display','none');
     });
     $('.expression img:eq('+i+')').each(function() {
       
        $(this).css('display','block');
        $(this).css('marginLeft',i*20)
     });
      $('.expression img:lt('+i+')').each(function() {
       
        $(this).css('display','none');
     });
  });

  //为买家印象
  $('.checkout').click(function(){
      color=$(this).attr('class');
      if(color=='checkout'){

        $(this).attr('class','check');
      }else{
        $(this).attr('class','checkout');
      }
           
  })


 $('input[type=submit]').click(function() { 

         var i=$('.score').find('span').html();
         $('input[name=score]').val(i);
         if(i==0)
         {
          $('.score').next().css('display','block');
          return false;
         }else{
          $('.score').next().css('display','none');
         }
            var num=$('.check'); 
        if(num.length==0)
        {
              return false;
        }else{
              res='';
              num.each(function(){

                res+=$(this).html();
              })
            $('input[name=impression]').val(res); 
        }
        var comment=$('textarea[name=comment]').val();
        if(comment.length==0)
        {
            $('.ug_comment').find('span').css('display','block');

             return false;
        }else{
            $('input[name=comment]').val(comment);
             $('.ug_comment').find('span').css('display','none');
        }

        return true;


  
 });


 </script>
<p class="blank10"></p>


@include('/my_ug/ug_footer')
