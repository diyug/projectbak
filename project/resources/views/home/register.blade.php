﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache,no-store, must-revalidate">
    <meta http-equiv="expires" content="0">
    <title>优购时尚商城_用户注册</title>
    <meta name="description" content="优购时尚商城" />
    <meta name="keywords" content="优购时尚商城" />
  <link href="/home/css/base-2.css" type="text/css" rel="stylesheet" /> 
  <link href="/home/css/validator.css" type="text/css" rel="stylesheet" />
  <link href="/home/css/new_log_reg.v2.0.css" type="text/css" rel="stylesheet" />
  <script type="text/javascript" src="/home/js/jquery-1.4.2.min.js"></script>
  <script type="text/javascript" src="/home/js/yg.common.js"></script>
  <script type="text/javascript" src="/home/js/yg.login.js"></script>
  <script type="text/javascript" src="/home/bootstrap/js/jquery-1.8.3.js"></script>
<style type="text/css">
#ug_logo{ width:300px; height:80px; margin-right:0px; } #ug_logo div { width: 150px; height:70px; float:left; text-align:center; line-height:60px; } #ug_logo div img{ margin-top:20px; } #ug_login_error{ margin-bottom:0px; height:40px; background:#EDB9D8;} #ug_login_error ul { height:40px; line-height:40px; font-size:20px; text-align:center; }
</style>    
    <body>
<div id="top_nav">
  @include('/home_public/header_login')
</div><!--view_area end -->   
      <div id="ug_logo" class="cen clearfix rel">
        <div>
          <a href="/home/index">
            <img src="/home/picture/signin-register-logo.png" alt="" /></a>
        </div>
        <div>
          <font size="9">|</font>
          <font size="5">用户注册</font></div>
      </div>

@if(session('fail'))
                          <script type="text/javascript">
                          alert('注册失败');
                          </script>
@endif

      <form action="/home/User/doregister" method="post">
        <div class="cen-new rel">
         <a id="ug_lunbo" href="/home/index" target="_blank" class="cen-new" style="background: url('/home/images/1.jpg') no-repeat top center;position: absolute;top: 0;left: 0;z-index: 0"></a>
          <div class="nreg_box clearfix" style="left: 50%;margin-left: 115px;position: absolute;top: 0;width: 378px;z-index: 1;">
            <div id="emailDiv" class="nreg_left fr" style="height: 530px;margin-top: 12px">
              <ul class="tab_sig_reg clearfix">
                <li class="tab_sig">
                  <a href="/home/User/login">登录</a></li>
                <li class="tab_reg tab_cur">
                  <a href="/home/User/register">注册</a></li>
              </ul>
              <div class="nreg_form">

                <!-- 第一个dl -->
                <dl class="nreg_item mobile_regitem clearfix rel" id="mobile_nreg_item">
                  <dd>
                    <div class="nreg_input_bg rel">
                      <label class="lab1 nreg_mob_ema" id="ug_register_lab1">
                        <span class="fl">手机号</span>
                        <i class="tab_nreg"></i>
                      </label>
                      <div class="tab_user_name">
                        <a id="mobileLink" class="reg_Tab reg_TabCurr"  regtype="mobile">
                          <span>手机号</span>
                          <i class="tab_nreg"></i>
                        </a>
                        <a id="emailLink" class="reg_Tab " href="javascript:;" regtype="email">
                          <span>邮箱</span></a>
                    </div>
                    <input type="text" name="mobile" id="reg_mobile_" class="nreg_input" valid="Mobile" placeholder="请输入您的手机号码"  />
                  </div>
                  </dd>
                  <!-- 提示信息 -->
                  <dt>
                    <div id="reg_mobile_tip" class="errortips">
                       @if(session('accounterror'))
                          账户名已存在
                        @endif

                    </div>
                  </dt>
                </dl>
                <!-- 第二个dl -->
                <dl class="nreg_item email_regitem clearfix rel" id="email_nreg_item" style="display:none;">
                  <dd>
                    <div class="nreg_input_bg">
                      <label id="ug_register_lab2" class="lab1 nreg_mob_ema" >
                        <span class="fl">邮箱</span>
                        <i class="tab_nreg"></i>
                      </label>
                      <div class="tab_user_name">
                        <a id="mobileLink1" class="reg_Tab " href="javascript:;" regtype="mobile">
                          <span>手机号</span>
                          <i class="tab_nreg"></i>
                        </a>
                        <a id="emailLink1" class="reg_Tab reg_TabCurr" href="javascript:;" regtype="email">
                          <span>邮箱</span></a>
                      </div>
                      <input type="text" name="email" id="reg_email_" class="nreg_input" valid="Email" placeholder="不建议填写gmail、hotmail、qq邮箱"/></div>

                  </dd>
                  <dt>
                    <div id="reg_email_tip" class="errortips"></div>
                  </dt>
                </dl>

                <!-- 第三个dl -->
                <dl id="dlCode" class="nreg_item email_regitem mobile_regitem clearfix rel">
                  <dd>
                    <div class="nreg_input_bg fl" style="width:200px">
                      <label class="lab1" for="ug_code">验证码</label>
                      <input type="text" name="ug_code" id="code2_" class="nreg_sinput" maxlength="5" valid="IdentifyCode" style="width: 128px" /></div>
                    <div class="fr" style="margin-right: 17px;">
                      <!-- 这是验证码 -->
                      <img src="{{URL('/admin/vcode')}}" alt="" onclick="this.src=this.src+'?s'" style="display:inline">
                    </div>
                  </dd>
                  <dt>
                    <div id="code2_tip" class="errortips">
                        @if(session('vcodeerror'))
                          验证码错误
                        @endif
                    </div>
                  </dt>
                </dl>
                <!-- 第四个dl -->
                <dl id="dlMobileCode" class="nreg_item mobile_regitem clearfix rel">
                  <dd>
                    <div class="nreg_input_bg fl" style="width:235px;">
                      <label class="lab1" for="reg_mobile_code_">短信验证码</label>
                      <input type="text" name="mobilecode" id="reg_mobile_code_" class="nreg_sinput" valid="MsgIdentifyCode" style="width: 155px;" /></div>
                    <div class="fr">
                      <a id="sendMsgBtn" href="javascript:;" class="regAutoBtn_1">
                        <span style="margin-left:0;" id="getMsgSpan">获取验证码</span></a>
                    </div>
                  </dd>
                  <dt>
                    <div id="sendMsgTips" data-msg="sendmsgtips"></div>
                  </dt>
                </dl>
                <!-- 第五个dl -->
                <dl class="nreg_item clearfix rel">
                  <div style="position:absolute;top:38px;right: 17px;">
                    <div id="pwdStrength" class="pwdStrength none">
                      <em>低</em>
                      <em>中</em>
                      <em>高</em></div>
                  </div>
                  <dd>
                    <div class="nreg_input_bg">
                      <label class="lab1" for="reg_password_">密码</label>
                      <input type="password" name="password1" id="reg_password_" class="nreg_input" maxlength="25" valid="Password" placeholder="字母、数字、符号均可，6-25个字符以内" />
                  </dd>
                  <dt>
                    <div id="reg_password_tip1" class="errortips"></div>
                  </dt>
                </dl>
              {{ csrf_field() }}
                <!-- 第六个dl -->
                <dl class="nreg_item clearfix rel">
                  <dd>
                      <!-- 确定密码 -->
                    <div class="nreg_input_bg">
                      <label class="lab1" for="reg_password2">确认密码</label>
                      <input type="password" id="reg_password2" class="nreg_input" maxlength="25" valid="RePassword" /></div>
                  </dd>
                  <dt>
                    <div id="reg_password_tip2" class="errortips"></div>
                   <input type="hidden" name="type" value="auto" /> 
                   <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                  </dt>
                </dl>
                <p style="text-align: center">
                  <span>
                    <input type="checkbox" id="rules"  style="margin-top:0" id="rules" checked="checked" /></span>
                  <label style="padding-left:3px" for="rules">我已阅读并同意
                    <a href="http://www.yougou.com/help/registrationagreement.html" class="Red undline" target="_blank">《优购会员注册协议》</a></label>
                  <span id="ruleTips"></span>
                </p>
                <p class="blank5"></p>
                <p style="text-align: center">
                  <a href="javascript:void();" class="nreg_submit">
                    <input type="submit" title="立即注册" value="确认并注册" id="reg_buton"></a>
                </p>
              </div>
            </div>
        </div>
      </form>
      <!--底部start-->
      <div class="footer Gray">
        <p class="tright">Copyright &copy; 2011-2014 Yougou Technology Co., Ltd.
          <a href="http://www.miibeian.gov.cn" target="_blank">粤ICP备09070608号-4</a>增值电信业务经营许可证：
          <a href="http://www.miibeian.gov.cn" target="_blank" style="padding-left:0">粤 B2-20090203</a></p>
      </div>

      <script type="text/javascript">
      //背景图片轮播事件
var j=1;
setInterval(function(){
  i=j%6+1;
  // $('#ug_lunbo').css('background','url("/home/images/'+(i+1)+'.jpg")')
  $('#ug_lunbo').attr('style','background: url("/home/images/'+i+'.jpg") no-repeat top center;position: absolute;top: 0;left: 0;z-index: 0">')

  j++;
},8000);
     
      //错误框隐藏事件
  $('#ug_login_error').click(function() {

    $(this).slideUp(1000);
  });

      // logo 的动画效果
      $('#ug_logo').click(function() {
          $(this).animate({
            marginRight: "65%",
          },
          3000);
        }); 
      $('#ug_logo').trigger('click');

      
      //显示邮箱手机下拉列表的事件
      $('#ug_register_lab1').click(function()
        {         
             $('.tab_user_name').css('display','block');    
        });
      $('#emailLink').click(function(){ 

             $('#mobile_nreg_item').css('display','none');
             $('#dlMobileCode').css('display','none');
             $('#email_nreg_item').css('display','block');
             $('.tab_user_name').css('display','none');                   
      });
      $('#mobileLink').click(function(){

           $('.tab_user_name').css('display','none');

      })
    $('#ug_register_lab2').click(function(){

         $('.tab_user_name').css('display','block');        
     })
     $('#mobileLink1').click(function(event) {
          $('#mobile_nreg_item').css('display','block');
         $('#dlMobileCode').css('display','block');
         $('#email_nreg_item').css('display','none');
         $('.tab_user_name').css('display','none');        
     });
     $('#emailLink1').click(function(){

         $('.tab_user_name').css('display','none'); 
     }) 

     /**
     *  这里开始判断用户手机注册
     */


     //手机号码失去焦点
      mo_state = false;

     $('#reg_mobile_').blur(function()
      {
        $('#reg_mobile_tip').attr('class','errortips');
        $('#reg_mobile_tip').text('');
        if($(this).val().length>0){
            var phone_moblie=$(this).val();

           var reg_mobile=/^1[3|4|5|7|8]\d{9}$/;

            res=reg_mobile.test(phone_moblie);
            if(!res)
            {
              $('#reg_mobile_tip').attr('class','errortips');
              $('#reg_mobile_tip').text('手机号码不合法');
              

            } else {
                $('#reg_mobile_tip').attr('class','righttips');
               $('#reg_mobile_tip').text('');

               mo_state=true;
            }

        }else{

            $('#reg_mobile_tip').text('请输入手机号码');
        }     
     });
     var code_state=false;
     /**
     *  这里判断用户邮箱注册
     */
     email_state=false;
     $('#reg_email_').blur(function(){

            $('#reg_email_tip').text('');
            $('#reg_email_tip').attr('class','errortips');
            var  email_val=$(this).val();
            reg_email=/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/;
            res_email=reg_email.test(email_val);

            if(!res_email)
            {
              $('#reg_email_tip').text('格式不正确');
            }else{
              email_state=true;
              $('#reg_email_tip').attr('class','righttips');
            }
     })
     //验证码失去焦点
     $('#code2_').blur(function() 
     {
              var codeval=$(this).val().length;
              if(codeval==0)
              {
               $('#code2_tip').text('验证码不能为空'); 
              } else{

                $('#code2_tip').text('');
                code_state=true;
              }
     });

     //手机短信验证码
     $('#getMsgSpan').click(function()
     {      
        var i=60;
        into=setInterval(function()
        {
          
            i--;
          $('#sendMsgTips').attr('class','righttips sendmsg_tips');
          $('#sendMsgTips').text('验证码以发送,请注意查收');
          $('#getMsgSpan').text(i);

          if(i==0)
          {
            clearInterval(into);
            $('#getMsgSpan').text('重新获取');
          }
       
          
         },1000);

     })
    //密码框失去焦点事件
   var pwd_state=false;
    $('#reg_password_').blur(function()
    {
          $('#reg_password_tip1').text('');
          $('#reg_password_tip1').attr('class','errortips');
         var len=$(this).val().length;
          //判断用户是否输入密码
          if(len==0){
             pwd_state=false;
            $('#reg_password_tip1').text('请输入密码');
            $('#pwdStrength').css('display','none');
          }else if(len<6 || len>16 )
          {
             pwd_state=false;
            $('#reg_password_tip1').text('密码长度应在6-16之间');
            $('#pwdStrength').css('display','none');
          }else{
               pwd_state=true;
            $('#reg_password_tip1').attr('class','righttips');
            $('#reg_password_tip1').text('');
             
          }
    })

    //当密码框的失去焦点事件
     var reg_password=document.getElementById('reg_password_');
     reg_password.oninput=function()
     { 
          $('#pwdStrength').find('em').each(function() {
                          
             $(this).attr('class','');
           });
            var  reg_lower=/[a-z]{2}/;
            var   res_lower = reg_lower.test($(this).val());
            var  reg_upper =/[A-Z]{2}/;
            var  res_upper =reg_upper.test($(this).val());
            var  reg_num= /[0-9]{2}/;
            var res_num = reg_num.test($(this).val());
             if(res_lower)
             {
              res_lower=1;
             } else {
              res_lower=0;
             }
              if(res_upper)
             {
              res_upper=1;
             } else {
              res_upper=0;
             }
              if(res_num)
             {
             res_num=1;
             } else {
             res_num=0;
             }
             res=res_upper+res_num+res_lower;
             switch(res)
             {
                case 0:
                        $('#pwdStrength').css('display','none');
                break;
                case 1:
                        $('#pwdStrength').css('display','block');
                         $('#pwdStrength').find('em').eq(0).attr('class','pwdLow');
                break;
                case 2:
                        $('#pwdStrength').css('display','block');
                         $('#pwdStrength').find('em').eq(1).attr('class','pwdMid');
                break;
                case 3:
                        $('#pwdStrength').css('display','block');
                         $('#pwdStrength').find('em').eq(2).attr('class','pwdHeight');
                break;
             }    
     }

      var  repwd_state=false;
     //当重复密码框失去焦点事件
     $('#reg_password2').blur(function(){
        //获取用户的输入的密码
        var val=$('#reg_password_').val();
        //获取用户输入的重复密码
        var reval = $(this).val();
       if(val!=reval)
       {
          $('#reg_password_tip2').text('两次密码输入不一致');

       }else{

          $('#reg_password_tip2').text('');

            repwd_state=true;
       }
     })

       //当单机提交 触发所有的事件
        $('input[type=submit]').click(function(){

            $('input').trigger("blur");
            $('#mobileLink').trigger('click');
        })

        
     $('form').submit(function(){

         //获取用户选择注册的方式
         mo_status=$('#mobile_nreg_item').css('display');
         
         if(mo_status=='block')
         {
          $('input[type=hidden]').eq(1).val('phone');
         }else{
            $('input[type=hidden]').eq(1).val('email');
         }
      
      if(repwd_state&&pwd_state&&mo_state&& code_state)
        {
          return true;
        }else if(email_state&&repwd_state&&pwd_state&&code_state){

          return true;
        }
      return false;
     })
     
</script>
    </body>
</html>