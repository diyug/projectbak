<!DOCTYPE html>
<html><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache,no-store, must-revalidate">
<meta http-equiv="expires" content="0">
<title>订单提交中心_优购网上鞋城</title>

	<link href="/home/css/base-2.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="/home/css/shopcar_v4.css">
    <link rel="shortcut icon" href="/home/images/favicon.ico">

</head>
<body>

<div class="ygDialog" id="addressPopDialog" style="width: 860px; height: 302px; left: 206.5px; top: 60px; margin-left: 0px; margin-top: 0px; visibility: visible; display:none">
  <div class="dialogSkin3">
    <div class="uiDiv">
      <table class="uiTable">
        <tbody>
          <tr>
            <td>
              <div class="clearfix dg_title" style="width: 850px;">
                <h3 class="uiTitle">使用新地址</h3>
                <a class="uiClose">关闭</a></div>
            </td>
          </tr>
          <tr>
            <td>
              <div class="dg_content" style="width: 850px; height: 275px; overflow: hidden;">
                <table style="display:block;width:100%;" id="newAddress" class="table_address">
					  <tbody class="evil">
					    <tr>
					      <td class="tdleft">收货人姓名
					        <i>*</i>
					      </td>
					      <td>
					        <input type="text" value="" class="minput" name="receivingName" id="receivingName" maxlength="16">
					        <span class="mltips" id="receivingName_tips">
					          <font class="cgray">（请使用真实姓名，不能全部是数字/英文/包含特殊符号（括号、井号等））</font></span>
					      </td>
					    </tr>
					    <tr>
					      <td class="tdleft">收货人地址
					        <i>*</i>
					      </td>
					      <td style="width: 700px;">
					        <div class="dropdown_span">
					          <div style="display: none;" id="province_select" class="fl">
					            <ul class="fl drop_window">

					            </ul>
					          </div>
					       
					        <div class="dropdown_span">
					          <div style="display: none;" id="city_select" class="fl">
					            <ul class="fl drop_window"></ul>
					          </div>
						        <select id="loc_province" style="width:80px;"></select>
								<select id="loc_city" style="width:100px;"></select>
								<select id="loc_town" style="width:120px;"></select>
					          <div style="display: none;" class="fl" id="area_select">
					            <ul class="fl drop_window"></ul>
								<input type="hidden" name="location_id" />

					          <span class="mltips" id="receivingZipCode_tips"></span>
					        </div>
					      </td>
					    </tr>
					    <tr>
					      <td class="tdleft"></td>
					      <td>
					        <span id="province_i"></span>
					        <span id="city_i"></span>
					        <span id="area_i"></span>
					        <input type="text" placeholder="" style="width: 250px;" class="linput" maxlength="60" name="receivingAddress" id="receivingAddress">
					        <span class="mltips" id="receivingAddress_tips">
					          <font class="cred">&nbsp;&nbsp;（请填写详细地址，确保准确送达）</font></span>
					      </td>
					    </tr>
					    <tr>
					      <td class="tdleft">收货人手机
					        <i>*</i>
					      </td>
					      <td style="vertical-align:middle;">
					        <span class="fl">
					          <input type="text" placeholder="" maxlength="11" class="minput" onkeyup="$('#phoneCodeOrder_tips').hide();$('#phoneCodeOrderTxt').hide();$('#checkSmsCodeOrderBtn').hide();$('#codeCheckOrderResult_tips').hide();" name="receivingMobilePhone" id="receivingMobilePhone"></span>
					        <span class="pl12 fl none" id="updatePhoneOrder_tips">
					          <font class="cred">您的手机号码已经变更，请重新获取验证码</font></span>
					        <span class="mltips fl" id="receivingMobilePhone_tips"></span>
					        <span id="yzspanarea"></span>
					      </td>
					    </tr>
					    <tr>
					      <td class="tdleft">收货时间
					        <i>*</i>
					      </td>
					      <td>
					        <select class="mselect delivery_time" name="distributionType" id="distributionType">
					          <option value="1">不限时间</option>
					          <option value="2">周一至周五</option>
					          <option value="3">周六日/节假日</option>
					        </select>
					        <span class="cgray">（送货时间仅供参考，快递公司会尽量满足您的要求）</span></td>
					    </tr>
					    <tr>
					      <td class="tdleft"></td>
					      <td style="padding-top:20px;">
					        <input type="button" onclick="submitAddress();" class="subBtn" value="确认保存">&nbsp;&nbsp;
					        <input type="button" onclick="parent.closeDialog();" class="cancelBtn" value="取消"></td></tr>
					  </tbody>
					</table>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>

<div id="top_nav">
	<div class="view_areaimg">
		<div class="yg link_box"><a href="">时尚商城</a></div>
		
		<div class="phone link_box">
			<a href="" class="phone_text"><i class="mobile_ico"></i>手机优购<i class="tip"></i></a>
			<div class="phone_con">
                    <p class="clearfix">
                        <span class="fl qr_code">
                        </span>
                        <span class="fl ml10">
                            <a class="btn_app_store btn" href="" target="_blank">App Store</a>
                            <a class="btn_android_store btn" href="" target="_blank">Android</a>
                        </span>
                    </p>
                    <p class="qr_code_tip">下载安装 <strong>优购客户端</strong></p>
                </div>
		</div>
		<div class="outlets link_box" style="border-right:none"></div>
		<div class="fr">
		<div class="about_user">您好！<span id="user_id">刘鑫</span><a class="quit" href="">退出</a></div><!--about_user end -->
		<div class="my_yg link_box">
			<a href="" class="a1">我的优购</a>
			<ul class="info_con">
				<li><a href="">我的收藏</a></li>
				<li style="display: list-item;" id="commentcount"><a href="http://www.yougou.com/my/comment.jhtml?t=14744409420215712">等待点评(<i class="corg">0</i>)</a></li>
				<li style="display: list-item;" id="top_msg"><a href="">站内消息(<i class="corg">0</i>)</a></li>
			</ul>
		</div>
		<div class="my_order link_box"><a href="" rel="nofollow">我的订单</a></div>
		<div class="notice link_box">
			<span class="notice_text">公告</span>
			<ul class="notice_con">
                    <li><a target="_blank" href="">运动新风尚 新品5折起</a></li>
                    <li><a target="_blank" href="">摩登男装 秋冬大促 1折起</a></li>
                    <li><a target="_blank" href="">潮靴秀美腿 价比11.11</a></li>
                    <li><a target="_blank" href="">女装初冬热促 爆款2折起</a></li>
                    <li><a target="_blank" href="http://www.yougou.com/article/201411/87dc5ccf633611e4b7eea30f61b97b3f.shtml#ref=index&amp;po=notice_notice5">库房发货时间调整说明</a></li>
			</ul>
		</div>
		<div class="more link_box">
			<a href="" class="more_text">更多</a>
			<ul class="more_con">
				<li><a onclick="" href="">收藏优购</a></li>
				<li><a href="">帮助</a></li>
			</ul>
		</div>
		</div>
	</div><!--view_area end -->
</div><!--top_nav end -->


<form id="orderForm" method="POST" name="orderForm" action="">
<div class="cen cart_header">
    <h1 class="logo">
    <a href="" title="优生活，购时尚"><img src="/home/order/bank/logo-yg.png" alt="优购时尚商城"></a></h1>
    <div class="fr cart_step mt20">
        <ul>
            <li>我的购物车<em></em><i></i></li>
            <li class="current">提交订单中心<em></em><i></i></li>
            <li>成功提交订单<em></em><i></i></li>
        </ul>
    </div>
</div>

<input id="orderNum" value="1" type="hidden">
<input id="isLock" value="false" type="hidden">
<input id="limitOrderNum" value="40" type="hidden">
<input id="limitOrderCommBuyMaxNum" value="40" type="hidden">
<input id="isOrderDistributionSide" value="false" type="hidden">
<input id="tradeCurrency" name="tradeCurrency" value="CNY" type="hidden">
<input id="linkBuy" name="linkBuy" value="1" type="hidden">
<input id="noSupportCODMsg" value="1" type="hidden">
<input id="authToken" value="" type="hidden">
<input id="authSwitch" value="false" type="hidden">
	<!-- 收货地址 start -->
 <div class="cen mt30 clearfix">
	  <div class="col_hd">  
	      <h2 class="col_tlt" id="selAddrTitle">请选择您的收货地址</h2>

	      <span class="pay_addr_alipay">
	    	  <a class="cart_back_link ml10 cblue undline" href="">返回购物车</a>
	      </span>
		  <span class="col_trt"><a href="" target="_blank" class="cblue">管理收货地址</a></span>
	  </div>
<div id="reciever_list" class="pay_addr reciever_list" style="height: 135px;">
				<dl class="curr">
				<i>&nbsp;</i>
				<p class="actP">
						<a href="" title="修改收货地址" class="cblue address_update" addrid="65e5fb19a36e499db5ab99a5e6ad7ae7">修改</a>
						| <a href="j" class="delete cblue" addrid="65e5fb19a36e499db5ab99a5e6ad7ae7">删除</a>
				</p>
				<label><input style="display:none" name="addressRadio" addresskey="user_address_65e5fb19a36e499db5ab99a5e6ad7ae7" addressid="65e5fb19a36e499db5ab99a5e6ad7ae7" id="addressRadio_65e5fb19a36e499db5ab99a5e6ad7ae7" value="address_radio_65e5fb19a36e499db5ab99a5e6ad7ae7" checked="checked" type="radio"></label>
				<dt>
					<span class="area">北京<strong>北京市</strong></span>
					(<span class="conName">刘鑫</span>&nbsp;收)
				</dt>
				<dd>
					<p>昌平区****平区兄弟连&nbsp;<em class="Gray">188****3602</em></p>
					<p class="mt5">
						
						工作日、双休日与假日均可送货
						
					</p>
				</dd>
			</dl>
	<dl class="new_addr" id="add_address" style="display: block;">
        <label><input style="display:none" value="new_address" addressid="otherAddress" addresskey="otherAddress" name="addressRadio" type="radio"></label>
        <dd class="kns"><i>+</i>使用新地址</dd>
    </dl>
</div> 
  	  <!-- ===============qq彩贝地址================ -->
	  <!-- ===============qq彩贝地址================ -->

      <p class="blank10"></p>
    </div>
  <input name="receivingProvince" id="receivingProvince" value="root-6" type="hidden">
  <input name="receivingProvinceName" id="receivingProvinceName" value="北京" type="hidden">
  <input name="receivingCity" id="receivingCity" value="root-6-1" type="hidden">
  <input name="receivingCityName" id="receivingCityName" value="北京市" type="hidden">
  <input name="receivingDistrict" id="receivingDistrict" value="root-6-1-13" type="hidden">
  <input name="receivingDistrictName" id="receivingDistrictName" value="昌平区" type="hidden">
  <input name="addressState" id="addressState" value="2" type="hidden">
  <input name="addressId" id="addressId" value="65e5fb19a36e499db5ab99a5e6ad7ae7" type="hidden">
	<!--//收货地址end-->
	<!-- 支付方式 start -->
	<div id="paymentContainer" class="cen mt30">
 <!--付款方式-->
<div for="payment_1" style="display:none">在线支付<input id="payment_1" name="payment" value="ONLINE_PAYMENT" checked="checked" type="radio"></div>
<div class="col_hd" id="paymentBank">
    <h2 class="col_tlt">请选择您的付款方式<strong id="webbankpay_tips" class="ml10"></strong></h2>
    <span class="col_trt"><a href="" target="_blank" class="cblue">查看银行限额及付款帮助</a></span>
</div>
<div class="mt15"><strong>在线支付</strong>


    <span class="corg">(满99元在线支付免运费)</span>
    <span class="alipayTips" style="display:none">
        <span class="tit">支付宝扫码立减5元，先到先得！</span>
        <div class="desc">
            <p>
                <strong>活动规则：</strong><br>
                1.活动时间：3月5日-4月22日，每天零点开抢，限前209名使用支付宝扫码且金额满10元的用户有机会获得立减优惠，名额有限，送完为止。<br>
                2.同一用户（同一身份证、支付宝账户号、终端设备等同一用户条件）活动期间限享受优惠一次。<br>
                3.本活动优惠为实时立减，因系统存在一定延迟性，优惠的获得以支付宝收银台显示支付成功且优惠享用成功为准。<br>
                4.订单支付成功后，如产生订单取消，则取消立减优惠资格，若发生退款，优惠部分的金额将不予退回（同时由于该用户已经生成订单则不能再参与立减优惠）。<br>
            </p>
            <i class="sanjiao"></i>
        </div>
      </span>
</div>
  <!--上次支付方式-->
    <div id="lastPayType" class="lastPayType cart_paytype_list mt10 none"></div>
  <!--end 上次支付方式--> 
  
  <!--修改支付方式-->
  <div class="cart_paytype_list " id="payTypeList">
  		<div class="pay_online_div pay_online_div_new">
				<div id="ucTab" class="uc_tab ml15 clearfix evil">
					<a class="current" style="border-left:1px solid #c4c4c4;margin-left: -26px">
						<span>网银支付</span>
					</a>
					<a>
						<span>信用卡</span>
					</a>
					<a>
						<span>支付平台</span>
					</a>
				</div>
			   <div id="ucFavList" class="uc_fav_list ml15">
<!------------------------------------------------------------------------------------------------>
	<div class="jlf" style="display: block">
		<dl class="clearfix">
		  <dt>
			<span class="c9 ml10">支持国内几乎所有银行的储蓄卡及信用卡进行支付（需开通网上银行）</span>
		  </dt>
		  <dd class="mb15">
				<span><input name="webbankpay" id="bank_zhaoshang" value="zhaoshang" type="radio"></span>
				<img src="/home/order/bank/bank-img3.png" title="招商银行" alt="招商银行" height="40" width="139">
		  </dd>
		  <dd class="mb15">
			<span><input name="webbankpay" id="bank_gongshang" value="gongshang" type="radio"></span>
			<img src="/home/order/bank/bank-img2.png" title="中国工商银行" alt="中国工商银行" height="40" width="139">
		  </dd>
		  <dd class="mb15">
			<span><input name="webbankpay" id="bank_nongye" value="nongye" type="radio"></span>
			<img src="/home/order/bank/bank-img4.png" title="中国农业银行" alt="中国农业银行" height="40" width="139">
		  </dd>
		  <dd class="mb15">
			<span><input name="webbankpay" id="bank_jianshe" value="jianshe" type="radio"></span>
			<img src="/home/order/bank/bank-img5.png" title="中国建设银行" alt="中国建设银行" height="40" width="139">
		  </dd>
		  <dd class="mb15">
			<span><input name="webbankpay" id="bank_zhongguo" value="zhongguo" type="radio"></span>
			<img src="/home/order/bank/bank-img6.png" title="中国银行" alt="中国银行" height="40" width="139">
		  </dd>
		  <dd class="mb15">
			<span><input name="webbankpay" id="bank_jiaotong" value="jiaotong" type="radio"></span>
			<img src="/home/order/bank/bank-img7.png" title="交通银行" alt="交通银行" height="40" width="139">
		  </dd>
		  <dd class="mb15">
			<span><input name="webbankpay" id="bank_youzheng" value="youzheng" type="radio"></span>
			<img src="/home/order/bank/bank-img8.png" title="中国邮政储蓄银行" alt="中国邮政储蓄银行" height="40" width="139">
		  </dd>
		  <dd class="mb15">
			<span><input name="webbankpay" id="bank_pufa" value="pufa" type="radio"></span>
			<img src="/home/order/bank/bank-img9.png" title="浦发银行" alt="浦发银行" height="40" width="139">
		  </dd>
		  <dd class="mb15">
			<span><input name="webbankpay" id="bank_guangda" value="guangda" type="radio"></span>
			<img src="/home/order/bank/bank-img10.png" title="中国光大银行" alt="中国光大银行" height="40" width="139">
		  </dd>
		  <dd class="mb15">
			<span><input name="webbankpay" id="bank_guangfa" value="guangfa" type="radio"></span>
			<img src="/home/order/bank/bank-img11.png" title="广东发展银行" alt="广东发展银行" height="40" width="139">
		  </dd>
		  <div class="clear"></div>
		  <div style="width: 97%;margin: 0;margin-bottom: 15px;background: #f1f1f1;color: #f1f1f1;height:1px;border:0;"></div>
		  <dd class="">
			<span><input name="webbankpay" id="webbank_minsheng" value="minsheng" type="radio"></span>
			<img src="/home/order/bank/bank-img12.png" title="中国民生银行" alt="中国民生银行" height="40" width="139">
		  </dd>
		  <dd class="">
			<span><input name="webbankpay" id="webbank_xingye" value="xingye" type="radio"></span>
			<img src="/home/order/bank/bank-img20.png" title="兴业银行" alt="兴业银行" height="40" width="139">
		  </dd>
		  <dd class="">
			<span><input name="webbankpay" id="webbank_zhongxin" value="zhongxin" type="radio"></span>
			<img src="/home/order/bank/bank-img24.png" title="中信银行" alt="中信银行" height="40" width="139">
		  </dd>
		  <dd class="">
			<span><input name="webbankpay" id="webbank_beijing" value="beijing" type="radio"></span>
			<img src="/home/order/bank/bank-img23.png" title="北京银行" alt="北京银行" height="40" width="139">
		  </dd>

		  <dd class="">
			<span><input name="webbankpay" id="webbank_huaxia" value="huaxia" type="radio"></span>
			<img src="/home/order/bank/bank-img25_002.png" title="华厦银行" alt="华厦银行" height="40" width="139">
		  </dd>
		  <dd class="">
			<span><input name="webbankpay" id="webbank_shanghai" value="shanghai" type="radio"></span>
			<img src="/home/order/bank/bank-img26.png" title="上海银行" alt="上海银行" height="40" width="139">
		  </dd>
		  <dd class="">
			<span><input name="webbankpay" id="webbank_guangzhou" value="guangzhou" type="radio"></span>
			<img src="/home/order/bank/bank-img27.png" title="广州银行" alt="广州银行" height="40" width="139">
		  </dd>
		  <dd class="">
			<span><input name="webbankpay" id="webbank_hangzhou" value="hangzhou" type="radio"></span>
			<img src="/home/order/bank/bank-img28_002.png" title="杭州银行" alt="杭州银行" height="40" width="139">
		  </dd>
		  <dd class="">
			<span><input name="webbankpay" id="webbank_bohai" value="bohai" type="radio"></span>
			<img src="/home/order/bank/bank-img29.png" title="渤海银行" alt="渤海银行" height="40" width="139">
		  </dd>
		  <dd class="">
			<span><input name="webbankpay" id="webbank_dongya" value="dongya" type="radio"></span>
			<img src="/home/order/bank/bank-img30.png" title="BEA东亚银行" alt="BEA东亚银行" height="40" width="139">
		  </dd>
		  
		  <dd class="">
			<span><input name="webbankpay" id="webbank_ningbo" value="ningbo" type="radio"></span>
			<img src="/home/order/bank/bank-img31.png" title="宁波银行" alt="宁波银行" height="40" width="139">
		  </dd>
		  <dd class="">
			<span><input name="webbankpay" id="webbank_pingan" value="pingan" type="radio"></span>
			<img src="/home/order/bank/bank-img22.png" title="平安银行" alt="平安银行" height="40" width="139">
		  	<span class="bankSeltips">支持深圳发展银行</span>
		  </dd>
		  <dd class="">
			<span><input name="webbankpay" id="webbank_shenfazhan" value="shenfazhan" type="radio"></span>
			<img src="/home/order/bank/bank-img32.png" title="深圳发展银行" alt="深圳发展银行" height="40" width="139">
			  	<span class="bankSeltips">请选择平安银行付款</span>
			  </dd>
		   </dl>				  
	</div>
						  <!--信用卡-->
	<div class="jlf" style="display: none">
		<dl class="crdt_payment clearfix">
				 <dt>
					 信用卡<img class="ml5" src="/home/order/bank/kjpay.png" alt="快捷支付" height="17" width="74">
					 <span class="c9 ml10">由支付宝提供快捷支付保障，72小时安全赔付，<em class="corg">免开通网上银行</em>，有卡就能付</span>
					 <a class="payment_prvw rel" href="">预览需填写信息<i></i><span class="prvw_fllinf"><img src="/home/order/bank/prvw-fllinf.png" alt="" height="425" width="416"></span></a>
					 <a href="" target="_blank" class="cblue ml10">查看快捷支付演示</a>
				 </dt>
			   <dd>
				  <span><input name="webbankpay" id="bank_zhaoshang_quick" value="zhaoshang_quick" type="radio"></span>
				  <img src="/home/order/bank/bank-img3_002.png" title="招商银行" alt="招商银行" height="40" width="139"><sup></sup></dd>
			  <dd>
				<span><input name="webbankpay" id="bank_jianshe_quick" value="jianshe_quick" type="radio"></span>
				<img src="/home/order/bank/bank-img5_002.png" title="中国建设银行" alt="中国建设银行" height="40" width="139"><sup></sup></dd>
			  <dd>
				<span><input name="webbankpay" id="bank_gongshang_quick" value="gongshang_quick" type="radio"></span>
				<img src="/home/order/bank/bank-img2.png" title="中国工商银行" alt="中国工商银行" height="40" width="139"><sup></sup></dd>
			  <dd>
				<span><input name="webbankpay" id="bank_zhongguo_quick" value="zhongguo_quick" type="radio"></span>
				<img src="/home/order/bank/bank-img6.png" title="中国银行" alt="中国银行" height="40" width="139"><sup></sup></dd>
			  <dd>
				<span><input name="webbankpay" id="bank_nongye_quick" value="nongye_quick" type="radio"></span>
				<img src="/home/order/bank/bank-img4.png" title="中国农业银行" alt="中国农业银行" height="40" width="139"><sup></sup></dd>
			  <dd>
				<span><input name="webbankpay" id="bank_guangfa_quick" value="guangfa_quick" type="radio"></span>
				<img src="/home/order/bank/bank-img11.png" title="广发银行" alt="广发银行" height="40" width="139"><sup></sup>
			  </dd>
			  <dd>
				<span><input name="webbankpay" id="bank_huaxia_quick" value="huaxia_quick" type="radio"></span>
				<img src="/home/order/bank/bank-img25.png" title="华夏银行" alt="华夏银行" height="40" width="139"><sup></sup>
			  </dd>
			  <dd>
				<span><input name="webbankpay" id="bank_xingye_quick" value="xingye_quick" type="radio"></span>
				<img src="/home/order/bank/bank-img20_002.png" title="兴业银行" alt="兴业银行" height="40" width="139"><sup></sup>
			 </dd>
			  <dd>
				<span><input name="webbankpay" id="bank_minsheng_quick" value="minsheng_quick" type="radio"></span>
				<img src="/home/order/bank/bank-img21.png" title="中国民生银行" alt="中国民生银行" height="40" width="139"><sup></sup></dd>
			  <dd>
				<span><input name="webbankpay" id="bank_guangda_quick" value="guangda_quick" type="radio"></span>
				<img src="/home/order/bank/bank-img10.png" title="中国光大银行" alt="中国光大银行" height="40" width="139"><sup></sup></dd>
				
			  <div class="clear"></div>
				  <div style="width: 97%;margin: 0;margin-bottom: 15px;background: #f1f1f1;color: #f1f1f1;height:1px;border:0;"></div>
			  <dd>
				  <span><input name="webbankpay" id="bankquick_beijing_quick" value="beijing_quick" type="radio"></span>
				  <img src="/home/order/bank/bank-img23_002.png" title="北京银行" alt="北京银行" height="40" width="139"><sup></sup></dd>
			  <dd>
				<span><input name="webbankpay" id="bank_shanghainongshang_quick" value="shanghainongshang_quick" type="radio"></span>
				<img src="/home/order/bank/bank-img33.png" title="上海农商银行" alt="上海农商银行" height="40" width="139"><sup></sup>
			  </dd>
			  <dd>
				<span><input name="webbankpay" id="bank_ningxia_quick" value="ningxia_quick" type="radio"></span>
				<img src="/home/order/bank/bank-img43.png" title="宁夏银行" alt="宁夏银行" height="40" width="139"><sup></sup>
			  </dd>
			  <dd>
				<span><input name="webbankpay" id="bank_dalian_quick" value="dalian_quick" type="radio"></span>
				<img src="/home/order/bank/bank-img34.png" title="大连银行" alt="大连银行" height="40" width="139"><sup></sup>
			  </dd>
			  <dd>
				<span><input name="webbankpay" id="bank_dongguan_quick" value="dongguan_quick" type="radio"></span>
				<img src="/home/order/bank/bank-img35.png" title="东莞银行" alt="东莞银行" height="40" width="139"><sup></sup>
			  </dd>
			  <dd>
				<span><input name="webbankpay" id="bank_ningbo_quick" value="ningbo_quick" type="radio"></span>
				<img src="/home/order/bank/bank-img31.png" title="宁波银行" alt="宁波银行" height="40" width="139"><sup></sup>
			  </dd>
			   <dd>
				<span><input name="webbankpay" id="bank_tianjin_quick" value="tianjin_quick" type="radio"></span>
				<img src="/home/order/bank/bank-img36.png" title="天津银行" alt="天津银行" height="40" width="139"><sup></sup>
			  </dd>
			  <dd>
				<span><input name="webbankpay" id="bank_hangzhou_quick" value="hangzhou_quick" type="radio"></span>
				<img src="/home/order/bank/bank-img28.png" title="杭州银行" alt="杭州银行" height="40" width="139"><sup></sup>
			  </dd>
				<dd>
				<span><input name="webbankpay" id="bank_hebei_quick" value="hebei_quick" type="radio"></span>
				<img src="/home/order/bank/bank-img37.png" title="河北银行" alt="河北银行" height="40" width="139"><sup></sup>
			  </dd>
				<dd>
				<span><input name="webbankpay" id="bank_jiangsu_quick" value="jiangsu_quick" type="radio"></span>
				<img src="/home/order/bank/bank-img38.png" title="江苏银行" alt="江苏银行" height="40" width="139"><sup></sup>
			  </dd>
			  <dd>
				<span><input name="webbankpay" id="bank_nanjing_quick" value="nanjing_quick" type="radio"></span>
				<img src="/home/order/bank/bank-img39.png" title="南京银行" alt="南京银行" height="40" width="139"><sup></sup>
			  </dd>
			  <dd>
				<span><input name="webbankpay" id="bank_weishang_quick" value="weishang_quick" type="radio"></span>
				<img src="/home/order/bank/bank-img40.png" title="徽商银行" alt="徽商银行" height="40" width="139"><sup></sup>
			  </dd>
			  <dd>
				<span><input name="webbankpay" id="bank_tailong_quick" value="tailong_quick" type="radio"></span>
				<img src="/home/order/bank/bank-img41.png" title="浙江泰隆商业银行" alt="浙江泰隆商业银行" height="40" width="139"><sup></sup>
			  </dd>
			  <dd>
				<span><input name="webbankpay" id="bank_wujiangnongshang_quick" value="wujiangnongshang_quick" type="radio"></span>
				<img src="/home/order/bank/bank-img42.png" title="吴江商业银行" alt="吴江商业银行" height="40" width="139"><sup></sup>
			  </dd>
			  <dd>
				<span><input name="webbankpay" id="bank_zhongxin_quick" value="zhongxin_quick" type="radio"></span>
				<img src="/home/order/bank/bank-img24_002.png" title="中信银行" alt="中信银行" height="40" width="139"><sup></sup>
			  </dd>
			  <dd>
				<span><input name="webbankpay" id="bank_pufa_quick" value="pufa_quick" type="radio"></span>
				<img src="/home/order/bank/bank-img9.png" title="浦发银行" alt="浦发银行" height="40" width="139"><sup></sup>
			  </dd>
			  <dd>
				<span><input name="webbankpay" id="bank_pingan_quick" value="pingan_quick" type="radio"></span>
				<img src="/home/order/bank/bank-img22_002.png" title="平安银行" alt="平安银行" height="40" width="139"><sup></sup> 
				<span class="bankSeltips">支持深圳发展银行</span>
			  </dd>
			  <dd>
				<span><input name="webbankpay" id="bank_shenfazhan_quick" value="shenfazhan_quick" type="radio"></span>
				<img src="/home/order/bank/bank-img32_002.png" title="深圳发展银行" alt="深圳发展银行" height="40" width="139"><sup></sup>
				<span class="bankSeltips">请选择平安银行付款</span>
			  </dd>
			  
			  <p class="clear"></p>
			</dl>				 
	</div>
						<!-- 支付平台 -->
	<div class="jlf" style="display: none">
						 <dl class="zfpt_dl clearfix">
						  <dt>支付平台</dt>
						  <dd>
							  <span name="zhifubaoTip" class="kjtip">支持银行卡快捷</span>
							<span><input name="webbankpay" id="bank_zhifubao" value="zhifubao" type="radio"></span>
							<img src="/home/order/bank/bank-img13.png" title="支付宝" alt="支付宝" height="40" width="139">
						  </dd>
							<dd>
							  <span><input name="webbankpay" id="bank_weixin" value="weixin" type="radio"></span>
							  <img src="/home/order/bank/bank-img45.png" title="微信" alt="微信" height="40" width="139">
							</dd>
							  <dd>
									   <span class="kjtip">支持银行卡快捷</span>
							  <span><input name="webbankpay" id="bank_yinlian" value="yinlian" type="radio"></span>
							  <img src="/home/order/bank/bank-img14.png" title="银联" alt="银联" height="40" width="139">
							</dd>
							<dd>
							  <span><input name="webbankpay" id="bank_kuaiqian" value="kuaiqian" type="radio"></span>
							  <img src="/home/order/bank/bank-img15.png" title="快钱" alt="快钱" height="40" width="139">
							</dd>
							<dd>
							  <span><input name="webbankpay" id="bank_caifutong" value="caifutong" type="radio"></span>
							  <img src="/home/order/bank/bank-img16.png" title="财付通" alt="财付通" height="40" width="139">
							</dd>
							<dd>
							  <span><input name="webbankpay" id="bank_shouji" value="shouji" type="radio"></span>
							  <img src="/home/order/bank/bank-img17.png" title="手机支付" alt="手机支付" height="40" width="139">
							</dd>
							<dd>
								<span><input name="webbankpay" id="bank_lakala" value="lakala" type="radio"></span>
								<img src="/home/order/bank/bank-img18.png" title="拉卡拉" alt="拉卡拉" height="40" width="139">
							  </dd>
						</dl>
	</div>
					</div>
				</div>
		    </div>
		    <!-- 货到付款 --> 
		    <div>
      <div id="cashOnDelivery_1" class="arrive noArriveDiv clearfix" style="display:none">
        <dl class="arrive">
            <dd>
              <label class="fb"><input class="radio" id="payment_2" name="payment" value="CASH_ON_DELIVERY" type="radio"> 货到付款</label>
              &nbsp;&nbsp;
            <span class="c9">(每单收取<em class="corg">15元</em>运费)</span>
            </dd>
            <p class="clear"></p>
          </dl>
          <!--手机绑定start-->
            <div class="none" id="mobileCheckDiv">
                <input name="mobileCheck" id="mobileCheck" type="hidden">
              <p id="mobileCheck_tips" class="ml20"></p>
              <p class="clear"></p>
                <div class="mobile_check">
                    <div class="clearfix">
                         <span class="fl mt3">图片验证：</span><span class="fl"><input class="minput" name="code2_" id="code2_" maxlength="4" type="text"></span>
                          <img src="/home/order/bank/imageCaptcha.png" id="imageValidate2">
                          <a href="" onclick="changeValidateImage();return false;" class="Gray">换一张</a>
                           <span id="codeTips2" class="mltips">&nbsp;</span>
                      </div>
                    <div class="clearfix">
                        <div class="fl"><span class="fl mt3">手机号码：</span><span class="fl"><input name="mobile" id="Mobile" class="minput" maxlength="11" placeholder="请输入您的手机号码" type="text"></span></div>
                        <a class="u_send u_sendyzm ml10 fl" href="" id="sendCodeBtn"><span id="getMsgSpan">获取短信验证码</span></a>
                        <span id="sendTips" class="mltips">&nbsp;</span>
                        <div class="fl ml20 none" id="MobileCodeSpan">
                            <input name="Code" id="Code" class="minput code fl" maxlength="4" placeholder="请输入验证码" type="text">
                            <span class="fl ml5 none" id="codeTips"></span>
                            <span class="fl ml5 mt3"><a class="autobtn_orign  fl" href="" id="MobileCodeConfirm"><span class="fl">确 认</span></a></span>
                        </div>
                    </div>
                    <p class="c9 mt10 warntips">
                        为了确保您的商品顺利送达和保护账号安全，请进行<span class="corg">手机号码与账户绑定</span>。
                    </p>
                    <p id="Tips" class="row mt10 none">
            <span class="safeTipInfo corg">若您一直无法接收到验证码，可能是被拦截为垃圾短信，或者由于通信网络异常，请您稍后再重新尝试。</span>
            </p>
              </div>
            </div>
            <!--手机绑定end-->
        <p class="arriveTips">
          <span class="corg">提醒：</span>
          <span class="c9">现金支付，暂不支持刷卡。请当面验货，满意后再付款，1000元以上客服人员可能会与您取得联系。</span>
        </p>
      </div>
      <div id="cashOnDelivery_2" class="arrive noArriveDiv clearfix">
        <span class="fl rel" style="margin-top:3px;">
          <label class="fb"><input class="radio" id="payment_3" name="payment" disabled="disabled" type="radio"> <span class="c9">货到付款</span></label>
        </span>

        <div class="fl noArriveTips rel">
          <div style="display: block;" id="noSupportCODMsg_1">
            <span class="c9">抱歉，您所在地区暂不支持货到付款，请选择其他的支付方式</span><br>
          </div>
          <div id="noSupportCODMsg_2" style="display:none">
            由于您购买的是
            <span id="noArriveArr" class="noArriveArr rel"><font class="cred">由品牌商直接发货的商品</font><i>&nbsp;</i>
              <div id="noArriveGoodsList" class="noArriveGoodsList">
                <ul>
                </ul>
              </div>
              
            </span>不支持货到付款，请选择其他的支付方式<br>
          </div>
          <div id="noSupportCODMsg_3" style="display:none">
            <span class="c9">由于您购买的商品分属在不同仓库，需要拆单发货，不支持货到付款，请选择其他的支付方式</span>
          </div>
          <div id="noSupportCODMsg_4" style="display:none">
            <span class="c9">该商品所在的发货仓库不支持货到付款，请选择其他的支付方式</span>
          </div>
          <div id="noSupportCODMsg_5" style="display:none">
            <span class="c9">该商品所在的发货仓库不支持货到付款，请选择其他的支付方式</span>
          </div>
          <div id="noSupportCODMsg_7" style="display:none">
            <span class="c9">抱歉，首尔Station商品暂不支持货到付款，请选择其他支付方式</span>
          </div>
          <div id="noSupportCODMsg_6" style="display:none">
            <span class="c9">由于您的订单金额超过3000元，为安全考虑，请选择其他支付方式</span>
          </div>
        </div>
      </div>
      <!--end 不支持支持货到付款情况-->
    </div>  
<p class="blank40"></p>
</div>
	<!-- 发票信息 st -->
<!--电子发票Start-->
<div id="diveInvoice" style=""> 
    <div class="cen mt30">
	    <div class="col_hd" style="border-bottom:none;">
    	    <h2 class="titt">发票信息</h2>
        </div>
        <div class="order_lst mt10">		
		
            <div class="mt15">		
	            <div class="eInvoiceHd">	
		            <input id="einvc" onclick="$.oEInvoice.fClickInvoice()" name="needEInvoice" value="1" type="checkbox">
		            <label class="f12" for="einvc">我需要电子发票</label> <span class="t cred">提醒：</span><span class="cgray">
				            如您需要纸质发票，请不要选择电子发票，请您收货确认后联系客服开具纸质发票
			            </span>
	            </div>
	            <div class="eInvoiceBd  none ">
		            <ul class="invc_cbox">
			            <li><span class="t cgray">发票开具方式：</span>
				            <input id="pf" name="pf" checked="checked" value="0" type="radio">
				            <label for="pf" class="mr10">普通发票(电子)</label> <a href="" target="_blank" class="cblue tmiddle">什么是电子发票？</a> </li>
			            <li>
				            <span class="t cgray">发票抬头：</span>
				            <input id="pp" name="pp" checked="checked" value="0" type="radio">
				            <label for="pp" class="mr20">个人</label>
				            <input onblur="$.oEInvoice.fValidTitle(false)" class="sinput" name="eInvoiceTitle" id="eInvoiceTitle" type="text">
				            <span class="error_tipei  hid" id="eInvoiceInvalidMsg3">请输入正确的姓名(不输入则默认为个人)</span>
			            </li>
			            <li><span class="t cgray">发票内容：</span> 商品明细
			            </li>
			            <li>
				            <span class="t cgray"><i class="corg">*</i> 收票人手机：</span>
				            <span id="eInPhoneSpan"></span>
				            <input onblur="" class="sinput minput" size="40" type="text">
				            <input id="eInPhoneAddrId" name="eInPhoneAddrId" value="" type="hidden">
				            <input id="eInPhoneAddrIdBak" value="" type="hidden">
				            <input id="eInbtnEdit" value="修改" class="cart_submitbtn " style="height:23px; width:60px;" type="button">
				            <input id="eInbtnSubmit" value="提交" class="cart_submitbtn " style="height:23px; width:60px;" type="button">
				            <input id="eInbtnCancel" onclick="$.oEInvoice.feInCancel();" value="取消" class="cart_submitbtn " style="height:23px; width:60px;" type="button">
				            <span class="error_tipei hid">请输入收票人手机</span>
				            <span class="error_tipei hid">请输入正确收票人手机</span>
			            </li>
		            </ul>
	            </div>	           							
            </div>
	    </div>
    </div>
</div>

<!--电子发票end-->
	<!-- 发票信息 ed -->
	<!--// 支付方式 end -->
	<!--确认订单信息start-->
	<div id="orderSettlementContainer">
<input id="addProductNum" value="1" type="hidden">
<!--商品列表start-->
<div class="cen mt30">
    <div class="col_hd" style="border-bottom:none;">
        <h2 class="col_tlt">确认订单信息</h2>
    </div>

    <div class="cart_gray_box order_lst mt10">
        <table id="cart_goods_tb" class="order_goods_tb">
            <colgroup>
                <col width="400">
                <col width="110">
                <col width="130">
                <col width="130">
                <col width="175">
            </colgroup>
            <thead>
            <tr>
                <th>商品名称</th>
                <th>颜色尺码</th>
                <th>单价</th>
                <th>数量</th>
                <th>小计(元)</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td colspan="5">
                    <input value="1" id="productRecordNum" type="hidden">
                    <input value="0" id="productGiftRecordNum" type="hidden">
                    <input value="1" id="orderCommNum" type="hidden">
                    <input id="orderAmountLimit" value="1" type="hidden">
                    <input id="lackOrderAmount" value="0" type="hidden">

                    <!-- 优购时尚商城开始-->

                                        <div class="shpcrt_blank10 clearfix"></div>
                                        <div id="shopping_cart1" class="shopping_cart_tr ">
                                            <dl class="clearfix">
                                                <input value="2016-09-21 11:17:51" id="addTime" type="hidden">
                                                <dt class="col_1 fl rel">
                                                    <a target="_blank" href="">
                                                        <img  src="" class="thumImg" height="60" width="60">

                                                        
                                                    </a>
                                                </dt>
                                                <dd class="col_2 fl"><a target="_blank" href="" title="PUMA彪马2016新品男子彪马生活系列针织长裤57197601" class="shopLink">PUMA彪马2016新品男子彪马生活系列针织长裤57197601</a><br>

                                                </dd>
                                                <dd class="col_3 fl">
                                                    <div class="pl20">
                                                        <p class="cgray tleft">颜色：<em class="f_gray">彪马黑</em></p>

                                                        <p class="cgray tleft">尺码：<em class="f_gray">L</em></p>
                                                    </div>
                                                </dd>
                                                    <dd class="col_4 fl rel ">
                                                        359

                                                    </dd>
                                                    <dd class="col_5 fl rel">
                                                    1
                                                    </dd>
                                                    <dd class="col_6 fl"><strong class="corg">359</strong></dd>
                                            </dl>

                                            <div class="shpcrt_blank10 clearfix"></div>
                                        </div>
                </td>
            </tr>
            </tbody>
        </table>
        <div class="ygtjzx_ttl clearfix" id="shoppingCartContainer">
            <div id="couponarea" class="fl mt10">
                <dl class="clearfix">
                    <dt class="fl">使用优惠券：</dt>
                    <dd class="rel fl" style="z-index: 9;">
                        <input id="couponNumber" class="sinput cgray" style="width:170px;padding-left:25px;" maxlength="16" placeholder="请输入线下优惠券编码" type="text">

                        <div style="position: absolute; top: 25px; left: 0px; cursor: default;" id="coupon_list" class="coupon_list none">
                            <dl style="width:512px">
                                <dd style="width:25px">&nbsp;</dd>
                                <dd style="width:118px">优惠券编号</dd>
                                <dd style="width:90px">优惠券面额</dd>
                                <dd style="width:90px">最低消费金额</dd>
                                <dd style="width:179px">有效期</dd>
                                <a class="couponlist_close"></a></dl>
                            <div style="height:185px;overflow:auto;">
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                            <input id="useCouponBtn" class="cart_normal_btn jsUseCoup ml5" value="确认" type="button">
                            <input class="cart_normal_btn jsCancelCoup" id="cancelUseCouponBtn" value="取消" style="display:none" type="button">
                            <span id="couponNumberTips" class="corg coupwarn_tips"></span>
                        <div class="coupon_btn" jtype="coupon"></div>
                    </dd>
                </dl>
                    <dl class="mt25 clearfix">
                        <dt class="fl">使用礼品卡：</dt>
                        <dd class="rel fl">
                            <input id="giftcardNumber" class="sinput cgray" style="width:170px;padding-left:25px;" maxlength="16" placeholder="请输入线下礼品卡编码" type="text">

                            <div class="coupon_list none" id="giftcard_list" style="position:absolute;top:25px;left:0px; cursor:default"></div>
                                <span id="giftcardNumberTips" class="corg coupwarn_tips"></span>
								<span class="corg coupwarn_tips tips618" style="width:455px;top: 50px;">输入密令"618"再减20元，去APP下单，抢100元现金礼包
                                    &nbsp;&nbsp;</span>
                                <input id="usegiftcardBtn" class="cart_normal_btn jsUseCoup ml5" value="确认" type="button">
                                <input class="cart_normal_btn jsCancelCoup" id="cancelUsegiftcardBtn" value="取消" style="display:none" type="button">
                            <div class="coupon_btn" jtype="giftcard"></div>
							<style>
								.tips618 {	
                                	display: none;		
                            	}
                            </style>		
                        </dd>
                    </dl>
                <div style="display: none;" class="cen invc_box mt15 none" id="invcBox">
                    <div class="hd"><input class="jsInvcChck" id="invc" name="isInvc" value="0" type="checkbox"> <label for="invc">我需要发票</label></div>
                    <div class="bd none">
                        <ul class="invc_cbox">
                            <li><span class="t">发票类型：</span>普通发票</li>
                            <li>
                                <span class="t"><i class="corg">*</i> 发票抬头：</span>

                                <div class="ttbox">
                                    <div class="ttbox_hd jsInvcTt clearfix">
                                        <div class="fl mr5">
                                            <input id="prsnl" name="invcTt" checked="checked" value="0" type="radio"><label for="prsnl" class="mr20">个人</label>
                                            <input id="unit" name="invcTt" value="1" type="radio"><label for="unit">单位</label>
                                            <input name="unitName" size="40" placeholder="请填写正确单位名称(将打印到发票上)" type="text">
                                        </div>
                                        <span class="error_tip fl">请输入单位名称</span>
                                    </div>
                                    <p class="c9 mt5">发票内容统一为“商品明细”，发票金额以实际支付金额为准；如需要开具增值税专用发票请联系客服办理；<br>如需办理退换货时请将原发票随货品一同寄回，否则无法办理退换货。
                                    </p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="ygtjzx_ttl_bd fr">
                <table class="ygtjzx_ttl_lrtbl">
                    <colgroup><col>
                    <col>
                    </colgroup><tbody>
                    <tr>
                        <th><strong>商品总金额：</strong></th>
                        <td><strong class="corg">359</strong></td>
                    </tr>
                    <tr>
                        <th>运费：</th>
                        <td><strong class="corg">15</strong></td>
                    </tr>
                        <tr class="cart_reduce_price">
                            <th>满99元在线支付免运费：</th>
                            <td><strong>-15</strong></td>
                        </tr>
                    <tr>
                        <th><em class="pr25">可获得优购积分：<i>359</i>点</em><strong>应付金额：</strong>
                        </th>
                        <td><strong class="c9 f16 pr5">¥</strong><strong class="f18 fmTahoma corg" id="u_buyAmount">359</strong>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="mt15 mr40">
            <div class="clearfix tright">
                <p class="fr">
                    <a class="cart_back_link cblue undline" href="">返回购物车</a>
                    <button class="cart_submitbtn ml10" type="submit" name="submi21t">提交订单</button>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="cen mt20">
    <div class="lwst_cdje mr10">

    </div>
</div>

	</div>
	
	<input id="limitCartAddProductMaxNum" value="100" type="hidden">
	<input id="higestPayDeliveryOrderAmount" value="3000" type="hidden">
	<!--确认订单信息end-->
	<div class="clearfix cen mt15">
	    <div class="fr cgray cart_btinfo ">
	        <p class="cart_bz">
	            购物保障：<i class="icon_zp"></i>100%正品
					<i class="icon_thh"></i>10天退换货
					<i class="icon_bcj"></i>10天补差价
	        </p>
	        <p class="cart_paytips"><b></b>请您在订单提交成功后1小时内完成支付，否则订单将被自动取消</p>
	    </div>
	</div>
	<div class="clear"></div>
	<div id="help_order_pop" style="display:none"></div>
	<div id="help_pay_pop" style="display:none"></div>
	<div id="help_coupon_pop" style="display:none"></div>

</form>
		<!-- 引入jquery类库 -->
		<script src="/home/js/jquery-1.8.3.min.js"></script>
		<!-- 引入三级联动js插件 -->
		<script src="/home/js/threeopen/province.js"></script>
		<script src="/home/js/threeopen/area.js"></script>
		<script>
			// dl悬浮事件
			$('#add_address').hover(function(){
				$(this).addClass('kms');
				$(this).find('dd').addClass('kls');
			},function(){
				$(this).removeClass('kms');
				$(this).find('dd').removeClass('kls');
			})

			$('.cen').find('dl').eq(0).siblings().css('margin-left','40px');

			$('.cen').find('dl').eq(1).click(function(){
				$('#addressPopDialog').fadeIn(300);

			})

			$('.uiClose').click(function(){
				$('#addressPopDialog').fadeOut(300);
			})

			$(document).ready(function() {
				showLocation();
			});

			// for (var i = 0; i < $('.uc_tab a').length; i++) {
				$('.uc_tab a').eq(0).click(function(){
					$(this).addClass('current');
					$(this).siblings().removeClass('current');
					$('#ucFavList').find('.jlf').eq(0).siblings().css('display','none');
					$('#ucFavList').find('.jlf').eq(0).css('display','block');
				})

				$('.uc_tab a').eq(1).click(function(){
					$(this).addClass('current');
					$(this).siblings().removeClass('current');
					$('#ucFavList').find('.jlf').eq(1).siblings().css('display','none');
					$('#ucFavList').find('.jlf').eq(1).css('display','block');
				})

				$('.uc_tab a').eq(2).click(function(){
					$(this).addClass('current');
					$(this).siblings().removeClass('current');
					$('#ucFavList').find('.jlf').eq(2).siblings().css('display','none');
					$('#ucFavList').find('.jlf').eq(2).css('display','block');
				})
			// };
			

		</script>
<div class="cen mt20 clearfix">
    <span class="fl cgray"> 购物过程中

    遇到任何问题，请联系在线客服
    <a title="在线咨询" onclick="javascript:NTKF.im_openInPageChat();" class="lnk-ntalker Blue">在线咨询</a> 
    </span>
    <span class="fr"> <a style="text-decoration:underline;" class="cgray" target="_blank" href="">帮助我们改进购物流程</a> </span>
</div>
<!--底部start-->
<div class="cgray cen footer">
<span class="pr10">
	Copyright © 2011-2014 Yougou Technology Co., Ltd. 
</span>
<span class="pr10">
	<a href="" target="_blank">粤ICP备09070608号-4</a>
</span>
<span class="pr10">
	增值电信业务经营许可证：<a href="" target="_blank">粤 B2-20090203</a>
</span>
</div>
	
		
</body>
</html>