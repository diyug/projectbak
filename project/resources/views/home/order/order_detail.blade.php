<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache,no-store, must-revalidate">
    <meta http-equiv="expires" content="0">
    <link href="/home/css/base-2.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="/home/css/base.css">
    <link href="/home/css/ucenter_v2.css" type="text/css" rel="stylesheet">
    <link rel="shortcut icon" href="/home/images/favicon.ico">
    <link rel="stylesheet" href="/home/css/shopcar_v4.0.css" />
    <title>订单详情</title>
    <style type="text/css">

    </style>
    
    <body class="myorder">
      <!-- 引入公共头部 -->@include('home_public/header')
      <!--引入宏-->
      <div class="blank07"></div>
      <div class="cen">
        <p class="curLct">您当前的位置：
          <a href="http://www.yougou.com/?t=14743640369206563" target="blank">首页</a>&gt;
          <a href="1">我的优购</a>&gt;
          <a href="">我的订单</a>&gt; 订单详情</p>
        <div class="u_leftxin u_leftxin fl mgr10" id="umenu">
          <div class="wdygtit">
            <a href="">
              <span>我的优购</span></a>
          </div>
          <ul class="jiaoyizx">
            <li class="ultit">交易中心</li>
            <li class="myorderli">
              <a href="">
                <span>我的订单</span</a>
            </li>
            <li class="myfavorli">
              <a href="">
                <span>我的收藏</span></a>
            </li>
            <li class="mycommentli">
              <a href="">
                <span>商品评论/晒单</span></a>
            </li>
            <li class="msgli">
              <a href="">
                <span id="uc_msg_count">站内消息
                  <i class="huise">（
                    <em>0</em>）</i></span>
              </a>
            </li>
          </ul>
          <ul class="wodezc">
            <li class="ultit">我的资产</li>
            <li class="mycouponli">
              <a href="">
                <span id="my_coupon_tick">我的优惠券</span></a>
            </li>
            <li class="giftcardli">
              <a href="">
                <span id="my_giftcard_tick">我的礼品卡</span></a>
            </li>
            <li class="mypointli">
              <a href="">
                <span id="my_point_tick">我的积分</span></a>
            </li>
          </ul>
          <ul class="gerensz">
            <li class="ultit">个人设置</li>
            <li class="myinfoli">
              <a href="">
                <span>个人资料</span></a>
            </li>
            <li class="safesetli">
              <a href="">
                <span id="uc_safe_level">安全设置</span></a>
            </li>
            <li class="myaddrli">
              <a href="">
                <span>我的收货地址</span></a>
            </li>
          </ul>
          <ul class="shouhoufw">
            <li class="ultit">售后服务</li>
            <li class="afterservli">
              <a href="">
                <span>查看退换货</span></a>
            </li>
            <li class="appservli">
              <a href="">
                <span>申请退换货</span></a>
            </li>
          </ul>
          <ul class="hotlist">
            <li class="hotlist_tit">
              <div>
                <p>24小时热销推荐</p>
              </div>
            </li>
            <li class="hotlist_dl">
              <dl>
                <dt>
                  <a href="" target="_blank">
                    <img src="" title="teenmix 天美意 女鞋 满帮鞋" alt="天美意 满帮鞋"></a>
                </dt>
                <dd>
                  <p class="hotgoodsname">
                    <a href="" target="_blank" title="teenmix 天美意 女鞋 满帮鞋">天美意 满帮鞋</a></p>
                  <p class="hotgoodsprice">
                    <span class="Red">￥458</span>
                    <span class="Hui">￥1038</span></p>
                </dd>
              </dl>
              <dl>
                <dt>
                  <a href="" target="_blank">
                    <img src="" title="tata 他她 女鞋 满帮鞋" alt="他她 满帮鞋"></a>
                </dt>
                <dd>
                  <p class="hotgoodsname">
                    <a href="" target="_blank" title="tata 他她 女鞋 满帮鞋">他她 满帮鞋</a></p>
                  <p class="hotgoodsprice">
                    <span class="Red">￥348</span>
                    <span class="Hui">￥1038</span></p>
                </dd>
              </dl>
              <dl>
                <dt>
                  <a href="" target="_blank">
                    <img src="" title="adidas 阿迪达斯 运动 夹克" alt="阿迪达斯 夹克"></a>
                </dt>
                <dd>
                  <p class="hotgoodsname">
                    <a href="" target="_blank" title="adidas 阿迪达斯 运动 夹克">阿迪达斯 夹克</a></p>
                  <p class="hotgoodsprice">
                    <span class="Red">￥358</span>
                    <span class="Hui">￥469</span></p>
                </dd>
              </dl>
              <dl>
                <dt>
                  <a href="" target="_blank">
                    <img src="" title="nike 耐克 运动 单肩包" alt="耐克 单肩包"></a>
                </dt>
                <dd>
                  <p class="hotgoodsname">
                    <a href="" target="_blank" title="nike 耐克 运动 单肩包">耐克 单肩包</a></p>
                  <p class="hotgoodsprice">
                    <span class="Red">￥84</span>
                    <span class="Hui">￥129</span></p>
                </dd>
              </dl>
              <dl>
                <dt>
                  <a href="" target="_blank">
                    <img src="" title="converse 匡威 运动 帆布鞋/硫化鞋" alt="匡威 帆布鞋/硫化鞋"></a>
                </dt>
                <dd>
                  <p class="hotgoodsname">
                    <a href="" target="_blank" title="converse 匡威 运动 帆布鞋/硫化鞋">匡威 帆布鞋/硫化鞋</a></p>
                  <p class="hotgoodsprice">
                    <span class="Red">￥269</span>
                    <span class="Hui">￥369</span></p>
                </dd>
              </dl>
            </li>
            <li class="last">
              <a class="hotrenovate" href="">换一批</a></li>
          </ul>
        </div>
        <!-- right content -->
        <div class="u_order_details fl" id="myorderdetail">
          <div class="u_order_details_top">
            <div class="u_order_details_top1 clearfix">
              <h3 class="Size14 fl">订单详情</h3>
              <p class="u_order_phoneIco fr">
                </p>
            </div>
            <div class="u_order_details_top2 relative">
            <p class="codecoloraf">该订单会为您保留24小时（从下单之日算起），1小时之后如果还未付款，系统将自动取消该订单。</p>
              <p  style="margin-top:20px">
                <span class="codecolor">订单号：
                  <em class="codecolor">{{$orderdata -> code}}</em></span>
                <span class="lotm">下单时间：{{date('Y-m-d H:i:s',$ordermsg->order_create_time)}}</span></p>
              <p class="blank8"></p>
              
              @if($ordermsg -> order_status==1)
              <div class="doact">
                <a class="buynow" href="">立即付款</a>
                <a class="buynow" href="">取消订单</a>
              </div>
              @endif
            @if($ordermsg->order_status==1)
              <div class="times">
                <h3 class="state">等待付款</h3>
                <span class="remain-time"><b></b>剩余18小时</span>
              </div>
            @endif
          </div>
          <div class="u_order_details_c mt10 relative">
            <h3 class="u_order_details_ct w100 Size14">订单跟踪</h3>

            <div class="dodetail">

                <div class="actdetail">

                  <em>处理详情：</em>
                  <span class="f_yellow detail_span">
                    <strong>优购上海仓</strong>
                  </span> 
                @if(empty($ordermsg -> order_payment_time))
                  <span class="ca">
                    <strong class="f_yellow">等待买家付款</strong>
                  </span>
                @endif

                </div>

                <div class="actdetails">
                      <div class="hcolor"><i class="node-icon"></i><span class="oleft">{{date('Y-m-d H:i:s',$ordermsg->order_create_time)}}&nbsp;您提交了订单</span></div>

                      @if(!empty($ordermsg -> order_payment_time))
                      <div class="hcolor"><i class="node-icon"></i><span class="oleft">{{date('Y-m-d H:i:s',$ordermsg->order_payment_time)}}&nbsp;支付成功</span></div>
                      <div class="hcolor"><i class="node-icon"></i><span class="oleft">{{date('Y-m-d H:i:s',$ordermsg->order_payment_time)}}&nbsp;商品出库</span></div>
                      <div class="noleft"><span class="noto">订单派送中......</span></div>
                      @endif
                </div>
                <div style="margin-top:50px"></div>
           </div>
           
          </div>
          <div class="u_order_details_c mt10 relative">
            <h3 class="u_order_details_ct w100 Size14">订单信息</h3>
            <div class="order_info">
              <div class="order_info1">
                <h5>收货人信息</h5>
                <p>{{$orderdata -> username}}&nbsp;&nbsp;{{$orderdata -> phone_number}}&nbsp;&nbsp;{{$orderdata -> address}}&nbsp;&nbsp;</p>
              </div>
              <div class="order_info1 u_pay_ps_info">
                <h5>支付及配送信息</h5>
                <p class="u_pay_ps">
                  <span>
                    <em class="Gray">支付方式：</em>在线支付</span>
                  <span>
                    <em class="Gray">配送方式：</em>普通快递</span>
                  <span>
                    <em class="Gray">配送时间：</em>工作日、双休日与假日均可送货</span></p>
              </div>
              <!--发票信息-->
              <!--发票信息结束-->
              <div class="order_info1 goodsList">
                <h5 class="goodsList_t">商品清单</h5>
                <!--子订单 start-->
                <p class="blank8"></p>
                <p class="order_no">

                  <span class="mr10">国内保税仓</span></p>
                <table class="uc_default_table uc_dtl_table">
                  <colgroup>
                    <col width="92">
                      <col width="325">
                        <col width="119">
                          <col width="72">
                            <col width="68">
                              <col width="84"></colgroup>
                  <thead>
                    <tr>
                      <th>商品名称</th>
                      <th>&nbsp;</th>
                      <th>服务</th>
                      <th>单价</th>
                      <th>数量</th>
                      <th>小计</th></tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td colspan="6">
                        <div class="uc_goods_item uc_dtl_goods_item clearfix">
                          <dl class="uc_goods_list clearfix">
                            <dt class="info1 relative">
                              <a href="" target="_blank">
                                <img src="{{$goodsmsg -> goodspic}}" title="{{$goodsmsg -> goodsdes}}" alt="{{$goodsmsg -> goodsdes}}" width="70" height="70" style="border:1px solid #EFEFEF"></a>
                            </dt>
                            <dd class="info2">
                              <p>
                                <a class="f_blue" href="" target="blank">{{$goodsmsg -> goodsname}}</a></p>
                              <p>
                              </p>
                              <p>
                                <em class="Gray">颜色：</em>
                                <em class="f_gray">黑色</em>&nbsp;&nbsp;
                                <em class="Gray">尺码：</em>
                                <em class="f_gray">90码</em></p>
                              <p>
                              </p>
                            </dd>
                            <dd class="info3">--</dd>
                            <dd class="info4">
                                {{$goodsmsg -> newprice}} 元
                              <p class="act_tips">
                                <span>
                                  <i></i>
                                </span>限时抢</p>
                            </dd>
                            <dd class="info5">X{{$ordermsg -> goods_num}}</dd>
                            <dd class="info7"> {{$ordermsg->goods_num * $goodsmsg -> newprice}} 元</dd></dl>
                        </div>
                        <!-- 加价购 --></td>
                    </tr>
                  </tbody>
                </table>
                <input value="" type="hidden">
                <input value="等待买家付款" type="hidden">
                <!--子订单 end--></div>
              <div class="pay_details pay_details_statis">
                <p class="bold">
                  <span>商品总金额：</span>
                  <em>
                    <strong>{{$ordermsg->goods_num * $goodsmsg -> newprice}}元</strong></em>
                </p>
                <p>
                  <span>运费：</span>
                  <em>15元</em></p>
                  <span>满99元免运费</span>
                  <em>-15元</em></p>
                <p class="bold">
                  <span>订单总金额：</span>
                  <em class="f_yellow Size16">{{$ordermsg->goods_num * $goodsmsg -> newprice}}元</em></p>
              </div>
              @if($ordermsg -> order_status==1)
                <p class="u_order_details_btn mt20 fr clearfix actor">
                <a class="buynow" href="/home/order/up-order-sta?uid={{$ordermsg->uid}}&oid={{$ordermsg->id}}">立即付款</a>
                <a class="buynow" href="">取消订单</a></p>
              @endif
              <p class="blank20"></p>
            </div>
          </div>
        </div>
        <!-- right content end --></div>
      <div class="blank10"></div>

    <!-- header头部三级菜单显示 -->
    <script type="text/javascript" src="/home/js/cate/cate.js"></script>
    <script type="text/javascript" src="/home/js/jquery-1.8.3.min.js"></script>

    <script type="text/javascript">
        $('.doact').find('a').eq(1).hover(function(){
          $(this).removeClass('buynow').addClass('donow');
        },function(){
          $(this).removeClass('donow').addClass('buynow');
        })

        $('.doact').find('a').eq(0).hover(function(){
          $(this).removeClass('buynow').addClass('donowp');
        },function(){
          $(this).removeClass('donowp').addClass('buynow');
        })

        $('.actor').find('a').eq(0).hover(function(){
           $(this).removeClass('buynow').addClass('donowp');
        },function(){
            $(this).removeClass('donowp').addClass('buynow');
        })

        $('.actor').find('a').eq(1).hover(function(){
           $(this).removeClass('buynow').addClass('donow');
        },function(){
            $(this).removeClass('donow').addClass('buynow');
        })

    </script>

  </body>
</html>
    <!-- 引入公共小尾部 -->
    @include('home_public.footer_small')