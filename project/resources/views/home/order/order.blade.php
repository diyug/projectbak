<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache,no-store, must-revalidate">
    <meta http-equiv="expires" content="0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>我的优购--我的订单</title>

    <link href="/home/css/base-2.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="/home/css/base.css">
    <link href="/home/css/ucenter_v2.css" type="text/css" rel="stylesheet">
    <link rel="shortcut icon" href="/home/images/favicon.ico">

    <style type="text/css">
      .bgred{ padding:2px 4px 0 4px; color:#FFFFFE; background:#f67649; }
      .indexnav2list .popmenu2 {height:478px; border:1px solid #d1d3d4; box-shadow:none; right:-175px }
    </style>

    <!-- <link type="text/css" rel="stylesheet" href="myorder_files/bdsstyle.css"></head> -->
  
  <body class="myorder">
    <iframe style="display: none;" frameborder="0"></iframe>
    <!--公共头部 start-->
    <!--header created time: 2016-09-18T18:02:54+08:00 -->
    <!--公共头部start-->
   @include('home_public.header')
    <!--//导航end-->
    <!--//公共头部end-->
    <input id="basePath" value="" type="hidden">
    <p class="blank10"></p>
    <p class="curLct">您当前的位置：
      <a href="" class="f_blue">首页</a>&gt;
      <a href="" class="f_blue">我的优购</a>&gt; 我的订单</p>
    <div class="cen clearfix">
      <div class="u_leftxin u_leftxin fl mgr10" id="umenu">
        <div class="wdygtit">
          <a href="">
            <span>我的优购</span></a>
        </div>
        <ul class="jiaoyizx">
          <li class="ultit">交易中心</li>
          <li class="myorderli">
            <a href="">
              <span>我的订单</span></a>
          </li>
          <li class="myfavorli">
            <a href="">
              <span>我的收藏</span></a>
          </li>
          <li class="mycommentli">
            <a href="">
              <span>商品评论/晒单</span></a>
          </li>
          <li class="msgli">
            <a href="">
              <span id="uc_msg_count">站内消息
                <i class="huise">（
                  <em>0</em>）</i></span>
            </a>
          </li>
        </ul>
        <ul class="wodezc">
          <li class="ultit">我的资产</li>
          <li class="mycouponli">
            <a href="">
              <span id="my_coupon_tick">我的优惠券</span></a>
          </li>
          <li class="giftcardli">
            <a href="">
              <span id="my_giftcard_tick">我的礼品卡</span></a>
          </li>
          <li class="mypointli">
            <a href="">
              <span id="my_point_tick">我的积分</span></a>
          </li>
        </ul>
        <ul class="gerensz">
          <li class="ultit">个人设置</li>
          <li class="myinfoli">
            <a href="">
              <span>个人资料</span></a>
          </li>
          <li class="safesetli">
            <a href="">
              <span id="uc_safe_level">安全设置</span></a>
          </li>
          <li class="myaddrli">
            <a href="">
              <span>我的收货地址</span></a>
          </li>
        </ul>
        <ul class="shouhoufw">
          <li class="ultit">售后服务</li>
          <li class="afterservli">
            <a href="">
              <span>查看退换货</span></a>
          </li>
          <li class="appservli">
            <a href="">
              <span>申请退换货</span></a>
          </li>
        </ul>
        <ul class="hotlist">
          <li class="hotlist_tit">
            <div>
              <p>24小时热销推荐</p>
            </div>
          </li>
          <li class="hotlist_dl">
            <dl>
              <dt>
                <a href="" target="_blank">
                  <img src="/home/order/100180914_01_s.jpg" title="adidas 阿迪达斯 运动 双肩包" alt="阿迪达斯 双肩包" onerror="'"></a>
              </dt>
              <dd>
                <p class="hotgoodsname">
                  <a href="" target="_blank" title="adidas 阿迪达斯 运动 双肩包">阿迪达斯 双肩包</a></p>
                <p class="hotgoodsprice">
                  <span class="Red">￥269</span>
                  <span class="Hui">￥269</span></p>
              </dd>
            </dl>
            <dl>
              <dt>
                <a href="" target="_blank">
                  <img src="/home/order/100195869_01_s.jpg" title="teenmix 天美意 女鞋 满帮鞋" alt="天美意 满帮鞋" onerror=""></a>
              </dt>
              <dd>
                <p class="hotgoodsname">
                  <a href="" target="_blank" title="teenmix 天美意 女鞋 满帮鞋">天美意 满帮鞋</a></p>
                <p class="hotgoodsprice">
                  <span class="Red">￥428</span>
                  <span class="Hui">￥1038</span></p>
              </dd>
            </dl>
            <dl>
              <dt>
                <a href="" target="_blank">
                  <img src="/home/order/100177558_01_s.jpg" title="tata 他她 女鞋 满帮鞋" alt="他她 满帮鞋" onerror=""></a>
              </dt>
              <dd>
                <p class="hotgoodsname">
                  <a href="" target="_blank" title="tata 他她 女鞋 满帮鞋">他她 满帮鞋</a></p>
                <p class="hotgoodsprice">
                  <span class="Red">￥358</span>
                  <span class="Hui">￥1038</span></p>
              </dd>
            </dl>
            <dl>
              <dt>
                <a href="" target="_blank">
                  <img src="/home/order/100362097_01_s.jpg" title="converse 匡威 运动 帆布鞋/硫化鞋" alt="匡威 帆布鞋/硫化鞋" onerror=""></a>
              </dt>
              <dd>
                <p class="hotgoodsname">
                  <a href="" target="_blank" title="converse 匡威 运动 帆布鞋/硫化鞋">匡威 帆布鞋/硫化鞋</a></p>
                <p class="hotgoodsprice">
                  <span class="Red">￥229</span>
                  <span class="Hui">￥369</span></p>
              </dd>
            </dl>
            <dl>
              <dt>
                <a href="" target="_blank">
                  <img src="/home/order/100076718_01_s.jpg" title="adidas 阿迪达斯 运动 夹克" alt="阿迪达斯 夹克" onerror=""></a>
              </dt>
              <dd>
                <p class="hotgoodsname">
                  <a href="" target="_blank" title="adidas 阿迪达斯 运动 夹克">阿迪达斯 夹克</a></p>
                <p class="hotgoodsprice">
                  <span class="Red">￥358</span>
                  <span class="Hui">￥469</span></p>
              </dd>
            </dl>
          </li>
          <li class="last">
            <a class="hotrenovate" href="">换一批</a></li>
        </ul>
      </div>
      <!--左侧公共部分 end-->
      <div id="myorder" class="uc_right fr">

        <form action="/home/order/index" method="post">
          <div class="uc_orderfilter">
            <h2 class="uc_odrfiltertit">我的订单</h2>
            <div class="uc_orderselect">
              <p class="uc_fll">显示：</p>
              <div class="uc_selectsty uc_select_6m">
                <input disabled="disabled" class="uc_selchecked" value="6个月内订单" id="orderCreateTime" type="text">
                <ul class="uc_selectpop" style="display: none;">
                  <input class="orderfilterKV" name="orderCreateTime" value="" type="hidden">
                  <li value="" selected="selected">6个月内订单</li>
                  <li value="2016">今年内其他订单</li>
                  <li value="2015">2015</li></ul>
                <p class="dropdownbtn dropdowntgt">
                  <span class="dropdowntgt"></span>
                </p>
              </div>
              <div class="uc_selectsty uc_select_stat">
                <input disabled="disabled" class="uc_selchecked" value="所有状态" id="status" type="text">
                <ul class="uc_selectpop" style="display: none;">
                  <input class="orderfilterKV" name="status" value="0" type="hidden">
                  <li value="0">所有状态</li>
                  <li value="1">订单生成</li>
                  <li value="2">配货中</li>
                  <li value="3">已发货</li>
                  <li value="4">已取消</li></ul>
                <p class="dropdownbtn dropdowntgt">
                  <span class="dropdowntgt"></span>
                </p>
              </div>
              <div class="uc_selectsty uc_select_paystat">
                <input disabled="disabled" class="uc_selchecked" value="支付状态" id="payStatus" type="text">
                <ul class="uc_selectpop" style="display: none;">
                  <input class="orderfilterKV" name="payStatus" value="0" type="hidden">
                  <li value="0">支付状态</li>
                  <li value="1">已支付</li>
                  <li value="2">未支付</li></ul>
                <p class="dropdownbtn dropdowntgt">
                  <span class="dropdowntgt"></span>
                </p>
              </div>
              <a class="uc_ordersearchbtn">搜索订单</a>

              <div class="uc_ordersearch">

                <input ="text" name="searchordercode" placeholder="订单号" type>

              </div>

              <div id="canceledOrder" style="">
                <input id="canceledSearch" name="canceledSearch" value="1" type="checkbox">已取消</div></div>
          </div>
          <input value="14742087482808667" name="t" type="hidden">
        </form>

<!-------------------------------------------------------------------------------->
@if(!empty($user_orderdata))
      @foreach($user_orderdata as $key => $value)
        <!--我最近的订单 start-->
        <div class="zjorder">

          <form name="buyFormObj0" method="post" target="_blank">

          <!-- 订单号隐藏域 -->
            <input name="orderNo" value="{{$value -> order_code}}" type="hidden">
            <input name="orderRouteWay" value="user" type="hidden">
          </form>

          <!--主订单1 start-->
          <table class="zjorderbody2" cellpadding="0" cellspacing="1" width="200">
            <colgroup>
              <col width="571">
                <col width="106">
                  <col width="153"></colgroup>
            <thead>
              <tr>
                <th colspan="3" class="order-msg">
                  <span class="codec">{{date('Y-m-d H:i:s',$value -> order_create_time)}}</span>
                  <span><font class="codec">订单号：</font>{{$value -> order_code}}</span>
                  
                  <span>
                    <font class="Gray">(满99元免运费)</font></span>
                    <a href="/home/order/order-details?uid={{$value -> uid}}&oid={{$value -> id}}" class="hoverorderdetails" id="trues" target="_blank">订单详情</a>
                    @if($value -> order_status==1)
                      <a href="/home/order/removeOrder" class="removeorder recyclecon">取消订单</a>
                    @else
                      <a href="/home/order/removeOrder" class="replacenow recyclecon">退货</a>
                      <a href="/home/order/removeOrder" class="norenow recyclecon">换货</a>
                    @endif
                </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <!--子订单1 start-->
                  <div class="uc_goods_item uc_myorder_item relative clearfix">

                    <div class="uc_goods_lt clearfix ">
                      <dl class="uc_goods_list relative clearfix">
                        <dt class="info1 relative">
                          <a href="" target="_blank">
                            <img src="{{$value -> goods_pic}}" alt="{{$value -> goods_name}}" title="{{$value -> goods_name}}" height="80" width="80" style="border:1px solid #EFEFEF"></a>
                        </dt>

                        <dd class="info2">
                            <p>
                              <a href="" target="_blank" class="f_blue">{{$value -> goods_name}}</a>
                              <span class="codec">X{{$value -> goods_num}}</span>
                            
                            </p>

                          <p>
                          <em class="Gray">颜色：</em>
                            <em class="f_gray">{{$value->goods_color}}</em>&nbsp;&nbsp;
                            <em class="Gray">尺码：</em>
                            <em class="f_gray">{{$value->goods_size}}</em>
                          </p>
                        </dd>

                        <dd class="info3">
                          <p>
                            <strong>¥</strong>{{$value -> newUnlinePrice}}.00
                          </p>
                        </dd>

                        <dd class="info5 clearfix"></dd>
                        <dd class="infoReturn"></dd>
                      </dl>
                    </div>
                  </div>
                </td>
                <td>
                  <div class="goodsmn">
                    <p class="mncount">总额{{$value -> newUnlinePrice * $value -> goods_num}}.00元</p>
                    <p class="hui"></p>
                    <p class="hui">( {{showOrderStatus($value -> order_status)}} )</p></div>
                </td>
                <td>
                  <div class="pingjia">

                    @if($value -> order_status==1)
                      <a class="pingjaisub payments" href="">立即支付</a>
                    @elseif($value -> order_status==2)
                      <a class="pingjaisub payments" href="">确认收货</a>
                    @elseif($value -> order_status==3)
                      <a class="pingjaisub payments" href="">评论</a>
                    @endif

                    @if($value -> order_status==1)
                      <p class="hui tip">请尽快付款
                      <br>否则
                      <i class="hei">
                        <span class="timeOuts">{{$value -> order_setout}}</span>&nbsp;<i class="minutes">分钟</i></i>后订单将取消</p>
                        <input type="hidden" value="{{$value -> order_create_time}}" class="timec"/>
                        <input type="hidden" value="{{$value -> id}}" class="oids"/>
                    @elseif($value -> order_setout<=0)
                      <span class="mncount">订单已取消</span>
                    @endif
                    <p class="placec">
                      
                    </p>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
          <!--主订单1 end-->

        <p class="blank10"></p>
        <div id="paginator" class="paginator">
        @endforeach
        <div style="float:left;">总记录数：
        <font color="red">{{count($user_orderdata)}}</font></div>
        <div class="page">
        </div>
      </div>
              <!--我最近的订单 end-->
      </div>
    </div>
        @else if

    <table class="uc_default_table uc_myorder_table">
        <thead><tr><th class="orderth llb lrb" colspan="2"></th></tr></thead>
        <tbody>
          <tr>
            <td colspan="2">
              <p class="norecords">
                  您最近没购买过任何商品，先去<a href="/home/goodslist" class="f_blue">挑选商品</a>吧！~
              </p>
            </td>
          </tr>
        </tbody>
      </table>
  @endif

    <script type="text/javascript">

      $('.payments').hover(function(){
        $(this).addClass('buynow').removeClass('pingjaisub');
      },function(){
        $(this).removeClass('buynow').addClass('pingjaisub');
      })

      $('.order-msg').find('a').eq(1).hover(function(){
        $(this).removeClass('removeorder').addClass('removenow');
      },function(){
        $(this).removeClass('removenow').addClass('removeorder');
      })
      $('.order-msg').find('a').eq(0).hover(function(){
        $(this).removeClass('hoverorderdetails').addClass('orderdetails');
      },function(){
        $(this).removeClass('orderdetails').addClass('hoverorderdetails');;
      })

      // 获取倒计时节点元素
      var timer = $('.timeOuts');
      var ints = 1;

      // ajax的CSRF保护
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

      // 定时器
      setInterval(function(){
        // 循环操作
        for (var i = 0; i < timer.length; i++) {

          // 获取节点值
          var fire = parseInt(timer.eq(i).text());
          var oid = parseInt(timer.eq(i).parent().parent().next().next().val());

          $.post('/home/order/cancel-setout',{id:oid,setout:fire},function(data){

          },'json')

          // 判断是否为0
          if(fire<=0) {
            // 发送ajax
            $.post('/home/order/cancel-order',{id:oid},function(data){

            },'json')
          }

            // 进行减减运算
            --fire;

            // 写入值
            timer.eq(i).text(fire);

        };
        
      },60000)

      $('.info2 p').eq(1).css('margin-top','13px');

      // 订单表格头鼠标悬浮事件
      $('.order-msg').hover(function(){

        $(this).find('.removeorder').eq(0).show();

      },function(){

        $(this).find('.removeorder').eq(0).hide();

      })

      // 删除订单按钮点击事件
      $('.recyclecon').click(function(){
        var wrther = confirm('确认要取消该订单吗?');

        if(!wrther) {
          return false;
        }
      })


    </script>
    <script type="text/javascript" src="/home/js/cate/cate.js"></script>

    <p class="blank20"></p>
    </html>

    @include('home_public.footer_small')