<!DOCTYPE html>
<html><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache,no-store, must-revalidate">
<meta http-equiv="expires" content="0">
<meta name="csrf-token" content="{{ csrf_token() }}">
@section('title')
<title>购物车liebiao</title>
@show
<link href="/home/css/uc.css" type="text/css" rel="stylesheet">
<link href="/home/css/base.css" type="text/css" rel="stylesheet">
<link href="/home/css/shopcar_v4.css" type="text/css" rel="stylesheet">
<script src="cart_files/v.htm" charset="utf-8"></script><script src="cart_files/hm.txt"></script>

<link href="/home/css/ygdialog.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
    var basePath = "";
    var shoppingProcess=true;
</script>

<!-- 登录框 -->
<style type="text/css">
  ul li a{padding:0px;margin:0px;}
  ul {list-style:none;} 
  .title li{float:right;width:120px;height:40px;font-size:18px;text-align:center;line-height:40px;margin-top:25px;margin-right:40px;}
  .inter{width:230px;height:40px;border:solid 1px grey;margin-left:50px;margin-top:30px;}
   .inter input{width:160px;height:40px;border:none;} 
   .close{color:black;font-size:15px;float:right;}
</style>

</head>
<body>
<!--
-->
    <!--header start-->
    <!-- top nav bar created time: 2014-11-28 18:34:02-->
<div id="top_nav">
    <div class="view_area clearfix">
        <div class="yg link_box"><a href="http://www.yougou.com/#ref=all&amp;po=logo_yougou">时尚商城</a></div>
        
        <div class="phone link_box">
            <a href="http://www.yougou.com/topics/mobile.html" class="phone_text"><i class="mobile_ico"></i>手机优购<i class="tip"></i></a>
            <div class="phone_con">
                    <p class="clearfix">
                        <span class="fl qr_code">
                        </span>
                        <span class="fl ml10">
                            <a class="btn_app_store btn" href="http://itunes.apple.com/cn/app/zhang-shang-you-gou/id504493912?mt=8" target="_blank">App Store</a>
                            <a class="btn_android_store btn" href="http://mobile.yougou.com/appVersion/package.sc?channelCode=YgYougouwebA59" target="_blank">Android</a>
                        </span>
                    </p>
                    <p class="qr_code_tip">下载安装 <strong>优购客户端</strong></p>
                </div>
        </div>
        <div class="outlets link_box" style="border-right:none"></div>
        <div class="fr">
        @if(session('id'))
        <div class="about_user">您好！<span id="user_id">{{session('username')}}</span><a class="quit" href="/home/User/logout">退出</a></div><!--about_user end -->
        @else
        <div class="about_user"><span id="user_id"><a href="/home/User/login">登录</a></span><a class="quit" href="/home/User/register">注册</a></div><!--about_user end -->
        @endif
        <div class="my_yg link_box">
            <a href="http://www.yougou.com/my/ucindex.jhtml?t=14738211617475593" class="a1">我的优购</a>
            <ul class="info_con">
                <li><a href="http://www.yougou.com/my/favorites.jhtml?t=147382116174810428">我的收藏</a></li>
                <li style="display: list-item;" id="commentcount"><a href="http://www.yougou.com/my/comment.jhtml?t=14738211622166423">等待点评(<i class="corg">0</i>)</a></li>
                <li style="display: list-item;" id="top_msg"><a href="http://www.yougou.com/my/msg.jhtml?t=14738211620485898">站内消息(<i class="corg">0</i>)</a></li>
            </ul>
        </div>
        <div class="my_order link_box"><a href="http://www.yougou.com/my/order.jhtml?t=14738211617487175" rel="nofollow">我的订单</a></div>
        <div class="notice link_box">
            <span class="notice_text">公告</span>
            <ul class="notice_con">
                    <li><a target="_blank" href="http://www.yougou.com/topics/1416561897997.html#ref=index&amp;po=notice_notice1">运动新风尚 新品5折起</a></li>
                    <li><a target="_blank" href="http://www.yougou.com/topics/1415597386968.html#ref=index&amp;po=notice_notice2">摩登男装 秋冬大促 1折起</a></li>
                    <li><a target="_blank" href="http://www.yougou.com/topics/1415587130097.html#ref=index&amp;po=notice_notice3">潮靴秀美腿 价比11.11</a></li>
                    <li><a target="_blank" href="http://www.yougou.com/topics/1415605960629.html#ref=index&amp;po=notice_notice4">女装初冬热促 爆款2折起</a></li>
                    <li><a target="_blank" href="http://www.yougou.com/article/201411/87dc5ccf633611e4b7eea30f61b97b3f.shtml#ref=index&amp;po=notice_notice5">库房发货时间调整说明</a></li>
            </ul>
        </div>
        <div class="more link_box">
            <a href="javascript:;" class="more_text">更多</a>
            <ul class="more_con">
                <li><a onclick="YouGou.Biz.WebToolkit.addFavorite();" href="javascript:;">收藏优购</a></li>
                <li><a href="http://www.yougou.com/help/help.html">帮助</a></li>
            </ul>
        </div>
        </div>
    </div><!--view_area end -->
</div><!--top_nav end -->

    <!--header end-->
    
    <div class="cen cart_header">
        <h1 class="logo"><a href="http://www.yougou.com/" title=""><img src="/home/images/logo_n.jpg" height="50px" 2alt=""></a></h1>
        <div class="fr cart_step mt20">
            <ul>
                <li class="current">我的购物车<em></em><i></i></li>
                <li>提交订单中心<em></em><i></i></li>
                <li>成功提交订单<em></em><i></i></li>
            </ul>
        </div>
    </div>
     <!--=================提示购物车登录成功还是失败 ================= -->
     <script type="text/javascript">
      @if (session('success'))
                alert('恭喜你登录成功!!!');
          @endif
          @if (session('error'))
                alert('很遗憾登录失败!!!');
          @endif  
     </script>      
    <!--===================== 当购物车为空时 =============================-->
@if(empty($res))
<!-- 链接bootstrop -->
<link rel="stylesheet" href="/home/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="/home/bootstrap/css/bootstrap-theme.min.css">
<script type="text/javascript" src="/home/bootstrap/js/jquery.js"></script>
<script type="text/javascript" src="/home/bootstrap/js/bootstrap.min.js"></script>
<!-- 结束bootstrop -->
    <div id="shoppingCartContainerCNY0">
        <div class="cen mt10 cart_null_div" style="height:300px;">
            <div class="container">
           <img src="/home/cartpic/4c5e8a46045b2031cdc42d6130bba5a4.jpg" style="float:right;margin-right:300px;">
           <a href="/home/index" class="btn btn-danger btn-lg" style="margin-top:50px;width:150px;margin-left:50px;">返回首页</a>
            </div>
        </div>
    </div>
@endif

    <!--================ 当购物车不为空时 ============================-->
@if(!empty($res))

    <span class="none">
        <input id="addProductMaxNum" value="2" type="hidden">
        <input id="limitCartAddProductMaxNum" value="100" type="hidden">
        <input id="orderNum" value="0" type="hidden">
        <input id="isLock" value="false" type="hidden">
        <input id="limitOrderNum" value="40" type="hidden">
        <input id="limitOrderCommBuyMaxNum" value="40" type="hidden">
        <input id="isKrwFreeDuty" value="false" type="hidden">
    </span>
    

<!-- 若所有购物车均为空提示一下内容   -->
    <!-- 普通购物车 -->
        <div id="shoppingCartContainerCNY1">
    @section('content')
<div class="cen cart_gray_box mt20" id ='ddiv'>
    <table class="cart_list_tb">
        <colgroup>
            <col width="60">
            <col width="370">
            <col width="110">
            <col width="110">
            <col width="110">
            <col width="110">
            <col width="120">
        </colgroup>
        <thead>
        <tr>
            <th>
                    
            </th>
            <th>
                <span class="pr25">商品名称</span>
            </th>
            <th>颜色尺码</th>
            <th>单价</th>
            <th>数量</th>
            <th>小计(元)</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td id="tr_td" colspan="7">
                <input value="1" id="productRecordNum" type="hidden">
                <input value="0" id="productGiftRecordNum" type="hidden">
                <input value="2" id="orderCommNum" type="hidden">


   

@foreach($res as $k=>$v)
<div id="shopping_cart0" class="shopping_cart_tr ">
    <div class="shpcrt_blank10 clearfix"></div>
    <dl class="clearfix">
        <input value="2016-09-14 10:45:45" id="addTime" type="hidden">
        <dd class="fl rel col_chkbox">
            <input type="checkbox" name="check[]" class="chbox">
        </dd>
        <dt class="col_1 fl rel">
            <a target="_blank" href="http://www.yougou.com/c-adidas/sku-brn80-100481252.shtml">
                <img onerror="this.src='/images/common/160x160.jpg'" src="{{$v['into']->goodspic}}" class="thumImg" height="60" width="60">
                
            </a>
        </dt>
        <dd class="col_2 fl rel">
            <a target="_blank" href="http://www.yougou.com/c-adidas/sku-brn80-100481252.shtml" title="adidas阿迪达斯2016年新款男子运动基础系列针织外套AY3757" class="shopLink">
                <div style="padding-left:50px;">{{$v['into']->goodsname}} {{$v['into']->goodsdes}}</div>
            </a>

        </dd>
        <dd class="col_3 fl">
            <div class="pl20">
                <p class="cgray tleft">颜色：<em class="f_gray">{{$v['color']}}</em></p>

                <p class="cgray tleft">尺码：<em class="f_gray">{{$v['size']}}</em></p>
            </div>
        </dd>
            <dd class="col_4 fl rel ">
                {{$v['into']->newprice}}

                       
            </dd>
            <dd class="col_5 fl rel">
                    <a class="goodsSub fl" proindex="1" href="javascript:void(0);">-</a>
                        <input id="selectNum_1" proindex="1" class="goodsTxt fl" value="{{$v['num']}}" name="goodsCount" type="text">
                    <a class="goodsPlus fl" proindex="1" href="javascript:void(0);">+</a>
                    <!-- 获取购物车商品id -->
                    @if(session('id'))
                    <input type="hidden" value="{{$v['id']}}" name="gouid" class="gid">
                    @endif
                <input id="productId_1" value="f8fae88d16da9e59583d24f86a82087f__m" type="hidden">
                <input id="productNo_1" value="100481252005" type="hidden">
                <input id="minCanBuyNum_1" value="1" type="hidden">
                <input id="maxCanBuyNum_1" value="2147483647" type="hidden">
                
                <input name="oldNum_100481252005" id="oldNum_1" value="2" type="hidden">
            </dd>
            <dd class="col_6 fl"><strong class="corg">{{$v['num']*$v['into']->newprice}}</strong></dd>
            <dd class="col_7 fl">
                <a class="JsFavorite" pid="c5bc3c268c8d4509846068d5a9670170" psize="XL" prono="100481252005" spid="f8fae88d16da9e59583d24f86a82087f__m" href="javascript:void(0)">移入收藏夹</a>
                <br>
                <input type="hidden" value="{{$v['sid']}}" name="shangpin">
                <input type="hidden" value="{{$k}}" name="xiabiao">
                <!-- <a href="javascript:void(0)" prono="100481252005" class="JsRemoveGood JsDel" pid="f8fae88d16da9e59583d24f86a82087f__m">删除</a> -->
                <button class="JsRemoveGood JsDel">删除</button>
            </dd>
    </dl>

    <div class="shpcrt_blank10 clearfix"></div>
</div>
 @endforeach

            </td>
        </tr>
        </tbody>
    </table>
    <ul class="shpcrt_operations">
        <li>
                <!-- <label tradecurrency="CNY" name="CNYcheckAllLabel" class="ygChkbox">
                    <input class="none" id="CNYcheckEndAll" name="CNYcheckAll" checked="checked" type="checkbox"> -->
                    <i class="skin fl mt5 ml20 checked"></i>
                     <button class="all">全选</button>&nbsp;&nbsp;&nbsp;&nbsp;
                     <button class="not">全不选</button>
                <!-- </label> -->
        </li>
        <li>
            <a class="JsFavorite" pid="all" href="javascript:;">移入收藏夹</a>
        </li>
        
    </ul>


    <div class="shpcrt_opt clearfix">
        <div class="opt_bd fr">
            <table class="ygtjzx_ttl_lrtbl cart_reduce_price">
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="cen cart_gray_box_ft">
    <a href= "/home/index"
    
     class="cart_goon_link mt5 ml20 fl">继续购物</a>
        <div class="fl ml15 mt15">
            <button href="" class="cblue ml20 clearShopcart" tradecurrency="CNY">清空购物车</button>
        </div>
    <div class="fr">
        <strong>总计（不含运费
            ）</strong>：<strong class="cgray f16">¥</strong><strong class="corg f20">0</strong>
        <button class="cart_b_paybtn ml10 ">去结算</button>
    </div>
</div>
@endif
<!--//最低成单金额start-->
<!--//最低成单金额end-->
<!-- =====================登录框========================= -->
    <div class="window" style="display:none;position:absolute;left:0px;top:0px;width:100%;height:100%;background:rgba(0,0,0,0.3);width:100%;height:100%;z-index:999;">
        <div style="background:rgba(255,255,255,1);width:370px;height:470px;margin-left:500px;margin-top:100px;height:350px;position:absolute;border:solid 4px grey;">
            <ul>
                <li><button class='close'>关闭 X</button></li>
                <li>你尚未登录</li>
            </ul>
            <div style="clear:both;"></div>
            <ul class="title">
                <li style="background:#f3f3f3;color:block;"><a href="/home/User/register" style="cursor:pointer;">注&nbsp;册</a></li>
                <li style="background:#333333;color:#ffffff;">登&nbsp;录</li>
            </ul>
            <form method="post" action="/home/cart/login">
            {{csrf_field()}}
           
            <div class="inter" style="margin-top:90px;"><img src="/home/cartpic/2016-09-20_142152.png"><input type="text" name="account"></div>
            <div class="inter"><img src="/home/cartpic/2016-09-20_142207.png"><input type="password" name="password"></div>
            <div class="inter" style="background:#e1373a;border:none;"><input type="submit" value="点&nbsp;击&nbsp;登&nbsp;录" style="background:#e1373a;font-size:19px;font-family:Microsoft YaHei;margin-left:18px;color:#ffffff;"></div>
            </form>
        </div>

    </div>
<!-- =====================登录框结束========================= -->

<!--//最大成单金额start-->
<!--//最大成单金额end-->
  @show  
        </div>
    <!-- 首尔站购物车   -->
      
    
        <div id="shoppingCartContainerKRW_ZF"></div>
    
        <div id="shoppingCartContainerHKD"></div>
<script type="text/javascript">
    checkProductNum();
    function checkProductNum(){
        var addProductNum=$('#addProductNum').val();
        var limitCartAddProductMaxNum=$('#limitCartAddProductMaxNum').val();
        if(addProductNum!='' && parseInt(addProductNum)>parseInt(limitCartAddProductMaxNum) && 0 != parseInt(limitCartAddProductMaxNum)){
            //alert("购物车最多存放"+limitCartAddProductMaxNum+"件商品，请编辑购物车!");
            //return false;
            ygDialog({ 
                title:'提示',
                content:'<br/><br/><h2 class="Red"><center>购物车最多存放'+limitCartAddProductMaxNum+'件商品，请编辑购物车!</center></h2>',
                width:300,
                height:120,
                lock:true
            });
        }
    }
 </script>      
    <div class="clearfix cen mt15">
        <div class="fr cgray cart_btinfo">
            <p class="cart_bz">
                购物保障：<i class="icon_zp"></i>100%正品<i class="icon_thh"></i>10天退换货<i class="icon_bcj"></i>10天补差价
            </p>
            <p>优惠券/礼品卡在下一步使用</p>
            <p><a href="http://www.yougou.com/shoppingcart/shoppingcart_advise.jhtml" target="_blank" class="cgray">帮助我们改进购物流程</a></p>
        </div>
    </div>
    
    <!--凑单提醒start-->
    <div class="cen uc_main mt30">
        <h3 class="f16 c6 f_yahei"></h3>
        <div class="cart_recpro">
            <div class="uc_slide uc_cnlike rel zdtx">
                <div class="slide_hd"><h2></h2></div>
                <div class="slide_bd">
                    <a class="slide_lf abs" href="javascript:void(0);"></a>
                    <a class="slide_rt abs" href="javascript:void(0);"></a>
                    <p class="slide_page abs"></p>
                    <div class="slide_bd_c rel">
                        <ul class="uc_hpro_lst abs clearfix">
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--凑单提醒end-->
    <!--加价购推荐 start-->
    <div class="cen uc_main mt30">
        <h3 class="f16 c6 f_yahei"></h3>
        <div class="cart_recpro">
            <div class="uc_slide uc_cnlike rel jjg">
                <div class="slide_hd"><h2></h2></div>
                <div class="slide_bd">
                    <a class="slide_lf abs" href="javascript:void(0);"></a>
                    <a class="slide_rt abs" href="javascript:void(0);"></a>
                    <p class="slide_page abs"></p>
                    <div class="slide_bd_c rel">
                        <ul class="uc_hpro_lst abs clearfix">
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--//加价购推荐end>
    <!--//再买就满足活动 start--> 

    <div class="cen uc_main mt30">
        <h3 class="f16 c6 f_yahei"></h3>
            <div class="cart_recpro">
                <div class="uc_slide uc_cnlike rel zmjmz0">
                    <input class="loa" value="0" type="hidden">
                    <div class="slide_hd"><h2></h2></div>
                    <div class="slide_bd">
                        <a class="slide_lf abs" href="javascript:void(0);"></a>
                        <a class="slide_rt abs" href="javascript:void(0);"></a>
                        <p class="slide_page abs"></p>
                        <div class="slide_bd_c rel">
                            <ul class="uc_hpro_lst abs clearfix">
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
     <div class="cen uc_main mt30">
        <h3 class="f16 c6 f_yahei"></h3>
            <div class="cart_recpro">
                <div class="uc_slide uc_cnlike rel zmjmz1">
                    <input class="loa" value="0" type="hidden">
                    <div class="slide_hd"><h2></h2></div>
                    <div class="slide_bd">
                        <a class="slide_lf abs" href="javascript:void(0);"></a>
                        <a class="slide_rt abs" href="javascript:void(0);"></a>
                        <p class="slide_page abs"></p>
                        <div class="slide_bd_c rel">
                            <ul class="uc_hpro_lst abs clearfix">
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
     <div class="cen uc_main mt30">
        <h3 class="f16 c6 f_yahei"></h3>
            <div class="cart_recpro">
                <div class="uc_slide uc_cnlike rel zmjmz2">
                    <input class="loa" value="0" type="hidden">
                    <div class="slide_hd"><h2></h2></div>
                    <div class="slide_bd">
                        <a class="slide_lf abs" href="javascript:void(0);"></a>
                        <a class="slide_rt abs" href="javascript:void(0);"></a>
                        <p class="slide_page abs"></p>
                        <div class="slide_bd_c rel">
                            <ul class="uc_hpro_lst abs clearfix">
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
     <div class="cen uc_main mt30">
        <h3 class="f16 c6 f_yahei"></h3>
            <div class="cart_recpro">
                <div class="uc_slide uc_cnlike rel zmjmz3">
                    <input class="loa" value="0" type="hidden">
                    <div class="slide_hd"><h2></h2></div>
                    <div class="slide_bd">
                        <a class="slide_lf abs" href="javascript:void(0);"></a>
                        <a class="slide_rt abs" href="javascript:void(0);"></a>
                        <p class="slide_page abs"></p>
                        <div class="slide_bd_c rel">
                            <ul class="uc_hpro_lst abs clearfix">
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
     <div class="cen uc_main mt30">
        <h3 class="f16 c6 f_yahei"></h3>
            <div class="cart_recpro">
                <div class="uc_slide uc_cnlike rel zmjmz4">
                    <input class="loa" value="0" type="hidden">
                    <div class="slide_hd"><h2></h2></div>
                    <div class="slide_bd">
                        <a class="slide_lf abs" href="javascript:void(0);"></a>
                        <a class="slide_rt abs" href="javascript:void(0);"></a>
                        <p class="slide_page abs"></p>
                        <div class="slide_bd_c rel">
                            <ul class="uc_hpro_lst abs clearfix">
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!--//再买就满足活动end>   
    
   
    <!--//您最近收藏的商品start-->
         <div class="cen uc_main mt30">
             <h3 class="f16 c6 f_yahei"></h3>
                 <div class="cart_recpro">
                     <div class="uc_slide uc_cnlike rel zjsc">
                         <div class="slide_hd"><h2></h2></div>
                         <div class="slide_bd">
                             <a class="slide_lf abs" href="javascript:void(0);"></a>
                             <a class="slide_rt abs" href="javascript:void(0);"></a>
                             <p class="slide_page abs"></p>
                             <div class="slide_bd_c rel">
                                 <ul class="uc_hpro_lst abs clearfix">
                                 </ul>
                         </div>
                     </div>
                 </div>
             </div>
        </div>

    <!--//您最近收藏的商品end-->    
    
    <!--最近浏览过的商品startt-->
    
    <!--//最近浏览过的商品end-->
    
    <!--底部start-->
    <div class="cgray cen footer">
            <span class="pr10">
                Copyright © 2011-2014 Yougou Technology Co., Ltd. 
            </span>
            <span class="pr10">
                <a href="http://www.miibeian.gov.cn/" target="_blank">粤ICP备09070608号-4</a>
            </span>
            <span class="pr10">
                增值电信业务经营许可证：<a href="http://www.miibeian.gov.cn/" target="_blank">粤 B2-20090203</a>
            </span>
    </div>
    <!--//底部end-->
<!--//问卷调查-->   


<script type="text/javascript">
    $(function(){
        getActiveRecommendList();
        getSimilarRecommendList();
        //16-03-29 轮播页
        var $ul = $('.ul1');
        var $li = $ul.find('li');
        $ul.css('width',$li.outerWidth()*$li.length);
        
        uc.proLfSlide(".zjcs");
        uc.proLfSlide(".zjll");
        uc.proLfSlide(".cdsp");
        uc.proLfSlide(".cjhd",9,9);
        uc.proLfSlide(".mjhd",9,9);     
        uc.proLfSlide(".gwcgm",5,5);
        
    });
</script>
    
<script type="text/javascript">
    //google
 var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23566531-1']);
  _gaq.push(['_setDomainName', '.yougou.com']);
  _gaq.push(['_addOrganic', 'baidu', 'word']);
  _gaq.push(['_addOrganic', 'soso', 'w']);
  _gaq.push(['_addOrganic', '3721', 'name']);
  _gaq.push(['_addOrganic', 'yodao', 'q']);
  _gaq.push(['_addOrganic', 'vnet', 'kw']);
  _gaq.push(['_addOrganic', 'sogou', 'query']);
  _gaq.push(['_addOrganic', '360', 'q']);
  _gaq.push(['_addOrganic', 'so', 'q']);
  _gaq.push(['_trackPageview']);
  _gaq.push(['_trackPageLoadTime']);

(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://analytic' : 'http://analytic') + '.yougou.com/ga.js?4.3.3';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>
<!-- Vizury数据(跟踪代码) -->

<!-- baidu marketing -->
<script>
var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "//hm.baidu.com/hm.js?bc66790de6f87c591da5936f04e03efb";
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(hm, s);
})();
</script>
<!-- Google Code for &#21152;&#20837;&#36141;&#29289;&#36710; Remarketing List -->

<!-- Google Code Parameters -->
<script type="text/javascript">
var google_tag_params = {
    ecomm_prodid: '100481252,100428717',
    brand:'',
    firstCategoryName:'',
    subCategoryName:'',
    thirdCategoryName:'',
    ecomm_pagetype:'cart',
    webType:'yg',
    ecomm_totalvalue: 0
};
</script>
<!-- Google Code for Main List -->
<!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. For instructions on adding this tag and more information on the above requirements, read the setup guide: google.com/ads/remarketingsetup -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1016027598;
var google_conversion_label = "189vCLqHowQQzrO95AM";
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>

</script>       
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1016027598/?value=0&amp;label=189vCLqHowQQzrO95AM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>


<script type="text/javascript">
<!-- 
var bd_cpro_rtid="n1f4nf";
//-->
</script>

<noscript>
<div style="display:none;">
<img height="0" width="0" style="border-style:none;" src="http://eclick.baidu.com/rt.jpg?t=noscript&rtid=n1f4nf" />
</div>
</noscript>
<!-- 好耶埋码 -->
<script type="text/javascript">
  var idgJsHost = (("https:" == document.location.protocol) ? "https" : "http");
  var idgDomain = idgJsHost == "http" ? "1" : "2";
  document.write(unescape("%3Cscript src='" + idgJsHost + "://" + idgDomain + ".allyes.com.cn/idigger_yg.js' type='text/javascript'%3E%3C/script%3E"));
</script><script src="cart_files/idigger_yg.htm" type="text/javascript"></script>
<script type="text/javascript">
 var s={};
 //用户ID
    s.userid="c81b4b8f386b46bc94ce1b96d23110db";
    s.shopcart="100481252|100428717";
</script>
<script type="text/javascript">
try {
  var idgTracker = _alysP.init("mso","T-000436-01", "");
  idgTracker._setTrackPath(idgJsHost + '://idigger.allyes.com/main/adftrack');
  idgTracker._setECM(s);
  idgTracker._trackPoint();
} catch(err) {}
</script><script type="text/javascript">
var _mvq = _mvq || [];
_mvq.push(['$setAccount', 'm-344-0']);
_mvq.push(['$setGeneral', 'cartview', /*用户名*/'18803203602', /*用户id*/ 'c81b4b8f386b46bc94ce1b96d23110db']);
_mvq.push(['$logConversion']);
_mvq.push(['$addItem', '',/*商品id*/ '100481252,100428717','','','']);
_mvq.push(['$logData']);
</script>

<!-- 百度DSP start-->

<script type="text/javascript">
var dsp_config = {
commodities: [
    {no:'100481252005',price:'429.00',number:'2'},{no:'100428717001',price:'929.00',number:'1'}
],
bd_list_type: 'ecom_cart'
}
</script>
<script type="text/javascript">
    $(document).ready(function () {
        if (typeof dsp_config !== 'undefined') {
            if (dsp_config['bd_list_type'] == 'ecom_reg') {
                $("body").append($("<iframe style=\"display:none;width: 1px; border: 0px none; position: absolute; left: -100px; top: -100px; height: 1px;\" src=\"" + "/inc/code_iframe.shtml?url=registerEmailSucceed" + "\"></iframe>\""));
            } else if (dsp_config['bd_list_type'] == 'ecom_cart') {
                $("body").append($("<iframe style=\"display:none;width: 1px; border: 0px none; position: absolute; left: -100px; top: -100px; height: 1px;\" src=\"" + "/inc/code_iframe.shtml?url=order" + "\"></iframe>\""));
            } else if (dsp_config['bd_list_type'] == 'ecom_order') {
                $("body").append($("<iframe style=\"display:none;width: 1px; border: 0px none; position: absolute; left: -100px; top: -100px; height: 1px;\" src=\"" + "/inc/code_iframe.shtml?url=continueOrder" + "\"></iframe>\""));
            } else if (dsp_config['bd_list_type'] == 'ecom_pay_submit') {
                $("body").append($("<iframe style=\"display:none;width: 1px; border: 0px none; position: absolute; left: -100px; top: -100px; height: 1px;\" src=\"" + "/inc/code_iframe.shtml?url=payOnline" + "\"></iframe>\""));
            } else if (dsp_config['bd_list_type'] == 'ecom_pay_offline') {
                $("body").append($("<iframe style=\"display:none;width: 1px; border: 0px none; position: absolute; left: -100px; top: -100px; height: 1px;\" src=\"" + "/inc/code_iframe.shtml?url=createOrder" + "\"></iframe>\""));
            } else if (dsp_config['bd_list_type'] == 'ecom_search') {
                $("body").append($("<iframe style=\"display:none;width: 1px; border: 0px none; position: absolute; left: -100px; top: -100px; height: 1px;\" src=\"" + "/inc/code_iframe.shtml?url=search_list" + "\"></iframe>\""));
            } else if(dsp_config['bd_list_type'] == 'ecom_view'){
                $("body").append($("<iframe style=\"display:none;width: 1px; border: 0px none; position: absolute; left: -100px; top: -100px; height: 1px;\" src=\"" + "/inc/code_iframe.shtml?url=prod-csku"+ "\"></iframe>\""));
            } else {
                $("body").append($("<iframe style=\"display:none;width: 1px; border: 0px none; position: absolute; left: -100px; top: -100px; height: 1px;\" src=\"" + "/inc/code_iframe.shtml" + "\"></iframe>\""));
            }
        }
    });
</script> 
<!-- 引入jquery -->
<script type="text/javascript" src="/home/js/jquery-1.8.3.min.js"></script>
<!-- 数量的加js -->
<script type="text/javascript">
   $('.goodsPlus').click(function(){
   var v = $(this).prev().val();
    v++;
    $(this).prev().val(v);
    //单价*数量
    //获取单价
    var price = parseInt($(this).parents('dl').find('.col_4').html());
   
    $(this).parents('dl').find('.col_6').find('strong').html(price*v);
    total();
   }) 
</script>
<!-- 数量的减js -->
<script type="text/javascript">
$('.goodsSub').click(function(){
    var v = $(this).next().val();
    v--;
    if(v<1){
        v=1;
    }
    $(this).next().val(v);
    var price = parseInt($(this).parents('dl').find('.col_4').html());
   
    $(this).parents('dl').find('.col_6').find('strong').html(price*v);
  
    total();
})
 // 添加数量js结束 
    
    // 总数量的获取
    $('.chbox').click(function(){ 
        total();
    })

    function total(){
        var arr = 0;
       $('input[type=checkbox]:checked').each(function(){
        var rop = parseInt($(this).parents('dl').find('.col_6').find('strong').html());
      // alert(111);
         arr += rop; 
        // alert(arr);
       })
        $('.f20').html(arr);
   };

//全选
    $('.all').click(function(){
       $('input[name="check[]"]').attr('checked',true);
       total();
    })
//全不选
$('.not').click(function(){
    $('input[name="check[]"]').attr('checked',false);
    total();
})

//======================删除购物车单个信息==============================//
$('.JsDel').click(function(){
   //获取商品下标
    var k=$(this).prev().val();
    // var k = $(this).parent().find('input[name="xiabiao"]').val();
   //获取商品id
    // var i =$(this).parent().find('input[name="shangpin"]').val();
    var i = $(this).parents('#shopping_cart0').find('.gid').val();
   //token验证
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });
   //获取此次点击事件
    re=$(this);
   //发送ajax请求
    $.get('/home/cart/delete',{sub:k,sid:i},function(data){
           
            if(data == 1){
                re.parent().parent().parent().remove();
            }else{
                location.reload(true);
            }   

    },'json');


    
})
//====================清空购物车==================//

$('.clearShopcart').click(function(){
     $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             }
             });
    if(confirm('你确定要清空购物车吗?')){
        $.get('/home/cart/empty',function(data){
            if(data){
               location.reload(true);
            }
        },'json');       
    }

})
//======================去结算==========================//
//点击事件
$('.cart_b_paybtn').click(function(){
   //token验证
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             }
             });
    $.get('/home/cart/price',function(data){
            var ids='';
        if(data == 1){

              $('input[name="check[]"]:checked').each(function(){
                    ids += $(this).parents('#shopping_cart0').find('.gid').val()+',';
              })

            location.replace('/home/cart/index?gid = '+ids);
        }else{

            $('.window').css('display','block');   
        }
        
    },'json');

    //未登录
    
})
//======================关闭登录弹框=========================//
$('.close').click(function(){
    $('.window').css('display','none');
})

//========================添加数量操作数据库=======================//
$('.goodsPlus').click(function(){
    var k = $(this).next().val();
    var s = $(this).prev().val();
    //token验证
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             }
             });
    //发送ajax
    $.get('/home/cart/change',{id:k,num:s},function(data){

    })
})
//减操作
$('.goodsSub').click(function(){
    var k = $(this).next().next().next().val();
    var s = $(this).next().val();
    //token验证
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             }
             });
    //发送ajax
    $.get('/home/cart/change',{id:k,num:s},function(data){

    })
})
</script>
<!-- 百度DSP end-->



<div id="cartSelector" class="cart_selector_pop none"><b class="icon arr jsarr">&nbsp;</b><a href="javascript:;" class="icon close jsclose">&nbsp;</a><div class="bd" id="cartSelectorCon"><p class="loading">加载中，请稍后...</p></div></div><iframe style="display:none;width: 1px; border: 0px none; position: absolute; left: -100px; top: -100px; height: 1px;" src="cart_files/code_iframe.htm"></iframe></body></html>