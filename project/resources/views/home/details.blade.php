﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--2016-08-29 14:22:54 星期一/10.10.80.80-->
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  <!-- 引入公用的头部 -->
  @include('home_public.header')
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <!-- 这是商品名 -->
    <title>【VIISHOWVIISHOWWDY1163藏青色】VIISHOW2016秋装新款印花时尚字母卫衣男套头衫潮全棉圆领上衣WDY1163</title>
    <meta name="description" content="VIISHOW2016秋装新款印花时尚字母卫衣男套头衫潮全棉圆领上衣WDY1163,全场折扣促销，立即购买藏青色 VIISHOWVIISHOWWDY1163" />
    <meta name="keywords" content="只有你想不到的,没有我们做不到的'  VIISHOW2016秋装新款印花时尚字母卫衣男套头衫潮全棉圆领上衣WDY1163 , WDY1163" />
      
     <meta name="mobile-agent" content="format=html5;url=http://m.yougou.com/c-viishow/sku-wdy1163-100486073" />
    <link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.yougou.com/c-viishow/sku-wdy1163-100486073" />
    <link href="/details/css/base.css" type="text/css" rel="stylesheet" />
    <link href="/details/css/goods_outlets.css" type="text/css" rel="stylesheet" />



    <!-- 设置商品尺寸和颜色的样式 -->

        <style type="text/css">
    #color .tagcolor li{
      list-style:none;
      float: left;
      margin-right: 4px;
      margin-top: 6px;
      padding: 3px 12px;
      border: 1px solid #E5E5E5;
      font-size: 15px !important;
      color: #666;
      border-radius: 2px;
      cursor:pointer;
    }
    #size .tagcolor li{
      list-style:none;
      float: left;
      margin-right: 4px;
      margin-top: 6px;
      padding: 3px 12px;
      border: 1px solid #E5E5E5;
      font-size: 15px !important;
      color: #666;
      border-radius: 2px;
      cursor:pointer;
    }
    </style>

    <!-- 设置商品尺寸和颜色被选中的样式 -->
    <style type="text/css">

    #color .tagcolor .color{

              border:solid 2px red;
          }

      #size .tagcolor .size{

              border:solid 2px red;
          }

          /*购物车弹框样式的设计*/

  /*  .dg_content{
      
      position:absolute; 
      z-index:2;
      top:70px; left:30%;
       }

       #guan{

      position:absolute; 

      left:90%;
       }*/
    </style>
  </head>
  <body class="selfadaption">
    <!--公用头部start-->
    <!--header created time: 2016-08-29T16:59:24+08:00 -->
    <!--引入公共头部start-->
  
    <!--弹出尺码选择层 start-->
    <div class="tcar_out select_size" id="select_size">
      <div class="tcar_titleBar">
        <div class="tcar_title">您尚未选择尺码</div>
        <a href="javascript:void(0)" class="tcar_close" id="select_size_close">关闭</a></div>
      <div class="tcar_outc art_size_select">
        <!--尺码遍历-->
        <p data-property="尺码" class="prosize">
          <span class="fl" style="display: inline;">尺码</span>
          <span class="fl prodSpec size" style="display: inline; width: 400px; margin-left: 10px;">
            <a data-value="size_SXXXL" data-name="XXXL">XXXL
              <i></i>
            </a>
            <a data-value="size_SS" data-name="S">S
              <i></i>
            </a>
            <a data-value="size_SM" data-name="M">M
              <i></i>
            </a>
            <a data-value="size_SL" data-name="L">L
              <i></i>
            </a>
            <a data-value="size_SXL" data-name="XL">XL
              <i></i>
            </a>
            <a data-value="size_SXXL" data-name="XXL">XXL
              <i></i>
            </a>
          </span>
        </p>
        <p class="blank5"></p>
        <p id="size_select_btn" style="padding-left: 33px; visibility: hidden; *margin-top: 10px;">
          <a href="javascript:void(0);" class="btn-confirm">确&nbsp;&nbsp;定</a></p>
        <p class="clear"></p>
      </div>
    </div>
    <!--弹出尺码选择层 end-->
    <!-- 购物车弹窗 -->
    <div class="tcar_out" id="add_to_car" style="width: 458px; height: 200px; position: absolute; bottom: 0px; right: 0; display: none; overflow: hidden;">
      <div class="tcar_titleBar">
        <div class="tcar_title">放入购物车</div>
        <a href="javascript:void(0)" class="tcar_close" id="add_to_car_close">关闭</a></div>
      <div class="tcar_outc">
        <div class="add_to_car">
          <div class="showpic"></div>
          <div class="showcon">
            <p class="tleft Yellow  Size14">商品已放入购物车中！</p>
            <p class="tleft Size14">您的购物车中共有
              <b class="Yellow" id="pordNum"></b>件商品,金额共计
              <b class="Yellow" id="priceSum"></b>元。</p>
            <p class="tleft" style="padding-top: 20px">
              <a href="javascript:void(0);" onclick="YouGou.Biz.ShoppingCart.checkShoppingCart();" class="gotobuy1"></a>
              <span>
              <a href="javascript:void(0);" id="closeBuyLink" class="Blue">继续购物</a></span>
            </p>
          </div>
        </div>
      </div>
    </div>

    <!-- end of 购物车弹窗 -->
    <!-- 商品已上架 -->
    <div class="ygwrap">
      <!-- 分类路径start -->
      <div class="curLct ngoods_bor">您当前位置：
        <a href="/home/index">首页</a>&gt;
        <a href="/home/list?id={{$row->pid}}">{{$cate->name}}</a>&gt;&gt;
        <a href="/list/details?id={{$res->id}}">{{$res->goodsname}}</a>&gt;&gt;&gt;
        <a href="/home/details?id={{$res->id}}">{{$res->goodsdes}}</a>
        </div>
      <!-- 分类路径end -->
      <div class="ygwrap fl relative" id="goodsContainer">
        <!--商品图片 start-->
        <!-- 图片默认组(默认展示一张大图,全部小图,其余大图异步加载) -->
        <div class="goodsblank"></div>
        <div class="goodsImg" id="goodsImg0">
          <!-- [issue #4146] 运动鞋显示第六张图片 -->
          <div class="goodsPic">
            <div class="jqzoom" id="spec-n1" onclick="void(0)">
              <!-- 第一张大图 start -->
              <sup no="100486073" class="mark_big_100486073 salepic2"></sup>
              <img id="pD-bimg" width="480px" height="480px" jqimg="{{$res->goodspic}}" src="{{$res->goodspic}}" alt="{{$res->goodsdes}}"/>
              <!-- 第一张大图 end --></div>
            <div class="ZoomLoading">
              <div class="tips">
                <p>正在加载图片</p>
              </div>
            </div>
            <div class="seemorepic">
              <a href="/c-viishow/sku-wdy1163-100486073-viewpic.shtml" target="_blank"></a>
            </div>
          </div>
          <div id="spec-list" class="myspeclist">
            <ul class="list-h">
              <!-- 图片列表 start -->
              @foreach($pic as $k=>$v)
              <li class="hover">
                <img width="60" height="60" picbigurl="{{$v->images1}}" piclargeurl="{{$v->images1}}" src="{{$v->images1}}" loadflag="0" class="picSmallClass0" alt="" /></li>
              <li>
              
                <img width="60" height="60" picbigurl="{{$v->images2}}" piclargeurl="{{$v->images2}}" src="{{$v->images2}}" loadflag="0" class="picSmallClass2" alt="VIISHOW2016秋装新款印花时尚字母卫衣男套头衫潮全棉圆领上衣WDY1163" /></li>
              <li>
                <img width="60" height="60" picbigurl="{{$v->images3}}" piclargeurl="{{$v->images3}}" src="{{$v->images3}}" loadflag="0" class="picSmallClass3" alt="VIISHOW2016秋装新款印花时尚字母卫衣男套头衫潮全棉圆领上衣WDY1163" /></li>

              <!-- 图片列表 end --></ul>
              @endforeach
          </div>
<!--111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 -->
        <!-- 这是管理图片放大将的位置 -->

          <script type="text/javascript" src='/home/js/jquery-1.8.3.min.js'></script>
          <script>
          // 获取图片的单击事件
      
          
          </script>

<!-- 11111111111111111111111111111111111111111111111111111111111111111111111111111111 -->

          <!--新分享2016-02-18-->
          <div class="share_goods">
            <span id="pordNoSpan" class="fl Gray">商品编号：100486073</span>
            <div class="share_goods_new fr">
              <div class="fl bdsharebuttonbox bdshare_t bds_tools get-codes-bdshare">
                <i class="fl"></i>
                <a href="#" class="fl bds_more" data-cmd="more">分享</a></div>
              <script>window._bd_share_config = {
                  "common": {
                    "bdSnsKey": {},
                    "bdText": "",
                    "bdMini": "2",
                    "bdMiniList": ["mshare", "weixin", "sqq", "qzone", "tsina", "tqq", "tqf", "renren", "douban", "kaixin001", "bdhome", "meilishuo"],
                    "bdPic": "",
                    "bdStyle": "0",
                    "bdSize": "16"
                  },
                  "share": {}
                };
                with(document) 0[(getElementsByTagName('head')[0] || body).appendChild(createElement('script')).src = 'http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion=' + ~ ( - new Date() / 36e5)];</script>
              <span id="favoriteImg" class="fr good_sc goodsClear">
                <a href="javascript:void(0)">
                  <i>
                  </i>收藏
                </a>
              </span>
            </div>
          </div>
          <!---->
          <!--新分享2016-02-18 end--></div>
        <!--商品图片 end-->
        <form name="buyForm" id="buyForm" method="post">
          <input type="hidden" name="productNo" id="productNo" />
          <input type="hidden" name="productNum" id="productNum" />
          <input type="hidden" name="productUrl" id="productUrl" />
          <input type="hidden" name="refProductUrl" id="refProductUrl" />
        </form>
        <div class="goodsCon fl">
          <h1>{{$res->goodsdes}}</h1>
          <p class="">
            <span class="Red f14 none"></span>
          </p>
          <p class="ht31">
            <span class="goods_more" id="pordNoSpan">
              <a class="" target="_blank" href="/home/list?id={{$row->pid}}">点我查看更多类似产品&gt;&gt;</a></span>
          </p>
          <div class="good_ygprcarea clearfix">
            <p class="fl ygprcarea" id="ygprice_area">
              <span id="yitianPrice" class="ygprc35">&yen; {{$res->newprice}}
                <i></i>
              
              </span>
              <del>&yen; {{$res->oldprice}}</del>
              <span id="jies" class="ico_discount">
                <i>
                </i>折</span>
            </p>
          </div>
          <div class="ht25" id="deliverTime" style="height: 25px; display: none;">
            <span class="fl">送&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;至：</span>
            <div id="areaArrive" class="areaArrive fl">
              <span class="areaSel" id="areaSel"></span>
              <i>&nbsp;</i>
              <em class="blankLine">&nbsp;</em>
              <ul id="provinceBox" class="areaBox"></ul>
            </div>
            <span id="deliverTimeDay" class="fl arriveTip">现在下单，预计
              <em id="arriveDay">-</em>日内达到</span></div>
          <div id="zengpin"></div>
          <div id="zengquan"></div>
          <p class="ht31 cmnt">
            <span id="commentStatisticsData" class="fl">商品评分：
              <i class="rate_point_s point5s_0"></i>
              <a href="#goodsBar1" class="Blue">已有0人点评</a></span>
            <a title="在线咨询" onclick="" class="lnk-ntalker Blue fr">在线咨询</a></p>
          <p class="ht31 cmnt none">
            <span>运费说明：</span>
            <span class="Yellow" id="f_feight">0</span>
            <span>&nbsp;元</span>
            <span style="padding-left: 20px">退换货运费：</span>
            <span class="Yellow" id="f_feight2">0</span>
            <span>&nbsp;元</span></p>

          <!-- 选择尺寸和大小 -->
          <div class="buy ylbgbd mt5 goods_buy" style="height:160px">
            <!-- 尺码 end -->
            <div class="clear"></div>
            <div class="line"></div>
            <!-- 获取颜色 -->
            <div id='color'>
              <ul class='tagcolor'>
                <li style='border:none;color:black;font-size:2px'>颜色:</li>@foreach($data['color'] as $k => $v)
                <li>{{$v}}</li>
                @endforeach</ul></div>
            <div class="clear"></div>
            <div class="line"></div>
            <!-- 获取尺寸 -->
            <div id='size' style="margin-top:2px;height:40px;">
              <ul class='tagcolor'>
                <li style='border:none;color:black;font-size:6px'>尺寸:</li>@foreach($data['size'] as $k => $v)
                <li>{{$v}}</li>
                @endforeach</ul>
                </div>
            <div class="goods_erweima">
              <span style="text-align: center">下载安装优购客户端</span>
              <p>
                <img alt="二维码" src="/home/picture/qrcode.png" id="qrCodeImg" class="qrCodeImg" width="128" /></p>
              <p>
                <span style="text-align: center">更多手机专享优惠</span></p>
            </div>
            <form class="cart nobottommargin clearfix" method="get" action='/home/cart/insert' enctype='multipart/form-data'>
              <div class="" style="width:150px; height:50px; font-size:16px;color:black;margin-left:12px">数量:&nbsp;&nbsp;&nbsp;
                <input class="min" name="jian" type="button" value="--" / style="width:20px;">
                <input class="text_box" name="num" type="text" value="1" style="width:30px;text-align:center;" />
                <input class="add" name="jia" type="button" value="+" /></div>
              <!-- 使用隐藏域获取值 -->
              <input type="hidden" name='color' value=''>
              <input type="hidden" name='size' value=''>
              <input type="hidden" name='sid' value="{{$res->id}}">{{ csrf_field() }}
              <button type="submit" class="add-to-cart button nomargin">加入购物车</button>
              </form>
            <!-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1111111111111111111111111111 -->
          
            <!-- 选择尺寸和大小 -->
        
            <div class="buy ylbgbd mt5 goods_buy" style="height:160px">
              
              <!-- 尺码 end -->
                <div class="clear"></div>
                            <div class="line"></div>

                           <!-- 获取颜色 -->
                            <div id='color'>
                              <ul class='tagcolor'>
                  <li style='border:none;color:black;font-size:2px'>颜色:</li>
                  @foreach($data['color'] as $k => $v)
                      <li>{{$v}}</li>
                      @endforeach
                    </ul>

                  </div>

                  <div class="clear"></div>
                  <div class="line"></div>

                   <!-- 获取尺寸 -->
                  <div id='size' style="margin-top:2px;height:40px;">
                    <ul class='tagcolor'>
                      <li style='border:none;color:black;font-size:6px'>尺寸:</li>
                      @foreach($data['size'] as $k => $v)
                      <li>{{$v}}</li>
                      @endforeach
                    </ul>
                  </div>

              <div class="goods_erweima">
                <span style="text-align: center">下载安装优购客户端</span>
                <p>
                  <img alt="二维码" src="/home/picture/qrcode.png" id="qrCodeImg" class="qrCodeImg" width="128" /></p>
                <p>
                  <span style="text-align: center">更多手机专享优惠</span></p>
              </div>
              
          <form class="cart nobottommargin clearfix" method="post"    action='/home/cart/insert' enctype='multipart/form-data'>
                            <div class="" style="width:150px; height:50px; font-size:16px;color:black;margin-left:12px">

                           数量:&nbsp;&nbsp;&nbsp;<input class="min" name="jian" type="button" value="--" / style="width:20px;">
                <input class="text_box" name="goodnum" type="text" value="1" style="width:30px;text-align:center;" />
                <input class="add" name="jia" type="button" value="+" />      
                     </div> 

                    <!-- 使用隐藏域获取值 -->
                    <input type="hidden" name='color' value=''>
                    <input type="hidden" name='size' value=''>
                    <input type="hidden" name='id' value="{{$res->id}}">
    
                     {{ csrf_field() }}

                     <button type="submit" id="submit" class="add-to-cart button nomargin">加入购物车</button>
    
            </form>

<!--111111111111111111111111-->
                           

                 <script type="text/javascript" src='/home/js/jquery-1.8.3.min.js'></script>
               <script type="text/javascript">
                // <!-- 关于购物车加减号添加的效果 -->
                $(".add").click(function() {

                   // $(this).prev() 就是当前元素的前一个元素，即 text_box
                  $(this).prev().val(parseInt($(this).prev().val()) + 1);
                        setTotal();
                });
                  
                $(".min").click(function() {
                      // $(this).next() 就是当前元素的下一个元素，即 text_box

                  //获取数量的值
                  var v = $(this).next().val();
                  v--;
                  if(v <=1) {

                    v= 1;
                  }
                  $(this).next().val(v);

                });

              
              </script>
                <!--购物车的弹出框 -->
  
  

    
<!--1111111111111111111111111111111111111111111111111111111111111111111111111111111111-->
              <p class="blank10"></p>
              <p class="mt5 soldoutdiv">猜您喜欢：</p>
              <p class="blank8 soldoutdiv"></p>
            </div>
          </form>


          <!-- 发货方式 start-->
          <p class="c9 col ml10 shipper_yg" id="shipper_yg">本商品由
            <span id="ShopName" class="c6">品牌商</span>直接发货并由商家开具普通发票</p>
           发货方式 end
           猜你喜欢
          <div id="guessUlike" class="soldoutdiv guess_like">
            <ul style="list-style: none;"></ul>
          </div>
          <p class="blank8"></p>
          <div></div>

          <!--新服务承诺2012-12-17 start-->
          <dl class="ser_promise clearfix">
            <dt>服务承诺</dt>
            <dd>
              <ul class="clearfix">
                <li class="promise1">
                  <i class="fl"></i>
                  <a class="fl" href="http://www.yougou.com/help/aboutus.html" target="_blank" rel="nofollow">正品保证</a></li>
                <li class="promise2">
                  <i class="fl"></i>
                  <a class="fl" href="http://www.yougou.com/help/returnpolicy.html" target="_blank" rel="nofollow">10天退换货</a></li>
                <li class="promise3">
                  <i class="fl"></i>
                  <a class="fl" href="http://www.yougou.com/help/priceprotection.html" target="_blank" rel="nofollow">10天补差价</a></li>
              </ul>
            </dd>
          </dl>
          <!--新服务承诺2012-12-17 end--></div>
        <!-- 看了又看 start -->
        <div class="look_look fr">
          <div class="look_ht">
           <span>看了又看</span>
            <s></s>
          <!-- 历史浏览记录的 -->
           
          </div>

          <!-- <div class="look_goods">
            <div class="look_con">
              <ul class="look_goods_ul"></ul>
            </div>
            <ul class="look_switchable-trigger">
              <li class="look-switchable-prev-btn"></li>
              <li class="look-switchable-next-btn"></li>
            </ul>
          </div> -->
          <!-- 右侧浏览历史记录的位置 -->
        <!--    <div id="post-list-footer">
                            
                            <div class="spost clearfix" style="width:100px;height:100px;">
                            
                                <div class="entry-image">
                                    <a href="#"><img src="" alt="Image" width="50%"></a>
                                </div>
                                <div class="entry-c">
                                    <div class="entry-title">
                                        <h4><a href="#"></a></h4>
                                    </div>
                                    <ul class="entry-meta">
                                        <li class="color"></li>
                                        <li><i class="icon-star3"></i> <i class="icon-star3"></i> <i class="icon-star3"></i> <i class="icon-star3"></i> <i class="icon-star-half-full"></i></li>
                                    </ul>
                                </div>                            
                           </div>
                          
              </div> -->
        </div>
        <!-- 看了又看 end -->
        </div>
      <p class="blank10"></p>
      <!--组合销售 -->
      <div class="bindSaleTj js-tab">
        <div class="bindSt bindTj js-tab-bd"></div>
      </div>
      <!--商品介绍tab开始-->
      <div class="blank20"></div>
      <!-- 右则列表start -->
      <div class="goods_rc fr">
        <!--其他相似商品start-->
        <div class="good_rList mt10">
          <h3 class="fz12px">其他相似商品</h3>
          <ul id="otherlikegoods2" class="pro_otherPro"></ul>
        </div>
        <!--其他相似商品end -->
        <!-- 想买还想买start -->
        <div class="good_rList mt10">
          <h3 class="fz12px">购买了该商品的用户还购买了</h3>
          <ul id="buybuy2" class="pro_otherPro"></ul>
        </div>
        <!-- 想买还想买end -->
        <!-- 其它商品品类推荐-->
        <!--右侧品牌介绍start-->
        <div class="brand_box mt10">
          <h3>
            <a href="" target="_blank" class="Blue">
              <img src="picture/viishow466483366.jpg" width="85" height="40" alt="VIISHOW/VIISHOW" /></a>
          </h3>
          <div class="bcon">VIISHOW创立于1991年的西班牙快时尚男装品牌,忠诚信奉欧洲简约艺术。VIISHOW集合色块,面料何图形,印花,在布料上大肆铺成开展。所设计的男装带有一股简约质感与紧致搭配的美学。关键词:VIISHOW男装、VIISHOWT恤、VIISHOW西装、VIISHOW衬衣、VIISHOW休闲服——【VIISHOW官网专卖店】VIISHOW官网旗舰店_2016新款VIISHOW衬衫|外套|西装|T恤|休闲服专柜正品折扣店-优购网</div>
          <div class="bshop">
            <a href="http://www.yougou.com/viishow-brand.html#ref=detail&amp;po=shipflag_shop" target="_blank" class="Blue">更多VIISHOW商品&gt;&gt;</a></div>
        </div>
        <!--右侧品牌介绍end--></div>
      <!-- 右则列表end -->
      <!--2013-02-18 左侧内容开始 start-->
      <div class="goods_lc" id="goods_lc">
        <div class="goods_lc_triggle" id="goodsBar1">
          <a href="" class="" >商品详情</a>
          <a href="" class="current"  >商品点评
            <em id="comment_count" class="ygprc14">
              <i></i>
            </em>
          </a>
          <a href="" >购物保障
            <i class="newtip"></i></a>
          <a href="" >退换货无忧</a>
          <a href=""  >支付与配送</a>
          <a href=""  >如何保养</a>
      </div>
        <div class="goods_lc_box">
          <!--商品详情开始-->
          <div class="goods_lc_item " id="one" style="display:none">
            <div class="bd">
              <div class="goods_lc_tb">
                <table class="detail_table">
                  <tbody>
                    <tr>
                      <td class="td1">基础风格：</td>
                      <td class="td2">青春流行</td>
                      <td class="td1">细分风格：</td>
                      <td class="td2">潮</td></tr>
                    <tr>
                      <td class="td1">适用场景：</td>
                      <td class="td2">休闲</td>
                      <td class="td1">图案：</td>
                      <td class="td2">纯色</td></tr>
                    <tr>
                      <td class="td1">适用季节：</td>
                      <td class="td2">秋季</td>
                      <td class="td1">材质：</td>
                      <td class="td2">棉</td></tr>
                    <tr>
                      <td class="td1">面料分类：</td>
                      <td class="td2">卫衣布</td>
                      <td class="td1">服装款式细节：</td>
                      <td class="td2">印花</td></tr>
                    <tr>
                      <td class="td1">厚薄：</td>
                      <td class="td2">常规</td>
                      <td class="td1">服饰工艺：</td>
                      <td class="td2">水洗</td></tr>
                    <tr>
                      <td class="td1">适用对象：</td>
                      <td class="td2">青年</td>
                      <td class="td1">领型：</td>
                      <td class="td2">圆领</td></tr>
                    <tr>
                      <td class="td1">袖型：</td>
                      <td class="td2">常规</td>
                      <td class="td1"></td>
                      <td class="td2"></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <p class="blank8"></p>
              <!--关联销售版式设置模板生成Start时间：2016-08-24 12:32:11 -->
              <div style="width:790px;overflow:hidden;"></div>
              <!--关联销售版式设置模板生成End：2016-08-24 12:32:11 -->
              <p class="blank8"></p>
              <!--尺码开始-->
              <!--尺码结束-->
              <p>
                <br /></p>
              <p>
                <br /></p>
              <div id="contentDetail">
                <p align="center"></p>
                <p align="center"></p>
                <p align="center">
                  @foreach($pic as $k=>$v)
                  <img align="absmiddle" src="{{$v->images1}}" />&nbsp;&nbsp;
                  <img align="absmiddle" src="{{$v->images2}}" />&nbsp;&nbsp;
                  <img align="absmiddle" src="{{$v->images3}}" />&nbsp;&nbsp; 
                  @endforeach</p>
                </p>
                <table border="0" width="790" class="ke-zeroborder">
                  <tbody>
                    <tr>
                      <td valign="middle" width="114"></td>
                      <td valign="middle" width="743"></td>
                    </tr>
                  </tbody>
                </table>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
              </div>
              <p>
                <br /></p>
              <!-- 品牌介绍start -->
              <p>
                <br /></p>
              <!-- 品牌介绍end --></div>
          </div>
          <!--商品详情结束-->

          <!-- 开始循环的地方 -->

          <!--商品点评start-->
          <div class="goods_lc_item " id="two" style="display:block">
            <!-- <div class="hd mt20 clearfix">
              <h3 class="hd_t fl">商品点评</h3>
              <p class="hd_tip fr">购买过该商品的用户才能进行评论</p>
            </div> -->
            <div class="bd">
              <div id="dpbox">
                <div id="userCommentContainer"></div>
                <!-- 点评内容分页start -->
                <div class="paginator" id="paginator">
                  <p class="pageJump">
                    <span>跳转至</span>
                    <input class="txt" id="jumpToPage" type="text" />
                    <input class="btnqd rbtn" type="button" value="确定" /></p>
                  <p id="page" class="page"></p>
                </div>
                <!-- 点评内容分页end --></div>
            </div>
          </div>
          <!--商品点评end-->
          <!--优购站购买保障start-->
          <div class="goods_lc_item " id="three" style="display:none">
            <div class="hd mt20 clearfix">
              <h3 class="hd_t fl">购物保障</h3></div>
            <div class="bd">
              <ul class="goods_lc_list1">
                <li class="li">
                  <span class="ico ico1"></span>
                  <h4>正品保证</h4>
                  <p class="li_gs">诚信为本，优购销售的商品均为品牌商直接供货</p>
                  <p>优购为百丽集团旗下时尚及运动鞋服类购物网站，始终坚持优质正品的品牌理念，以用户体验、服务至上为宗旨，依托投资方百丽国际强大的供应链、资金及品牌优势，经营范围涉及&quot;男鞋、女鞋、男装、女装、运动、户外、童鞋、箱包&quot; 八大品类，涵盖百丽、天美意、莱尔斯丹、fed、哈森、森达、季候风、OZZO、利郎、耐克、阿迪达斯、CAT、哥伦比亚等数百个知名品牌，数十万款商品。</p>
                </li>
                <li class="li">
                  <span class="ico ico2"></span>
                  <h4>10天退换货服务</h4>
                  <p class="li_gs">优购为您提供10天（自签收之日起）退换货服务，让您购物无风险</p>
                  <table class="goods_lc_table1 thfw_table">
                    <colgroup>
                      <col width="50%" />
                      <col width="50%" /></colgroup>
                    <thead>
                      <tr>
                        <th>以下情况可办理退换货</th>
                        <th>以下情况不可办理退换货</th></tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>
                          <div class="kth">商品不满意可退换</div></td>
                        <td>
                          <div class="bkth">超过10天有效期不可退换</div></td>
                      </tr>
                      <tr>
                        <td>
                          <div class="kth">尺码不适合可退换</div></td>
                        <td>
                          <div class="bkth">商品无法二次销售不可退换</div></td>
                      </tr>
                      <tr>
                        <td>
                          <div class="kth">质量问题可退换</div></td>
                        <td>
                          <div class="bkth">鞋盒损坏、粘绑胶带不可退换</div></td>
                      </tr>
                      <tr>
                        <td>
                          <div class="kth">配送错误可退换</div></td>
                        <td>
                          <div class="bkth">商品相关附件丢失不可退换</div></td>
                      </tr>
                    </tbody>
                  </table>
                  <div class="mt10 relative">
                    <p>
                      <span class="Yellow">温馨提示：</span>退换货时请将购物凭证随货寄回，并填写相关信息</p>
                    <a href="" target="_blank" class="show_dtl Blue">点击查看售后服务详细说明&gt;&gt;</a></div>
                </li>
                <li class="li">
                  <span class="ico ico3"></span>
                  <h4>10天调价补差价</h4>
                  <p class="li_gs">优购网商品售价（优购价）因季节等因素发生变化时，将给予顾客10天补差价</p>
                  <p>即：在订单签收起10日内，如订单内商品降价
                    <span class="Yellow">(优购价降价)</span>，您可以通过售后热线、在线客服、客服邮箱等方式申请价保，我们会在接到申请后，将您申请时的价格与您订购商品的价格差以礼品卡的形式予以返还。</p>
                  <p class="mt5">
                    <span class="Yellow">温馨提示：</span>退符合补差价条件的同款商品10天内仅享受一次补差价</p>
                  <div class="jbyz_table">
                    <h5>价保原则如下：</h5>
                    <table class="goods_lc_table1">
                      <colgroup>
                        <col width="17%" />
                        <col width="20%" />
                        <col width="15%" />
                        <col width="48%" /></colgroup>
                      <thead>
                        <tr>
                          <th>什么时候申请？</th>
                          <th>怎么申请？</th>
                          <th>怎么返还？</th>
                          <th>补差礼品卡使用规则</th></tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>签收之日起10天内</td>
                          <td>
                            <ol class="ol_list">
                              <li>1、联系售后热线申请</li>
                              <li>2、联系在线客服申请
                                <br />
                                <a class="lnk-ntalker Blue" onclick="javascript:NTKF.im_openInPageChat();" title="在线咨询">在线咨询</a></li>
                              <li>3、发送客服邮箱申请
                                <br />
                                <span class="Blue">service@yougou.com</span></li>
                            </ol>
                          </td>
                          <td>差价以礼品卡的形式予以返还</td>
                          <td>
                            <ol class="ol_list">
                              <li>1、礼品卡通常不受网站促销活动限制，可与优惠券同时使用（每张订单只能使用一张优惠券和一张礼品卡）</li>
                              <li>2、已享受补差的商品如发生退货，我司将作废礼品卡或直接从商品退款中扣除同等礼品卡金额</li>
                              <li>注：此规则从2012年12月5日起执行</li></ol>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="jbyz_table">
                    <h5>不享受十天补差声明：</h5>
                    <ol class="ol_list">
                      <li>1、换货商品不享受十天补差价；</li>
                      <li>2、秒杀、团购商品不享受十天补差；</li>
                      <li>3、差价在5元或以下的不享受十天补差；</li>
                      <li>4、购买时包含赠品的商品不享受十天补差；</li>
                      <li>5、有售馨、断码标识、已下架的商品不享受十天补差价；</li>
                      <li>6、如订单使用优惠券，申请退差后达不到优惠券使用条件的不享受十天补差；</li>
                      <li>7、涉及全场活动，如：套餐组合、加价换购、满减、多件折等促销活动不享受十天补差；</li>
                      <li>8、在线支付订单的部分商品补差剩余金额不满99元，按照订单99元免运费条件，需扣除15元运费计算补差。</li></ol>
                  </div>
                </li>
                <li class="li">
                  <span class="ico ico4"></span>
                  <h4>7&times;24小时贴心客服</h4>
                  <p class="li_gs">优购客服中心为您提供
                    <span class="Yellow">7&times;24小时全年无休</span>的优质服务
                    <br />如果您有任何问题，欢迎与我们联系。
                    <br />客服邮箱：
                    <span class="cblue">service@yougou.com</span></p>
                </li>
              </ul>
              <dl class="goods_lc_dl1">
                <dt>公司展示</dt>
                <dd>
                  <img src="/home/picture/blank.gif" original="./images/goods_detail_img1.jpg" /></dd>
                <dt>媒体报道</dt>
                <dd>
                  <p>优购从2011年7月上线以来，历时多年的发展，依托百丽国际投资背景，凭借优质、时尚货品资源，新货占比量在70%以上，其快速发展历程也受到来自各大媒体的关注与聚焦。</p>
                  <p class="news mt10">
                    <a href="" target="_blank" class="f_blue">优购及百丽电商双十一截止上午11：00已过50万单</a>
                    <a href="http://finance.qq.com/a/20141105/003228.htm" target="_blank" class="f_blue">“双十一”百丽国际给钱给货给人</a>
                    <a href="" target="_blank" class="f_blue">优购将推自有品牌运动鞋</a>
                    <a href="" target="_blank" class="f_blue">优购周年庆推出每日免单</a>
                    <a href="" target="_blank" class="f_blue">10月优购时尚商城大规模引进美国牛仔品牌LEE</a></p>
                  <p class="mt20 ml40">
                    <img src="/home/picture/blank.gif" original="./images/danpin_8_1.jpg" /></p>
                </dd>
                <dt>热点问题</dt>
                <dd>
                  <h5>订单生成后如何修改订单：</h5>
                  <p>您的订单生成后，所有订单信息您将无法自行修改，如需修改可通过以下两种方式进行操作：
                    <br />方法一：自主取消订单后重新订购商品
                    <br />方法二：联系客服人员修改订单</p>
                  <h5 class="mt20">如何查询订单配送情况：</h5>
                  <p>订单发货后，无需致电客服查询物流进度，大约48小时后，自主登录优购网——“我的订单”中了解快递配送进度。</p>
                  <h5 class="mt20">签收商品时，您需要注意的是：</h5>
                  <p>第一步：检查物流包装、“优购安全封条”是否完好
                    <br />第二步：开箱验货
                    <br />第三步：1. 满意后再签收</p>
                  <p style="margin-left:48px;">2. 商品不满意/存在质量问题：直接拒收</p>
                  <p style="margin-left:48px;">3. 缺少商品，直接拒收</p></dd>
              </dl>
              <div class="good_hot_issue">
                <dl>
                  <dt>
                    <i class="ico_clr"></i>关于色差</dt>
                  <dd>因拍照、灯光、各显示器显示效果不同等原因，实物与图片显示可能会有微弱色差，实际颜色以您收到的商品为准，色差问题不属于质量问题。</dd></dl>
                <dl>
                  <dt>
                    <i class="ico_size"></i>关于尺码</dt>
                  <dd>因品牌尺码体系，鞋型及测量方法不同，商品实际尺码与我们提供的数据可能存在细微的差别，属正常现象，请您保留最终决定权。</dd></dl>
                <dl>
                  <dt>
                    <i class="ico_time"></i>关于发货</dt>
                  <dd>由优购直接出库在线支付订单最快会在24小时内为您安排出库，系统会根据收货地址、商品库存等因素自动分配最近的仓库配送。</dd></dl>
                <dl>
                  <dt>
                    <i class="ico_truck"></i>物流公司</dt>
                  <dd>优购目前不支持指定快递，系统会根据填写的收货地址自动分配圆通，申通，中通、通路、微特派等快递进行派送，客户在48小时后，自主登录优购网——“我的订单”中了解快递配送进度。</dd></dl>
              </div>
            </div>
          </div>
          <!--购买保障end-->
          <!--优购站退换货无忧 start-->
          <div class="goods_lc_item " id="four" style="display:none">
            <div class="bd">
              <p class="thhwy_t mt40">
                <img src="/home/picture/blank.gif" original="/home/images/goods_detail_img6.gif" /></p>
              <dl class="goods_lc_dl1">
                <dt>优购“10天退换货”规则
                  <span>退换货时请将购物凭证随货寄回，并填写相关信息</span></dt>
                <dd>
                  <table class="goods_lc_table1 thfw_table">
                    <colgroup>
                      <col width="50%" />
                      <col width="50%" /></colgroup>
                    <thead>
                      <tr>
                        <th>以下情况可办理退换货</th>
                        <th>以下情况不可办理退换货</th></tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>
                          <div class="kth">商品不满意可退换</div></td>
                        <td>
                          <div class="bkth">超过10天有效期不可退换</div></td>
                      </tr>
                      <tr>
                        <td>
                          <div class="kth">尺码不适合可退换</div></td>
                        <td>
                          <div class="bkth">商品无法二次销售不可退换</div></td>
                      </tr>
                      <tr>
                        <td>
                          <div class="kth">质量问题可退换</div></td>
                        <td>
                          <div class="bkth">鞋盒损坏、粘绑胶带不可退换</div></td>
                      </tr>
                      <tr>
                        <td>
                          <div class="kth">配送错误可退换</div></td>
                        <td>
                          <div class="bkth">商品相关附件丢失不可退换</div></td>
                      </tr>
                    </tbody>
                  </table>
                </dd>
                <dt>退换货费用承担</dt>
                <dd>
                  <table class="goods_lc_table1">
                    <colgroup>
                      <col width="20%" />
                      <col width="20%" />
                      <col width="30%" />
                      <col width="30%" /></colgroup>
                    <thead>
                      <tr>
                        <th>业务类型</th>
                        <th>办理费用</th>
                        <th>优购承担</th>
                        <th>承担形式</th></tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>换货</td>
                        <td rowspan="3">免费办理</td>
                        <td rowspan="3">商品寄回费用</td>
                        <td rowspan="3">以礼品卡形式返还，
                          <br />最高不超过15元礼品卡</td></tr>
                      <tr>
                        <td>退货</td></tr>
                      <tr>
                        <td>拒收</td></tr>
                    </tbody>
                  </table>
                  <p class="mt15">符合优购网承诺退、换货标准的商品，运费均由优购网承担（一个订单优购只承担最高不超过15元的退、换货运费，超出部分将由客户自行承担，优购收件后经过检查，情况属实会以礼品卡的方式返还运费）</p></dd>
                <dt>退换货办理流程</dt>
                <dd>
                  <img src="/home/picture/blank.gif" original="/home/images/goods_detail_img7_1.gif" /></dd>
                <dt>注意事项</dt>
                <dd>
                  <ol class="goods_lc_ol1">
                    <li>需要办理退换货，请您务必在签收10天之内通过用户中心“我的优购”在线自主申请；</li>
                    <li class="mt20">请在优购购物凭证上注明退、换货原因以及换货的码数，并使用外包装将产品寄回；</li>
                    <li class="mt20">请保持商品相关附件、保修单或三包卡、吊牌、发票（若开具过发票）、赠品、购物凭证等完整无缺寄回；
                      <br />温馨提示：请不要剪掉或损坏吊牌，吊牌被剪掉或损坏，将无法进行退换货服务；发票丢失也将无法办理退货；</li>
                    <li class="mt20">请保持商品的包装完整(包括包裹填充物及原包装盒)、邮寄时敬请使用我司配送的外包装盒，请不要在原装鞋盒上直接粘绑胶粘带；
                      <br />温馨提示：商品原包装损坏或不完整，鞋盒破损或鞋盒直接缠绕胶纸都直接影响您的退换货；</li>
                    <li class="mt20">请采用普通快递方式寄回商品，不接受邮局平邮、快递到付及货运的方式寄回商品；</li>
                    <li class="mt20">出于安全和卫生考虑，贴身用品如内衣裤、短袜/打底袜/丝袜/美腿袜、塑身裤、塑身连体衣、插片/胸垫、泳衣类商品不予退换货（经权威部门检测商品存在内在质量问题者除外），其它服装类商品沾有香水或异味的概不接受退换货。收货前可先验货，签收后将不提供退换货服务，请收货前注意确认。</li></ol>
                </dd>
              </dl>
            </div>
          </div>
          <!--退换货无忧 end-->
          <!--优购站支付与配送 start-->
          <div class="goods_lc_item " id="five" style="display:none">
            <div class="bd">
              <p class="pay_t f_yellow">优购目前提供四种支付方式：信用卡支付（快捷支付）、第三方平台支付、网上银行、货到付款。</p>
              <dl class="goods_lc_dl1">
                <dt style="margin-top:10px;">信用卡支付（快捷支付）</dt>
                <dd>
                  <p>快捷支付是第三方支付平台联合各大银行推出的全新的支付方式，是最安全、轻松的付款方式之一。
                    <b>用户无需事先开通网银，只要输入卡面和手机动态口令等信息就能完成快速付款。</b>快捷支付让各类银行卡用户不需要特别开通网银就能方便地网上付款。它提供给用户更为便利的支付流程体验。</p>
                  <h5 class="mt20">快捷支付常见问题：</h5>
                  <ol class="goods_lc_ol1">
                    <li>快捷支付分为多少种类型？
                      <br />储蓄卡快捷支付和信用卡快捷支付两种。
                      <br />
                      <span class="f_yellow">优购为您提供的信用卡（快捷支付），是使用支付宝接口的信用卡快捷支付</span>，为了让您无更方便的使用，我们只是从支付平台（支付宝）前置出来了，没有支付宝账户也可以轻松完成支付。</li>
                    <li class="mt20">没有开通信用卡，想要使用储蓄卡（借记卡）快捷支付怎么办？
                      <br />请选择第三方平台支付的支付宝，登录支付宝后就可以选择储蓄卡（借记卡）快捷支付进行支付。</li></ol>
                </dd>
                <dt>第三方平台支付包括：支付宝，快钱，财付通，手机支付</dt>
                <dd>
                  <p>
                    <img src="/home/picture/blank.gif" original="/home/images/danpin-12.gif" /></p>
                  <ol class="goods_lc_ol1 mt15">
                    <li>支付宝：支付宝是由阿里巴巴公司为网络交易提供安全支付服务的第三方支付平台；</li>
                    <li>快钱：国内领先的独立第三方支付企业，旨在为各类企业及个人提供安全、便捷和保密的综合电子支付服务；</li>
                    <li>财付通：是腾讯公司创办的中国领先的在线支付平台，拥有QQ号即可使用；</li>
                    <li>银联在线支付：是中国银联联合商业银行推出的线上支付平台，无需开通网银，选择银联在线支付方式就可支付成功。购物时，选择银联在线支付平台（无需选择银行），直接点击“确认订单，去支付”，订单提交成功后，点击“立即支付”，在支付页面输入用户名、密码、 手机校验码等相关信息就可完成支付。</li>
                    <li>中国移动手机支付：就是允许移动用户使用其移动终端（通常是手机）对所消费的商品或服务进行账务支付的一种服务方式。手机支付的基本原理是将用户手机SIM卡与用户本人的银行卡账号建立一种一一对应的关系，用户通过发送短信的方式，在系统短信指令的引导下完成交易支付请求，操作简单，可以随时随地进行交易。</li></ol>
                </dd>
                <dt>网上银行</dt>
                <dd>
                  <p>优购支持中国工商银行、招商银行、中国农业银行、中国建设银行、中国银行、交通银行、兴业银行、 中国民生银行、广东发展银行、浦发银行、平安银行、北京银行、中国光大银行、中信银行、深圳发展银行等绝大多数银行借记卡及信用卡，即时到帐，准确快捷！</p>
                  <p class="mt20 f_yellow">使用网上银行进行支付，必须先开通网上银行。</p>
                  <h5>如何开通网上银行：</h5>
                  <ol class="goods_lc_ol1">
                    <li>您需要携带本人有效身份证件以及希望关联到网银的银行卡、存折等所有相关材料准备齐全，到银行柜台办理开通手续。为避免您多次往返银行，请一次性完成网银用户注册、关联网银账户、电子银行口令卡、开通短信认证、办理U盾等所有必要的手续。</li>
                    <li>有部分银行可以到银行官方网在线自主开通，或者致电银行服务热线咨询。</li></ol>
                  <p class="mt15">
                    <img src="/home/picture/blank.gif" original="/home/images/danpin-13.gif" /></p>
                </dd>
                <dt>货到付款</dt>
                <dd>
                  <ol class="goods_lc_ol1">
                    <li>由YouGou优购直接发货的商品，全国700多个地区使用货到付款的支付方式，仍有一部分地区由于较为偏远或承运的快递公司不提供代收货款的服务而暂时无法使用货到付款。由此给您造成的不便，还请您谅解。
                      <a href="" target="_blank" class="f_blue">点击查询货到付款配送范围</a></li>
                    <li>由品牌商直接发货的商品不支持货到付款，请您选择其他的支付方式，如：信用卡（快捷支付）、第三方支付、网上银行 ；</li>
                    <li>如果您订单的商品分属在不同仓库，需要拆单发货，不支持货到付款，请选择其他的支付方式，如：信用卡（快捷支付）、第三方支付、网上银行。</li>
                    <li>货到付款订单每单收取15元运费。</li></ol>
                  <p class="mt20">
                    <a href="" target="_blank" class="f_blue">更多详细帮助请点击&gt;&gt;</a></p>
                </dd>
                <dt>配送说明</dt>
                <dd>
                  <ol class="goods_lc_ol1">
                    <li>
                      <b>配送范围：</b>
                      <br />全国各地均可到达。（香港、澳门、台湾除外）。</li>
                    <li class="mt20">
                      <b>配送方式：</b>
                      <br />如发现快递人员没有按以上方式提供相应的服务，请马上与优购网客服人员联系，我们将以最快速度解决您的问题。</li>
                    <li class="mt20">
                      <b>配送时间：</b>
                      <br />在线支付订单，正常情况下支付成功后24小时之内出库。
                      <br />客户可以登陆优购网，在&quot;我的订单&quot;中查询物流信息，或联系客服查询帮您的货物配送状态。</li>
                    <li class="mt20">
                      <b>配送异常：</b>
                      <br />如遇国家法定节假日或者是异常天气状况，订单配送可能会出现一定的延迟，敬请谅解！届时优购网会在首页发布公告，您可以随时关注。</li>
                    <li class="mt20">
                      <b>物流包装：</b>
                      <br />优购网采用统一的物流包装，保障商品在配送中的完整无损。
                      <br />如有任何疑问请联系客服，我们将竭诚为您服务。</li></ol>
                </dd>
              </dl>
            </div>
          </div>
          <!--支付与配送 end-->
          <!--按脚型选鞋start 女鞋品类的时候才显示 -->
          <!--按脚型选鞋end-->
          <!-- 如何保养start -->
          <div class="goods_lc_item none" id="six" style="display:none">
            <div class="bd">
              <dl class="goods_lc_dl1">
                <dt>正确的穿鞋方法</dt>
                <dd>
                  <ol class="goods_lc_ol1">
                    <li>遇到新鞋不易上脚时，不要勉强，请用鞋拨相助。</li>
                    <li>脱鞋时不要踩着后帮，以免出现塌跟变形。</li>
                    <li>避免雨天湿滑地使用，不慎遇水应尽快以干布擦净，用纸擦好，放通风干燥处晾干，切忌阳光曝晒、水洗、火烤。如果处理不当，轻者鞋变型，皮面脱色，严重的会造成皮质变硬产生裂面或掉涂层。</li>
                    <li>不可接触酸、碱、油溶剂等化工物品。很容易造成鞋底变型，开胶及皮面变色。</li>
                    <li>深色真皮内里如受潮及磨擦产生掉色属正常现象，建议不要穿浅色袜子。皮鞋穿用时定期查看掌面（鞋跟着地的添加层）发现有深度磨损应及时更换。</li></ol>
                </dd>
                <dt>运动鞋保养须知</dt>
                <dd>
                  <ol class="goods_lc_ol1">
                    <li>定期清洁鞋子。鞋面脏后，准备一块干净软布，一盆温水和清洁液轻拭；</li>
                    <li>把软布完全浸湿，拧干、在布的角落倒点清洁液；</li>
                    <li>在鞋面上打圈拭擦，去除鞋面污垢，小心不要把面弄得湿透；</li>
                    <li>拿一把旧牙刷。清洁皱摺和接缝部位。小心轻刷，避免破坏缝线；</li>
                    <li>清洗时不可使用漂白剂和切勿用刷子猛刷；</li>
                    <li>皮质运动鞋应打油以保证皮质韧性；</li>
                    <li>维持两双以上可替换的鞋子，让鞋子轮流休息；</li>
                    <li>要注意专鞋专用、休闲鞋、凉鞋不宜做剧烈运动，室内运动鞋不宜在室外进行剧烈运动。</li></ol>
                </dd>
                <dt>皮鞋保养须知</dt>
                <dd>
                  <ol class="goods_lc_ol1">
                    <li>
                      <b>光牛皮、乌面牛皮 保养：</b>
                      <br />最好不要在5℃以下穿着，在10℃以上穿着最佳，涂层不易折裂；表面保养最好用无色的高级鞋乳，不可用光亮剂类保养，否则会造成假性涂层脱落。</li>
                    <li>
                      <b>平面漆皮、皱漆皮 保养：</b>
                      <br />皱漆皮是用中性的树脂涂料制成的，所以不易掉色，表面不易吸水，护理比较方便，表面灰尘可用干净的湿布（以不滴水为佳）轻轻擦洗干净，用不起绒的软布擦干净即可，也可用皮革清洗剂或清洁膏保养，但不能用光亮剂或鞋乳保养，光亮剂会造成假性涂层脱落，鞋乳会使鞋面发乌。皱漆皮最好在10℃以上穿着为佳。</li>
                    <li>
                      <b>暗花羊皮保养：</b>
                      <br />方法比较容易，用较好的鞋乳、鞋膏均可，色彩选用无色的为佳。</li>
                    <li>
                      <b>花纹羊皮、条纹格子皮保养：</b>
                      <br />不可用亮光剂，要用较好的无色高级鞋乳，保养时鞋乳不易太多，擦拭要均匀，处理后会发现皮面色略微加深，鞋乳挥发后基本恢复如初，如略有颜色变深属正常现象。</li>
                    <li>
                      <b>水染皮、打腊牛皮保养：</b>
                      <br />水染皮、打腊牛皮是无涂层的皮料，容易吸收水份及污渍，彩色的更为突出，穿着时要注意避免水渍或其它污渍，保持鞋面清洁，不可雨淋，不穿时擦净灰尘用鞋乳保养，保持鞋面的光度，防止霉变。</li></ol>
                </dd>
                <dt>布鞋保养须知</dt>
                <dd>
                  <ol class="goods_lc_ol1">
                    <li>刷鞋时切不可将鞋泡在水盆里，应蘸水刷；</li>
                    <li>如遇雨水或鞋底被浸湿后，切不可摔拧，应及时刷净，鞋面朝上晾干，不可暴晒；</li>
                    <li>绣花鞋、缎面鞋，因鞋面薄软，不能与硬物磕碰、刮蹭，以免破损；</li>
                    <li>请您在存放时，将鞋置于阴凉通风处，切勿受潮。翻毛面料、毡绒里应放樟脑防虫蛀;</li>
                    <li>全棉、真丝、沙丁等。不能用水刷洗，布面有污渍使用干净的白布粘取少量布料清洁剂擦拭．再改用清水擦拭．如果清洁后，鞋面有轻微的分层，在布面还没有全干透时，用手捏住鞋面和里捋平服即可。如果分层比较严重，在鞋面垫上白布，用烫斗加热加压15秒左右,并放置1小时以上再穿。</li></ol>
                </dd>
              </dl>
            </div>
          </div>
          <!-- 如何保养end -->
          <!-- 测评开始end -->
          <!-- 测评开始end --></div>
      </div>
<!-- 11111111111111111111111111111111111111111111111111111111111111111111111111111111 -->
      <!--获取所有元素-->
      <script type="text/javascript" src='/home/js/jquery-1.8.3.min.js'></script>
      <script type="text/javascript">
      //颜色以及尺寸的选择
        $('#color li:gt(0)').click(function(){
              //改变边框
              $(this).addClass('color');
              $(this).siblings().removeClass('color');

              //获取选中的值
              var v = $(this).html();
              //把值放到表单的隐藏域中  name='color'
              $('input[name="color"]').val(v);

          })

          $('#size li:gt(0)').click(function(){
              //改变边框的颜色
              $(this).addClass('size');
              $(this).siblings().removeClass('size');

              //获取选中的值
              var v = $(this).html();
              //把选中的值放入到隐藏域中  name='size'
              $('input[name="size"]').val(v);
          })

        //   商品详情处的效果

        $('#goodsBar1').find('a').click(function() {
          res='';
          $(this).attr('class','current');
          $(this).siblings().attr('class','');
          var res=$(this).text();
          if(res=='商品详情')
          {
              $('#one').css('display','block');
              $('#two').css('display','none');
              $('#three').css('display','none');
              $('#four').css('display','none');
              $('#five').css('display','none');
              $('#six').css('display','none');
          }
          if(res=='商品点评')
          {
              $('#one').css('display','none');
              $('#two').css('display','block');
              $('#three').css('display','none');
              $('#four').css('display','none');
              $('#five').css('display','none');
              $('#six').css('display','none');
          }

          return false;
        });

    
      </script>
      <!--2013-02-18 左侧内容结束 end--></div>
    <p class="blank8"></p>
    <!--底部最近浏览与才你喜欢start-->
    <div class="histyGuessGuds" id="histyGuessGuds">
      <div class="histyGuds">
        <div class="histytit">
          <h3>最近浏览</h3>
          <ul class="qiehuan">
            <li class="hisprev">&lt;</li>
            <li>1</li>
            <li>/</li>
            <li>0</li>
            <li class="hisnext">&gt;</li></ul>
        </div>
        <div class="histyGudsul">
          <ul id="historyGoods2" style="top: 0px;"></ul>
        </div>
      </div>
      <div class="guessGuds goodsTjCon"></div>
    </div>
    <!--底部最近end-->
    <p class="blank8"></p>
    <!--遮罩-->
    <div class="body_mask"></div>
    <!--底部购买导航start-->
    <!-- <div class="bot_buy_nav fixed_bottom" id="botBuyNav">
      <div class="bot_buyLeft">
        <p class="bot_buyLeft_content">
          <i>
          </i>
        </p>
      </div>
      <div class="bot_buyRight">
        <p class="bot_buyRight_content">
          <i>
          </i>
        </p>
      </div>
      <div class="bot_buyMiddle">
        <div class="bot_buyContent art_size_select">
          <p data-property="尺码" class="prosize">
            <span class="fl" style="display: inline;">尺码</span>
            <span class="fl prodSpec size" style="display: inline; width: 400px; margin-left: 10px;">
              <a data-value="size_SXXXL" data-name="XXXL">XXXL
                <i></i>
              </a>
              <a data-value="size_SS" data-name="S">S
                <i></i>
              </a>
              <a data-value="size_SM" data-name="M">M
                <i></i>
              </a>
              <a data-value="size_SL" data-name="L">L
                <i></i>
              </a>
              <a data-value="size_SXL" data-name="XL">XL
                <i></i>
              </a>
              <a data-value="size_SXXL" data-name="XXL">XXL
                <i></i>
              </a>
            </span>
          </p>
          <!-数量-->
         <!--  <p class="num">
            <span class="nums">数量</span>
            <span>
              <a href="javascript:void(0);" id="subtract" class="subtract"></a>
              <em class="numdis">
                <input type="hidden" name="oldNum" class="oldNum" />
                <input type="text" value="1" maxlength="1" onbeforepaste="clipboardData.setData('text ',clipboardData.getData('text ').replace(/[^\d]/g, ' ')) " class="mycartxt newNum" /></em>
              <a href="javascript:void(0);" class="plus" id="plus" style="width:20px"></a>
            </span>
            <span id="huotips"></span>
          </p>
          <span class="buySel selected">已选择
            <strong class="Red">藏青色</strong>
            <strong class="selectsize Red"></strong>
          </span>
          <a href="javascript:void(0);" class="bot_buy_buybtn" id="botBuybtn">立即购买</a>
          <a href="javascript:void(0);" class="bot_buy_cartbtn" id="botCartbtn">加入购物车</a>
          <i class="bot_buy_caption"></i>
          <i class="bot_buyClose" id="bot_buyClose"></i>
        </div>
      </div>
    </div>  -->

    <!-- 引入公共的底部 -->
    @include('home_public.footer_small')
    <!-- 优购导购 -->
    <script type="text/javascript" src="/details/js/jquery-1.4.2.min.js" charset="UTF-8"></script>
  <script type="text/javascript">
    var cdnUrl="http://s2.ygimg.cn";
  </script>
  <script type="text/javascript" src="/details/js/jquery.rotate.js"></script>
  <script type="text/javascript">
    var basePath = "";
    var commodityStatus = 2;
    var projectNo = "yg";
    var commoditySiteNo = "yg";
      var show_deliver_time = false;

    var supplierCode = '';
    var orderDistributionSide = "2";
</script>
  <!--商品信息js  start-->
  <script>
    // 规格组合对应属性值Map
    var prodInfo = {
        publicPrice : "338",save : "229",discount : "32%",salePrice : "109",stock : "",cId : "a7c2a1298bbc469f9ad017afe7e2864f",cNo : "100486073",cName : "VIISHOW2016秋装新款印花时尚字母卫衣男套头衫潮全棉圆领上衣WDY1163",cAlias : "",styleNo : "WDY1163",url : "/c-viishow/sku-wdy1163-100486073.shtml",
        picThumbnail : "Picture(id:75576065, commodityNo:100486073, picName:100486073_01_t.jpg?1, picType:t, picPath:/viishow/2016/100486073/, createTime:Mon Aug 29 09:57:54 CST 2016, createUser:SP20141013137136)",picSmallLest : "Picture(id:75576103, commodityNo:100486073, picName:100486073_01_s.jpg?1, picType:s, picPath:/viishow/2016/100486073/, createTime:Mon Aug 29 09:57:54 CST 2016, createUser:SP20141013137136)",picMiddle : "Picture(id:75576103, commodityNo:100486073, picName:100486073_01_s.jpg?1, picType:s, picPath:/viishow/2016/100486073/, createTime:Mon Aug 29 09:57:54 CST 2016, createUser:SP20141013137136)",picLarge : "Picture(id:75576107, commodityNo:100486073, picName:100486073_01_l.jpg?1, picType:l, picPath:/viishow/2016/100486073/, createTime:Mon Aug 29 09:57:54 CST 2016, createUser:SP20141013137136)",brandId:"afc05fd6615f4fcf9809698523c9c471",catId:"518853c75b2211e483c4a77c76dae2e0"
    ,commodity_nos:"100486073"};
    var goodSizeNos = "100486073006,100486073003,100486073002,100486073001,100486073004,100486073005";
</script>
  <!--商品信息js  end-->
  <script type="text/javascript" src="/details/js/yg.common.js"></script>
  <script type="text/javascript">
      var _mvq = _mvq || [];
      _mvq.push(['$setAccount', 'm-344-0']);

      _mvq.push(['$setGeneral', 'goodsdetail', '', '', '']);
      _mvq.push(['$logConversion']);
      _mvq.push(['$addGoods', '', '', /*品牌名 三级分类名(中间用空格分隔)*/"VIISHOW卫衣", /*商品id*/ "100486073", /*商品价格*/ "0",
          /*商品图片url*/
         " http://i1.ygimg.cn/pics/viishow/2016/100486073/100486073_01_m.jpg?1"
          , /*三级分类名*/ "卫衣", /*品牌名*/ "VIISHOW", '', '', ''
      ])
      ;
      _mvq.push(['$logData']);
  </script>
  <script type="text/javascript" src="/details/js/yg.goodsnew.js"></script>
  <script type="text/javascript" src="/details/js/commodity.js"></script>
  <!--baidu share start-->
<script type="text/javascript" id="bdshare_js" data="type=tools&amp;uid=6492363" ></script>
<script type="text/javascript" id="bdshell_js"></script>
<script type="text/javascript">
var bds_config = {
'wbUid': '2170730475',
'snsKey':{
'tsina':'2051478043',
'qzone':'1991789003',
'tqq':'2352293a29264911e331ea610ea358f6'
}
};
document.getElementById("bdshell_js").src = "http://bdimg.share.baidu.com/static/js/shell_v2.js?cdnversion=" + Math.ceil(new Date()/3600000);
</script>
<!--baidu share end-->
  <!-- common js -->
  <span class="none"></span>
  <!-- 1. sourceChannel -->
  <script type="text/javascript" src="/details/js/sourcechannel.js"></script>
  <!-- 4. google analytics -->
  <script>
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-23566531-1']);
    _gaq.push(['_setDomainName', '.yougou.com']);
    _gaq.push(['_addOrganic', 'baidu', 'word']);
    _gaq.push(['_addOrganic', 'baidu', 'wd']);
    _gaq.push(['_addOrganic', 'soso', 'query']);
    _gaq.push(['_addOrganic', 'soso', 'key']);
    _gaq.push(['_addOrganic', '3721', 'name']);
    _gaq.push(['_addOrganic', 'yodao', 'q']);
    _gaq.push(['_addOrganic', 'vnet', 'kw']);
    _gaq.push(['_addOrganic', 'sogou', 'query']);
    _gaq.push(['_addOrganic', 'sogou', 'keyword']);
    _gaq.push(['_addOrganic', '360', 'q']);
    _gaq.push(['_addOrganic', 'so', 'q']);
    _gaq.push(['_addOrganic', 'sm', 'q']);
    _gaq.push(['_trackPageview']);
    _gaq.push(['_trackPageLoadTime']);
    (function () {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://analytic' : 'http://analytic') + '.yougou.com/ga.js?4.3.4';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();
</script>
  <!-- common js end -->
  <!-- vizury code -->
  <!--
Vizury 跟踪代码
tanfeng 2013.06.25
-->
  <!-- Vizury code start -->
  <script type="text/javascript">
    (function () {
        try {
            var viz = document.createElement("script");
            viz.type = "text/javascript";
            viz.async = true;
            viz.src = ("https:" == document.location.protocol ? "https://cn-tags.vizury.com" : "http://cn-tags.vizury.com") + "/analyze/pixel.php?account_id=VIZVRM680";

            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(viz, s);
            viz.onload = function () {
                try {
                    pixel.parse();
                } catch (i) {
                }
            };
            viz.onreadystatechange = function () {
                if (viz.readyState == "complete" || viz.readyState == "loaded") {
                    try {
                        pixel.parse();
                    } catch (i) {
                    }
                }
            };
        } catch (i) {
        }
    })();
</script>
  <!-- Vizury code end -->
  <!-- baidu analysis code -->
  <script type="text/javascript">
    var dsp_config = {
        bd_list_type: 'ecom_view',
        p_id: "100486073",
        p_name: "VIISHOW2016秋装新款印花时尚字母卫衣男套头衫潮全棉圆领上衣WDY1163",
        p_brand: "VIISHOW",
        p_price: "109",
        p_class1: "",
        p_class2: "",
        p_class3: "",
        p_class4: "",
        p_stock: "",
        p_img_url: "Picture(id:75576103, commodityNo:100486073, picName:100486073_01_s.jpg?1, picType:s, picPath:/viishow/2016/100486073/, createTime:Mon Aug 29 09:57:54 CST 2016, createUser:SP20141013137136)",
        p_url: " 'http://www.yougou.com/ c-viishow/sku-wdy1163-100486073.shtml"
    }
</script>
  <!-- google remarketing code -->
  <!-- Google Code Parameters -->
  <script type="text/javascript">
    var google_tag_params = {
        ecomm_prodid: "100486073",
        brand: "VIISHOW",
        firstCategoryName: "",
        subCategoryName: "",
        thirdCategoryName: "",
        ecomm_pagetype: "product",
        webType: "yg"
    };
</script>
  <!-- Google Code for Main List -->
  Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. For instructions on adding this tag and more information on the above requirements, read the setup guide: google.com/ads/remarketingsetup
  <script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 1016027598;
    var google_conversion_label = "189vCLqHowQQzrO95AM";
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
</script>
  <noscript>
    <div style="display: inline;">
      <img height="1" width="1" style="border-style: none;" alt=""
        src="picture/0313c13481be46988df9b273d57a2b61.gif" />
    </div>
  </noscript>
  <!-- idigger code -->
  <!--
好耶统计代码,单品页
tanfeng 2013.09.25
-->
  <script type="text/javascript">
    var idgJsHost = (("https:" == document.location.protocol) ? "https" : "http");
    var idgDomain = idgJsHost == "http" ? "1" : "2";
    document.write(unescape("%3Cscript src='" + idgJsHost + "://" + idgDomain + ".allyes.com.cn/idigger_yg.js' type='text/javascript'%3E%3C/script%3E"));
</script>
  <script type="text/javascript">
    var s = {};
    s.product = "100486073";
    //用户ID
    s.userid = '';
</script>
  <script type="text/javascript">
    try {
        var idgTracker = _alysP.init("mso", "T-000436-01", "");
        idgTracker._setTrackPath(idgJsHost + '://idigger.allyes.com/main/adftrack');
        idgTracker._setECM(s);
        idgTracker._trackPoint();
    } catch (err) {
    }
</script>
</body>
</html>