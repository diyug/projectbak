<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache,no-store, must-revalidate">
<meta http-equiv="expires" content="0">
<title>追加评价</title>
<script type="text/javascript" src="/home/bootstrap/js/jquery-1.8.3.min.js"></script>
<link href="/home/my_ug/base-2.css" type="text/css" rel="stylesheet">
<link href="/home/my_ug/ucenter_v2.css" type="text/css" rel="stylesheet">
<link rel="shortcut icon" href="/home/images/favicon.ico">
<link href="/home/css/base.css" type="text/css" rel="stylesheet">
<link href="/home/css/channel.vs.css" type="text/css" rel="stylesheet">
<link href="/home/css/index.css" type="text/css" rel="stylesheet">
<script src="/home/my_ug/ga.js" async="" type="text/javascript"></script>
<script type="text/javascript" src="/home/my_ug/jquery-1.js"></script>
<script type="text/javascript" src="/home/my_ug/yg.js"></script>
<script type="text/javascript" src="/home/my_ug/ygdialog.js"></script>
<link href="/home/my_ug/ygdialog.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/home/my_ug/yg_002.js"></script>
<!-- 省市县三级联动 -->
 <script type="text/javascript" src="/home/my_ug/jquery.js"></script>
<script type="text/javascript" src="/home/my_ug/date_birthday.js"></script>
<script type="text/javascript" src="/home/my_ug/memberBasicinfo_editInfo.js"></script>
<script type="text/javascript" src="/home/my_ug/area.js"></script> 
<style type="text/css">
  .out{height:650px;background:#F8F8F8;}
  .main{width:90%;height:600px;margin:auto;background:#FFFFFF;}
  .line-left{float:left;width:44.3%;height:2px;background:#E3E3E5;margin-top:30px;}
  .line-right{float:right;width:44.5%;height:2px;background:#E3E3E5;margin-top:30px;}
  .goods_show{
              height:470px;
              border:1px solid #E3E3E5;
              
            }
  .blank{
             background:#F8F8F8;
             height:80px;}
  .order_num{
              height:50px;
              border-top:1px solid #E3E3E5;
              border:1px solid #E3E3E5;
              line-height:50px;
        }
  .goods{
              width:300px;
              height:470px;
              border-right:1px solid #E3E3E5;
               position:relative;
               float:left;
        } 
  .goods_pic{
               width:150px;
               margin:50px 75px;
               position:absolute;
               border-left:1px dotted #E3E3E5;
               border-right:1px dotted #E3E3E5;
            }
  .goods_des{
              position:absolute;
              top:200px;
              line-height:20px;
              text-align:center;
              padding:10px;
  }
  .goods_price{
              height: 20px;
              position:absolute;
              top:280px;
              left:42%; 
  }
  .goods_color{
              height:20px;
              position:absolute;
              top:310px;
              left:41%;
  }
  .goods_comment{
              height:470px;
              width:73.5%;
              float:left;
              position:relative;
  }
  .top{
              height:40px;
              width:100%;
              position:absolute;
              top:40px;
  }
  .top>div{
              float:left;
  }
  .top_left{
              margin-left:20px;
              height:40px;
              line-height:40px;
  }
  .top_right{
              margin-left:22px;
              height:40px;
              line-height:40px;
  }
  #comment{
      margin-top:120px;
      height:200px;
  }
  #comment .ug_comment{
       margin-left:20px;
      height:40px;
      line-height:40px;

  }
  .comment_pic{
      position:absolute;
      top:180px;
      height:40px;
  }
  .comment_pic>div{
    width:90px;
    height:20px;
    float:left;
    margin-left:20px;
    line-height:20px;
  }
</style>
</head>
<body class="myinfo">
@include('/home_public/header')
<!--//公共头部end-->
<div class="blank10"></div>
<div id="out">
    <div class="main">
    <div class="blank">
       <div class="line-left"></div><font size="6"><b>评价订单</b></font><div class="line-right"></div>                
    </div>
    <form action="/home/comment/docommentagain" method="post" enctype="multipart/form-data">
      <div class="order_num">
          <span style="margin-left:20px;">订单号:&nbsp;&nbsp;<b>20160920123</b></span>
          <span style="margin-left:20px;">时间:<b>2016-09-20&nbsp;&nbsp;02:00:05</b></span>
      </div>
      <div class="goods_show">
            <div class="goods">
                  <div class="goods_pic">
                     <a href="/home/details?id={{$row->id}}"> <img src="{{$row->goodspic}}" alt="{{$row->goodsname}}" width="100%" /></a>
                  </div>
                  <div class="goods_des">
                   <a href="/home/details?id="{{$row->id}}">{{$row->goodsdes}}</a> 
                  </div>
                  <div class="goods_price">
                    <b>&yen;{{$row->newprice}}</b>
                  </div>
                  <div class="goods_color">
                    {{$row->color}}
                  </div>
            </div>
            <div class="goods_comment">
                   <div id="comment">
                        <div class="ug_comment">
                              <font size="3">追加评价:</font>
                              <span style="color:red;line-height:45px;display:none;">
                              <img src="/home/comment/8.png" alt=""  />&nbsp;&nbsp;请您为商品评价
                              </span>
                        </div>
                        <div>
                            <textarea name="comment" id="" cols="60" rows="5" style="margin-left:140px;resize:none;font-size:16px;"placeholder="请输入追加内容"></textarea>  
                        </div>
                   </div>
                   {{csrf_field()}}
                   <input type="hidden" value="" name="comment" />
                   <input type="hidden" value="{{$row->id}}" name="gid" />
                  <input type="submit" value="发表评价" style="width:100px;height:30px;background:#FCAF4D;border:none;margin-left:30%; border-radius:3px; "/>
                  <a href="/home/comment/test">发送</a>
             </div>
      </div>

     </form>
    </div>
</div>
 
 <script type="text/javascript">
 
 $('input[type=submit]').click(function() { 
        var comment=$('textarea[name=comment]').val();
        if(comment.length==0)
        {
            $('.ug_comment').find('span').css('display','block');

             return false;
        }else{
            $('input[name=comment]').val(comment);
             $('.ug_comment').find('span').css('display','none');
        }

        return true;
 });


 </script>
<p class="blank10"></p>


@include('/my_ug/ug_footer')
