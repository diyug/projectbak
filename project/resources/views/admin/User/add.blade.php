@extends('/layout/index')

@section('title','添加用户')

@section('one','open')
@section('content')
<style type="text/css">
	#submit_left{ margin-left:145px; }
</style>   
	<div class="mws-panel grid_8">
  <div class="mws-panel-header">
    <span><i class="icon-plus"></i>添加用户</span></div>
  <div class="mws-panel-body no-padding">
    <form action="/admin/User/insert" class="mws-form" method="post">
      <div class="mws-form-inline">
        <div class="mws-form-row">
          <label class="mws-form-label">　　　　账户名:　</label>
          <div class="mws-form-item">
            <input type="text" class="small" name="account" placeholder="请输入手机号码/email"></div>
        </div>
        <div class="mws-form-row">
          <label class="mws-form-label">　　　　密码:　</label>
          <div class="mws-form-item">
            <input type="password" class="small" name="password" placeholder="请输入6-12位密码"></div>
        </div>
         <div class="mws-form-row">
          <label class="mws-form-label">　　　　重复密码:　</label>
          <div class="mws-form-item">
            <input type="password" class="small" name="repassword" placeholder="请在此输入密码"></div>
        </div>
        <div class="mws-form-row bordered">
            <label class="mws-form-label">　　　　状态</label>
            <div class="mws-form-item" style="width:200px">
                <select class="large" name="status">
                    <option value="1">普通用户</option>
                    <option value="2">管理员</option>
                    <option value="0">未激活用户</option>
                </select>
            </div>
        </div>
        {{ csrf_field() }}
      <div class="mws-button-row">
       　<input id="submit_left" type="submit" class="btn btn-danger" value="添加"　>
        <input type="reset" class="btn " value="重置">
      </div>
    </form>
  </div>
</div>
@endsection