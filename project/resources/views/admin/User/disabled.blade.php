@extends('layout.index')

@section('title','未激活用户列表')
@section('one','open')
@section('content')
  <div class="mws-panel grid_8">
  <div class="mws-panel-header">
    <span>
      <i class="icon-users"></i>未激活用户列表</span>
  </div>
  <div class="mws-panel-body no-padding">
    <div role="grid" class="dataTables_wrapper" id="DataTables_Table_1_wrapper">
     <form action="/admin/User/disabled" method="get"> 
      <div id="DataTables_Table_1_length" class="dataTables_length">
        <label>当前显示
          <select name="num" size="1" aria-controls="DataTables_Table_1" id="change">
            <option  num="10" @if(!empty($request['num'])&&$request['num']==10)
                  selected
                 @endif
            >10</option>
            <option  num="25" @if(!empty($request['num'])&&$request['num']==25)
                  selected
                 @endif>25</option>
            <option  num="50" @if(!empty($request['num'])&&$request['num']==50)
                  selected
                 @endif
            >50</option>
            <option　num="100" @if(!empty($request['num'])&&$request['num'])
                  selected
                 @endif
        >100</option></select>　条信息</label>
      </div>
      <div class="dataTables_filter" id="DataTables_Table_1_filter">
        <label>关键字
          <input type="text" aria-controls="DataTables_Table_1" name="account"         @if(empty($request['account']))
                  placeholder="请输入账户名
            @else
            value="{{$request['account']}}"
            @endif  
            "></label>  
         <button class="btn btn-success">搜索</button> 
      </div>
      </form>
      <script type="text/javascript">
      sel= document.getElementById('change');
    var get_account=document.getElementsByName('account')[0];
    account=get_account.value     
      sel.onchange=function(){
        val=this.value;
        location.href='/admin/User/disabled?num='+val+'&account='+account;     
      }
      </script>
      <table class="mws-datatable-fn mws-table dataTable" id="DataTables_Table_1" aria-describedby="DataTables_Table_1_info">
        <thead>
          <tr role="row">
            <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" style="width: 139px;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">ID</th>
            <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" style="width: 190px;" aria-label="Browser: activate to sort column ascending">账户</th>
            <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" style="width: 176px;" aria-label="Platform(s): activate to sort column ascending">昵称</th>
            <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" style="width: 118px;" aria-label="Engine version: activate to sort column ascending">状态</th>
            <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" style="width: 85px;" aria-label="CSS grade: activate to sort column ascending">注册时间</th>
             <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" style="width: 139px;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">操作</th>
            </tr>

        </thead>
        <tbody role="alert" aria-live="polite" aria-relevant="all">
          
          @foreach($row as $k=>$v)
            <tr class="@if($k%2==0)
              odd
            @else
              even
            @endif  
            ">
              <td class="  sorting_1">{{$v->id}}</td>
              <td class=" ">{{$v->account}}</td>
              <td class=" ">{{$v->nickname}}</td>
              <td class=" ">{{getStatus( $v->status)}}</td>
              <td class=" ">{{date('Y-m-d H:i:s',$v->regtime)}}</td>
              <td>　
              <a href="/admin/User/edit?id={{$v->id}}" style="font-size:20px;color:black;" title="编辑"><i class="icon-edit"></i></a>　　　
              <a href="/admin/User/delete?url=admin/User/disabled&id={{$v->id}}" style="font-size:20px;color:black;" title="删除"><i class="icon-trash"></i></a>
              </td>
            </tr>
        @endforeach
        </tbody>
      </table>
        <div class="dataTables_paginate paging_full_numbers" id="pages">
      @if(isset($request))
        {!! $row->appends(['account' =>$request['account']])
              ->appends(['num' =>$request['num']])
        ->render() !!}
      @else
              {!! $row->render() !!}
            @endif
      </div>
  </div>
</div>
@endsection