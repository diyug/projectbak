@extends('layout.index')

@section('title','用户信息编辑')
@section('one','open')
@section('content')
<style type="text/css">
	 #let_right{
		margin-left:155px;
	}
</style>
	<form action="/admin/User/update" class="mws-form" method="post">
  <div class="mws-form-inline">
    <div class="mws-form-row">
      <label class="mws-form-label">账户名</label>
      <div class="mws-form-item">
        <input type="text" class="small" name="account" readonly value="{{$row[0]->account}}"></div>
    </div>
    <div class="mws-form-row">
      <label class="mws-form-label">昵称</label>
      <div class="mws-form-item">
        <input type="text" class="small" name="nickname" value="{{$row[0]->nickname}}"></div>
    </div>
    <div class="mws-form-row">
      <label class="mws-form-label">email</label>
      <div class="mws-form-item">
        <input type="text" class="small" name="email" value="{{$row[0]->email}}"></div>
    </div>
    
    <div class="mws-form-row">
      <label class="mws-form-label">状态</label>
      <div class="mws-form-item">
        <select name="status">
          <option value="1"
          				    @if($row[0]->status==1)
									selected
							@endif

         >普通用户</option>
          <option value="2"
							@if($row[0]->status==2)
									selected
							@endif
          >管理员</option>
          <option value="0"
							@if($row[0]->status==0)
									selected
							@endif
          >未激活用户</option>
         </select>
      </div>
    </div>
    <div class="mws-form-row" id="ug_update"> 
      <div class="mws-form-item">
          
        		<a><i class="icol-add"></i>修改密码</a>
          
       </div>
    </div>
    {{ csrf_field() }}
    <input type="hidden" name="id" value="{{$row[0]->id}}">
  <div class="mws-button-row">
    <input type="submit" id="let_right" class="btn btn-danger" value="修改"　>
    <input type="reset" class="btn " value="重置"></div>
</form>
	<script type="text/javascript" src="/back/js/libs/jquery-1.8.3.min.js"></script>

	<script type="text/javascript">
	$('#ug_update a').click(function(){

		$(this).parents('#ug_update').attr('style','display:none;');
		
		var pas=$('<div class="mws-form-row"><label class="mws-form-label"><font color="red" size="3">* * * 新密码 * * * </font></label><div class="mws-form-item"><input type="password" class="small" name="password"></div></div>');
		var repas=$('<div class="mws-form-row"><label class="mws-form-label">重复密码</label><div class="mws-form-item"><input type="password" class="small" name="repassword"></div></div>');

		$(this).parents('#ug_update').prev().after(pas);
		$(this).parents('#ug_update').prev().after(repas);
	})
	</script>
@endsection
