@extends("layout.index")

@section("title","添加专题")

@section("five","placeholder")

@section("content")

	<div class="mws-panel grid_8">
		<div class="mws-panel-header">
	    	<span>修改专题信息</span>
	    </div>
	    <div class="mws-panel-body no-padding">
	    	<form action="/admin/Brand/updatespecial" class="mws-form" method="post">
	    		<!-- 隐藏域传token -->
			    {{csrf_field()}}

			    <!-- 传id的隐藏域 -->
			    <input type="hidden" name="id" value="{{$data -> id}}">

	    		<div class="mws-form-inline">
    			<div class="mws-form-row">
    				<label class="mws-form-label" for="brandname">专题名称</label>
    				<div class="mws-form-item">
    					<input type="text" class="small" name="specialname" id="brandname" autocomplete="off" value="{{$data -> specialname}}">
    				</div>
    			</div>


	    </div>
	    		<div class="mws-button-row">
	    			<input type="submit" class="btn btn-primary" value="添加">
	    			<input type="reset" class="btn btn-info" value="重置" style="margin-left:20px">
	    		</div>
	    	</form>
	    </div>    	
	</div>

@endsection