@extends('layout.index')

@section('five','open')

@section('title','导购logo清单')

@section('content')
	
	<div class="mws-panel grid_8">
  <div class="mws-panel-header">
    <span>
      <i class="icon-table"></i>所在专区:{{$specialname}}</span>
  </div>
  <div class="mws-panel-body no-padding">
    <div role="grid" class="dataTables_wrapper" id="DataTables_Table_1_wrapper">
      <div id="DataTables_Table_1_length" class="dataTables_length">
        <form action="/admin/Brand/brandlist" method="get">

            <label>显示
                <select name="num" size="1" aria-controls="DataTables_Table_1">
                <option value="10"
                    @if(!empty($all['num']) && $all['num']==10)
                      selected
                    @endif
                >10</option>
                <option value="20"
                    @if(!empty($all['num']) && $all['num']==20)
                      selected
                    @endif
                >20</option>
                <option value="30"
                    @if(!empty($all['num']) && $all['num']==30)
                      selected
                    @endif
                >30</option>
                <option value="50"
                    @if(!empty($all['num']) && $all['num']==50)
                      selected
                    @endif
                >50</option>
              </select>条数据
            </label>
          </div>
          
          <div class="dataTables_filter" id="DataTables_Table_1_filter">
          <label>显示
              <select name="branch" size="1" aria-controls="DataTables_Table_1">
              <option value="0"
                  @if(!empty($all['branch']) && $all['branch']==0)
                    selected
                  @endif
              >小图</option>
              <option value="1"
                  @if(!empty($all['branch']) && $all['branch']==1)
                    selected
                  @endif
              >中图</option>
              <option value="2"
                  @if(!empty($all['branch']) && $all['branch']==2)
                    selected
                  @endif
              >大图</option>
              </select>条数据
            </label>
          <select name="specialer" style="margin-right:20px">
                @foreach($specials as $key => $value)
                  <option value="{{$value -> id}}"
                  @if(!empty($all['specialer']) && $all['specialer'] == $value->id)
                    selected
                  @endif
                  >{{$value -> specialname}}</option>
                @endforeach
          </select>
            <label>搜索:
              <input type="text" aria-controls="DataTables_Table_1" id="search" name="search" autocomplete="off"
                @if(!empty($all['search']))
                  value="{{$all['search']}}"
                @endif
              placeholder="图片排位号"></label>
              <button class="btn-primary btn">搜索</button>
        </form>
      </div>
      <table class="mws-datatable-fn mws-table dataTable" id="DataTables_Table_1" aria-describedby="DataTables_Table_1_info">
        <thead>
          <tr role="row">
            <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" style="width: 141px;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
            ID</th>

            <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" style="width: 193px;" aria-label="Browser: activate to sort column ascending">
            品牌名称</th>

            <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" style="width: 179px;" aria-label="Platform(s): activate to sort column ascending">
            图片</th>

            <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" style="width: 179px;" aria-label="Platform(s): activate to sort column ascending">
            隶属专题</th>

            <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" style="width: 86px;" aria-label="CSS grade: activate to sort column ascending">
            排位</th>

            <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" style="width: 86px;" aria-label="CSS grade: activate to sort column ascending">
            分支</th>

            <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" style="width: 86px;" aria-label="CSS grade: activate to sort column ascending">
            操作</th>
          </tr>
        </thead>

        <tbody role="alert" aria-live="polite" aria-relevant="all">
        	@foreach($data as $key => $value)
	        <tr class="
           @if($key%2==0)
            odd
           @else
            even
           @endif
          ">
	            <td class="sorting_1" align="center">{{$value->id}}</td>
	            <td align="center">{{$value -> brandname}}</td>
              <td align="center"><img src="{{$value->img_path}}"
              @if($value -> special==1)
                width="100" class="lightense" data-background="rgba(5, 14, 51, .6)" 
              @elseif($branch != 0)
                width="120" 
              @endif
              alt=""></td>
	            <td align="center">{{showSpecial($value -> special)}}</td>
              <td align="center">{{$value->picline}}</td>
	            <td align="center">{{showBranch($value->branch)}}</td>
	            <td>
					<!-- 编辑和删除两个图标 -->
                <a href="/admin/Brand/editbrand?id={{$value->id}}" style="font-size:18px;color:#666;">
                	<i class="icon-pencil"></i>
                </a>

                <a href="/admin/Brand/deletebrand?id={{$value->id}}" style="font-size:18px;color:#666;margin-left:30px">
                	<i class="icon-remove"></i>
                </a>
	            </td>
	        </tr>

	          @endforeach
        </tbody>
      </table>
      <div class="dataTables_paginate paging_full_numbers" id="pages">
          {!! $data->appends($all)->render()!!}
      </div>
  </div>
</div>

  <script type="text/javascript">
    // 图片放大
    window.addEventListener('load', function () {
      var el = document.querySelectorAll('img.lightense');
      Lightense(el);
    }, false);

    // 专题下拉列表改变事件
    var num = document.getElementsByName('num')[0];
    var search = document.getElementsByName('search')[0];
    var specialer = document.getElementsByName('specialer')[0];
    var branch = document.getElementsByName('branch')[0];
    var send_search = search.value   
      specialer.onchange=function(){
         var send_num = num.value;
         var send_specialer = this.value;
         var send_branch = branch.value;
        location.href='/admin/Brand/brandlist?num='+send_num+'&branch'+send_branch+'&specialer='+send_specialer+'&search='+send_search;
      }
  </script>

@endsection