@extends('layout.index')

@section('title','添加logo')

@section('five','opend')

@section('content')
	<div class="mws-panel grid_8">
		<div class="mws-panel-header">
	    	<span>logo添加</span>
	    </div>
	    <div class="mws-panel-body no-padding">
	    	<form action="/admin/Brand/insertbrand" class="mws-form" method="post" enctype="multipart/form-data">
	    		<!-- 传token的隐藏域 -->
			    {{csrf_field()}}
	    		<div class="mws-form-inline">

	    			<div class="mws-form-row">
	    				<label class="mws-form-label" for="catename">品牌名称</label>
	    				<div class="mws-form-item">
	    					<input type="text" class="small" name="brandname" id="catename" autocomplete="off" value="{{old("name")}}">
	    				</div>
	    			</div>

	    			<!-- 上传图片 -->
	    			<div class="mws-form-row" style="width:400px">
					    <label for="files" class="mws-form-label">上传图片</label>
					    <div class="mws-form-item">
					    	<input type="file" name="img_path" id="files">
					    </div>
					</div>

					<!-- 排位 -->
					<div class="mws-form-row" style="width:280px">
	    				<label class="mws-form-label" for="catename">次序排位</label>
	    				<div class="mws-form-item" style="width:270px">

	    					<input type="radio"name="branch[]" value="2" id="big_"> <label for="big_">大图</label>
	    					<input type="radio" name="branch[]" value="1" style="margin-left:30px" id="middle_"> <label for="middle_">热门</label>
	    					<input type="radio" name="branch[]" value="0" style="margin-left:25px" id="small_"> <label for="small_">品牌</label>
	    					<div style="margin-top:10px;width:130px;padding:0px">
	    						<input type="number" class="small" name="picline" id="catename" autocomplete="off" value="{{old("picline")}}" style="margin-left:20px">
	    					</div>

	    				</div>
	    			</div>


	    			<!-- 选择分类下拉列表 -->
	    			<div class="mws-form-row">
	    				<label class="mws-form-label">所属专题</label>
	    				<div class="mws-form-item">
	    					<select name="special">
	    						@foreach($data as $key => $value)
									<option value="{{$value->id}}">{{$value -> specialname}}</option>
	    						@endforeach
	    					</select>
	    				</div>
	    			</div>

	    		</div>
	    		<div class="mws-button-row">
	    			<input type="submit" class="btn btn-primary" value="添加">
	    			<input type="reset" class="btn btn-info" value="重置" style="margin-left:20px">
	    		</div>

	    	</form>
	    </div>    	
	</div>
@endsection