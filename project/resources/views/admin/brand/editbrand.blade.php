@extends("layout.index")

@section("title","品牌信息编辑")

@section("five","placeholder")

@section("content")

	<div class="mws-panel grid_8">
		<div class="mws-panel-header">
	    	<span>分类编辑</span>
	    </div>
	    <div class="mws-panel-body no-padding">
	    	<form action="/admin/Brand/updatebrand" class="mws-form" method="post">
	    		<!-- 隐藏域传token -->
			    {{csrf_field()}}

			    <!-- 传id的隐藏域 -->
			    <input type="hidden" name="id" value="{{$data -> id}}">

			    <!-- 传旧排位的隐藏域 -->
			    <input type="hidden" name="oldpicline" value="{{$data -> picline}}">
				
				<!-- 传旧专题的隐藏域 -->
			    <input type="hidden" name="oldspecial" value="{{$data -> special}}">
			    <!-- 传分支的隐藏域 -->
			    <input type="hidden" name="branch" value="{{$data -> branch}}">

	    		<div class="mws-form-inline">
    			<div class="mws-form-row">
    				<label class="mws-form-label" for="brandname">品牌名称</label>
    				<div class="mws-form-item">
    					<input type="text" class="small" name="name" id="brandname" autocomplete="off" value="{{$data->brandname}}" disabled>
    				</div>
    			</div>

    			<div class="mws-form-row">
	    		<img src="{{$data->img_path}}"
	    		@if(strpos($data->img_path,'middle') || strpos($data ->img_path,'big'))
					width="150"
	    		@endif
	    		data-background="rgba(0, 10, 45, .6)" class="lightense">
				</div>

				<!-- 排位 -->
				<div class="mws-form-row" style="width:280px">
    				<label class="mws-form-label" for="catename">次序排位</label>
    				<div class="mws-form-item">
    					<input type="number" class="small" name="picline" id="catename" autocomplete="off" value="{{$data->picline}}">
    				</div>
    			</div>

    			<!-- 选择分类下拉列表 -->
    			<div class="mws-form-row">
    				<label class="mws-form-label">所属专题</label>
    				<div class="mws-form-item">
    					<select name="special">
    						@foreach($special as $key => $value)
								<option value="{{$value->id}}"
								@if($value -> id == $data->special)
								selected
								@endif
								>{{$value -> specialname}}</option>
    						@endforeach
    					</select>
    				</div>
    			</div>

	    		</div>
	    		<div class="mws-button-row">
	    			<input type="submit" class="btn btn-primary" value="修改">
	    			<input type="reset" class="btn btn-info" value="重置" style="margin-left:20px">
	    		</div>
	    	</form>
	    </div>    	
	</div>

	<script type="text/javascript">
		window.addEventListener('load', function () {
		  var el = document.querySelectorAll('img.lightense');
		  Lightense(el);
		}, false);  
	</script>

@endsection