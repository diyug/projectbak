@extends('layout.index')

@section('five','open')

@section('title','专题列表')

@section('content')
	
	<div class="mws-panel grid_8">
  <div class="mws-panel-header">
    <span>
      <i class="icon-table"></i>图片清单</span>
  </div>
  <div class="mws-panel-body no-padding">
    <div role="grid" class="dataTables_wrapper" id="DataTables_Table_1_wrapper">
      <div id="DataTables_Table_1_length" class="dataTables_length">
        
      </div>
      <table class="mws-datatable-fn mws-table dataTable" id="DataTables_Table_1" aria-describedby="DataTables_Table_1_info">
        <thead>
          <tr role="row">
            <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" style="width: 141px;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
            ID</th>
            <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" style="width: 193px;" aria-label="Browser: activate to sort column ascending">
            专题名称</th>

            <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" style="width: 86px;" aria-label="CSS grade: activate to sort column ascending">
            操作</th>
          </tr>
        </thead>

        <tbody role="alert" aria-live="polite" aria-relevant="all">
        	@foreach($data as $key => $value)
	        <tr class="
           @if($key%2==0)
            odd
           @else
            even
           @endif
          ">
	            <td class="sorting_1" align="center">{{$value->id}}</td>
	            <td align="center">{{$value -> specialname}}</td>

	            <td>
					<!-- 编辑和删除两个图标 -->
                <a href="/admin/Brand/editspecial?id={{$value->id}}" style="font-size:18px;color:#666;">
                	<i class="icon-pencil"></i>
                </a>

                <a href="/admin/Brand/deletespecial?id={{$value->id}}" style="font-size:18px;color:#666;margin-left:30px">
                	<i class="icon-remove"></i>
                </a>
	            </td>
	        </tr>

	          @endforeach
        </tbody>
      </table>
  </div>
</div>

@endsection