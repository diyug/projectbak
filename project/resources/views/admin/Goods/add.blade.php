@extends("layout.index")

@section('three','open')

@section("title","商品添加")

@section("content")
	<div class="mws-panel grid_8">
	<div class="mws-panel-header">
    	<span>商品添加</span>
    </div>
    <div class="mws-panel-body no-padding">

      <!--   @if (count($errors) > 0)
		   <div class="mws-form-message error">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif -->

    	<form action="/admin/Goods/insert" method='post' class="mws-form" enctype="multipart/form-data">
    		<div class="mws-form-inline">
    			<div class="mws-form-row">
    				<label class="mws-form-label">商品名:</label>
    				<div class="mws-form-item">
    					<input type="text" name='goodsname'class="small">
    				</div>
    			</div>
              {{ csrf_field() }}
                <div class="mws-form-row">
                    <label class="mws-form-label">父级分类</label>
                    <div class="mws-form-item">
                        <select class="small" name='pid'>
                            <option value='0'>请选择</option>
                            @foreach($data as $k=>$v)
                            <option value="{{$v->id}}" >{{$v->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <!-- 性别添加开始 -->
                <div class="mws-form-row">
                    <label class="mws-form-label">性别:</label>
                    <div class="mws-form-item" style="width:46%;">
                       <select name="sex" id="">
                           <option value="男性" selected>男性</option>
                           <option value="女性">女性</option>
                       </select>
                    </div>
                </div>

                <!-- 性别添加结束 -->

                <div class="mws-form-row">
                    <label class="mws-form-label">商品图片:</label>
                    <div class="mws-form-item" style="width:46%;">
                        <input type="file" name='goodspic' class="small">
                    </div>
                </div>

                <div class="mws-form-row">
                    <label class="mws-form-label">商品现价:</label>
                    <div class="mws-form-item">
                        <input type="text" name='newprice' class="small">
                    </div>
                </div>
                 <div class="mws-form-row">
                    <label class="mws-form-label">商品原价:</label>
                    <div class="mws-form-item">
                        <input type="text" name='oldprice' class="small">
                    </div>
                </div>
                 <div class="mws-form-row">
                    <label class="mws-form-label">商品数量:</label>
                    <div class="mws-form-item">
                        <input type="number" name='goodsnum' class="small">
                    </div>
                </div>
                <!-- 添加颜色尺寸 -->
               <div class="mws-form-row">
                    <label class="mws-form-label">尺寸:</label>
                    <div class="mws-form-item">
                        <input type="text" name='size' class="small">
                    </div>
                </div>
                <div class="mws-form-row">
                    <label class="mws-form-label">颜色:</label>
                    <div class="mws-form-item">
                        <input type="text" name='color' class="small">
                    </div>
                </div>
                <!-- 添加颜色尺寸结束 -->
                   <div class="mws-form-row">
                    <label class="mws-form-label">商品描述:</label>
                    <div class="mws-form-item">
                        <input type="text" name='goodsdes' class="small">
                    </div>
                    
                </div>

                    <div class="mws-form-row">
                    <label class="mws-form-label">商品备注:</label>
                    <div class="mws-form-item">
                        <input type="text" name='beizhu' class="small">
                    </div>
                </div>	
    		</div>
    		<div class="mws-button-row">
    			
    			<input type="submit" class="btn btn-danger" value="提交">
    			<input type="reset" class="btn " value="重置">
    		</div>
    	</form>
    </div>    	
</div>
@endsection