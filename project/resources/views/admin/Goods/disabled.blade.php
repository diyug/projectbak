@extends("layout.index")

@section("title","下架商品列表")

@section('three','open')

@section("content")
	
	<div class="mws-panel grid_8">
    	<div class="mws-panel-header">
        	<span><i class="icon-table"></i>下架列表</span>
        </div>
        <div class="mws-panel-body no-padding">
            <div role="grid" class="dataTables_wrapper" id="DataTables_Table_0_wrapper">
            	<form action="/admin/Goods/index" method="post">
            		<div id="DataTables_Table_0_length" class="dataTables_length"><label>显示
			            <select name="num" size="1" aria-controls="DataTables_Table_0">
				            <option  num="10" @if(!empty($request['num'])&&$request['num']==10)
									selected
							   @endif
				            >10</option>
				            <option  num="25" @if(!empty($request['num'])&&$request['num']==25)
													selected
											   @endif>25</option>
				            <option  num="50" @if(!empty($request['num'])&&$request['num']==50)
													selected
											   @endif
				            >50</option>
				            <option　num="100" @if(!empty($request['num'])&&$request['num'])
													selected
											   @endif
						    >100</option>
		            	</select> 条数据</label>
	            	</div>
		            <div class="dataTables_filter" id="DataTables_Table_0_filter">
		            <label for="search" style="cursor:pointer">搜索: 
		            	<input type="search" name="search" id="search" aria-controls="DataTables_Table_0"  autocomplete="off" placeholder="请搜索商品名称"
							@if(!empty($request['search']))
								value="{{$request['search']}}"
							@endif
		            	>
		            </label>
		            <button class="btn btn-primary">搜索</button>
		            </div>
            	</form>
	           <table class="mws-datatable mws-table dataTable" id="DataTables_Table_0" aria-describedby="DataTables_Table_0_info">
                <thead>
                    <tr role="row">
	                    <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 141px;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">ID</th>
	                    <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 193px;" aria-label="Browser: activate to sort column ascending">商品名</th>
	                    <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 179px;" aria-label="Platform(s): activate to sort column ascending">分类名称</th>
	                     <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 179px;" aria-label="Platform(s): activate to sort column ascending">商品图片</th>
	                    <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 120px;" aria-label="Engine version: activate to sort column ascending">商品现价</th>
	                    <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 120px;" aria-label="Engine version: activate to sort column ascending">商品原价</th>
	                     <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 120px;" aria-label="Engine version: activate to sort column ascending">商品数量</th>
	                     <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 120px;" aria-label="Engine version: activate to sort column ascending">商品描述</th>
	                     <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 120px;" aria-label="Engine version: activate to sort column ascending">商品备注</th>
	                     <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 120px;" aria-label="Engine version: activate to sort column ascending">商品添加时间</th>

	                     <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 120px;" aria-label="Engine version: activate to sort column ascending">商品状态</th>
	                    <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 86px;" aria-label="CSS grade: activate to sort column ascending">操作</th>
                    </tr>
                </thead>
                
	            <tbody role="alert" aria-live="polite" aria-relevant="all">
		            	@foreach($row as $key=> $value)

		            		<tr class="
								@if($value->id%2==0)
								even
								@else
								odd
								@endif
		            		">
		                        <td class="  sorting_1">{{$value -> id}}</td>
		                        <td class=" ">{{$value -> goodsname}}</td>

		                        <td class=" ">{{$value -> pid}}</td>
		                        <td class=" ">
					          	<img src="{{$value->goodspic}}" alt="" width="50">
					          	</td>

		                        <td class=" ">{{$value -> newprice}}</td>
		                        <td class=" ">{{$value -> oldprice}}</td>
		                        <td class=" ">{{$value -> goodsnum}}</td>
		                        <td class=" ">{{$value -> goodsdes}}</td>
		                        <td class=" ">{{$value -> beizhu}}</td>
		                      	<td class=" ">{{date('Y-m-d H:i:s',$value->goodstime)}}</td></td>

		                        <td class=" ">
		                        	@if($value->status == 1)
									上架
		                        	@else
		                        	下架
									@endif
		                        </td>
		                        <td class="except">
			                        <!-- 编辑和删除两个图标 -->
			                        <a href="/admin/Goods/edit?id={{$value->id}}"  style='color:black'>
			                        	<i class="icon-pencil" color:black></i>
			                        </a>&nbsp;

			                        <a href="/admin/Goods/delete?id={{$value->id}}"  style='color:black'>
			                        	<i class="icon-remove"></i>
			                        </a>
		                        </td>
		            		</tr>
		            	@endforeach
	            </tbody>
            </table>

            <div class="dataTables_paginate paging_full_numbers" id="pages">
		       {!! $row->appends($request)->render()!!}    
		    </div>

            </div>
        </div>
    </div>

@endsection