@extends("layout.index")

@section('three','open')

@section("title","分类添加")

@section("content")
	<div class="mws-panel grid_8">
	<div class="mws-panel-header">
    	<span>商品编辑</span>
    </div>
    <div class="mws-panel-body no-padding">

    	<form action="/admin/Goods/update" method='post' class="mws-form" enctype="multipart/form-data">
    		<div class="mws-form-inline">
    			<div class="mws-form-row">
    				<label class="mws-form-label">商品名:</label>
    				<div class="mws-form-item">
    					<input type="text" name='goodsname' class="small" value="{{$row->goodsname}}">
    				</div>
    			</div>
              <!--姓别 -->
               <div class="mws-form-row">
                    <label class="mws-form-label">性别:</label>
                    <div class="mws-form-item" style="width:46%;">
                       <select name="sex" id="">
                           <option value="男性"
                            @if($row->sex == "男性")
                            selected
                            @endif
                           >男性</option>
                           <option value="女性"
                             @if($row->sex == "女性")
                            selected
                            @endif
                           >女性</option>
                       </select>
                    </div>
                </div>
              <!--姓别 -->
               
                <div class="mws-form-row">
                    <label class="mws-form-label">商品图片:</label>
                    <div class="mws-form-item" style="width:46%;">
                        <img src="{{$row->goodspic}}" alt="" style="width:60px;margin-bottom:10px;">
                        <input type="file" name='goodspic' class="small">
                    </div>
                </div>

                <div class="mws-form-row">
                    <label class="mws-form-label">商品现格:</label>
                    <div class="mws-form-item">
                        <input type="text" name='newprice' class="small" value="{{$row->newprice}}">
                    </div>
                </div>
                   <div class="mws-form-row">
                    <label class="mws-form-label">商品原价:</label>
                    <div class="mws-form-item">
                        <input type="text" name='oldprice' class="small" value="{{$row->oldprice}}">
                    </div>
                </div>
                 <div class="mws-form-row">
                    <label class="mws-form-label">商品数量:</label>
                    <div class="mws-form-item">
                        <input type="number" name='goodsnum' class="small" value="{{$row->goodsnum}}">
                    </div>
                </div>

                   <div class="mws-form-row">
                    <label class="mws-form-label">商品描述:</label>
                    <div class="mws-form-item">
                        <input type="text" name='goodsdes' class="small" value="{{$row->goodsdes}}">
                    </div>
                </div>

                    <div class="mws-form-row">
                    <label class="mws-form-label">商品备注:</label>
                    <div class="mws-form-item">
                        <input type="text" name='beizhu' class="small" value="{{$row->beizhu}}">
                    </div>
                </div>	

                <div class="mws-form-row">
                    <label class="mws-form-label">商品状态:</label>
                    <div class="mws-form-item">
                        <!-- <input type="text" name='status' class="small" value="{{$row->status}}"> -->
                        <select name="status" id="">
                            <option value="0" 
                            @if($row->status == 0)
                            selected
                            @endif
                            
                            >下架</option>
                            <option value="1"
                            @if($row->status == 1)
                            selected
                            @endif
                            >上架</option>
                            
                        </select>
                    </div>
                </div>

    		</div>
    		<div class="mws-button-row">
    			{{ csrf_field() }}
                <input type="hidden" name='id' value="{{$id}}">
    			<input type="submit" class="btn btn-danger" value="编辑">
    			<input type="reset" class="btn " value="重置">
    		</div>
    	</form>
    </div>    	
</div>
@endsection