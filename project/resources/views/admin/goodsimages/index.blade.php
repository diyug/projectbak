@extends("layout.index")

@section("title","商品图片显示")

@section('seven','close')

@section("content")
	
	<div class="mws-panel grid_8">
    	<div class="mws-panel-header">
        	<span><i class="icon-table"></i>商品图片列表</span>
        </div>
        <div class="mws-panel-body no-padding">
            <div role="grid" class="dataTables_wrapper" id="DataTables_Table_0_wrapper">
            	<form action="/admin/goodsimages/index" method="get">
            		<div id="DataTables_Table_0_length" class="dataTables_length"><label>显示
			            <select name="num" size="1" aria-controls="DataTables_Table_0">
				            <option  num="10" @if(!empty($all['num'])&&$all['num']==10)
									selected
							   @endif
				            >10</option>
				            <option  num="25" @if(!empty($all['num'])&&$all['num']==25)
													selected
											   @endif>25</option>
				            <option  num="50" @if(!empty($all['num'])&&$all['num']==50)
													selected
											   @endif
				            >50</option>
				            <option　num="100" @if(!empty($all['num'])&&$all['num'])
													selected
											   @endif
						    >100</option>
		            	</select> 条数据</label>
	            	</div>
		            <div class="dataTables_filter" id="DataTables_Table_0_filter">
		            <label for="search" style="cursor:pointer">搜索: 
		            	<input type="search" name="search" id="search" aria-controls="DataTables_Table_0"  autocomplete="off" placeholder="请搜索商品名称"
							@if(!empty($row['search']))
								value="{{$row['search']}}"
							@endif
		            	>
		            </label>
		            <button class="btn btn-primary">搜索</button>
		            </div>
            	</form>
	           <table class="mws-datatable mws-table dataTable" id="DataTables_Table_0" aria-describedby="DataTables_Table_0_info">
                <thead>
                    <tr role="row">
	                    <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 141px;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">ID</th>
	                    <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 193px;" aria-label="Browser: activate to sort column ascending">商品id</th>
	                    <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 179px;" aria-label="Platform(s): activate to sort column ascending">商品图片1</th>
	                     <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 179px;" aria-label="Platform(s): activate to sort column ascending">商品图片2</th>
	                    <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 120px;" aria-label="Engine version: activate to sort column ascending">商品图片3</th>
	                    
	                    <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 86px;" aria-label="CSS grade: activate to sort column ascending">操作</th>
                    </tr>
                </thead>
                
	            <tbody role="alert" aria-live="polite" aria-relevant="all">
		            	@foreach($row as $key=> $value)

		            	dd($value);
				            	<tr class="
								@if($value->id%2==0)
								even
								@else
								odd
								@endif
		            		">
		            			 <td class="  sorting_1">{{$value ->id}}</td>
		                        <td class="  sorting_1">{{$value ->goods_id}}</td>
		                      

		                        
		                        <td class=" ">
					          	<img src="{{$value->images1}}" alt="" width="70">
					          	</td>

					          	<td class=" ">
					          	<img src="{{$value->images2}}" alt="" width="70">
					          	</td>

					          	<td class=" ">
					          	<img src="{{$value->images3}}" alt="" width="70">
					          	</td>

		                        <td class="except">
			                        <!-- 编辑和删除两个图标 -->
			                        <a href="/admin/goodsimages/edit?id={{$value->id}}"  style='color:black'>
			                        	<i class="icon-pencil" style="font-size:18px;color:#666;"></i>
			                        </a>&nbsp;&nbsp;&nbsp;

			                        <a href="/admin/goodsimages/delete?id={{$value->id}}" 	style="font-size:18px;color:#666;">
			                        	<i class="icon-remove"></i>
			                        </a>
		                        </td>
		            		</tr>
		            	@endforeach
	            </tbody>
            </table>

            <div class="dataTables_paginate paging_full_numbers" id="pages">
		       {!! $row->appends($request)->render()!!}    
		    </div>

            </div>
        </div>
    </div>

@endsection