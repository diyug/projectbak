@extends("layout.index")

@section('seven','close')

@section("title","商品图片添加")

@section("content")
	<div class="mws-panel grid_8">
	<div class="mws-panel-header">
    	<span>商品图片添加</span>
    </div>
    <div class="mws-panel-body no-padding">

      <!--   @if (count($errors) > 0)
		   <div class="mws-form-message error">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif -->

    	<form action="/admin/goodsimages/insert" method='post' class="mws-form" enctype="multipart/form-data">
    		<div class="mws-form-inline">
    			<div class="mws-form-row">
    				<label class="mws-form-label">商品id:</label>
    				<div class="mws-form-item">
    					<input type="text" name='goods_id'class="small">
    				</div>
    			</div>
              {{ csrf_field() }}
             
                <div class="mws-form-row">
                    <label class="mws-form-label">商品图片1:</label>
                    <div class="mws-form-item">
                        <input type="file" name='images1' class="small">
                    </div>
                </div>

                 <div class="mws-form-row">
                    <label class="mws-form-label">商品图片2:</label>
                    <div class="mws-form-item">
                        <input type="file" name='images2' class="small">
                    </div>
                </div>

                 <div class="mws-form-row">
                    <label class="mws-form-label">商品图片3:</label>
                    <div class="mws-form-item">
                        <input type="file" name='images3' class="small">
                    </div>
                </div>



              
    		<div class="mws-button-row">
    			
    			<input type="submit" class="btn btn-danger" value="提交">
    			<input type="reset" class="btn " value="重置">
    		</div>
    	</form>
    </div>    	
</div>
@endsection