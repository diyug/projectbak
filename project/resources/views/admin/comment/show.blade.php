@extends('layout.index')

@section('title','商品评论展示')

@section('eight','placeholder')

@section('content')
	<div class="mws-panel grid_8">
  <div class="mws-panel-header">
    <span>评论列表</span>
  </div>
  <div class="mws-panel-body no-padding">
    <div role="grid" class="dataTables_wrapper" id="DataTables_Table_1_wrapper">
     <form action="/admin/comment/show" method="get"> 
      <div id="DataTables_Table_1_length" class="dataTables_length">
        <label>当前显示
          <select name="num" size="1" aria-controls="DataTables_Table_1" id="change">
            <option  num="10" @if(!empty($request['num'])&&$request['num']==10)
									selected
							   @endif
            >10</option>
            <option  num="25" @if(!empty($request['num'])&&$request['num']==25)
									selected
							   @endif>25</option>
            <option  num="50" @if(!empty($request['num'])&&$request['num']==50)
									selected
							   @endif
            >50</option>
            <option　num="100" @if(!empty($request['num'])&&$request['num'])
									selected
							   @endif
		    >100</option></select>　条信息</label>
      </div>
      <div class="dataTables_filter" id="DataTables_Table_1_filter">
        <label>关键字:
          <input type="text" aria-controls="DataTables_Table_1" name="gid" 			   @if(empty($request['gid']))
          				placeholder="请输入商品ID
						@else
						value="{{$request['gid']}}"
						@endif	
          	"></label>	
         <button class="btn btn-success">搜索</button> 
      </div>
      </form>
      <script type="text/javascript">
      sel= document.getElementById('change');
	  var get_account=document.getElementsByName('gid')[0];
		gid=get_account.value   	
      sel.onchange=function(){
      	val=this.value;
      	location.href='/admin/comment/show?num='+val+'&gid='+gid;     
      }
      </script>
      <table class="mws-datatable-fn mws-table dataTable" id="DataTables_Table_1" aria-describedby="DataTables_Table_1_info">
        <thead>
          <tr role="row">
            <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" style="width:100px;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">ID</th>
            <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" style="width: width:100px;;" aria-label="Browser: activate to sort column ascending">商品ID</th>
             <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" style="width:width:100px;;" aria-label="Browser: activate to sort column ascending">用户ID</th>
            <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" style="width:width:100px;;" aria-label="Platform(s): activate to sort column ascending">商品评分</th>
            <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" style="width:100px;" aria-label="Engine version: activate to sort column ascending">评论内容</th>
            <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" style="width:100px;" aria-label="CSS grade: activate to sort column ascending">评论时间</th>
             <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" style="width:100px;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">操作</th>
            </tr>

        </thead>
        <tbody role="alert" aria-live="polite" aria-relevant="all">
          
          @foreach($row as $k=>$v)
	          <tr class="@if($k%2==0)
							odd
						@else
							even
						@endif	
	          ">
	            <td class="  sorting_1">{{$v->id}}</td>
	            <td class=" ">{{$v->gid}}</td>
	            <td class=" ">{{$v->uid}}</td>
	            <td class=" ">@for($v->score; $v->score>0; $v->score--)
                                <i class="icol-heart"></i>
                            @endfor
              </td>
              <td class=" ">{{$v->comment}}</td>
	            <td class=" ">{{date('Y-m-d H:i:s',$v->comment_time)}}</td>
	            <td>　
	            <a href="/admin/comment/disabled?id={{$v->id}}" style="font-size:20px;color:black;" title="禁止前台展示"><i class="icol-delete"></i></a>　　
	            <a href="/admin/comment/delete?id={{$v->id}}" style="font-size:20px;color:black;" title="删除评论"><i class="icol-cross"></i></a>
	            </td>
	          </tr>
	      @endforeach
        </tbody>
      </table>
        <div class="dataTables_paginate paging_full_numbers" id="pages">
		 	@if(isset($request))
		 	  {!! $row->appends(['gid' =>$request['gid']])
		 	  			->appends(['num' =>$request['num']])
		 	  ->render() !!}
			@else
          		{!! $row->render() !!}
      @endif
    	</div>
  </div>
</div>
@endsection