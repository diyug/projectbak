<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--><html lang="en"><!--<![endif]-->
<head>
<meta charset="utf-8">

<!-- Viewport Metatag -->
<meta name="viewport" content="width=device-width,initial-scale=1.0">

<!-- Required Stylesheets -->
<link rel="stylesheet" type="text/css" href="/back/bootstrap/css/bootstrap.min.css" media="screen">
<link rel="stylesheet" type="text/css" href="/back/css/fonts/ptsans/stylesheet.css" media="screen">
<link rel="stylesheet" type="text/css" href="/back/css/fonts/icomoon/style.css" media="screen">

<link rel="stylesheet" type="text/css" href="/back/css/login.css" media="screen">

<link rel="stylesheet" type="text/css" href="/back/css/mws-theme.css" media="screen">

<title>优购之家</title>

</head>
<style type="text/css">
    #mws-login-wrapper{
        
        margin-top:-120px; 
        display:none;
    }
    #ug_login_main{    
        width:300px;
        height:300px;       
        position:absolute;
        left:36%;
        top:28%;
    }
    #ug_login_main button{

        position:absolute;
        top:190px;
        left:50%;
    }
   
    #ug_warning{
            position:absolute;
            font-size:50px;
            width:400px;
            height:50px;
            line-height:50px;
            text-align:center;        
            top:100px;
            left:33%;
    }  


    #ug_bg_fly{
        position:absolute;
        right:0px;
        top:20px;
        border-radius:5%;
        width:80px;
        height:43px;
        background-image: url(/back/images/core/bg/fly.jpg);
    } 
    #ug_clock{
        height:50px;
        width:300px;
        position:absolute;
        left: 36%; 
        color:red;
        font-size:20px;
         font-weight:600;
        display:none;
        text-align:center;
        line-height:50px;
    }
    #ug_message_error{
        width:243px;
        height:15px;
        position:absolute;
        left:39%;
        top:24%;
    }
   
</style>
<body>
    <div id="ug_clock"></div>
    <div id="ug_bg_fly"> 

    </div> 
     <div id="ug_warning"><i>优购后台管理系统</i></div>

    @if (session('error'))
       <div id="ug_message_error" class="mws-form-message error" >
                                
                                <ul>
                                    <li>{{session('error')}}</li>
                                </ul>
        </div>
     @endif    
    <div id='ug_login_main'>       
        　<button class="btn btn-info">登录</but　ton>
    </div>
     
    <div id="mws-login-wrapper">
        <div id="mws-login">

            <h1>登录</h1>

            <div class="mws-login-lock"><i class="icon-lock"></i></div>
            <div id="mws-login-form">

                    <form class="mws-form" action="/admin/doaction" method="post">
                    
                    <div class="mws-form-row">
                        <div class="mws-form-item">
                            <input type="text" name="account" class="mws-login-username required" placeholder="账户名">
                        </div>
                    </div>
                    <div class="mws-form-row">
                        <div class="mws-form-item">
                            <input type="password" name="password" class="mws-login-password required" placeholder=密码>
                        </div>
                    </div>
                    <div id="mws-login-remember" class="mws-form-row mws-inset">
                        <ul class="mws-form-list inline">
                            <li>
                                <input type="text" name="vcode" placeholder="请输入验证码">　
                                <img src="{{URL('/admin/vcode')}}" alt="" onclick="this.src=this.src+'?s'">
                            </li>
                        </ul>
                    </div>
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <div class="mws-form-row">
                        <input type="submit" value="登录" class="btn btn-success mws-login-button">
                    </div>
                </form>
            </div>
        </div>
    </div>
   
    <!-- JavaScript Plugins -->
    <script src="/back/js/libs/jquery-1.8.3.min.js"></script>
    <script src="/back/js/libs/jquery.placeholder.min.js"></script>
    <script src="/back/custom-plugins/fileinput.js"></script>
    
    <!-- jQuery-UI Dependent Scripts -->
    <script src="/back/jui/js/jquery-ui-effects.min.js"></script>

    <!-- Plugin Scripts -->
    <script src="/back/plugins/validate/jquery.validate-min.js"></script>

    <!-- Login Script -->
    <script src="/back/js/core/login.js"></script>
    <script type="text/javascript">


       var but=$('#ug_login_main').find('button').eq(0);
       var div=$('#mws-login-wrapper'); 

      but.click(function() {

        but.parent().hide('normal',function(){

       div.slideDown('slow');

        });
      });

        setInterval(function(){
                    function getNowFormatDate() {
                    var date = new Date();
                    var seperator1 = "-";
                    var seperator2 = ":";
                   

                    if (date.getMonth()>=0 &&  date.getMonth()<=9) {
                        month = "0" + (date.getMonth()+1);
                    }else{
                        month = date.getMonth() + 1;
                    }

                    if ( date.getDate()<= 9) {

                        strDate = "0" + date.getDate();

                    }else{
                         strDate=date.getDate();
                    }

                    if (date.getSeconds()<=9) {

                        strSec = "0" + date.getSeconds();

                    }else{

                        strSec = date.getSeconds();
                    };
                    if(date.getMinutes()<=9){

                        strMin = "0" + date.getMinutes();
        
                    }else{

                        strMin=date.getMinutes();
                    }
                    if(date.getHours()<=9){

                        StrHour='0'+date.getHours();

                    }else{

                        StrHour=date.getHours();
                    }
                    var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate
                            + " " +StrHour+ seperator2 + strMin
                            + seperator2 + strSec;
                    return currentdate;
                } 



              var str=getNowFormatDate();
                 $('#ug_clock').html(str);

        },1000);

           $('#ug_bg_fly').click(function(){
                  $(this).animate({ 
                    right:'47%',
                    
                  },3000,function(){
                    $(this).delay(1000);
                     $("#ug_bg_fly").animate({ 
                       right:'94%',
                        
                      }, 3000,function(){                
                        $('#ug_clock').show();
                      } );
                  });
           });

           $('#ug_message_error').click(function() {
               
               $(this).hide();
           
           });  


            $('#ug_bg_fly').trigger('click');

                 
    </script>
</body>
</html>
