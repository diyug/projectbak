@extends("layout.index")

@section("title","编辑元素信息")

@section("four","placeholder")

@section("content")
	
	<div class="mws-panel grid_8">
		<div class="mws-panel-header">
	    	<span>编辑元素</span>
	    </div>
	    <div class="mws-panel-body no-padding">
	    	<form action="/admin/Carousel/update" method="post" class="mws-form">

		    	<!-- 传token的隐藏域 -->
		    	{{ csrf_field() }}

		    	<!-- 发送ID的隐藏域 -->
		    	<input type="hidden" name="id" value="{{$data -> id}}">

		    	<!-- 发送排位次序的隐藏域 -->
		    	<input type="hidden" name="oldpicline" value="{{$data -> picline}}">

				<div class="mws-form-row">
	    		<img src="/home/carousel/{{$data->picname}}" class="img-responsive" width="380">
				</div>

				<!-- 单选 -->
				<div class="mws-form-row">
					<label class="mws-form-label">图片排位</label>
					<div class="mws-form-item clearfix">
						<ul class="mws-form-list">
							<li style="float:left;margin-left:10px">
							<input type="radio" name="picline" value="0"
							
							@if($data -> picline==0)
								checked
							@endif
							> <label>待定</label></li>

							@for($i=1;$i<=6;$i++)
							<li style="float:left;margin-left:10px">
							<input type="radio" name="picline" value="{{$i}}"
							@if($data -> picline == $i)
								checked
							@endif
							> <label>第{{$i}}张</label></li>
							@endfor

						</ul>
					</div>
				</div>

	    			<!-- 分类下拉列表 -->
	    			<div class="mws-form-row">
	    				<label class="mws-form-label">所属分类</label>
	    				<div class="mws-form-item">
	    					<select name="cid">

								@foreach($cates as $key=>$value)
	    						<option value="{{$value->id}}"
	    						@if($data -> cid == $value -> id)
									selected
								@endif
	    						>{{$value->name}}</option>
	    						@endforeach

	    					</select>
	    				</div>
	    			</div>
				
	    		<div class="mws-button-row">
	    			<input type="submit" class="btn btn-primary" value="修改">
	    		</div>

	    	</div>

	    	</form>
	    </div>
	</div>

@endsection