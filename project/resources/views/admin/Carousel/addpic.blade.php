@extends("layout.index")

@section("title","添加轮播图元素")

@section("four","placeholder")

@section("content")
	
	<div class="mws-panel grid_8">
		<div class="mws-panel-header">
	    	<span>添加元素</span>
	    </div>
	    <div class="mws-panel-body no-padding">
	    	<form action="/admin/Carousel/insertpic" method="post" class="mws-form" enctype="multipart/form-data">
	    	<!-- 传token的隐藏域 -->
	    	{{ csrf_field() }}

				<!-- 单选 -->
				<div class="mws-form-row">
					<label class="mws-form-label">图片排位</label>
					<div class="mws-form-item clearfix">
						<ul class="mws-form-list">
						
							@for($i=1;$i<=6;$i++)
							<li style="float:left;margin-left:10px">
							<input type="radio" name="picline" value="{{$i}}"> <label>第{{$i}}张</label></li>
							@endfor

						</ul>
					</div>
				</div>
					
					<!-- 上传图片 -->
	    			<div class="mws-form-row" style="width:200px">
					    <label for="files" class="mws-form-label">图片元素</label>
					    <input type="file" name="picname" id="files">
					</div>

	    			<!-- 选择分类下拉列表 -->
	    			<div class="mws-form-row">
	    				<label class="mws-form-label">所属分类</label>
	    				<div class="mws-form-item">
	    					<select name="cid">

								@foreach($cates as $key=>$value)
	    						<option value="{{$value->id}}">{{$value->name}}</option>
	    						@endforeach

	    					</select>
	    				</div>
	    			</div>
				
	    		<div class="mws-button-row">
	    			<input type="submit" class="btn btn-primary" value="添加">
	    		</div>
	    	</div>
	    	</form>
	    </div>
	</div>

@endsection