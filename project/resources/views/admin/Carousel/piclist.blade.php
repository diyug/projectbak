@extends("layout.index")

@section("four","placeholder")

@section("title","图片清单")

@section("content")

	<div class="mws-panel grid_8">
  <div class="mws-panel-header">
    <span>
      <i class="icon-table"></i>图片清单</span>
  </div>
  <div class="mws-panel-body no-padding">
    <div role="grid" class="dataTables_wrapper" id="DataTables_Table_1_wrapper">
      <div id="DataTables_Table_1_length" class="dataTables_length">
        <label>显示
                <select name="num" size="1" aria-controls="DataTables_Table_1">
                <option value="6" selected="selected"
                    @if(!empty($all['num']) && $all['num']==6)
                      selected
                    @endif
                >6</option>
                <option value="12"
                    @if(!empty($all['num']) && $all['num']==12)
                      selected
                    @endif
                >12</option>
                <option value="24"
                    @if(!empty($all['num']) && $all['num']==24)
                      selected
                    @endif
                >24</option>
                <option value="48"
                    @if(!empty($all['num']) && $all['num']==48)
                      selected
                    @endif
                >48</option>
              </select>条数据
            </label>
          </div>
          <div class="dataTables_filter" id="DataTables_Table_1_filter">
            <label>搜索:
              <input type="text" aria-controls="DataTables_Table_1" name="search" autocomplete="off"
                @if(!empty($all['search']))
                  value="{{$all['search']}}"
                @endif
              placeholder="图片排位号"></label>
      </div>
      <table class="mws-datatable-fn mws-table dataTable" id="DataTables_Table_1" aria-describedby="DataTables_Table_1_info">
        <thead>
          <tr role="row">
            <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" style="width: 141px;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
            ID</th>
            <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" style="width: 193px;" aria-label="Browser: activate to sort column ascending">
            分类ID</th>
            <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" style="width: 179px;" aria-label="Platform(s): activate to sort column ascending">
            图片</th>
            <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" style="width: 120px;" aria-label="Engine version: activate to sort column ascending">
            图片描述</th>
            <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" style="width: 86px;" aria-label="CSS grade: activate to sort column ascending">
            排位</th>
            <th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" style="width: 86px;" aria-label="CSS grade: activate to sort column ascending">
            操作</th>
          </tr>
        </thead>

        <tbody role="alert" aria-live="polite" aria-relevant="all">
        	@foreach($data as $key => $value)
	        <tr class="
           @if($key%2==0)
            odd
           @else
            even
           @endif
          ">
	            <td class="sorting_1" align="center">{{$value->id}}</td>
	            <td align="center">{{$value->cid}}</td>
	            <td align="center"><img src="{{$path.$value->picname}}" width="190" alt="" class="lightense" data-background="rgba(0, 10, 45, .6)"></td>
              <td align="center">{{$value->describe}}</td>
	            <td align="center">{{$value->picline}}</td>
	            <td>
					<!-- 编辑和删除两个图标 -->
                <a href="/admin/Carousel/edit?id={{$value->id}}" style="font-size:18px;color:#666;">
                	<i class="icon-pencil"></i>
                </a>

                <a href="/admin/Carousel/del?id={{$value->id}}" style="font-size:18px;color:#666;margin-left:30px">
                	<i class="icon-remove"></i>
                </a>
	            </td>
	        </tr>

	          @endforeach
        </tbody>
      </table>
      <div class="dataTables_paginate paging_full_numbers" id="pages">
           {!! $data->appends($all)->render()!!}    
      </div>
  </div>
</div>

<script type="text/javascript">
    // 下来列表发生改变触发事件
    var num = document.getElementsByName('num')[0];
    var search = document.getElementsByName('search')[0];  
      num.onchange=function(){
         var send_num=this.value;
         var send_search = search.value
        location.href='/admin/Carousel/piclist?num='+send_num+'&search='+send_search;
      }
</script>

<script type="text/javascript">
    window.addEventListener('load', function () {
      var el = document.querySelectorAll('img.lightense');
      Lightense(el);
    }, false);  
  </script>

@endsection