@extends("layout.index")

@section("title","分类编辑")

@section("two","placeholder")

@section("content")
	<div class="mws-panel grid_8">
		<div class="mws-panel-header">
	    	<span>分类编辑</span>
	    </div>
	    <div class="mws-panel-body no-padding">
	    	<form action="/admin/Cate/update" class="mws-form" method="post">
	    		<!-- 隐藏域传token -->
			    {{csrf_field()}}
			    <!-- 传id的隐藏域 -->
			    <input type="hidden" name="id" value="{{$data -> id}}">
	    		<div class="mws-form-inline">
	    			<div class="mws-form-row">
	    				<label class="mws-form-label" for="catename">分类名称</label>
	    				<div class="mws-form-item">
	    					<input type="text" class="small" name="name" id="catename" autocomplete="off" value="{{$data->name}}">
	    				</div>
	    			</div>
					
	    			<div class="mws-form-row">
	    				<label class="mws-form-label" for="parentname">分类状态</label>
	    				<div class="mws-form-item">
	    					<select class="small" name="status" id="parentname">
							        <option value="0"
								        @if($data->status == 0)
											selected
										@endif	
							        >下架</option>
							        <option value="1"
										@if($data->status == 1)
											selected
										@endif	
							        >上架</option>
	    					</select>
	    				</div>
	    			</div>

	    			<div class="mws-form-row">
	    				<label class="mws-form-label" for="parentname">父级分类</label>
	    				<div class="mws-form-item">
	    					<select class="small" name="pid" id="parentname">
	    						<option value="0">顶级分类</option>
	    						@foreach($all as $key => $value)
							        <option value="{{$value->id}}"
							        @if($data -> pid == $value -> id)
										selected
									@endif								
							        >{{$value->name}}</option>
	    						@endforeach
	    					</select>
	    				</div>
	    			</div>

	    			<div class="mws-form-row" style="width:280px">
	    				<label class="mws-form-label" for="catename">是否寄存</label>
	    				<div class="mws-form-item" style="width:270px">
    					<input type="radio" name="false[]" value="1" id="big_"
    					@if($data -> false===1)
    						checked
    					@endif)> <label for="big_">是</label>

    					<input type="radio" name="false[]" value="0" id="big_" style="margin-left:10px"
						@if($data -> false===0)
    						checked
    					@endif
    					> <label for="big_">否</label>
					</div>

	    		</div>
	    		<div class="mws-button-row">
	    			<input type="submit" class="btn btn-primary" value="修改">
	    			<input type="reset" class="btn btn-info" value="重置" style="margin-left:20px">
	    		</div>
	    	</form>
	    </div>    	
	</div>
@endsection