@extends("layout.index")

@section("two","placeholder")

@section("title","分类添加")

@section("content")
	<div class="mws-panel grid_8">
		<div class="mws-panel-header">
	    	<span>分类添加</span>
	    </div>
	    <div class="mws-panel-body no-padding">
	    	<form action="/admin/Cate/insert" class="mws-form" method="post">
			    {{csrf_field()}}
	    		<div class="mws-form-inline">
	    			<div class="mws-form-row">
	    				<label class="mws-form-label" for="catename">分类名称</label>
	    				<div class="mws-form-item">
	    					<input type="text" class="small" name="name" id="catename" autocomplete="off">
	    				</div>
	    			</div>

	    			<div class="mws-form-row">
	    				<label class="mws-form-label" for="parentname">父级分类</label>
	    				<div class="mws-form-item">
	    					<select class="small" name="pid" id="parentname">
	    						<option value="0">顶级分类</option>
	    						@foreach($data as $key => $value)
							        <option value="{{$value->id}}">{{$value->name}}</option>
	    						@endforeach
	    					</select>
	    				</div>
	    			</div>

	    			<div class="mws-form-row" style="width:280px">
	    				<label class="mws-form-label" for="catename">是否寄存</label>
	    				<div class="mws-form-item" style="width:270px">
    					<input type="radio" name="false[]" value="1" id="big_"> <label for="big_">是</label>

    					<input type="radio" name="false[]" value="0" id="big_" style="margin-left:10px"> <label for="big_">否</label>
					</div>

	    		</div>
	    		<div class="mws-button-row">
	    			<input type="submit" class="btn btn-primary" value="添加">
	    			<input type="reset" class="btn btn-info" value="重置" style="margin-left:20px">
	    		</div>
	    	</form>
	    </div>    	
	</div>
@endsection