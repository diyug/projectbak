<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>密码找回</title>
</head>
<body>
	 	
		亲爱的 {{$account}}<br>

		我们收到您重置密码的请求，如果是您提交的请求，请点击以下链接，完成密码重置：<br>

		http://www.gama.com/home/User/emailback?id={{$id}}&&_token={{$_token}}
		重置密码时需要用到的验证码为：{{$num}}( 请注意大小写 )<br>

		基于安全考虑，以上链接及验证码2小时内有效，24小时内最多允许找回5次密码。<br>

		如果不是您提交的请求，请忽略此邮件。 请放心，您的账户是安全的。<br>
		如果您有其他问题，可访问帮助中心或联系我们的在线咨询。<br>


	 
</body>
</html>