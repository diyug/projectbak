<!DOCTYPE html>
<html><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache,no-store, must-revalidate">
<meta http-equiv="expires" content="0">
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>添加收藏成功的页面</title>

<link href="/home/css/uc.css" type="text/css" rel="stylesheet">
<link href="/home/css/base.css" type="text/css" rel="stylesheet">
<link href="/home/css/shopcar_v4.css" type="text/css" rel="stylesheet">
<script src="cart_files/v.htm" charset="utf-8"></script><script src="cart_files/hm.txt"></script><script src="cart_files/ga.js" async="" type="text/javascript"></script><script type="text/javascript" src="cart_files/jquery-1.js"></script>
<script type="text/javascript" src="cart_files/jquery.js"></script>
<script type="text/javascript" src="cart_files/MD5.js"></script>
<script type="text/javascript" src="cart_files/ygdialog.js"></script><link href="/home/css/ygdialog.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
    var basePath = "";
    var shoppingProcess=true;
</script>
<script type="text/javascript" src="cart_files/yg.js"></script>
<script type="text/javascript" src="cart_files/payOnline.js"></script>
<script type="text/javascript" src="cart_files/yg_002.js"></script>
<!-- 链接bootstrop -->
<link rel="stylesheet" href="/home/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="/home/bootstrap/css/bootstrap-theme.min.css">
<script type="text/javascript" src="/home/bootstrap/js/jquery.js"></script>
<script type="text/javascript" src="/home/bootstrap/js/bootstrap.min.js"></script>
<!-- 结束bootstrop -->
</head>
<body>
<!--
-->
    <!--header start-->
    <!-- top nav bar created time: 2014-11-28 18:34:02-->
<div id="top_nav">
    <div class="view_area clearfix">
        <div class="yg link_box"><a href="http://www.yougou.com/#ref=all&amp;po=logo_yougou">时尚商城</a></div>
        
        <div class="phone link_box">
            <a href="http://www.yougou.com/topics/mobile.html" class="phone_text"><i class="mobile_ico"></i>手机优购<i class="tip"></i></a>
            <div class="phone_con">
                    <p class="clearfix">
                        <span class="fl qr_code">
                        </span>
                        <span class="fl ml10">
                            <a class="btn_app_store btn" href="http://itunes.apple.com/cn/app/zhang-shang-you-gou/id504493912?mt=8" target="_blank">App Store</a>
                            <a class="btn_android_store btn" href="http://mobile.yougou.com/appVersion/package.sc?channelCode=YgYougouwebA59" target="_blank">Android</a>
                        </span>
                    </p>
                    <p class="qr_code_tip">下载安装 <strong>优购客户端</strong></p>
                </div>
        </div>
        <div class="outlets link_box" style="border-right:none"></div>
        <div class="fr">
        <div class="about_user">您好！<span id="user_id"></span><a class="quit" href="http://www.yougou.com/logout.jhtml">退出</a></div><!--about_user end -->
        <div class="my_yg link_box">
            <a href="http://www.yougou.com/my/ucindex.jhtml?t=14738211617475593" class="a1">我的优购</a>
            <ul class="info_con">
                <li><a href="http://www.yougou.com/my/favorites.jhtml?t=147382116174810428">我的收藏</a></li>
                <li style="display: list-item;" id="commentcount"><a href="http://www.yougou.com/my/comment.jhtml?t=14738211622166423">等待点评(<i class="corg">0</i>)</a></li>
                <li style="display: list-item;" id="top_msg"><a href="http://www.yougou.com/my/msg.jhtml?t=14738211620485898">站内消息(<i class="corg">0</i>)</a></li>
            </ul>
        </div>
        <div class="my_order link_box"><a href="http://www.yougou.com/my/order.jhtml?t=14738211617487175" rel="nofollow">我的订单</a></div>
        <div class="notice link_box">
            <span class="notice_text">公告</span>
            <ul class="notice_con">
                    <li><a target="_blank" href="http://www.yougou.com/topics/1416561897997.html#ref=index&amp;po=notice_notice1">运动新风尚 新品5折起</a></li>
                    <li><a target="_blank" href="http://www.yougou.com/topics/1415597386968.html#ref=index&amp;po=notice_notice2">摩登男装 秋冬大促 1折起</a></li>
                    <li><a target="_blank" href="http://www.yougou.com/topics/1415587130097.html#ref=index&amp;po=notice_notice3">潮靴秀美腿 价比11.11</a></li>
                    <li><a target="_blank" href="http://www.yougou.com/topics/1415605960629.html#ref=index&amp;po=notice_notice4">女装初冬热促 爆款2折起</a></li>
                    <li><a target="_blank" href="http://www.yougou.com/article/201411/87dc5ccf633611e4b7eea30f61b97b3f.shtml#ref=index&amp;po=notice_notice5">库房发货时间调整说明</a></li>
            </ul>
        </div>
        <div class="more link_box">
            <a href="javascript:;" class="more_text">更多</a>
            <ul class="more_con">
                <li><a onclick="YouGou.Biz.WebToolkit.addFavorite();" href="javascript:;">收藏优购</a></li>
                <li><a href="http://www.yougou.com/help/help.html">帮助</a></li>
            </ul>
        </div>
        </div>
    </div><!--view_area end -->
</div><!--top_nav end -->

    <!--header end-->
    
    <div class="cen cart_header">
        <h1 class="logo"><a href="http://www.yougou.com/" title=""><img src="/home/images/logo_n.jpg" height="50px" 2alt=""></a></h1>
        <div class="fr cart_step mt20">
            <ul>
                <li class="current"><em></em><i></i></li>
                <li><em></em><i></i></li>
                <li><em></em><i></i></li>
            </ul>
        </div>
    </div>

    <span class="none">
        <input id="addProductMaxNum" value="2" type="hidden">
        <input id="limitCartAddProductMaxNum" value="100" type="hidden">
        <input id="orderNum" value="0" type="hidden">
        <input id="isLock" value="false" type="hidden">
        <input id="limitOrderNum" value="40" type="hidden">
        <input id="limitOrderCommBuyMaxNum" value="40" type="hidden">
        <input id="isKrwFreeDuty" value="false" type="hidden">
    </span>
    

<!-- 若所有购物车均为空提示一下内容   -->
    <!-- 普通购物车 -->
 <div id="shoppingCartContainerCNY">
        <div class="cen mt10 cart_null_div" style="height:300px;">
        <div class="container">
            <div><img src="/home/images/shoucang.png" style="width:350px;float:right;margin-right:200px;margin-top:-30px;"></div>
            <div style="width:400px;">
              <span class="glyphicon glyphicon-send" aria-hidden="true"></span>
        
          <h2 ><img src="/home/images/right_icon.png" width='30px'>&nbsp;&nbsp;商品已加入您的收藏中 !</h2><br />
            <a href="/my_ug/collect/jia" class="btn btn-success btn-lg">瞧一瞧收藏</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="/home/list" class="btn btn-info btn-lg" style="width:150px;">继续购物</a>
            <br />
            <br />
            <!-- <h5>倒计时5秒后自动跳转到详情页</h5> -->
        </div>
           </div>
        </div>
    </div>
    <!-- 首尔站购物车 -->
      
    
        <div id="shoppingCartContainerKRW_ZF"></div>
    
        <div id="shoppingCartContainerHKD"></div>
        <script type="text/javascript">
            checkProductNum();
            function checkProductNum(){
                var addProductNum=$('#addProductNum').val();
                var limitCartAddProductMaxNum=$('#limitCartAddProductMaxNum').val();
                if(addProductNum!='' && parseInt(addProductNum)>parseInt(limitCartAddProductMaxNum) && 0 != parseInt(limitCartAddProductMaxNum)){
                    //alert("购物车最多存放"+limitCartAddProductMaxNum+"件商品，请编辑购物车!");
                    //return false;
                    ygDialog({ 
                        title:'提示',
                        content:'<br/><br/><h2 class="Red"><center>购物车最多存放'+limitCartAddProductMaxNum+'件商品，请编辑购物车!</center></h2>',
                        width:300,
                        height:120,
                        lock:true
                    });
                }
            }
         </script>      
            <div class="clearfix cen mt15">
                <div class="fr cgray cart_btinfo">
                    <p class="cart_bz">
                        购物保障：<i class="icon_zp"></i>100%正品<i class="icon_thh"></i>10天退换货<i class="icon_bcj"></i>10天补差价
                    </p>
                    <p>优惠券/礼品卡在下一步使用</p>
                    <p><a href="http://www.yougou.com/shoppingcart/shoppingcart_advise.jhtml" target="_blank" class="cgray">帮助我们改进购物流程</a></p>
                </div>
            </div>
            
            <!--凑单提醒start-->
            <div class="cen uc_main mt30">
                <h3 class="f16 c6 f_yahei"></h3>
                <div class="cart_recpro">
                    <div class="uc_slide uc_cnlike rel zdtx">
                        <div class="slide_hd"><h2></h2></div>
                        <div class="slide_bd">
                            <a class="slide_lf abs" href="javascript:void(0);"></a>
                            <a class="slide_rt abs" href="javascript:void(0);"></a>
                            <p class="slide_page abs"></p>
                            <div class="slide_bd_c rel">
                                <ul class="uc_hpro_lst abs clearfix">
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--凑单提醒end-->
            <!--加价购推荐 start-->
            <div class="cen uc_main mt30">
                <h3 class="f16 c6 f_yahei"></h3>
                <div class="cart_recpro">
                    <div class="uc_slide uc_cnlike rel jjg">
                        <div class="slide_hd"><h2></h2></div>
                        <div class="slide_bd">
                            <a class="slide_lf abs" href="javascript:void(0);"></a>
                            <a class="slide_rt abs" href="javascript:void(0);"></a>
                            <p class="slide_page abs"></p>
                            <div class="slide_bd_c rel">
                                <ul class="uc_hpro_lst abs clearfix">
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--//加价购推荐end>
            <!--//再买就满足活动 start--> 

            <div class="cen uc_main mt30">
                <h3 class="f16 c6 f_yahei"></h3>
                    <div class="cart_recpro">
                        <div class="uc_slide uc_cnlike rel zmjmz0">
                            <input class="loa" value="0" type="hidden">
                            <div class="slide_hd"><h2></h2></div>
                            <div class="slide_bd">
                                <a class="slide_lf abs" href="javascript:void(0);"></a>
                                <a class="slide_rt abs" href="javascript:void(0);"></a>
                                <p class="slide_page abs"></p>
                                <div class="slide_bd_c rel">
                                    <ul class="uc_hpro_lst abs clearfix">
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
             <div class="cen uc_main mt30">
                <h3 class="f16 c6 f_yahei"></h3>
                    <div class="cart_recpro">
                        <div class="uc_slide uc_cnlike rel zmjmz1">
                            <input class="loa" value="0" type="hidden">
                            <div class="slide_hd"><h2></h2></div>
                            <div class="slide_bd">
                                <a class="slide_lf abs" href="javascript:void(0);"></a>
                                <a class="slide_rt abs" href="javascript:void(0);"></a>
                                <p class="slide_page abs"></p>
                                <div class="slide_bd_c rel">
                                    <ul class="uc_hpro_lst abs clearfix">
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
             <div class="cen uc_main mt30">
                <h3 class="f16 c6 f_yahei"></h3>
                    <div class="cart_recpro">
                        <div class="uc_slide uc_cnlike rel zmjmz2">
                            <input class="loa" value="0" type="hidden">
                            <div class="slide_hd"><h2></h2></div>
                            <div class="slide_bd">
                                <a class="slide_lf abs" href="javascript:void(0);"></a>
                                <a class="slide_rt abs" href="javascript:void(0);"></a>
                                <p class="slide_page abs"></p>
                                <div class="slide_bd_c rel">
                                    <ul class="uc_hpro_lst abs clearfix">
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
             <div class="cen uc_main mt30">
                <h3 class="f16 c6 f_yahei"></h3>
                    <div class="cart_recpro">
                        <div class="uc_slide uc_cnlike rel zmjmz3">
                            <input class="loa" value="0" type="hidden">
                            <div class="slide_hd"><h2></h2></div>
                            <div class="slide_bd">
                                <a class="slide_lf abs" href="javascript:void(0);"></a>
                                <a class="slide_rt abs" href="javascript:void(0);"></a>
                                <p class="slide_page abs"></p>
                                <div class="slide_bd_c rel">
                                    <ul class="uc_hpro_lst abs clearfix">
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
             <div class="cen uc_main mt30">
                <h3 class="f16 c6 f_yahei"></h3>
                    <div class="cart_recpro">
                        <div class="uc_slide uc_cnlike rel zmjmz4">
                            <input class="loa" value="0" type="hidden">
                            <div class="slide_hd"><h2></h2></div>
                            <div class="slide_bd">
                                <a class="slide_lf abs" href="javascript:void(0);"></a>
                                <a class="slide_rt abs" href="javascript:void(0);"></a>
                                <p class="slide_page abs"></p>
                                <div class="slide_bd_c rel">
                                    <ul class="uc_hpro_lst abs clearfix">
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <!--//再买就满足活动end>   
            
            <!--//根据购物车商品推荐 start-->
               
            
            <!--//根据购物车商品推荐end>

            <!--//您最近收藏的商品start-->
                 <div class="cen uc_main mt30">
                     <h3 class="f16 c6 f_yahei"></h3>
                         <div class="cart_recpro">
                             <div class="uc_slide uc_cnlike rel zjsc">
                                 <div class="slide_hd"><h2></h2></div>
                                 <div class="slide_bd">
                                     <a class="slide_lf abs" href="javascript:void(0);"></a>
                                     <a class="slide_rt abs" href="javascript:void(0);"></a>
                                     <p class="slide_page abs"></p>
                                     <div class="slide_bd_c rel">
                                         <ul class="uc_hpro_lst abs clearfix">
                                         </ul>
                                 </div>
                             </div>
                         </div>
                     </div>
                </div>

            <!--//您最近收藏的商品end-->    
            
            <!--最近浏览过的商品startt-->
            
            <!--//最近浏览过的商品end-->
            
            <!--底部start-->
            <div class="cgray cen footer">
                    <span class="pr10">
                        Copyright © 2011-2014 Yougou Technology Co., Ltd. 
                    </span>
                    <span class="pr10">
                        <a href="http://www.miibeian.gov.cn/" target="_blank">粤ICP备09070608号-4</a>
                    </span>
                    <span class="pr10">
                        增值电信业务经营许可证：<a href="http://www.miibeian.gov.cn/" target="_blank">粤 B2-20090203</a>
                    </span>
            </div>
            <!--//底部end-->
        <!--//问卷调查-->   


        <script type="text/javascript" src="cart_files/yg_003.js"></script>

        <script type="text/javascript">
            $(function(){
                getActiveRecommendList();
                getSimilarRecommendList();
                //16-03-29 轮播页
                var $ul = $('.ul1');
                var $li = $ul.find('li');
                $ul.css('width',$li.outerWidth()*$li.length);
                
                uc.proLfSlide(".zjcs");
                uc.proLfSlide(".zjll");
                uc.proLfSlide(".cdsp");
                uc.proLfSlide(".cjhd",9,9);
                uc.proLfSlide(".mjhd",9,9);     
                uc.proLfSlide(".gwcgm",5,5);
                
            });
        </script>
            
        <script type="text/javascript">
            //google
         var _gaq = _gaq || [];
          _gaq.push(['_setAccount', 'UA-23566531-1']);
          _gaq.push(['_setDomainName', '.yougou.com']);
          _gaq.push(['_addOrganic', 'baidu', 'word']);
          _gaq.push(['_addOrganic', 'soso', 'w']);
          _gaq.push(['_addOrganic', '3721', 'name']);
          _gaq.push(['_addOrganic', 'yodao', 'q']);
          _gaq.push(['_addOrganic', 'vnet', 'kw']);
          _gaq.push(['_addOrganic', 'sogou', 'query']);
          _gaq.push(['_addOrganic', '360', 'q']);
          _gaq.push(['_addOrganic', 'so', 'q']);
          _gaq.push(['_trackPageview']);
          _gaq.push(['_trackPageLoadTime']);

        (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://analytic' : 'http://analytic') + '.yougou.com/ga.js?4.3.3';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
        </script>
        <!-- Vizury数据(跟踪代码) -->
        <script type="text/javascript" src="cart_files/mv.js"></script>
        <!-- baidu marketing -->
        <script>
        var _hmt = _hmt || [];
        (function() {
          var hm = document.createElement("script");
          hm.src = "//hm.baidu.com/hm.js?bc66790de6f87c591da5936f04e03efb";
          var s = document.getElementsByTagName("script")[0]; 
          s.parentNode.insertBefore(hm, s);
        })();
        </script>
        <!-- Google Code for &#21152;&#20837;&#36141;&#29289;&#36710; Remarketing List -->

        <!-- Google Code Parameters -->
        <script type="text/javascript">
        var google_tag_params = {
            ecomm_prodid: '100481252,100428717',
            brand:'',
            firstCategoryName:'',
            subCategoryName:'',
            thirdCategoryName:'',
            ecomm_pagetype:'cart',
            webType:'yg',
            ecomm_totalvalue: 0
        };
        </script>
        <!-- Google Code for Main List -->
        <!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. For instructions on adding this tag and more information on the above requirements, read the setup guide: google.com/ads/remarketingsetup -->
        <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 1016027598;
        var google_conversion_label = "189vCLqHowQQzrO95AM";
        var google_custom_params = window.google_tag_params;
        var google_remarketing_only = true;
        /* ]]> */
        </script>
        <script type="text/javascript" src="cart_files/conversion.js">
        </script><iframe name="google_conversion_frame" title="Google conversion frame" src="cart_files/a.htm" marginwidth="0" marginheight="0" vspace="0" hspace="0" allowtransparency="true" scrolling="no" frameborder="0" height="13" width="300"></iframe><iframe name="google_cookie_match_frame" title="Google cookie match frame" src="cart_files/pixel.htm" marginwidth="0" marginheight="0" vspace="0" hspace="0" allowtransparency="true" style="display:none" scrolling="no" frameborder="0" height="1" width="1"></iframe>
        <noscript>
        <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1016027598/?value=0&amp;label=189vCLqHowQQzrO95AM&amp;guid=ON&amp;script=0"/>
        </div>
        </noscript>


        <script type="text/javascript">
        <!-- 
        var bd_cpro_rtid="n1f4nf";
        //-->
        </script>
        <script type="text/javascript" src="cart_files/rt.js"></script>
        <noscript>
        <div style="display:none;">
        <img height="0" width="0" style="border-style:none;" src="http://eclick.baidu.com/rt.jpg?t=noscript&rtid=n1f4nf" />
        </div>
        </noscript>
        <!-- 好耶埋码 -->
        <script type="text/javascript">
          var idgJsHost = (("https:" == document.location.protocol) ? "https" : "http");
          var idgDomain = idgJsHost == "http" ? "1" : "2";
          document.write(unescape("%3Cscript src='" + idgJsHost + "://" + idgDomain + ".allyes.com.cn/idigger_yg.js' type='text/javascript'%3E%3C/script%3E"));
        </script><script src="cart_files/idigger_yg.htm" type="text/javascript"></script>
        <script type="text/javascript">
         var s={};
         //用户ID
            s.userid="c81b4b8f386b46bc94ce1b96d23110db";
            s.shopcart="100481252|100428717";
        </script>
        <script type="text/javascript">
        try {
          var idgTracker = _alysP.init("mso","T-000436-01", "");
          idgTracker._setTrackPath(idgJsHost + '://idigger.allyes.com/main/adftrack');
          idgTracker._setECM(s);
          idgTracker._trackPoint();
        } catch(err) {}
        </script><script type="text/javascript">
        var _mvq = _mvq || [];
        _mvq.push(['$setAccount', 'm-344-0']);
        _mvq.push(['$setGeneral', 'cartview', /*用户名*/'18803203602', /*用户id*/ 'c81b4b8f386b46bc94ce1b96d23110db']);
        _mvq.push(['$logConversion']);
        _mvq.push(['$addItem', '',/*商品id*/ '100481252,100428717','','','']);
        _mvq.push(['$logData']);
        </script>

        <!-- 百度DSP start-->

        <script type="text/javascript">
        var dsp_config = {
        commodities: [
            {no:'100481252005',price:'429.00',number:'2'},{no:'100428717001',price:'929.00',number:'1'}
        ],
        bd_list_type: 'ecom_cart'
        }
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                if (typeof dsp_config !== 'undefined') {
                    if (dsp_config['bd_list_type'] == 'ecom_reg') {
                        $("body").append($("<iframe style=\"display:none;width: 1px; border: 0px none; position: absolute; left: -100px; top: -100px; height: 1px;\" src=\"" + "/inc/code_iframe.shtml?url=registerEmailSucceed" + "\"></iframe>\""));
                    } else if (dsp_config['bd_list_type'] == 'ecom_cart') {
                        $("body").append($("<iframe style=\"display:none;width: 1px; border: 0px none; position: absolute; left: -100px; top: -100px; height: 1px;\" src=\"" + "/inc/code_iframe.shtml?url=order" + "\"></iframe>\""));
                    } else if (dsp_config['bd_list_type'] == 'ecom_order') {
                        $("body").append($("<iframe style=\"display:none;width: 1px; border: 0px none; position: absolute; left: -100px; top: -100px; height: 1px;\" src=\"" + "/inc/code_iframe.shtml?url=continueOrder" + "\"></iframe>\""));
                    } else if (dsp_config['bd_list_type'] == 'ecom_pay_submit') {
                        $("body").append($("<iframe style=\"display:none;width: 1px; border: 0px none; position: absolute; left: -100px; top: -100px; height: 1px;\" src=\"" + "/inc/code_iframe.shtml?url=payOnline" + "\"></iframe>\""));
                    } else if (dsp_config['bd_list_type'] == 'ecom_pay_offline') {
                        $("body").append($("<iframe style=\"display:none;width: 1px; border: 0px none; position: absolute; left: -100px; top: -100px; height: 1px;\" src=\"" + "/inc/code_iframe.shtml?url=createOrder" + "\"></iframe>\""));
                    } else if (dsp_config['bd_list_type'] == 'ecom_search') {
                        $("body").append($("<iframe style=\"display:none;width: 1px; border: 0px none; position: absolute; left: -100px; top: -100px; height: 1px;\" src=\"" + "/inc/code_iframe.shtml?url=search_list" + "\"></iframe>\""));
                    } else if(dsp_config['bd_list_type'] == 'ecom_view'){
                        $("body").append($("<iframe style=\"display:none;width: 1px; border: 0px none; position: absolute; left: -100px; top: -100px; height: 1px;\" src=\"" + "/inc/code_iframe.shtml?url=prod-csku"+ "\"></iframe>\""));
                    } else {
                        $("body").append($("<iframe style=\"display:none;width: 1px; border: 0px none; position: absolute; left: -100px; top: -100px; height: 1px;\" src=\"" + "/inc/code_iframe.shtml" + "\"></iframe>\""));
                    }
                }
            });
        </script> 
        <!-- 引入jquery -->
        <script type="text/javascript" src="/home/js/jquery-1.8.3.min.js"></script>
        <!-- 数量的加js -->
        <script type="text/javascript">
         // var i = 5;
         //  setInterval(function(){
         //    i--;
         //    if(i = 0){
         //       local.href='/home/cart/index';
         //    }
         //  },1000)


        // </script>
<!-- 百度DSP end-->



<div id="cartSelector" class="cart_selector_pop none"><b class="icon arr jsarr">&nbsp;</b><a href="javascript:;" class="icon close jsclose">&nbsp;</a><div class="bd" id="cartSelectorCon"><p class="loading">加载中，请稍后...</p></div></div><iframe style="display:none;width: 1px; border: 0px none; position: absolute; left: -100px; top: -100px; height: 1px;" src="cart_files/code_iframe.htm"></iframe></body></html>