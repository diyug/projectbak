<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache,no-store, must-revalidate">
<meta http-equiv="expires" content="0">
<title>我的收货地址</title>
<link href="/home/my_ug/base-2.css" type="text/css" rel="stylesheet">
<link href="/home/my_ug/ucenter_v2.css" type="text/css" rel="stylesheet">
<!-- 我引入的样式 -->
<link href="/home/myug_receipt.css" type="text/css" rel="stylesheet">
<link rel="shortcut icon" href="/home/images/favicon.ico">
<link href="/home/css/base.css" type="text/css" rel="stylesheet">
<link href="/home/css/channel.vs.css" type="text/css" rel="stylesheet">
<link href="/home/css/index.css" type="text/css" rel="stylesheet">
<link href="/home/css/index.css" type="text/css" rel="stylesheet">
<script src="/home/my_ug/ga.js" async="" type="text/javascript"></script><script type="text/javascript" src="/home/my_ug/jquery-1.js"></script>
<script type="text/javascript" src="/home/my_ug/yg.js"></script>
<script type="text/javascript" src="/home/my_ug/ygdialog.js"></script>
<link href="/home/my_ug/ygdialog.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/home/my_ug/yg_002.js"></script>
<!-- <!-- 省市县三级联动 -->
<script type="text/javascript" src="/home/my_ug/jquery.js"></script>
<script type="text/javascript" src="/home/my_ug/date_birthday.js"></script>
<script type="text/javascript" src="/home/my_ug/memberBasicinfo_editInfo.js"></script>
<script type="text/javascript" src="/home/my_ug/area.js"></script>
</head>
<body class="myinfo">
@include('/home_public/header')
<!--//公共头部end-->
<div class="blank10"></div>
<div class="cen">
  <input id="loginPassword" name="loginPassword" value="c385217e3a19850ca1329f3c9233cbc6" type="hidden">
  <p class="curLct">
    您当前的位置：<a href="/home/index">首页</a> &gt; 
    <a href="">我的优购</a> &gt; 个人资料
  </p>
<div class="u_leftxin u_leftxin fl mgr10" id="umenu">
  <div class="wdygtit"><a href=""><span>我的优购</span></a></div>
  <ul class="jiaoyizx">
      <li class="ultit">交易中心</li>
      <li class="myorderli"><a href=""><span>我的订单</span></a></li>
      <li class="myfavorli"><a href=""><span>我的收藏</span></a></li>
      <li class="mycommentli"><a href=""><span>商品评论/晒单</span></a></li>
        <li class="msgli"><a href=""><span id="uc_msg_count">站内消息<i class="huise">（<em>0</em>）</i></span></a></li>
    </ul>
    <ul class="wodezc">
      <li class="ultit">我的资产</li>
      <li class="mycouponli"><a href=""><span id="my_coupon_tick">我的优惠券</span></a></li>
        <li class="giftcardli"><a href=""><span id="my_giftcard_tick">我的礼品卡</span></a></li>
        <li class="mypointli"><a href="">我的积分</span></a></li>
    </ul>
  <ul class="gerensz">
      <li class="ultit">个人设置</li>
      <li class="myinfoli"><a href="/home/myug/ucenter"><span>个人资料</span></a></li>
        <li class="safesetli"><a href="/home/myug/security"><span id="uc_safe_level">安全设置</span></a></li>
        <li class="myaddrli"><a href="/home/myug/receipt"><span>我的收货地址</span></a></li>
    </ul>
    <ul class="shouhoufw">
      <li class="ultit">售后服务</li>
      <li class="afterservli"><a href=""><span>查看退换货</span></a></li>
        <li class="appservli"><a href=""><span>申请退换货</span></a></li>
    </ul>
<ul class="hotlist">
  <li class="hotlist_tit">
    <div>
      <p>24小时热销推荐</p>
    </div>
  </li>
  <li class="hotlist_dl">
    <dl>
      <dt>
        <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank">
          <img src="/home/images/100180914_01_s.jpg" title="adidas 阿迪达斯 运动 双肩包" alt="阿迪达斯 双肩包"></a>
      </dt>
      <dd>
        <p class="hotgoodsname">
          <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank" title="adidas 阿迪达斯 运动 双肩包">阿迪达斯 双肩包</a></p>
        <p class="hotgoodsprice">
          <span class="Red">￥269</span>
          <span class="Hui">￥269</span></p>
      </dd>
    </dl>
     <dl>
        <dt>
          <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank">
            <img src="/home/images/100180914_01_s.jpg" title="adidas 阿迪达斯 运动 双肩包" alt="阿迪达斯 双肩包"></a>
        </dt>
        <dd>
          <p class="hotgoodsname">
            <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank" title="adidas 阿迪达斯 运动 双肩包">阿迪达斯 双肩包</a></p>
          <p class="hotgoodsprice">
            <span class="Red">￥269</span>
            <span class="Hui">￥269</span></p>
        </dd>
    </dl>
     <dl>
      <dt>
        <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank">
          <img src="/home/images/100180914_01_s.jpg" title="adidas 阿迪达斯 运动 双肩包" alt="阿迪达斯 双肩包"></a>
      </dt>
      <dd>
        <p class="hotgoodsname">
          <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank" title="adidas 阿迪达斯 运动 双肩包">阿迪达斯 双肩包</a></p>
        <p class="hotgoodsprice">
          <span class="Red">￥269</span>
          <span class="Hui">￥269</span></p>
      </dd>
    </dl>
     <dl>
        <dt>
          <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank">
            <img src="/home/images/100180914_01_s.jpg" title="adidas 阿迪达斯 运动 双肩包" alt="阿迪达斯 双肩包"></a>
        </dt>
        <dd>
          <p class="hotgoodsname">
            <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank" title="adidas 阿迪达斯 运动 双肩包">阿迪达斯 双肩包</a></p>
          <p class="hotgoodsprice">
            <span class="Red">￥269</span>
            <span class="Hui">￥269</span></p>
        </dd>
    </dl>
     <dl>
      <dt>
        <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank">
          <img src="/home/images/100180914_01_s.jpg" title="adidas 阿迪达斯 运动 双肩包" alt="阿迪达斯 双肩包"></a>
      </dt>
      <dd>
        <p class="hotgoodsname">
          <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank" title="adidas 阿迪达斯 运动 双肩包">阿迪达斯 双肩包</a></p>
        <p class="hotgoodsprice">
          <span class="Red">￥269</span>
          <span class="Hui">￥269</span></p>
      </dd>
    </dl>
     <dl>
        <dt>
          <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank">
            <img src="/home/images/100180914_01_s.jpg" title="adidas 阿迪达斯 运动 双肩包" alt="阿迪达斯 双肩包"></a>
        </dt>
        <dd>
          <p class="hotgoodsname">
            <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank" title="adidas 阿迪达斯 运动 双肩包">阿迪达斯 双肩包</a></p>
          <p class="hotgoodsprice">
            <span class="Red">￥269</span>
            <span class="Hui">￥269</span></p>
        </dd>
    </dl>
  </li>
  <li class="last">
    <a class="hotrenovate" href="" onclick="">换一批</a></li>
</ul>


</div>
  <div class="u_right fl">
    <p class="tab_content">
      </p><form action="/home/myug/doreceipt" method="post" name="form1" id="form1">

      <ul class="tab_top"><li class="on">我的收货地址</li></ul>
      <ul class="tab_con">
        <table class="tab_tb">
          <tbody>
            <tr>
              <td style="border-top:none">
              <a id="newAddressBtn" class="myaddr_addbtn">+新增收货地址</a>
              <div id="addressFormDiv" class="myaddr_add" style="display:none;">
                        <form id="addressForm" name="addressForm" method="post">
              <p class="titt">
             
              <span id="closeFormBtn" class="cancel">X</span>
              </p>
              <div class="myaddr_addrow clearfix">
                  <p class="tit">所&nbsp;在&nbsp;地&nbsp;区</p>
                 <span id="district">
                  <div class="info">
                      <div style="width:500px;">
                      <select id="s_province" name="s_province"></select>  
                        <select id="s_city" name="s_city" ></select>  
                        <select id="s_county" name="s_county"></select>
                        <span  class="myaddr_tip" id="area_tip">　　　　　
                        </span>
                        <script class="resources library" src="area.js" type="text/javascript"></script>
                         
                        <script type="text/javascript">_init_area();</script>
                        </div>
                        <div id="show">
                        </div>
                        
                  </div>

                        <script type="text/javascript">
                        var Gid  = document.getElementById ;
                        var showArea = function(){
                          Gid('show').innerHTML = "<h3>省" + Gid('s_province').value + " - 市" +  
                          Gid('s_city').value + " - 县/区" + 
                          Gid('s_county').value + "</h3>"
                                      }
                        Gid('s_county').setAttribute('onchange','showArea()');
                        </script>
                 </span>
                
                </div>
             
                <div class="myaddr_addrow clearfix">
                  <p class="tit">详&nbsp;细&nbsp;地&nbsp;址</p><input type="text" name="receivingAddress" id="receivingAddress" class="myaddr_input error" placeholder="">
                  <span style="display: none;" id="receivingAddress_tip" class="myaddr_tip">请输入收货人地址，要求5-120个字符</span>
                </div>
                <div class="myaddr_addrow clearfix">
                  <p class="tit">收货人姓名</p><input type="text" id="receivingName" name="receivingName" class="myaddr_input error" style="width:102px">
                  <span style="display: none;" id="receivingName_tip" class="myaddr_tip">请输入收货人姓名，要求3-25个字符</span>
                </div>
                <div class="myaddr_addrow clearfix">
                  <p class="tit">收货人手机</p>
                  <input type="text" maxlength="11" name="receivingMobilePhone" id="receivingMobilePhone" class="myaddr_input error" style="width:136px" placeholder="">
                  <span style="display: none;" id="receivingMobilePhone_tip" class="myaddr_tip">请输入手机号码</span>
                </div>
                         {{csrf_field()}}
                   <div class="myaddr_addrow clearfix">
                  <input type="submit" style="border:0px;cursor:pointer;" id="saveBtn" class="save" value="保存">
                  <a href="javascript:void(0);" id="cancelBtn" class="cancel">取消</a>
                </div>
                </form>
                
              </div>
              <script type="text/javascript" src="/home/my_ug/ug_receipt_address.js"></script>
                @if(session('numerror'))
                 <div id="ug_message" style="background:white;height:200px;width:400px;position:fixed;top:43%;left:30%;border:4px solid #9D9D9D"> 
                 <input type="button" value="X" style="position:absolute;right:0px;width:40px;border:none;background:white;"/>
                  <div id="ug_message_small" style="margin-top:20%;">
                      <font size="3" color="#FF5000">每位用户添加的地址不能超过三条<br>您可以更改或者删除已有地址</font>  
                  </div>
                  </div>

                  <script type="text/javascript">
                       $(window).scroll(function(){
                            $(window).scrollTop(0);
                          });
                      $('input[type=button]').click(function() {
                        $('#ug_message').css('display','none');
                            $(window).unbind('scroll');
                      });

                  </script>
                @endif
              </td>
            </tr>
          </tbody>
          
        </table>
        @if(session('defaulterror'))
            <script type="text/javascript">
                alert('请先取消其他默认地址');
            </script>
        @endif
        <table cellspacing="0" cellpadding="0" style="border-collapse:collapse" id="tbl_address" class="myaddr_list">
                <tbody >
                      <tr class="">
                        <th width="136px">收货人</th>
                        <th width="150px">所在地区</th>
                        <th width="200px">详细地址</th>
                        <th width="142px">收货人手机</th>
                        <th width="100px"></th>
                        <th width="100px">操作</th>
                      </tr>
                @foreach($row as $k=>$v)
                      <tr >
                      <td>{{$v->receivingName}}</td>
                      <td>{{$v->s_province}} {{$v->s_city}} {{$v->s_county}}</td>
                      <td>{{$v->receivingAddress}}</td>
                      <td>{{substr_replace($v->receivingMobilePhone,"****",3,4)}}</td>
                      <td class="ug_default_address">
                      @if($v->status==0)
                          <a href="/home/myug/setdefault?id={{$v->id}}" style="display: none;" class="set_default">设为默认
                          </a>
                      @else
                          <a href="/home/myug/canceldefault?id={{$v->id}}" style="display: none;" class="cancel_default">取消默认
                          </a>
                      @endif
                      </td> 

                      <td><p class="cC0c0">
                      <a href="/home/myug/showupdate?id={{$v->id}}" class="myaddr_mdf">修改</a>|
                      <a href="/home/myug/dodelete?id={{$v->id}}" class="myaddr_mdf">删除</a></p></td>
                      </tr>
              @endforeach  
              <script type="text/javascript">
                          //设置默认地址
                    $('.ug_default_address').mousemove(function(){

                     $(this).find('a').css('display','block');
                    })
                     $('.ug_default_address').mouseout(function(){

                     $(this).find('a').css('display','none');
                    })
               </script>
            </tbody>
        </table>
      </ul>
    </form>
    <p></p>
  </div>
</div>
<p class="blank10"></p>


@include('/my_ug/ug_footer')
