<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache,no-store, must-revalidate">
<meta http-equiv="expires" content="0">
<title>我的收藏</title>
<link href="/home/my_ug/base-2.css" type="text/css" rel="stylesheet">
<link href="/home/my_ug/ucenter_v2.css" type="text/css" rel="stylesheet">
<link rel="shortcut icon" href="/home/images/favicon.ico">
<link href="/home/css/base.css" type="text/css" rel="stylesheet">
<link href="/home/css/channel.vs.css" type="text/css" rel="stylesheet">
<link href="/home/css/index.css" type="text/css" rel="stylesheet">
<script src="/home/my_ug/ga.js" async="" type="text/javascript"></script>
<script type="text/javascript" src="/home/my_ug/jquery-1.js"></script>
<script type="text/javascript" src="/home/my_ug/yg.js"></script>
<script type="text/javascript" src="/home/my_ug/ygdialog.js"></script>
<link href="/home/my_ug/ygdialog.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/home/my_ug/yg_002.js"></script>
<!-- 省市县三级联动 -->
 <script type="text/javascript" src="/home/my_ug/jquery.js"></script>
<script type="text/javascript" src="/home/my_ug/date_birthday.js"></script>
<script type="text/javascript" src="/home/my_ug/memberBasicinfo_editInfo.js"></script>
<script type="text/javascript" src="/home/my_ug/area.js"></script> 
</head>
<body class="myinfo">
@include('/home_public/header')
<!--//公共头部end-->
<div class="blank10"></div>
<div class="cen">
	<input id="loginPassword" name="loginPassword" value="c385217e3a19850ca1329f3c9233cbc6" type="hidden">
	<p class="curLct">
		您当前的位置：<a href="/home/index">首页</a> &gt; 
		<a href="http://www.yougou.com/my/ucindex.jhtml?t=14740399798957752">我的优购</a> &gt; 我的收藏
	</p>
<div class="u_leftxin u_leftxin fl mgr10" id="umenu">
	<div class="wdygtit"><a href="http://www.yougou.com/my/ucindex.jhtml?t=14740399798961534"><span>我的优购</span></a></div>
	<ul class="jiaoyizx">
    	<li class="ultit">交易中心</li>
    	<li class="myorderli"><a href="http://www.yougou.com/my/order.jhtml?t=14740399798966613"><span>我的订单</span></a></li>
    	<li class="myfavorli"><a href="/home/myug/collect" style="background:#EFEFEF"><span>我的收藏</span></a></li>
    	<li class="mycommentli"><a href="http://www.yougou.com/my/comment.jhtml?t=14740399798969024"><span>商品评论/晒单</span></a></li>
        <li class="msgli"><a href="http://www.yougou.com/my/msg.jhtml?t=14740399798968609"><span id="uc_msg_count">站内消息<i class="huise">（<em>0</em>）</i></span></a></li>
    </ul>
    <ul class="wodezc">
    	<li class="ultit">我的资产</li>
    	<li class="mycouponli"><a href="http://www.yougou.com/my/coupon.jhtml?couponState=1&amp;t=14740399798964797"><span id="my_coupon_tick">我的优惠券</span></a></li>
        <li class="giftcardli"><a href="http://www.yougou.com/my/giftcard.jhtml?couponState=1&amp;t=14740399798967658"><span id="my_giftcard_tick">我的礼品卡</span></a></li>
        <li class="mypointli"><a href="http://www.yougou.com/my/point.jhtml?t=14740399798967249"><span id="my_point_tick">我的积分</span></a></li>
    </ul>
	<ul class="gerensz">
    	<li class="ultit">个人设置</li>
    	<li class="myinfoli"><a href="/home/myug/ucenter" style="background:white;"><span>个人资料</span></a></li>
        <li class="safesetli"><a href="/home/myug/security"><span id="uc_safe_level">安全设置</span></a></li>
        <li class="myaddrli"><a href="/home/myug/receipt"><span>我的收货地址</span></a></li>
    </ul>
    <ul class="shouhoufw">
    	<li class="ultit">售后服务</li>
    	<li class="afterservli"><a href="http://www.yougou.com/my/afterService.jhtml?t=14740399798975220"><span>查看退换货</span></a></li>
        <li class="appservli"><a href="http://www.yougou.com/my/applicationService.jhtml?t=14740399798971719"><span>申请退换货</span></a></li>
    </ul>
<ul class="hotlist">
  <li class="hotlist_tit">
    <div>
      <p>24小时热销推荐</p>
    </div>
  </li>
  <li class="hotlist_dl">
    
     <dl>
        <dt>
          <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank">
            <img src="/home/images/100180914_01_s.jpg" title="adidas 阿迪达斯 运动 双肩包" alt="阿迪达斯 双肩包"></a>
        </dt>
        <dd>
          <p class="hotgoodsname">
            <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank" title="adidas 阿迪达斯 运动 双肩包">阿迪达斯 双肩包</a></p>
          <p class="hotgoodsprice">
            <span class="Red">￥269</span>
            <span class="Hui">￥269</span></p>
        </dd>
    </dl>
     <dl>
      <dt>
        <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank">
          <img src="/home/images/100180914_01_s.jpg" title="adidas 阿迪达斯 运动 双肩包" alt="阿迪达斯 双肩包"></a>
      </dt>
      <dd>
        <p class="hotgoodsname">
          <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank" title="adidas 阿迪达斯 运动 双肩包">阿迪达斯 双肩包</a></p>
        <p class="hotgoodsprice">
          <span class="Red">￥269</span>
          <span class="Hui">￥269</span></p>
      </dd>
    </dl>
     <dl>
        <dt>
          <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank">
            <img src="/home/images/100180914_01_s.jpg" title="adidas 阿迪达斯 运动 双肩包" alt="阿迪达斯 双肩包"></a>
        </dt>
        <dd>
          <p class="hotgoodsname">
            <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank" title="adidas 阿迪达斯 运动 双肩包">阿迪达斯 双肩包</a></p>
          <p class="hotgoodsprice">
            <span class="Red">￥269</span>
            <span class="Hui">￥269</span></p>
        </dd>
    </dl>
     <dl>
      <dt>
        <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank">
          <img src="/home/images/100180914_01_s.jpg" title="adidas 阿迪达斯 运动 双肩包" alt="阿迪达斯 双肩包"></a>
      </dt>
      <dd>
        <p class="hotgoodsname">
          <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank" title="adidas 阿迪达斯 运动 双肩包">阿迪达斯 双肩包</a></p>
        <p class="hotgoodsprice">
          <span class="Red">￥269</span>
          <span class="Hui">￥269</span></p>
      </dd>
    </dl>
     <dl>
        <dt>
          <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank">
            <img src="/home/images/100180914_01_s.jpg" title="adidas 阿迪达斯 运动 双肩包" alt="阿迪达斯 双肩包"></a>
        </dt>
        <dd>
          <p class="hotgoodsname">
            <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank" title="adidas 阿迪达斯 运动 双肩包">阿迪达斯 双肩包</a></p>
          <p class="hotgoodsprice">
            <span class="Red">￥269</span>
            <span class="Hui">￥269</span></p>
        </dd>
    </dl>
  </li>
  <li class="last">
    <a class="hotrenovate" href="javascript:void(0);" onclick="YGM.GlobeTip.hottop24r()">换一批</a>
      </li>
    </ul>
  </div>
	 <div class="uc_right fr">
      <div class="uc_tab" id="fav_prd">
        <a class="current a_choosed" href="http://www.yougou.com/my/favorites.jhtml?type=all&amp;t=14742033605613850">所有收藏<span class="numspan"></span></a>
      <a class="current zeronum" href="javascript:void(0);">已降价<span class="numspan">0</span></a>
      <a class="current zeronum" href="javascript:void(0);">正促销<span class="numspan">0</span></a>
      <a class="current zeronum" href="javascript:void(0);">即将断货<span class="numspan">0</span></a>
        </div>
        <!--商品收藏列表 start-->
        <div class="uc_fav_list">
            <ul class="uc_fav_header">
        <li class="pro_name">商品信息</li>
        <li class="price">价格</li>
        <li class="cz">操作</li>
      </ul>
            <ul class="uc_fav_c uc_goods_fav_c">
            <li class="li_fav clearfix">
              <span class="s_h fav_select">
                <a href="javascript:void(0);" dataid="e69afe0382164d90a204f9b30b4ce5e7" class="uc_ico u_d up" title="置顶">置顶</a>
                            <input name="p1" id="p1" value="e69afe0382164d90a204f9b30b4ce5e7" class="check_fav" type="checkbox">
              </span>
              <a href="http://www.yougou.com/c-adidas/sku-gty43-100488599.shtml?t=14742033605623607" target="_blank" class="s_h pro_img">
                          <img src="collect/100488599_01_s.jpg" alt="" height="60" width="60">
              </a>
              <div class="s_h fav_pro_c clearfix">
                <div class="s_h pro_info">
                  <p><a href="http://www.yougou.com/c-adidas/sku-gty43-100488599.shtml?t=147420336056210342" target="_blank">adidas阿迪达斯2016年新款男子BOOST系列篮球鞋AQ7761</a></p>  
                      <p>
                        <span class="cgray">
                          颜色：<em class="f_gray">绿色</em>&nbsp;&nbsp;
                          
                        </span>
                      </p>
                </div>
                <div class="s_h price_cs">
                  <p class="last_price">¥1199</p>
                </div>
                <div class="s_h cz">
                    <p class="cz1"><a class="shop_car uc_addTo_cart" href="javascript:void(0);" cid="e69afe0382164d90a204f9b30b4ce5e7" goodsno="100488599">移入购物车</a></p>
                  <p class="cz3"><a class="f_blue uc_delete_fav" dataid="e69afe0382164d90a204f9b30b4ce5e7" href="javascript:void(0);">取消收藏</a></p>
                </div>
                <div class="uc_buy_goods none"></div>
              </div>
            </li>
            <li class="li_fav clearfix">
              <span class="s_h fav_select">
                <a href="javascript:void(0);" dataid="4b96380f8db5410b9fadb9163f984f5d" class="uc_ico u_d up" title="置顶">置顶</a>
                            <input name="p1" id="p1" value="4b96380f8db5410b9fadb9163f984f5d" class="check_fav" type="checkbox">
              </span>
              <a href="http://www.yougou.com/c-nike/sku-843881-100469696.shtml?t=14742033605628254" target="_blank" class="s_h pro_img">
                          <img src="collect/100469696_01_s.jpg" alt="" height="60" width="60">
              </a>
              <div class="s_h fav_pro_c clearfix">
                <div class="s_h pro_info">
                  <p><a href="http://www.yougou.com/c-nike/sku-843881-100469696.shtml?t=14742033605637534" target="_blank">NIKE耐克2016年新款男子AIR RELENTLESS 6 MSL跑步鞋843881-001</a></p>  
                      <p>
                        <span class="cgray">
                          颜色：<em class="f_gray">黑/白/煤黑</em>&nbsp;&nbsp;
                          
                        </span>
                      </p>
                </div>
                <div class="s_h price_cs">
                  <p class="last_price">¥569</p>
                </div>
                <div class="s_h cz">
                    <p class="cz1"><a class="shop_car uc_addTo_cart" href="javascript:void(0);" cid="4b96380f8db5410b9fadb9163f984f5d" goodsno="100469696">移入购物车</a></p>
                  <p class="cz3"><a class="f_blue uc_delete_fav" dataid="4b96380f8db5410b9fadb9163f984f5d" href="javascript:void(0);">取消收藏</a></p>
                </div>
                <div class="uc_buy_goods none"></div>
              </div>
            </li>
            </ul>
            <div class="uc_page clearfix">
              <!-- 20条记录换页 -->
        <div id="paginator" class="paginator" style="display: block;">
          <p class="selall"><input id="selectall" type="checkbox">&nbsp;&nbsp;
            <label class="selectall">全选</label>&nbsp;&nbsp;
            <a href="javascript:void(0);" class="f_blue cancel_sc" id="cancel_sc">取消收藏</a>&nbsp;&nbsp;
            
          </p>

          <script>

            var formId = "queryForm";
            var totalRows = '2';
            var pageSize = 15;


            /**
            *检查是否含有财务千分位分隔符
            *当totalRows>1000时默认格式会加财务千分位逗号
            *例如11,628，在js当做字符串处理
            */
            function ck(txt){
             if(txt.indexOf(',')>-1){
              return true;
             }
             return false;
            }

            /**
            *以逗号进行字符串分割
            *返回去掉逗号的字符串
            *例如11,628->11628
            */
            function split(datastr){
              var arr= new Array();
              var str = "";
              arr=datastr.split(",");
                for (i=0;i<arr.length ;i++ )
                {
                    str+=arr[i];
                }
                return str;
            }
            /**
            *如果totalRows>=1000,则去除财务分隔符逗号
            *否则转换为数字类型
            */
            if(ck(totalRows)){
              totalRows = split(totalRows);
            }else{
              totalRows = Number(totalRows);
            }

            function queryPage(pageNo){

              if(isNaN(pageSize)){
                  alert('每页条数只能为数字');
                  return;
                }

                var totalPage=Math.ceil(totalRows/pageSize);
                var toPage=pageNo;
                if(pageNo==0){
                  toPage=document.getElementById("query.page").value;
                  if(isNaN(toPage) || toPage<=0){
                    alert("请输入大于0的整数.");
                    return;
                  }

                  if(toPage>totalPage){
                    alert("没有当前页数");
                    return;
                  }
                }

                //校验是跳转页是否在记录有效范围内
                //取大于等于总页数的值
                if(toPage>totalPage){
                  alert("已经到最后一页");
                  return;
                }


                var pageForm = (formId&&formId!="")?document.getElementById(formId):document.forms[0];

                var arr = pageForm.elements;
                var flag = false;
                for(var i=0,j=arr.length;i<j;i++){
                  if(arr[i].getAttribute("name")=="query.page"){
                    flag = true;
                    break;
                  }
                }
                var pageEle = document.getElementById("page");

                if(!flag && pageEle == null){
                  var artionUrl = pageForm.getAttribute("action");
          //        if(artionUrl.indexOf("?")>0){
          //          artionUrl = artionUrl + "&page="+toPage;
          //        }else{
          //          artionUrl = artionUrl + "?page="+toPage;
          //        }
          //        pageForm.setAttribute("action",artionUrl);

                  var pageInput = document.createElement("input");
                  pageInput.setAttribute("type", "hidden");
                  pageInput.setAttribute("name", "page");
                  pageInput.setAttribute("id", "page");
                  pageInput.setAttribute("value", toPage);
                  pageForm.appendChild(pageInput);

                }
                pageForm.submit();
            }

          </script>
    <div style="float:left;">总记录数：<font color="red">2</font></div>
  <div class="page">
  </div>
          <form action="/my/favorites.jhtml" method="GET" id="queryForm">
            <input value="14742033562851174" name="t" type="hidden">
            <input name="type" value="all" type="hidden">
          </form>
        </div>
            </div>
        </div>
    </div>
</div>
<p class="blank10"></p>
<!--调查问卷入口-->
   <!--footer created time: 2016-08-29T14:33:04+08:00-->
<!--Ntalker start-->
<script src="/home/my_ug/ntkf.js" id="ntkfjs" src1="http://s2.ygimg.cn/template/common/ntalker/ntkf.js?4.3.4" type="text/javascript">
</script>
<script type="text/javascript">
            
</script>

@include('/my_ug/ug_footer')