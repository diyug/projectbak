<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache,no-store, must-revalidate">
<meta http-equiv="expires" content="0">
<title>安全设置</title>
<link href="/home/my_ug/base-2.css" type="text/css" rel="stylesheet">
<link href="/home/my_ug/ucenter_v2.css" type="text/css" rel="stylesheet">
<script src="/home/my_ug/ga.js" async="" type="text/javascript"></script>
<script src="/home/my_ug/ga.js" async="" type="text/javascript"></script>
<script type="text/javascript" src="/home/my_ug/jquery-1.js"></script>
<script type="text/javascript" src="/home/my_ug/yg.js"></script>
<script type="text/javascript" src="/home/my_ug/ygdialog.js"></script><link href="/home/my_ug/ygdialog.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/home/my_ug/yg_002.js"></script>
</head>
<body class="safeset">
@include('/home_public/header')
<!--//公共头部end--><div class="blank07"></div>
<div class="cen">
	<p class="curLct">您当前的位置：<a target="blank" href="/home/index" class="f_blue">首页</a> &gt; <a href="http://www.yougou.com/my/ucindex.jhtml?t=14742047208188536" class="f_blue">我的优购</a> &gt; 安全设置</p>
	
	<!-- menu -->
<div class="u_leftxin u_leftxin fl mgr10" id="umenu">
	<div class="wdygtit"><a href="http://www.yougou.com/my/ucindex.jhtml?t=14742047208183381"><span>我的优购</span></a></div>

    <ul class="wodezc">
    	<li class="ultit">我的资产</li>
    	<li class="mycouponli"><a href="http://www.yougou.com/my/coupon.jhtml?couponState=1&amp;t=14742047208194968"><span id="my_coupon_tick">我的优惠券</span></a></li>
        <li class="giftcardli"><a href="http://www.yougou.com/my/giftcard.jhtml?couponState=1&amp;t=14742047208193554"><span id="my_giftcard_tick">我的礼品卡</span></a></li>
        <li class="mypointli"><a href="http://www.yougou.com/my/point.jhtml?t=14742047208195855"><span id="my_point_tick">我的积分</span></a></li>
    </ul>
	<ul class="gerensz">
    	<li class="ultit">个人设置</li>
    	<li class="myinfoli"><a href="/home/myug/ucenter"><span>个人资料</span></a></li>
        <li class="safesetli"><a href="/home/myug/security"><span id="uc_safe_level">安全设置</span></a></li>
        <li class="myaddrli"><a href="/home/myug/receipt"><span>我的收货地址</span></a></li>
    </ul>
    <ul class="shouhoufw">
    	<li class="ultit">售后服务</li>
    	<li class="afterservli"><a href="http://www.yougou.com/my/afterService.jhtml?t=14742047208197064"><span>查看退换货</span></a></li>
        <li class="appservli"><a href="http://www.yougou.com/my/applicationService.jhtml?t=14742047208195790"><span>申请退换货</span></a></li>
    </ul>
<ul class="hotlist">
  <li class="hotlist_tit">
    <div>
      <p>24小时热销推荐</p>
    </div>
  </li>
  <li class="hotlist_dl">
    <dl>
      <dt>
        <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank">
          <img src="/home/images/100180914_01_s.jpg" title="adidas 阿迪达斯 运动 双肩包" alt="阿迪达斯 双肩包"></a>
      </dt>
      <dd>
        <p class="hotgoodsname">
          <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank" title="adidas 阿迪达斯 运动 双肩包">阿迪达斯 双肩包</a></p>
        <p class="hotgoodsprice">
          <span class="Red">￥269</span>
          <span class="Hui">￥269</span></p>
      </dd>
    </dl>
     <dl>
        <dt>
          <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank">
            <img src="/home/images/100180914_01_s.jpg" title="adidas 阿迪达斯 运动 双肩包" alt="阿迪达斯 双肩包"></a>
        </dt>
        <dd>
          <p class="hotgoodsname">
            <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank" title="adidas 阿迪达斯 运动 双肩包">阿迪达斯 双肩包</a></p>
          <p class="hotgoodsprice">
            <span class="Red">￥269</span>
            <span class="Hui">￥269</span></p>
        </dd>
    </dl>
     <dl>
      <dt>
        <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank">
          <img src="/home/images/100180914_01_s.jpg" title="adidas 阿迪达斯 运动 双肩包" alt="阿迪达斯 双肩包"></a>
      </dt>
      <dd>
        <p class="hotgoodsname">
          <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank" title="adidas 阿迪达斯 运动 双肩包">阿迪达斯 双肩包</a></p>
        <p class="hotgoodsprice">
          <span class="Red">￥269</span>
          <span class="Hui">￥269</span></p>
      </dd>
    </dl>
     <dl>
        <dt>
          <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank">
            <img src="/home/images/100180914_01_s.jpg" title="adidas 阿迪达斯 运动 双肩包" alt="阿迪达斯 双肩包"></a>
        </dt>
        <dd>
          <p class="hotgoodsname">
            <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank" title="adidas 阿迪达斯 运动 双肩包">阿迪达斯 双肩包</a></p>
          <p class="hotgoodsprice">
            <span class="Red">￥269</span>
            <span class="Hui">￥269</span></p>
        </dd>
    </dl>
     <dl>
      <dt>
        <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank">
          <img src="/home/images/100180914_01_s.jpg" title="adidas 阿迪达斯 运动 双肩包" alt="阿迪达斯 双肩包"></a>
      </dt>
      <dd>
        <p class="hotgoodsname">
          <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank" title="adidas 阿迪达斯 运动 双肩包">阿迪达斯 双肩包</a></p>
        <p class="hotgoodsprice">
          <span class="Red">￥269</span>
          <span class="Hui">￥269</span></p>
      </dd>
    </dl>
     <dl>
        <dt>
          <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank">
            <img src="/home/images/100180914_01_s.jpg" title="adidas 阿迪达斯 运动 双肩包" alt="阿迪达斯 双肩包"></a>
        </dt>
        <dd>
          <p class="hotgoodsname">
            <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank" title="adidas 阿迪达斯 运动 双肩包">阿迪达斯 双肩包</a></p>
          <p class="hotgoodsprice">
            <span class="Red">￥269</span>
            <span class="Hui">￥269</span></p>
        </dd>
    </dl>
  </li>
  <li class="last">
    <a class="hotrenovate" href="" onclick="">换一批</a></li>
</ul>
</div>
<script type="text/javascript" src="/home/my_ug/bindcheck.js"></script>   
	<!-- menu end --> 
	
	<!-- right content -->
	<div class="u_right fl" id="safeset">
		<p class="ubtitle">
			<span class="Size14 fb">安全设置</span>
		</p>
		<div class="u_con2 u_safeSet">
            <dl class="u_safeCz">
            	<dd class="safeCz_degree">
                	<p class="blank8"></p>
                	<p style="margin-left:50px;"><img src="/home/my_ug/2016-09-19_102656.png" alt="" /></p>
                    
                </dd>
                <dd class="safeCz_name">登录密码</dd>
                	<dd class="safeCz_detail">安全性高的密码可以使账号更安全。建议您定期更换密码，且设置一个包含数字和字母，并长度超过6位以上的密码。</dd>
                	<dd class="safeCz_btn"><a href="/home/myug/securityone"><input type="button" value="修改密码" /></a></dd>
            </dl>
            <p class="blank15"></p>
    </div>
            <p class="blank15"></p>
    <div class="u_coupon_tips bindEmail_tips">
            <p class="tit">常见问题</p>
            <p class="bindEmail_qa">Q 为什么要进行安全设置？进行安全设置后有什么好处？<br><span>A 优购时尚商城账户涉及您的优惠券、礼品卡和积分资产，我们强烈建议您完善安全设置，以免造成不必要的损失；</span></p>
            <p class="bindEmail_qa">Q 安全设置完成之后可以修改吗？<br><span>A 您可以点击“修改”按钮进行修改。</span></p>
    </div>
	</div>
	<!-- right content end -->
    <div class="blank10"></div>
</div>

@include('/my_ug/ug_footer')