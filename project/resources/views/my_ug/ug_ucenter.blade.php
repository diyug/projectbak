<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache,no-store, must-revalidate">
<meta http-equiv="expires" content="0">
<title>个人资料</title>
<link href="/home/my_ug/base-2.css" type="text/css" rel="stylesheet">
<link href="/home/my_ug/ucenter_v2.css" type="text/css" rel="stylesheet">
<link rel="shortcut icon" href="/home/images/favicon.ico">
<link href="/home/css/base.css" type="text/css" rel="stylesheet">
<link href="/home/css/channel.vs.css" type="text/css" rel="stylesheet">
<link href="/home/css/index.css" type="text/css" rel="stylesheet">
<script src="/home/my_ug/ga.js" async="" type="text/javascript"></script>
<script type="text/javascript" src="/home/my_ug/jquery-1.js"></script>
<script type="text/javascript" src="/home/my_ug/yg.js"></script>
<script type="text/javascript" src="/home/my_ug/ygdialog.js"></script>
<link href="/home/my_ug/ygdialog.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/home/my_ug/yg_002.js"></script>
<!-- 省市县三级联动 -->
 <script type="text/javascript" src="/home/my_ug/jquery.js"></script>
<script type="text/javascript" src="/home/my_ug/date_birthday.js"></script>
<script type="text/javascript" src="/home/my_ug/memberBasicinfo_editInfo.js"></script>
<script type="text/javascript" src="/home/my_ug/area.js"></script> 
</head>
<body class="myinfo">
@include('/home_public/header')
<!--//公共头部end-->
<div class="blank10"></div>
<div class="cen">
	<input id="loginPassword" name="loginPassword" value="c385217e3a19850ca1329f3c9233cbc6" type="hidden">
	<p class="curLct">
		您当前的位置：<a href="/home/index">首页</a> &gt; 
		<a href="http://www.yougou.com/my/ucindex.jhtml?t=14740399798957752">我的优购</a> &gt; 个人资料
	</p>
<div class="u_leftxin u_leftxin fl mgr10" id="umenu">
	<div class="wdygtit"><a href="http://www.yougou.com/my/ucindex.jhtml?t=14740399798961534"><span>我的优购</span></a></div>
	<ul class="jiaoyizx">
    	<li class="ultit">交易中心</li>
    	<li class="myorderli"><a href="http://www.yougou.com/my/order.jhtml?t=14740399798966613"><span>我的订单</span></a></li>
    	<li class="myfavorli"><a href="http://www.yougou.com/my/favorites.jhtml?t=14740399798966769"><span>我的收藏</span></a></li>
    	<li class="mycommentli"><a href="http://www.yougou.com/my/comment.jhtml?t=14740399798969024"><span>商品评论/晒单</span></a></li>
        <li class="msgli"><a href="http://www.yougou.com/my/msg.jhtml?t=14740399798968609"><span id="uc_msg_count">站内消息<i class="huise">（<em>0</em>）</i></span></a></li>
    </ul>
    <ul class="wodezc">
    	<li class="ultit">我的资产</li>
    	<li class="mycouponli"><a href="http://www.yougou.com/my/coupon.jhtml?couponState=1&amp;t=14740399798964797"><span id="my_coupon_tick">我的优惠券</span></a></li>
        <li class="giftcardli"><a href="http://www.yougou.com/my/giftcard.jhtml?couponState=1&amp;t=14740399798967658"><span id="my_giftcard_tick">我的礼品卡</span></a></li>
        <li class="mypointli"><a href="http://www.yougou.com/my/point.jhtml?t=14740399798967249"><span id="my_point_tick">我的积分</span></a></li>
    </ul>
	<ul class="gerensz">
    	<li class="ultit">个人设置</li>
    	<li class="myinfoli"><a href="/home/myug/ucenter"><span>个人资料</span></a></li>
        <li class="safesetli"><a href="/home/myug/security"><span id="uc_safe_level">安全设置</span></a></li>
        <li class="myaddrli"><a href="/home/myug/receipt"><span>我的收货地址</span></a></li>
    </ul>
    <ul class="shouhoufw">
    	<li class="ultit">售后服务</li>
    	<li class="afterservli"><a href="http://www.yougou.com/my/afterService.jhtml?t=14740399798975220"><span>查看退换货</span></a></li>
        <li class="appservli"><a href="http://www.yougou.com/my/applicationService.jhtml?t=14740399798971719"><span>申请退换货</span></a></li>
    </ul>
<ul class="hotlist">
  <li class="hotlist_tit">
    <div>
      <p>24小时热销推荐</p>
    </div>
  </li>
  <li class="hotlist_dl">
    <dl>
      <dt>
        <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank">
          <img src="/home/images/100180914_01_s.jpg" title="adidas 阿迪达斯 运动 双肩包" alt="阿迪达斯 双肩包"></a>
      </dt>
      <dd>
        <p class="hotgoodsname">
          <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank" title="adidas 阿迪达斯 运动 双肩包">阿迪达斯 双肩包</a></p>
        <p class="hotgoodsprice">
          <span class="Red">￥269</span>
          <span class="Hui">￥269</span></p>
      </dd>
    </dl>
     <dl>
        <dt>
          <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank">
            <img src="/home/images/100180914_01_s.jpg" title="adidas 阿迪达斯 运动 双肩包" alt="阿迪达斯 双肩包"></a>
        </dt>
        <dd>
          <p class="hotgoodsname">
            <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank" title="adidas 阿迪达斯 运动 双肩包">阿迪达斯 双肩包</a></p>
          <p class="hotgoodsprice">
            <span class="Red">￥269</span>
            <span class="Hui">￥269</span></p>
        </dd>
    </dl>
     <dl>
      <dt>
        <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank">
          <img src="/home/images/100180914_01_s.jpg" title="adidas 阿迪达斯 运动 双肩包" alt="阿迪达斯 双肩包"></a>
      </dt>
      <dd>
        <p class="hotgoodsname">
          <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank" title="adidas 阿迪达斯 运动 双肩包">阿迪达斯 双肩包</a></p>
        <p class="hotgoodsprice">
          <span class="Red">￥269</span>
          <span class="Hui">￥269</span></p>
      </dd>
    </dl>
     <dl>
        <dt>
          <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank">
            <img src="/home/images/100180914_01_s.jpg" title="adidas 阿迪达斯 运动 双肩包" alt="阿迪达斯 双肩包"></a>
        </dt>
        <dd>
          <p class="hotgoodsname">
            <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank" title="adidas 阿迪达斯 运动 双肩包">阿迪达斯 双肩包</a></p>
          <p class="hotgoodsprice">
            <span class="Red">￥269</span>
            <span class="Hui">￥269</span></p>
        </dd>
    </dl>
     <dl>
      <dt>
        <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank">
          <img src="/home/images/100180914_01_s.jpg" title="adidas 阿迪达斯 运动 双肩包" alt="阿迪达斯 双肩包"></a>
      </dt>
      <dd>
        <p class="hotgoodsname">
          <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank" title="adidas 阿迪达斯 运动 双肩包">阿迪达斯 双肩包</a></p>
        <p class="hotgoodsprice">
          <span class="Red">￥269</span>
          <span class="Hui">￥269</span></p>
      </dd>
    </dl>
     <dl>
        <dt>
          <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank">
            <img src="/home/images/100180914_01_s.jpg" title="adidas 阿迪达斯 运动 双肩包" alt="阿迪达斯 双肩包"></a>
        </dt>
        <dd>
          <p class="hotgoodsname">
            <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank" title="adidas 阿迪达斯 运动 双肩包">阿迪达斯 双肩包</a></p>
          <p class="hotgoodsprice">
            <span class="Red">￥269</span>
            <span class="Hui">￥269</span></p>
        </dd>
    </dl>
  </li>
  <li class="last">
    <a class="hotrenovate" href="javascript:void(0);" onclick="YGM.GlobeTip.hottop24r()">换一批</a></li>
</ul>


</div>
	<div class="u_right fl">
		<p class="tab_content">
			</p><form action="/home/myug/doucenter" method="post" name="form1" id="form1">
			<ul class="tab_top"><li class="on">个人资料</li></ul>
			<ul class="tab_con">
				<table class="tab_tb">
					<tbody>
						<tr>
							<td style="border-top:none">
								<div class="address_unit address_add" style="float:left;width:764px">
									<div class="row">
										<label>邮箱:
                       
                    </label>
										<span>
                           @if(!empty($res['email']))
                            {{$res['email']}}
                           @endif          
                    </span>
									</div>
									<div class="row">
										<label>手机号码:
                       
                    </label>
										<span> @if(!empty($res['phone']))
                            {{$res['phone']}}
                           @endif
                    </span>
									</div>
									<div class="row">
										<label>会员积分：</label>
										<span>100</span>
										<span style="padding-left:28px"><a href="http://www.yougou.com/help/memberpoints.html?t=147403997983410201" class="f_blue" target="_blank">查看积分说明</a></span>
										<label>可用积分：</label>
										<span>100</span>
										<span style="padding-left:28px"><a href="http://www.yougou.com/my/point.jhtml?t=14740399798343152" class="f_blue" target="_blank">查询/兑换</a></span>
									</div>
									<div class="row">
										<label>真实姓名：</label>
										<span><input class="sinput" style="width:188px" name="username" id="memberName" value="{{$row_d->username}}"
                     maxlength="10" type="text"></span>
										<span id="memberName_tips"></span>
									</div>
									<div class="row">
										<label>性别：</label>
										<span>
											<input style="margin-top:0; padding-top:0" value="1" name="sex"
                      @if($row_d->sex ==1)
                      checked=checked
                      @endif 
                      type="radio">男
											<input style="margin-top:0; margin-left:10px; padding-top:0" value="2" 
                      @if($row_d->sex ==2)
                      checked=checked
                      @endif
                      name="sex" type="radio">女
							            </span>
									</div>
									<div class="row">
										<label>出生日期：</label>
					            <div class="collec">
  <span>
    <select id="year" name="year">
    @if(!empty($row_d->year))
      <option selected value="{{$row_d->year}}">{{$row_d->year}}</option>
    @endif
      <option value="2016" >2016</option>
      <option value="2015">2015</option>
      <option value="2014">2014</option>
      <option value="2013">2013</option>
      <option value="2012">2012</option>
      <option value="2011">2011</option>
      <option value="2010">2010</option>
      <option value="2009">2009</option>
      <option value="2008">2008</option>
      <option value="2007">2007</option>
      <option value="2006">2006</option>
      <option value="2005">2005</option>
      <option value="2004">2004</option>
      <option value="2003">2003</option>
      <option value="2002">2002</option>
      <option value="2001">2001</option>
      <option value="2000">2000</option>
      <option value="1999">1999</option>
      <option value="1998">1998</option>
      <option value="1997">1997</option>
      <option value="1996">1996</option>
      <option value="1995">1995</option>
      <option value="1994">1994</option>
      <option value="1993">1993</option>
      <option value="1992">1992</option>
      <option value="1991">1991</option>
      <option value="1990">1990</option>
      <option value="1989">1989</option>
      <option value="1988">1988</option>
      <option value="1987">1987</option>
      <option value="1986">1986</option>
      <option value="1985">1985</option>
      <option value="1984">1984</option>
      <option value="1983">1983</option>
      <option value="1982">1982</option>
      <option value="1981">1981</option>
      <option value="1980">1980</option>
      <option value="1979">1979</option>
      <option value="1978">1978</option>
      <option value="1977">1977</option>
      <option value="1976">1976</option>
      <option value="1975">1975</option>
      <option value="1974">1974</option>
      <option value="1973">1973</option>
      <option value="1972">1972</option>
      <option value="1971">1971</option>
      <option value="1970">1970</option>
      <option value="1969">1969</option>
      <option value="1968">1968</option>
      <option value="1967">1967</option>
      <option value="1966">1966</option>
  </select>
  </span>
  <span>年&nbsp;</span>
  <span>
    <select class="valid" id="month" name="month">
     @if(!empty($row_d->month))
      <option selected value="{{$row_d->month}}">{{$row_d->month}}</option>
    @endif
      <option value="01">01</option>
      <option value="02">02</option>
      <option value="03">03</option>
      <option value="04">04</option>
      <option value="05">05</option>
      <option value="06">06</option>
      <option value="07">07</option>
      <option value="08">08</option>
      <option value="09">09</option>
      <option value="10">10</option>
      <option value="11">11</option>
      <option value="12">12</option>
    </select>
  </span>
  <span>月&nbsp;</span>
  <span>
    <select id="date" name="date">
     @if(!empty($row_d->date))
      <option selected value="{{$row_d->date}}">{{$row_d->date}}</option>
      @endif
      <option value="01">01</option>
      <option value="02">02</option>
      <option value="03">03</option>
      <option value="04">04</option>
      <option value="05">05</option>
      <option value="06">06</option>
      <option value="07">07</option>
      <option value="08">08</option>
      <option value="09">09</option>
      <option value="10">10</option>
      <option value="11">11</option>
      <option value="12">12</option>
      <option value="13">13</option>
      <option value="14">14</option>
      <option value="15">15</option>
      <option value="16">16</option>
      <option value="17">17</option>
      <option value="18">18</option>
      <option value="19">19</option>
      <option value="20">20</option>
      <option value="21">21</option>
      <option value="22">22</option>
      <option value="23">23</option>
      <option value="24">24</option>
      <option value="25">25</option>
      <option value="26">26</option>
      <option value="27">27</option>
      <option value="28">28</option>
      <option value="29">29</option>
      <option value="30">30</option>
      <option value="31">31</option>
      </select>
  </span>

  <span>日&nbsp;</span></div>
									</div>
      <div class="row" id="district_span">
										<label>所在地区：</label>
                    <div style="width:460px;margin-bottom:10px;">
                            <div>
                            <select id="s_province" name="s_province">
                             
                            </select>  
                            <select id="s_city" name="s_city" >
                             
                            </select>  
                            <select id="s_county" name="s_county">
                             
                            </select>
                            <script class="resources library" src="area.js" type="text/javascript">
                            </script>
                              
                              <script type="text/javascript">_init_area();</script>
                              </div>
                              <div id="show">
                              </div>
                     </div>
                          <script type="text/javascript">
                          var Gid  = document.getElementById ;
                          var showArea = function(){
                            Gid('show').innerHTML = "<h3>省" + Gid('s_province').value + " - 市" +  
                            Gid('s_city').value + " - 县/区" + 
                            Gid('s_county').value + "</h3>"
                                        }
                          Gid('s_county').setAttribute('onchange','showArea()');
                          </script>
                    @if(!empty($row_d->s_province))
                        <script type="text/javascript">
                            $('#s_province').append("<option value='{{$row_d->s_province}}' selected>{{$row_d->s_province}}</option>");
                        </script>
									   @endif
                     @if(!empty($row_d->s_city))
                        <script type="text/javascript">
                            $('#s_city').append("<option value='{{$row_d->s_city}}' selected>{{$row_d->s_city}}</option>");
                        </script>
                     @endif
                     @if(!empty($row_d->s_county))
                        <script type="text/javascript">
                            $('#s_county').append("<option value='{{$row_d->s_county}}' selected>{{$row_d->s_county}}</option>");
                        </script>
                     @endif
									<div class="row">
										<label>详细地址：</label>
										<span><input class="sinput" style="width:323px" value="{{$row_d->address}}" name="address" id="address" type="text"></span>
										<span id="address_tips"></span>
									</div>
                  {{csrf_field()}}
									<div class="row">
										<label>邮政编码：</label>
										<span><input id="zipcode" class="sinput" style="width:91px" name="pc" maxlength="6" type="text" value="{{$row_d->pc}}"></span>
										<span id="zipcode_tips" width="200px"></span>
									</div>
									<div class="row">
                    <label></label>
                    <span>
                    <input  id="sub" type="submit" value="保存" style="width:80px;height:30px;background:#FCB73B;color:white;">
                    </span>
                
									</div>
			</div>
           
							</td>
						</tr>
					</tbody>
				</table>
			</ul>
		</form>
		<p></p>
	</div>
</div>
<p class="blank10"></p>
<!--调查问卷入口-->
   <!--footer created time: 2016-08-29T14:33:04+08:00-->
<!--Ntalker start-->
<script src="/home/my_ug/ntkf.js" id="ntkfjs" src1="http://s2.ygimg.cn/template/common/ntalker/ntkf.js?4.3.4" type="text/javascript">
</script>
<script type="text/javascript">
                     isok=true;
                    $('#zipcode').blur(function(){
                       var val=$(this).val();
                       pc_reg=/[1-9]\d{5}(?!\d)/;
                       pc_res=pc_reg.test(val);
                       if(pc_res)
                       {
                          isok=true;
                          $('#zipcode_tips').text('');
                          $('#zipcode_tips').attr('class','successHint');
                       }else{
                      
                          $('#zipcode_tips').text('邮箱格式不正确');
                          $('#zipcode_tips').attr('class','errorHint');
                          isok=false;
                       }

                    })
  $('#sub').click(function() {
          if(isok==false)
          {
            return false;
          }
          
        return true;
  });
</script>

@include('/my_ug/ug_footer')