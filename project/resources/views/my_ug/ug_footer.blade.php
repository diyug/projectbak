<div class="wcen footser">
  <div class="ygwrap">
    <ul class="hd">
      <li><i class="item1"></i><a href="" target="_blank" rel="nofollow"><em>正品</em>保证</a></li>
      <li><i class="item2"></i><a href="" target="_blank" rel="nofollow"><em>10天</em>退换货</a></li>
      <li><i class="item3"></i><a href="" target="_blank" rel="nofollow"><em>10天调价</em>补差额</a></li>
      <li><i class="item4"></i><a href="" target="_blank" rel="nofollow"><em>7X24小时</em>在线客服</a></li>
    </ul>
    <div id="n_help" class="clearfix">
      <dl>
        <dt>新手帮助</dt>
        <dd><a href="" target="_blank" rel="nofollow" >交易条款协议</a></dd>
        <dd><a href="" target="_blank" rel="nofollow" >注册新用户</a></dd>
        <dd><a href="" target="_blank" rel="nofollow" >会员积分详情</a></dd>
        <!-- <dd><a href="/help/memberlevel.shtml" target="_blank" rel="nofollow" >会员等级</a></dd> -->
      </dl>
      <dl>
        <dt>购物指南</dt>
        <dd><a href="" target="_blank" rel="nofollow" >订购流程</a></dd>
        <dd><a href="" target="_blank" rel="nofollow" >验货与签收</a></dd>
        <dd><a href="" target="_blank" rel="nofollow" >订单配送查询</a></dd>
      </dl>
      <dl>
        <dt>支付/配送</dt>
        <!--  <dd><a href="/help/payment.shtml" target="_blank" rel="nofollow" >支付说明</a></dd>-->
        <dd><a href="http://www.yougou.com/help/payonline.shtml" target="_blank" rel="nofollow" >支付方式</a></dd>
        <dd><a href="" target="_blank" rel="nofollow" >配送方式</a></dd>
        <dd><a href="" target="_blank" rel="nofollow" >配送时间及运费</a></dd>
      </dl>
      <dl>
        <dt>售后服务</dt>
        <dd><a href="" target="_blank" rel="nofollow" >退换货政策</a></dd>
        <!-- <dd><a href="/help/returnprocess.shtml" target="_blank" rel="nofollow" >退换货流程</a></dd> -->
        <dd><a href="" target="_blank" rel="nofollow" >退款说明</a></dd>
        <dd><a href="" target="_blank" rel="nofollow" >发票制度</a></dd>
        <!-- <dd><a href="http://www.yougou.com/help/referprocess.shtml" target="_blank" rel="nofollow" >售后服务咨询流程</a></dd>
        <dd><a href="http://www.yougou.com/help/priceprotection.shtml" target="_blank" rel="nofollow" >十天补差价</a></dd> -->
      </dl>
      <dl>
        <dt>会员服务</dt>
        <dd><a href="" target="_blank" rel="nofollow" >找回密码</a></dd>
        <!-- <dd><a href="http://www.yougou.com/help/sizes.shtml" target="_blank" rel="nofollow" >尺码选择</a></dd>
        <dd><a href="http://www.yougou.com/help/suggestions.shtml" target="_blank" rel="nofollow" >投诉和建议</a></dd>-->
        <dd><a href="" target="_blank" >联系我们</a></dd>
      </dl>
      <dl class="help">
        <dt>优购客服</dt>
        <dd class="kf tleft"><span>
        <a onclick="javascript:NTKF.im_openInPageChat();" class="lnk-ntalker" href="javascript:;">在线咨询</a>
        </span></dd>
        <dd>Email：<em class="Red Size12">service@yougou.com</em></dd>
        <dd>分享购QQ群：<em class="Red Size12">375298444</em></dd>
        <dd>分享购微信号：<em class="Red Size12">yougoufenxianggou</em></dd>
      </dl>
      <dl class="qrcode">
        <dd class="center"><a href="http://m.yougou.com/agent" target="_blank">
        <img src="/home/picture/footer-mobile-qrcode.jpg" width="114" height="103" /></a></dd>
        <dd class="tright">
        <img src="/home/picture/footer-wechat-qrcode.jpg" width="72" height="103" />
        </dd>
      </dl>
    </div>
  </div>
</div>

<div class="wcen n_footinfo">
  <div class="ygwrap">
    <div class="n_footl fl tleft" id="endlogo">
      <a href="http://www.yougou.com/" class="ba_logo" title="兄弟们做的项目" alt="兄弟们做的项目"><img src="/home/picture/blank.gif" title="兄弟们做的项目" alt="兄弟们做的项目" /></a>
    </div>
    <div class="n_footr fr f_white">
      <p class="tright">
        <a href="http://www.yougou.com/help/aboutus.shtml" target="_blank" >关于优购</a> |
        <a href="http://www.yougou.com/help/business.shtml" target="_blank" >品牌招商</a> |
        <a href="http://www.yougou.com/group_purchasing.shtml" target="_blank" >集团采购</a> |
        <a href="http://www.yougou.com/help/zhaopin.shtml" target="_blank">招贤纳士</a> |
        <a href="http://www.yougou.com/topics/mobile.shtml" target="_blank" >手机优购</a> |
        <a href="http://www.yougou.com/help/contactus.shtml" target="_blank" >联系我们</a> |
        <a target="_blank" href="http://www.yougou.com/brand_daqo.shtml">品牌大全</a> |
        <a href="http://www.yougou.com/sitemap.shtml" target="_blank">网站地图</a> |
        <a href="http://top.yougou.com" target="_blank">销售排行</a> |
        <a href="http://style.yougou.com/info.shtml" target="_blank">优购资讯</a> |
        <a href="http://cps.yougou.com" target="_blank">网站联盟</a> |
        <a href="http://www.yougou.com/friendlink.shtml" target="_blank" >友情链接</a>
        <!--<a href="/product_list/p1.shtml" target="_blank" >产品大全</a> -->
      </p>

      <p>
        Copyright &copy; 2011-2016 Yougou Technology Co., Ltd.
        <a href="http://www.miibeian.gov.cn" target="_blank" rel="nofollow">粤ICP备09070608号-4</a>
        增值电信业务经营许可证：
        <a href="http://www.miibeian.gov.cn" target="_blank" rel="nofollow">粤
        B2-20090203</a>&nbsp;<span>深公网安备：4403101910665 </span>
        <a target="_blank" href="http://www.beian.gov.cn/portal/registerSystemInfo?recordcode=44030502000017" style="text-decoration:none;height:20px;line-height:20px;"><img src="/home/picture/bei_an_tu_biao_.png"/><span style="height:20px;line-height:20px;margin: 0px 0px 0px 5px; color:#FFFFFF;">粤公网安备 44030502000017号</span></a>
      </p>
      <p class="tright beian">
        <a class="ba_link2" rel="nofollow" target="_blank" href="http://61.144.227.239:9002/"><img src="/home/images/beian2.png"></a>
        <a class="ba_link1" rel="nofollow" target="_blank" href=""><img src="/home/images/beian1.png"></a>
        
        <a rel="nofollow" title="众信网" target="_blank" class="ba_link2" href="">
          <img width="108" height="40" src="/home/images/ebs-logo.jpg">
        </a>
        <a target="_blank" key="521b3d2524306332d3107ff3" logo_size="124x47" logo_type="realname" href="" style="width:108px; height:40px;">
          <img width="124" height="47" alt="安全联盟认证" style="border: medium none;" src="/home/picture/sm_124x47.png">
        </a>
        <a rel="nofollow" title="众信网" target="_blank" class="ba_link2" href="">
          <img width="108" height="40" src="/home/images/ebs.png">
        </a>
      </p>
    </div>
  </div>
</div>       	


</html>      

