<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache,no-store, must-revalidate">
<meta http-equiv="expires" content="0">
<title>修改收货地址</title>
<link href="/home/my_ug/base-2.css" type="text/css" rel="stylesheet">
<link href="/home/my_ug/ucenter_v2.css" type="text/css" rel="stylesheet">
<link rel="shortcut icon" href="/home/images/favicon.ico">
<link href="/home/css/base.css" type="text/css" rel="stylesheet">
<link href="/home/css/channel.vs.css" type="text/css" rel="stylesheet">
<link href="/home/css/index.css" type="text/css" rel="stylesheet">
<link href="/home/css/index.css" type="text/css" rel="stylesheet">
<script src="/home/my_ug/ga.js" async="" type="text/javascript"></script><script type="text/javascript" src="/home/my_ug/jquery-1.js"></script>
<script type="text/javascript" src="/home/my_ug/yg.js"></script>
<script type="text/javascript" src="/home/my_ug/ygdialog.js"></script><link href="/home/my_ug/ygdialog.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/home/my_ug/yg_002.js"></script>
<!-- <!-- 省市县三级联动 -->
<script type="text/javascript" src="/home/my_ug/jquery.js"></script>
<script type="text/javascript" src="/home/my_ug/date_birthday.js"></script>
<script type="text/javascript" src="/home/my_ug/memberBasicinfo_editInfo.js"></script>
<script type="text/javascript" src="/home/my_ug/area.js"></script>
</head>
<body class="myinfo">
@include('/home_public/header')
<!--//公共头部end-->
<div class="blank10"></div>
<div class="cen">
  <input id="loginPassword" name="loginPassword" value="c385217e3a19850ca1329f3c9233cbc6" type="hidden">
  <p class="curLct">
    您当前的位置：<a href="/home/index">首页</a> &gt; 
    <a href="http://www.yougou.com/my/ucindex.jhtml?t=14740399798957752">我的优购</a> &gt; 个人资料
  </p>
<div class="u_leftxin u_leftxin fl mgr10" id="umenu">
  <div class="wdygtit"><a href="http://www.yougou.com/my/ucindex.jhtml?t=14740399798961534"><span>我的优购</span></a></div>
  <ul class="jiaoyizx">
      <li class="ultit">交易中心</li>
      <li class="myorderli"><a href="http://www.yougou.com/my/order.jhtml?t=14740399798966613"><span>我的订单</span></a></li>
      <li class="myfavorli"><a href="http://www.yougou.com/my/favorites.jhtml?t=14740399798966769"><span>我的收藏</span></a></li>
      <li class="mycommentli"><a href="http://www.yougou.com/my/comment.jhtml?t=14740399798969024"><span>商品评论/晒单</span></a></li>
        <li class="msgli"><a href="http://www.yougou.com/my/msg.jhtml?t=14740399798968609"><span id="uc_msg_count">站内消息<i class="huise">（<em>0</em>）</i></span></a></li>
    </ul>
    <ul class="wodezc">
      <li class="ultit">我的资产</li>
      <li class="mycouponli"><a href="http://www.yougou.com/my/coupon.jhtml?couponState=1&amp;t=14740399798964797"><span id="my_coupon_tick">我的优惠券</span></a></li>
        <li class="giftcardli"><a href="http://www.yougou.com/my/giftcard.jhtml?couponState=1&amp;t=14740399798967658"><span id="my_giftcard_tick">我的礼品卡</span></a></li>
        <li class="mypointli"><a href="http://www.yougou.com/my/point.jhtml?t=14740399798967249"><span id="my_point_tick">我的积分</span></a></li>
    </ul>
  <ul class="gerensz">
      <li class="ultit">个人设置</li>
      <li class="myinfoli"><a href="/home/myug/ucenter"><span>个人资料</span></a></li>
        <li class="safesetli"><a href="/home/myug/security"><span id="uc_safe_level">安全设置</span></a></li>
        <li class="myaddrli"><a href="/home/myug/receipt"><span>我的收货地址</span></a></li>
    </ul>
    <ul class="shouhoufw">
      <li class="ultit">售后服务</li>
      <li class="afterservli"><a href="http://www.yougou.com/my/afterService.jhtml?t=14740399798975220"><span>查看退换货</span></a></li>
        <li class="appservli"><a href="http://www.yougou.com/my/applicationService.jhtml?t=14740399798971719"><span>申请退换货</span></a></li>
    </ul>
<ul class="hotlist">
  <li class="hotlist_tit">
    <div>
      <p>24小时热销推荐</p>
    </div>
  </li>
  <li class="hotlist_dl">
    <dl>
      <dt>
        <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank">
          <img src="/home/images/100180914_01_s.jpg" title="adidas 阿迪达斯 运动 双肩包" alt="阿迪达斯 双肩包"></a>
      </dt>
      <dd>
        <p class="hotgoodsname">
          <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank" title="adidas 阿迪达斯 运动 双肩包">阿迪达斯 双肩包</a></p>
        <p class="hotgoodsprice">
          <span class="Red">￥269</span>
          <span class="Hui">￥269</span></p>
      </dd>
    </dl>
     <dl>
        <dt>
          <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank">
            <img src="/home/images/100180914_01_s.jpg" title="adidas 阿迪达斯 运动 双肩包" alt="阿迪达斯 双肩包"></a>
        </dt>
        <dd>
          <p class="hotgoodsname">
            <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank" title="adidas 阿迪达斯 运动 双肩包">阿迪达斯 双肩包</a></p>
          <p class="hotgoodsprice">
            <span class="Red">￥269</span>
            <span class="Hui">￥269</span></p>
        </dd>
    </dl>
     <dl>
      <dt>
        <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank">
          <img src="/home/images/100180914_01_s.jpg" title="adidas 阿迪达斯 运动 双肩包" alt="阿迪达斯 双肩包"></a>
      </dt>
      <dd>
        <p class="hotgoodsname">
          <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank" title="adidas 阿迪达斯 运动 双肩包">阿迪达斯 双肩包</a></p>
        <p class="hotgoodsprice">
          <span class="Red">￥269</span>
          <span class="Hui">￥269</span></p>
      </dd>
    </dl>
     <dl>
        <dt>
          <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank">
            <img src="/home/images/100180914_01_s.jpg" title="adidas 阿迪达斯 运动 双肩包" alt="阿迪达斯 双肩包"></a>
        </dt>
        <dd>
          <p class="hotgoodsname">
            <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank" title="adidas 阿迪达斯 运动 双肩包">阿迪达斯 双肩包</a></p>
          <p class="hotgoodsprice">
            <span class="Red">￥269</span>
            <span class="Hui">￥269</span></p>
        </dd>
    </dl>
     <dl>
      <dt>
        <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank">
          <img src="/home/images/100180914_01_s.jpg" title="adidas 阿迪达斯 运动 双肩包" alt="阿迪达斯 双肩包"></a>
      </dt>
      <dd>
        <p class="hotgoodsname">
          <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank" title="adidas 阿迪达斯 运动 双肩包">阿迪达斯 双肩包</a></p>
        <p class="hotgoodsprice">
          <span class="Red">￥269</span>
          <span class="Hui">￥269</span></p>
      </dd>
    </dl>
     <dl>
        <dt>
          <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank">
            <img src="/home/images/100180914_01_s.jpg" title="adidas 阿迪达斯 运动 双肩包" alt="阿迪达斯 双肩包"></a>
        </dt>
        <dd>
          <p class="hotgoodsname">
            <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank" title="adidas 阿迪达斯 运动 双肩包">阿迪达斯 双肩包</a></p>
          <p class="hotgoodsprice">
            <span class="Red">￥269</span>
            <span class="Hui">￥269</span></p>
        </dd>
    </dl>
  </li>
  <li class="last">
    <a class="hotrenovate" href="javascript:void(0);" onclick="YGM.GlobeTip.hottop24r()">换一批</a></li>
</ul>


</div>
  <div class="u_right fl">
    <p class="tab_content">
      </p><form action="/home/myug/doupdate" method="post" name="form1" id="form1">

      <ul class="tab_top"><li class="on">修改收货地址</li></ul>
      <ul class="tab_con">
        <table class="tab_tb">
          <tbody>
            <tr>
              <td style="border-top:none">
              <a id="newAddressBtn" class="myaddr_addbtn">修改收货地址</a>
              <div id="addressFormDiv" class="myaddr_add">
                  <form id="addressForm" name="addressForm" method="post">
                          <p class="titt">
                          </p>
                          <div class="myaddr_addrow clearfix">
                            <p class="tit">所&nbsp;在&nbsp;地&nbsp;区</p>
                           <span id="district">
                            <div class="info">
                                <div style="width:500px;">
                                <select id="s_province" name="s_province"></select>  
                                  <select id="s_city" name="s_city" ></select>  
                                  <select id="s_county" name="s_county"></select>
                                  <span  class="myaddr_tip" id="area_tip">　　　　　
                                  </span>
                                  <script class="resources library" src="area.js" type="text/javascript"></script>
                                   
                                  <script type="text/javascript">_init_area();</script>
                                  </div>
                                  <div id="show">
                                  </div>
                                  
                            </div>

                                  <script type="text/javascript">
                                  var Gid  = document.getElementById ;
                                  var showArea = function(){
                                    Gid('show').innerHTML = "<h3>省" + Gid('s_province').value + " - 市" +  
                                    Gid('s_city').value + " - 县/区" + 
                                    Gid('s_county').value + "</h3>"
                                                }
                                  Gid('s_county').setAttribute('onchange','showArea()');
                                  </script>
                           </span>
                          
                          </div>
                       
                          <div class="myaddr_addrow clearfix">
                            <p class="tit">详&nbsp;细&nbsp;地&nbsp;址</p>
                            <input type="text" name="receivingAddress" id="receivingAddress" class="myaddr_input error" value="{{$row->receivingAddress}}">
                            <span style="display: none;" id="receivingAddress_tip" class="myaddr_tip">请输入收货人地址，要求5-120个字符</span>
                          </div>
                          <div class="myaddr_addrow clearfix">
                            <p class="tit">收货人姓名</p>
                            <input type="text" id="receivingName" name="receivingName" class="myaddr_input error" style="width:102px" value="{{$row->receivingName}}">
                            <span style="display: none;" id="receivingName_tip" class="myaddr_tip">请输入收货人姓名，要求3-25个字符</span>
                          </div>
                          <div class="myaddr_addrow clearfix">
                            <p class="tit">收货人手机</p>
                            <input type="text" maxlength="11" name="receivingMobilePhone" id="receivingMobilePhone" class="myaddr_input error" style="width:136px" value="{{$row->receivingMobilePhone}}">
                            <span style="display: none;" id="receivingMobilePhone_tip" class="myaddr_tip">请输入手机号码</span>
                          </div>
                          {{csrf_field()}}
                          <div class="myaddr_addrow clearfix">
                                <input type="submit" style="border:0px;cursor:pointer;" id="saveBtn" class="save" value="保存">
                                <a href="javascript:void(0);" id="cancelBtn" class="cancel">取消</a>
                          </div>
                           @if(!empty($row->s_province))
                              <script type="text/javascript">
                                  $('#s_province').append("<option value='{{$row->s_province}}' selected>{{$row->s_province}}</option>");
                              </script>
                           @endif
                           @if(!empty($row->s_city))
                              <script type="text/javascript">
                                  $('#s_city').append("<option value='{{$row->s_city}}' selected>{{$row->s_city}}</option>");
                              </script>
                           @endif
                           @if(!empty($row->s_county))
                              <script type="text/javascript">
                                  $('#s_county').append("<option value='{{$row->s_county}}' selected>{{$row->s_county}}</option>");
                              </script>
                           @endif
                           <input type="hidden" name="id" value="{{$row->id}}" />
                  </form>              
              </div>
              </td>
            </tr>
          </tbody>
          <script type="text/javascript" src="/home/my_ug/ug_update_address.js"></script>
        </table>
      </ul>
    </form>
    <p></p>
  </div>
</div>
<p class="blank10"></p>
@include('/my_ug/ug_footer')
