<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache,no-store, must-revalidate">
<meta http-equiv="expires" content="0">
<title>安全设置</title>
<link href="/home/my_ug/base-2.css" type="text/css" rel="stylesheet">
<link href="/home/my_ug/ucenter_v2.css" type="text/css" rel="stylesheet">
<script src="/home/my_ug/ga.js" async="" type="text/javascript"></script>
<script src="/home/my_ug/ga.js" async="" type="text/javascript"></script>
<script type="text/javascript" src="/home/my_ug/jquery-1.js"></script>
<script type="text/javascript" src="/home/my_ug/yg.js"></script>
<script type="text/javascript" src="/home/my_ug/ygdialog.js"></script><link href="/home/my_ug/ygdialog.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/home/my_ug/yg_002.js"></script>
<script type="text/javascript" src="/home/bootstrap/js/jquery-1.8.3.min.js"></script>
<meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body class="safeset">
@include('/home_public/header')
<!--//公共头部end--><div class="blank07"></div>
<div class="cen">
  <p class="curLct">您当前的位置：<a target="blank" href="/home/index" class="f_blue">首页</a> &gt; <a href="http://www.yougou.com/my/ucindex.jhtml?t=14742047208188536" class="f_blue">我的优购</a> &gt; 安全设置</p>
  
  <!-- menu -->
<div class="u_leftxin u_leftxin fl mgr10" id="umenu">
  <div class="wdygtit"><a href="http://www.yougou.com/my/ucindex.jhtml?t=14742047208183381"><span>我的优购</span></a></div>

    <ul class="wodezc">
      <li class="ultit">我的资产</li>
      <li class="mycouponli"><a href="http://www.yougou.com/my/coupon.jhtml?couponState=1&amp;t=14742047208194968"><span id="my_coupon_tick">我的优惠券</span></a></li>
        <li class="giftcardli"><a href="http://www.yougou.com/my/giftcard.jhtml?couponState=1&amp;t=14742047208193554"><span id="my_giftcard_tick">我的礼品卡</span></a></li>
        <li class="mypointli"><a href="http://www.yougou.com/my/point.jhtml?t=14742047208195855"><span id="my_point_tick">我的积分</span></a></li>
    </ul>
  <ul class="gerensz">
      <li class="ultit">个人设置</li>
      <li class="myinfoli"><a href="/home/myug/ucenter"><span>个人资料</span></a></li>
        <li class="safesetli"><a href="/home/myug/security"><span id="uc_safe_level">安全设置</span></a></li>
        <li class="myaddrli"><a href="/home/myug/receipt"><span>我的收货地址</span></a></li>
    </ul>
    <ul class="shouhoufw">
      <li class="ultit">售后服务</li>
      <li class="afterservli"><a href="http://www.yougou.com/my/afterService.jhtml?t=14742047208197064"><span>查看退换货</span></a></li>
        <li class="appservli"><a href="http://www.yougou.com/my/applicationService.jhtml?t=14742047208195790"><span>申请退换货</span></a></li>
    </ul>
<ul class="hotlist">
  <li class="hotlist_tit">
    <div>
      <p>24小时热销推荐</p>
    </div>
  </li>
  <li class="hotlist_dl">
    <dl>
      <dt>
        <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank">
          <img src="/home/images/100180914_01_s.jpg" title="adidas 阿迪达斯 运动 双肩包" alt="阿迪达斯 双肩包"></a>
      </dt>
      <dd>
        <p class="hotgoodsname">
          <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank" title="adidas 阿迪达斯 运动 双肩包">阿迪达斯 双肩包</a></p>
        <p class="hotgoodsprice">
          <span class="Red">￥269</span>
          <span class="Hui">￥269</span></p>
      </dd>
    </dl>
     <dl>
        <dt>
          <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank">
            <img src="/home/images/100180914_01_s.jpg" title="adidas 阿迪达斯 运动 双肩包" alt="阿迪达斯 双肩包"></a>
        </dt>
        <dd>
          <p class="hotgoodsname">
            <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank" title="adidas 阿迪达斯 运动 双肩包">阿迪达斯 双肩包</a></p>
          <p class="hotgoodsprice">
            <span class="Red">￥269</span>
            <span class="Hui">￥269</span></p>
        </dd>
    </dl>
     <dl>
      <dt>
        <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank">
          <img src="/home/images/100180914_01_s.jpg" title="adidas 阿迪达斯 运动 双肩包" alt="阿迪达斯 双肩包"></a>
      </dt>
      <dd>
        <p class="hotgoodsname">
          <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank" title="adidas 阿迪达斯 运动 双肩包">阿迪达斯 双肩包</a></p>
        <p class="hotgoodsprice">
          <span class="Red">￥269</span>
          <span class="Hui">￥269</span></p>
      </dd>
    </dl>
     <dl>
        <dt>
          <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank">
            <img src="/home/images/100180914_01_s.jpg" title="adidas 阿迪达斯 运动 双肩包" alt="阿迪达斯 双肩包"></a>
        </dt>
        <dd>
          <p class="hotgoodsname">
            <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank" title="adidas 阿迪达斯 运动 双肩包">阿迪达斯 双肩包</a></p>
          <p class="hotgoodsprice">
            <span class="Red">￥269</span>
            <span class="Hui">￥269</span></p>
        </dd>
    </dl>
     <dl>
      <dt>
        <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank">
          <img src="/home/images/100180914_01_s.jpg" title="adidas 阿迪达斯 运动 双肩包" alt="阿迪达斯 双肩包"></a>
      </dt>
      <dd>
        <p class="hotgoodsname">
          <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank" title="adidas 阿迪达斯 运动 双肩包">阿迪达斯 双肩包</a></p>
        <p class="hotgoodsprice">
          <span class="Red">￥269</span>
          <span class="Hui">￥269</span></p>
      </dd>
    </dl>
     <dl>
        <dt>
          <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank">
            <img src="/home/images/100180914_01_s.jpg" title="adidas 阿迪达斯 运动 双肩包" alt="阿迪达斯 双肩包"></a>
        </dt>
        <dd>
          <p class="hotgoodsname">
            <a href="http://www.yougou.com/c-adidas/sku-abb00-100180914.shtml#ref=my_info&amp;po=hot24_5" target="_blank" title="adidas 阿迪达斯 运动 双肩包">阿迪达斯 双肩包</a></p>
          <p class="hotgoodsprice">
            <span class="Red">￥269</span>
            <span class="Hui">￥269</span></p>
        </dd>
    </dl>
  </li>
  <li class="last">
    <a class="hotrenovate" href="" onclick="">换一批</a></li>
</ul>
</div>
<script type="text/javascript" src="/home/my_ug/bindcheck.js"></script>   
  <!-- menu end --> 
  
  <!-- right content -->
  <div class="u_right fl" id="safeset">
    <p class="ubtitle">
      <span class="Size14 fb">安全设置</span>
    </p>
    <div class="u_con2 u_safeSet">
            <div style="text-align:center;position:absolute;top:50px;left:25px;">
            <img src="/home/my_ug/2016-09-19_104135.png" alt="" />
            </div>
            <p class="blank15"></p>
            <div style="height:400px;margin-top:100px;margin-left:200px;">
            <form action="/home/myug/dosecuritytwo" method="get">
               <dd>

                 旧密码:<input type="password" name="olduserpwd" style="height:20px;margin-left:30px;"/>
                 @if(session('olderror'))
                 <span style="color:red;">旧密码错误</span>
                 @else
                  <span>请输入旧密码</span>
                  @endif
               </dd>
               <div style="height:40px;"></div>
               <dd>
                 新密码:<input type="password" name="userpwd" style="height:20px;margin-left:30px;"/> 
                 <span>密码可使用字母+数字或符号的组合长度6-16个字符</span>
               </dd>
                <div style="height:40px;"></div>
                <dd>
                 重复新密码:<input type="password" name="reuserpwd" style="height:20px;margin-left:5px;" />
                 <span>请再次输入密码进行确认</span>
                </dd>
                {{csrf_field()}}
                <div style="height:40px;"></div>
                <input type="submit" value="下一步" style="margin-left:60px;width:80px;" />
              </form>
          </div>
          <script type="text/javascript">
          newpwd_isok=false;
          //判断用户输入密码长度是否符合
          $('input[name=userpwd]').blur(function()
          {
              newpwdlen=$(this).val().length;
              if(newpwdlen<6||newpwdlen>16)
              {
                $(this).next().text('密码长度不符合要求');
                $(this).next().css('color','red');
              }else{
                $(this).next().text('');
                $(this).next().css('color','block');
                newpwd_isok=true;
              }
          })
          //判断两次密码是否相同
          reuserpwd_isok=false;
          $('input[name=reuserpwd]').blur(function()
          {
              reuserpwdlen=$(this).val().length;
              if(reuserpwdlen==0)
              {
                $(this).next().text('重复密码不能为空');
                $(this).next().css('color','red');
              }
              reuserpwd=$(this).val();
              userpwd=$('input[name=userpwd]').val();
              if(reuserpwd!=userpwd)
              {
                   $(this).next().text('两次密码不一致');
                   $(this).next().css('color','red'); 
              }else{
                   $(this).next().text('');
                   $(this).next().css('color','block');
                   reuserpwd_isok=true; 
              }

          })
            $('input[type=submit]').click(function(){
              if(reuserpwd_isok==true&&newpwd_isok==true)
              {

                return true;
              }
              
                return false;
            });


          </script>
    </div>
            <p class="blank15"></p>
    <div class="u_coupon_tips bindEmail_tips">
            <p class="tit">常见问题</p>
            <p class="bindEmail_qa">Q 为什么要进行安全设置？进行安全设置后有什么好处？<br><span>A 优购时尚商城账户涉及您的优惠券、礼品卡和积分资产，我们强烈建议您完善安全设置，以免造成不必要的损失；</span></p>
            <p class="bindEmail_qa">Q 安全设置完成之后可以修改吗？<br><span>A 您可以点击“修改”按钮进行修改。</span></p>
    </div>
  </div>
  <!-- right content end -->
    <div class="blank10"></div>
</div>

@include('/my_ug/ug_footer')
