<div class="ygwrap">
  <div class="ygGuid mb10" id="ygGuid">
    <h2 class="ygGuidTit">优购导购</h2>

    <div class="item_bom">
      <a id="brand-left" class="brmove_l_btn" href="javascript:;"></a>
      <a id="brand-right" class="brmove_r_btn" href="javascript:;"></a>
      

      <div class="brand_move" id="brand-list">
            @foreach($all[0]['guide'] as $key => $value)
              <a href="" target="_blank">
                <img class="lazy_loading" width="85" height="40" src="{{$value -> img_path}}" alt="{{$value -> brandname}}"/>
              {{$value -> brandname}}
            </a>
            @endforeach
      </div>
      <div class="clear"></div>

      <!-- 遍历底部分类 -->
      <ul class="rec_buy_link mt20">
      @foreach($footercates as $key => $value)
         <li>
              <dl>
                <a href="" class="fr Gray">更多>></a>
                  <dd><a href="" target="_blank">{{$value -> name}}</a></dd>
                <dt>
                      
                      @foreach($value -> cate as $key => $value)
                        <a href="" target="_blank">{{$value -> name}}</a>
                      @endforeach
                </dt>
              </dl>
        </li>

        @if($key==count($cates)-1)
          break;
        @endif
        
      @endforeach
           

      </ul>
    </div>
  </div>
</div>

<div class="blank10"></div>
<!--footer created time: 2016-08-29T14:33:04+08:00-->
<!--Ntalker start-->
<script id="ntkfjs" src="/home/js/lastadd.js" type="text/javascript"></script>
<!--Ntalker end-->

<div class="wcen footser">
  <div class="ygwrap">
    <ul class="hd">
      <li><i class="item1"></i><a href="" target="_blank" rel="nofollow"><em>正品</em>保证</a></li>
      <li><i class="item2"></i><a href="" target="_blank" rel="nofollow"><em>10天</em>退换货</a></li>
      <li><i class="item3"></i><a href="" target="_blank" rel="nofollow"><em>10天调价</em>补差额</a></li>
      <li><i class="item4"></i><a href="" target="_blank" rel="nofollow"><em>7X24小时</em>在线客服</a></li>
    </ul>
    <div id="n_help" class="clearfix">
      <dl>
        <dt>新手帮助</dt>
        <dd><a href="" target="_blank" rel="nofollow" >交易条款协议</a></dd>
        <dd><a href="" target="_blank" rel="nofollow" >注册新用户</a></dd>
        <dd><a href="" target="_blank" rel="nofollow" >会员积分详情</a></dd>
        <!-- <dd><a href="/help/memberlevel.shtml" target="_blank" rel="nofollow" >会员等级</a></dd> -->
      </dl>
      <dl>
        <dt>购物指南</dt>
        <dd><a href="" target="_blank" rel="nofollow" >订购流程</a></dd>
        <dd><a href="" target="_blank" rel="nofollow" >验货与签收</a></dd>
        <dd><a href="" target="_blank" rel="nofollow" >订单配送查询</a></dd>
      </dl>
      <dl>
        <dt>支付/配送</dt>
        <!--  <dd><a href="/help/payment.shtml" target="_blank" rel="nofollow" >支付说明</a></dd>-->
        <dd><a href="http://www.yougou.com/help/payonline.shtml" target="_blank" rel="nofollow" >支付方式</a></dd>
        <dd><a href="" target="_blank" rel="nofollow" >配送方式</a></dd>
        <dd><a href="" target="_blank" rel="nofollow" >配送时间及运费</a></dd>
      </dl>
      <dl>
        <dt>售后服务</dt>
        <dd><a href="" target="_blank" rel="nofollow" >退换货政策</a></dd>
        <!-- <dd><a href="/help/returnprocess.shtml" target="_blank" rel="nofollow" >退换货流程</a></dd> -->
        <dd><a href="" target="_blank" rel="nofollow" >退款说明</a></dd>
        <dd><a href="" target="_blank" rel="nofollow" >发票制度</a></dd>
        <!-- <dd><a href="http://www.yougou.com/help/referprocess.shtml" target="_blank" rel="nofollow" >售后服务咨询流程</a></dd>
        <dd><a href="http://www.yougou.com/help/priceprotection.shtml" target="_blank" rel="nofollow" >十天补差价</a></dd> -->
      </dl>
      <dl>
        <dt>会员服务</dt>
        <dd><a href="" target="_blank" rel="nofollow" >找回密码</a></dd>
        <!-- <dd><a href="http://www.yougou.com/help/sizes.shtml" target="_blank" rel="nofollow" >尺码选择</a></dd>
        <dd><a href="http://www.yougou.com/help/suggestions.shtml" target="_blank" rel="nofollow" >投诉和建议</a></dd>-->
        <dd><a href="" target="_blank" >联系我们</a></dd>
      </dl>
      <dl class="help">
        <dt>优购客服</dt>
        <dd class="kf tleft"><span>
        <a onclick="javascript:NTKF.im_openInPageChat();" class="lnk-ntalker" href="javascript:;">在线咨询</a>
        </span></dd>
        <dd>Email：<em class="Red Size12">service@yougou.com</em></dd>
        <dd>分享购QQ群：<em class="Red Size12">375298444</em></dd>
        <dd>分享购微信号：<em class="Red Size12">yougoufenxianggou</em></dd>
      </dl>
      <dl class="qrcode">
        <dd class="center"><a href="http://m.yougou.com/agent" target="_blank"><img src="picture/footer-mobile-qrcode.jpg" width="114" height="103" /></a></dd>
        <dd class="tright"><img src="picture/footer-wechat-qrcode.jpg" width="72" height="103" /></dd>
      </dl>
    </div>
  </div>
</div>

<div class="wcen n_footinfo">
  <div class="ygwrap">
    <div class="n_footl fl tleft" id="endlogo">
      <a href="http://www.yougou.com/" class="ba_logo" title="兄弟们做的项目" alt="兄弟们做的项目"><img src="/home/picture/blank.gif" title="兄弟们做的项目" alt="兄弟们做的项目" /></a>
    </div>
    <div class="n_footr fr f_white">
      <p class="tright">
        <a href="http://www.yougou.com/help/aboutus.shtml" target="_blank" >关于优购</a> |
        <a href="http://www.yougou.com/help/business.shtml" target="_blank" >品牌招商</a> |
        <a href="http://www.yougou.com/group_purchasing.shtml" target="_blank" >集团采购</a> |
        <a href="http://www.yougou.com/help/zhaopin.shtml" target="_blank">招贤纳士</a> |
        <a href="http://www.yougou.com/topics/mobile.shtml" target="_blank" >手机优购</a> |
        <a href="http://www.yougou.com/help/contactus.shtml" target="_blank" >联系我们</a> |
        <a target="_blank" href="http://www.yougou.com/brand_daqo.shtml">品牌大全</a> |
        <a href="http://www.yougou.com/sitemap.shtml" target="_blank">网站地图</a> |
        <a href="http://top.yougou.com" target="_blank">销售排行</a> |
        <a href="http://style.yougou.com/info.shtml" target="_blank">优购资讯</a> |
        <a href="http://cps.yougou.com" target="_blank">网站联盟</a> |
        <a href="http://www.yougou.com/friendlink.shtml" target="_blank" >友情链接</a>
        <!--<a href="/product_list/p1.shtml" target="_blank" >产品大全</a> -->
      </p>

      <p>
        Copyright &copy; 2011-2016 Yougou Technology Co., Ltd.
        <a href="http://www.miibeian.gov.cn" target="_blank" rel="nofollow">粤ICP备09070608号-4</a>
        增值电信业务经营许可证：
        <a href="http://www.miibeian.gov.cn" target="_blank" rel="nofollow">粤
        B2-20090203</a>&nbsp;<span>深公网安备：4403101910665 </span>
        <a target="_blank" href="http://www.beian.gov.cn/portal/registerSystemInfo?recordcode=44030502000017" style="text-decoration:none;height:20px;line-height:20px;"><img src="/home/picture/bei_an_tu_biao_.png"/><span style="height:20px;line-height:20px;margin: 0px 0px 0px 5px; color:#FFFFFF;">粤公网安备 44030502000017号</span></a>
      </p>
      <p class="tright beian">
        <a href="http://61.144.227.239:9002/" target="_blank" rel="nofollow" class="ba_link2"><img src="/home/picture/blank.gif" original="/home/images/beian2.png" /></a>
        <a href="" target="_blank" rel="nofollow" class="ba_link1"><img src="picture/blank.gif" original="/home/images/beian1.png" /></a>
        
        <a href="" class="ba_link2" target="_blank" title="众信网" rel="nofollow">
          <img original="/home/images/ebs-logo.jpg" src="picture/blank.gif" width="108" height="40" />
        </a>
        <a style="width:108px; height:40px;" href="" logo_type="realname" logo_size="124x47" key="521b3d2524306332d3107ff3" target="_blank">
          <img width="124" height="47" src="/home/picture/sm_124x47.png" style="border: medium none;" alt="安全联盟认证">
        </a>
        <a href="" class="ba_link2" target="_blank" title="众信网" rel="nofollow">
          <img original="/home/images/ebs.png" src="/home/picture/blank.gif" width="108" height="40" />
        </a>
      </p>
    </div>
  </div>
</div>

<script type="text/javascript">
    var dsp_config = {
        bd_list_type: 'ecom_page',
        bd_page_type: 'index'
    }
</script>

<script type="text/javascript" src="/home/js/yg.index.js"></script>
<script type="text/javascript" src="/home/js/commodity.js"></script>
<script type="text/javascript" src="/home/js/yg_suggest.js"></script>

<!-- common js -->
<!-- 1. sourceChannel -->
<script type="text/javascript" src="/home/js/sourcechannel.js"></script>
<!-- 2.  mv    -->
<script type="text/javascript" src="/home/js/mv.js"></script>

<!-- common js end -->

<script type="text/javascript">
    <!--
    var bd_cpro_rtid="n1cYns";
    //-->
</script>
<script type="text/javascript" src="/home/js/rt.js"></script>
<noscript>
  <div style="display:none;">
    <img height="0" width="0" style="border-style:none;" src="/home/picture/rt.jpg" />
  </div>
  <div style="display:inline;">
    <img height="1" width="1" style="border-style:none;" alt="" src="/home/picture/0313c13481be46988df9b273d57a2b61.gif"/>
  </div>
</noscript>

<!-- google remarketing code -->
<!-- Google Code Parameters -->
<script type="text/javascript">
    var google_tag_params = {
        ecomm_prodid: "",
        brand: "",
        firstCategoryName: "",
        subCategoryName: "",
        thirdCategoryName: "",
        ecomm_pagetype: "index",
        webType: "yg"
    };
</script>
<!-- Google Code for Main List -->
<!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. For instructions on adding this tag and more information on the above requirements, read the setup guide: google.com/ads/remarketingsetup -->
<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 1016027598;
    var google_conversion_label = "189vCLqHowQQzrO95AM";
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
</script>
<script type="text/javascript" src="/home/js/conversion.js"></script>
<noscript>
  <div style="display:inline;">
    <img height="1" width="1" style="/home/border-style:none;" alt="" src="/home/picture/0313c13481be46988df9b273d57a2b61.gif"/>
  </div>
</noscript>

</html>