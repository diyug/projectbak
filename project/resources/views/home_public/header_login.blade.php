<div class="view_area clearfix">
    <div class="yg link_box"><a href="/home/index">时尚商城</a></div>
    
    <div class="phone link_box">
      <a href="http://www.yougou.com/topics/mobile.html" class="phone_text"><i class="mobile_ico"></i>手机优购<i class="tip"></i></a>
      <div class="phone_con">
                    <p class="clearfix">
                        <span class="fl qr_code">
                        </span>
                        <span class="fl ml10">
                            <a class="btn_app_store btn" href="http://itunes.apple.com/cn/app/zhang-shang-you-gou/id504493912?mt=8" target="_blank">App Store</a>
                            <a class="btn_android_store btn" href="http://mobile.yougou.com/appVersion/package.sc?channelCode=YgYougouwebA59" target="_blank">Android</a>
                        </span>
                    </p>
                    <p class="qr_code_tip">下载安装 <strong>优购客户端</strong></p>
                </div>
    </div>
    <div class="outlets link_box" style="border-right:none"></div>
    <div class="fr">
      @if(session('username'))
        <div class="about_user">
          你好！
               <span>{{session('username')}}　</span>
               <a href="/home/User/logout">退出　　</a>
        </div>
      @else
        <div class="about_user">
          <div class="login">
                    <a rel="nofollow" href="/home/User/login">登录</a>
          </div>
          <div class="register">
                    <a rel="nofollow" href="/home/User/register">注册</a>
          </div>
        </div>
      @endif
    <!--about_user end -->
    <div class="my_yg link_box">
      <a href="http://www.yougou.com/my/ucindex.jhtml" class="a1">我的优购</a>
      <ul class="info_con">
        <li><a href="http://www.yougou.com/my/favorites.jhtml">我的收藏</a></li>
            @if(session('id'))
            <li>
              <a href="">等待点评(0)</a>
            </li>         
            <li>    
              <a href="">站内消息(0)</a>            
            </li>
             @endif
      </ul>
    </div>
    <div class="my_order link_box"><a href="http://www.yougou.com/my/order.jhtml" rel="nofollow">我的订单</a></div>
    <div class="notice link_box">
      <span class="notice_text">公告</span>
      <ul class="notice_con">
                    <li><a target="_blank" href="http://www.yougou.com/topics/1416561897997.html#ref=index&po=notice_notice1">运动新风尚 新品5折起</a></li>
                    <li><a target="_blank" href="http://www.yougou.com/topics/1415597386968.html#ref=index&po=notice_notice2">摩登男装 秋冬大促 1折起</a></li>
                    <li><a target="_blank" href="http://www.yougou.com/topics/1415587130097.html#ref=index&po=notice_notice3">潮靴秀美腿 价比11.11</a></li>
                    <li><a target="_blank" href="http://www.yougou.com/topics/1415605960629.html#ref=index&po=notice_notice4">女装初冬热促 爆款2折起</a></li>
                    <li><a target="_blank" href='/article/201411/87dc5ccf633611e4b7eea30f61b97b3f.shtml#ref=index&po=notice_notice5'>库房发货时间调整说明</a></li>
      </ul>
    </div>
    <div class="more link_box">
      <a href="javascript:;" class='more_text'>更多</a>
      <ul class="more_con">
        <li><a onclick="YouGou.Biz.WebToolkit.addFavorite();" href="javascript:;">收藏优购</a></li>
        <li><a href="http://www.yougou.com/help/help.html">帮助</a></li>
      </ul>
    </div>
    </div>