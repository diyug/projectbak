<!-- header created time: 2016-08-29T16:59:24+08:00 -->
<!--公共头部start-->
<div class="yg_header" id="yg_header">
<script type="text/javascript" src="/home/js/jquery-1.8.3.min.js"></script>
  <div id="smallBannerShow">
    <!-- 最顶部通栏广告 -->

  </div>
  <!-- qq彩贝等第三方网站登录条-->
  <div class="mod_top_banner none" id="cb_head_info">
    <div class="main_area">
      <div class="sale_tip" id="cb_headshow"></div>
      <div class="login_status">
        <span class="login_span">您好，<span id="cb_showmsg"></span></span>
        <a class="my_caibei" id="jifen_url" target="_blank" href="#">我的积分</a>
      </div>
    </div>
  </div>
  <!-- qq彩贝等第三方网站登录条end-->
  <!--新头部导航9854 -->
  <div id="top_nav">
    <div class="view_area clearfix">
      <div class="yg link_box"><a class="clicked" href="/home/index">时尚商城</a></div>
      <div class="yg link_box seoul_red"><a  href=""><span class="english">Seoul Station</span><span class="chinese">首 尔 站</span></a></div>

      <div class="flashbuy link_box">
        <a href="">整点降价</a><span></span></div>
      <div class="phone link_box">
        <a href="" class="phone_text"><i class="mobile_ico"></i>手机优购<i class="tip"></i></a>

        <div class="phone_con">
          <p class="clearfix">
            <span class="fl qr_code">
            </span>
            <span class="fl ml10">
                <a class="btn_app_store btn" href="" target="_blank">App
                  Store</a>
                <a class="btn_android_store btn" href="" target="_blank">Android</a>
            </span>
          </p>

          <p class="qr_code_tip">下载安装 <strong>优购客户端</strong></p>
        </div>
      </div>
      <div class="outlets link_box" style="border-right:none"></div>
      <div class="fr">


      @if(session('username'))
        <div class="about_user">
          你好！
               <span><a href="/myyougou/ucenter">{{session('username')}}</a>　</span>
               <a href="/home/User/logout">退出　　</a>
        </div>
      @else
        <div class="about_user">
          <div class="login">
                    <a rel="nofollow" href="/home/User/login">登录</a>
          </div>
          <div class="register">
                    <a rel="nofollow" href="/home/User/register">注册</a>
          </div>
        </div>
      @endif
        <!--about_user end -->
        <div class="my_yg link_box">
          <a href="" class="a1">我的优购</a>
          <ul class="info_con">
            <li><a href="">我的收藏</a></li>
             @if(session('id'))
            <li>
              <a href="">等待点评(0)</a>
            </li>         
            <li>    
              <a href="">站内消息(0)</a>            
            </li>
              @endif
          </ul>
        </div>
        <div class="my_order link_box">
          <a href="/home/order/index" rel="nofollow">我的订单</a>
        </div>
        <div class="notice link_box">
          <span class="notice_text">公告</span>
          <ul class="notice_con">
            
            <li >
              <a target="_blank" href="">中秋佳节部分商品打折通知</a>
            </li>
            
            <li >
              <a target="_blank" href="">分享购官方微信</a>
            </li>
            
            <li >
              <a target="_blank" href="">项目小组qq群</a>
            </li>
            
            <li >
              <a target="_blank" href="">关于跨境商品征税新政策调整</a>
            </li>
            
            <li >
              <a target="_blank" href="">提醒会员谨防诈骗电话</a>
            </li>
            
          </ul>
        </div>
        <div class="more link_box">
          <a href="javascript:;" class='more_text'>更多</a>
          <ul class="more_con">
            <li><a onclick="" href="javascript:;">收藏优购</a></li>
            <li><a href="">帮助</a></li>
          </ul>
        </div>
      </div>
    </div>
    <!--view_area end -->
  </div>

  <!--top_nav end -->
  <!--新头部导航9854 end-->
  <div id="yg_logo_tab" class="clearfix ygwrap">
    <div class="logo fl">
      <a href="/home/index"></a>
    </div>
    <form action="" name="" id="J_TopSearchForm" method="get" class="search_box clearfix fl">
      <div class="zs_left fl"></div>
      <div class="input_box fl">
        <!-- csrf隐藏域 -->
        {{ csrf_field() }}
        <input type="text" id="keyword" name="keyword" maxlength="80" autocomplete="off" class="search flc"/>
      </div>
      <div class="zs_right fl"></div>
      <div class="search_btn fl"></div>
    </form>
    <div class="mycart_nav fl">
      <div class="mycart_btn">
        <div class="zs_left fl"></div>
        <div class="zs_center fl">

          <a href="/home/cart/index">
            <div class="tip_left fl"></div>
            <div id="paymoney" class="text fl">购物车(<span id="pordcount">0</span>)件</div>
            <div class="tip_right fl"></div>
          </a>

        </div>
        <div class="zs_right fl"></div>
      </div>
      <div id="shoppingcartContainer"></div>
    </div>
    <!--mycart_nav end-->
    <div></div>
    <div class="company_name" title="以上名字以姓名首字母排序">成员:刘鑫、路鑫浩、栾潘飞、王超</div>
    <div class="search_hot">
      
        <!--头部热门关键词-->
        
      
    </div>
  </div>
  <div class="yg_sites_tabtn"></div>
</div>
<!--导航start-->
<!-- nav created time: 2016-08-30T14:17:55+08:00 -->
<div class="yg_category_nav">
  <div class="ygwrap rel clearfix">
    <!--水平导航开始-->
    <div class="yginwrap yg_nav rel">
      <ul class="yg_nav_list">
  
      <li>
        
        <a  href="" target="_blank">
          运动馆
        </a>
      </li>
     
      <li>
        
        <a href="" target="_blank">
          户外馆
        </a>
      </li>
     
      <li  >
        
        <a href="" target="_blank">
          鞋靴馆
        </a>
      </li>
         
      <li>
        
        <a  href="" target="_blank">
          服装馆
        </a>
      </li>
     
      <li>
        
        <a  href="" target="_blank">
          母婴童
        </a>
      </li>   
          
      <li>
        
        <a  href="" target="_blank">
          首尔站
        </a>
      </li>
     
      <li class="fr" >
        
        <a  href="" target="_blank">
          聚优购
        </a>
      </li>
    
        
          
      <li class="fr" >
        
        <a  href="" target="_blank">
          秒杀
        </a>
      </li>
    
        
          
      <li class="last fr" >
        <i class="newtip_icon"></i>
        <a class="highlight" href="" target="_blank">
          兄弟连
        </a>
      </li>
    
        
      </ul>
    </div>
    <!--水平导航结束-->
    <div class="yg_category" id="yg_category">
      <a class="left_yg_logo" href="/home/index" target="_blank"><img src="/home/picture/left-yg-logo.jpg" width="198" height="57"/></a>

      <h2 class="hd rel">
        <a href="" target="_blank"> 全部商品分类 <i class="abs"></i></a></h2>

      <div class="bd rel">
        <!-- 坚导航开始 -->
        <ul class="indexnav2list clearfix">

          <!-- 前台分类遍历 -->
          @if(!empty($cates))
            @foreach($cates as $key => $value)
              <li>
              
                <div class="titt">
                    <i class="iconn {{$icon[$key]}}"></i>
                  <h3>

                  <!-- 判断对象中是否存在该成员属性 -->
                    @if(isset($value->bol))
                      
                      <!-- 遍历对象属性 -->
                      @foreach($value->liuxin as $k=>$v)
                        <!-- 一级分类 -->
                        <a href="/home/list/index?id={{$v->id}}" target="_blank">{{$v}}</a>
                        
                        <!-- 处理多余的 / -->
                        @if($k != $value->maxnum)
                          /
                        @endif

                      @endforeach
                      <!-- 结束遍历 -->

                    <!-- 若对象里没有该属性 -->
                    @else

                      <a href="/home/list/index?id={{$value->id}}" target="_blank">{{$value -> name}}</a>

                    @endif

                  </h3>
                </div>
                <p class="chldlist">
                  <!-- 如果二级分类存在 -->
                  @if(!empty($value))
                    <!-- 遍历二级分类 -->
                    @foreach($value -> cate as $key => $vt)
                      
                      <p style="float:left" id="ug_last">

                        <a href="/home/list/index?id={{$vt->id}}" target="_blank">{{$vt -> name}}</a>

                          <ul class="popmenu2 categoryul yummy">
                          <!-- 如果三级分类存在 -->

                          <!-- 遍历三级分类 -->
                          @foreach($vt -> cate as $k => $v)

                                
                                <li class="vae 
                                @if($v->false==1)
                                  model 
                                @endif>">
                                  <a href="/home/list/index?id={{$v->id}}" target="_blank">{{$v->name}}
                                  </a>
                                </li>

                         @endforeach

                        </ul>

                      </p>
                    @endforeach

                  @endif

                </p>
                
              </li>
            @endforeach
          @endif
          
        </ul>
        <!-- 导航结束 -->
        
      </div>
    </div>
  </div>
</div>

<!-- 导航end-->
<!-- 公共头部end -->

<!-- 三级菜单 -->
<script type="text/javascript">

    // 鼠标悬浮事件
    $('.indexnav2list li').hover(function(){

        $(this).find('titt').eq(0);
        $(this).find('.yummy').show();
        $(this).parent().find('li').removeClass('currr');
        $(this).removeClass('currr').addClass('currr');
        $(this).siblings().find('ul').eq(1).hide();
        $(this).siblings().find('ul').eq(1).addClass('currr');

    },function(){
        // console.log($(this).index());
        $(this).removeClass('currr');
        $(this).parent().find('li').eq($(this).index()).addClass('currr');
        $(this).find('.yummy').hide()
    })

    // 我的订单出现是事件
    $('.notice_text').hover(function(){
      $('.notice_con').slideDown();
    },function(){
      $('.notice_con').hide();
    })

    $('.notice_con').hover(function(){
      $(this).show();
    },function(){
      $(this).slideUp();
    })

    $('.a1').hover(function(){
      $('.info_con').slideDown();
    },function(){
      $('.info_con').hide();
    })
    
    $('.info_con').hover(function(){
      $(this).show();
    },function(){
      $(this).slideUp();
    })

    $('.phone').hover(function(){
      $('.phone_con').show();
    },function(){
      $('.phone_con').slideUp();
    })

    $('.phone_con').hover(function(){
      $(this).show();
    },function(){
      $(this).hide();
    })

    $('.more').find('a').eq(0).hover(function(){
      $(this).next().show();
    },function(){
      $(this).next().hide();
    })

    $('.more').find('ul').eq(0).hover(function(){
      $(this).show();
    },function(){
      $(this).hide();
    })
</script>