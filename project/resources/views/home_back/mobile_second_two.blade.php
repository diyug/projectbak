<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache,no-store, must-revalidate">
<meta http-equiv="expires" content="0">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>优购网_找回密码_通过手机找回密码</title>
<link href="/home/getback/base-2.css" type="text/css" rel="stylesheet">
<link href="/home/getback/validator.css" type="text/css" rel="stylesheet">
<link href="/home/getback/new_log_reg.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="/home/bootstrap/js/jquery-1.8.3.min.js"></script>
</head>
<body>
<style type="text/css">
	#sendCodeBtn{height:26px;}
	#code2_{border: 1px solid #d5d5d5;}
</style>

<!-- reghead start-->
<!-- top nav bar created time: 2014-11-28 18:34:02-->
@include('/home_back/public_header')
<!--top_nav end -->

<div class="uc_hd">
	<div class="cen clearfix rel">
		<h2>找回密码</h2>
		<p class="link fl">
			<a href="/home/index" class="cblue">返回时尚商城</a>
			<!--
			|<a href="http://www.yougou.com/topics/1394617951051.html" class="cblue">OUTLETS 购划算</a>
			-->
		</p>
	</div>
</div>

<!--更换雅虎邮箱提示 start-->
<div class="uc_email_tip" id="uc_email_tip" style="display:none;">
	<i class="warn"></i><strong>由于雅虎邮箱即将停止服务</strong>，为了保障您以后能够通过邮箱找回密码、接收订单提醒等，建议尽快把账号完成绑定其他邮箱。<a class="Blue" href="javascript:void(0);" id="email_bind_modify">[立即绑定]</a><i class="close"></i>
</div>
<!--更换雅虎邮箱提示 end--><!-- reghead end-->
<form action="/home/User/third" method="post" name="phonefrm" id="phonefrm">

<div class="findPwd-box cen">
	<h2 class="findPwd-title">找回密码</h2>
    <ul class="findPwd-step findPwd-step2">

        <li class="step1">1.输入账号</li>
        <li class="on">2.账户验证及密码重置</li>
        <li>3.密码修改成功</li>
    </ul>
	<div class="findPwd-form findPwd-step2-list3">
		<div class="findPwd-item">
      
        	<label class="findPwd-label1 fl">图片验证码：</label>
			<div class="findPwd-input fl">
            	<span class="mobleNumber">
            	<input name="code2_" id="code2_" class="nreg_sinput nreg_yellowbor" type="text"></span>
                <img id="img_switch" src="{{URL('/admin/vcode')}}" 
             alt="" onclick="this.src=this.src+'?s'" style="display:inline" >
                 <a href="#" class="Gray changeImg">换一张</a>
                 <div id="codeTips2" style="color:red;">
                 	@if(session('error'))
                 		验证码错误
					@endif
                 </div>
			</div>
            <div class="clear"></div>
        </div>
         {{ csrf_field() }}
        <div class="findPwd-item">
        	<label class="findPwd-label1 fl">您的验证手机号码：</label>
			<div class="findPwd-input fl">
            	<span class="mobleNumber">{{$account}}</span>
                <input type="button" class="regetYzm" id="sendCodeBtn"  value="获取验证码">
			</div>
            <div class="findPwd-tips mt2" style="display:block; width:300px;" id="sendTips"></div>
            <div class="clear"></div>
        </div>

        <div class="findPwd-item">
        	<label class="findPwd-label2 fl">输入验证码：</label>
			<div class="findPwd-input fl">
			<input name="code" id="code" class="nreg_sinput" type="text">
			</div>
            <div class="errortips findPwd-tips mt8" id="code_msg"></div>
            <div class="clear"></div>
        </div>
        <div class="findPwd-item">
        	<label class="findPwd-label2 fl">&nbsp;</label>

			<div class="findPwd-sbt fl">
			<input class="findPwd-btn findPwd-next" type="submit" value="">
			</div>
            <div class="clear"></div>
        </div> 
  </form>
	</div>
    <div class="seeProblem">
		<p>收不到验证短信？</p>
        <ul>
            <li>若当前号码已停用，请利用注册邮箱找回。</li>
			<li>请确认短信是否被拦截。</li>
            <li>由于网络原因，可能会有延迟。</li>
            <li>如果按照以上方法还是无法解决问题，请拨打客服热线：<span class="orange b">400 163 8888</span>。</li>
        </ul>
	</div>		
</div>
<!--底部start-->
<div class="footer Gray">

	<p class="tright">Copyright © 2011-2014 Yougou Technology Co., Ltd. <a href="http://www.miibeian.gov.cn/" target="_blank">粤ICP备09070608号-4</a> 增值电信业务经营许可证：<a href="http://www.miibeian.gov.cn/" target="_blank" style="padding-left:0">粤 B2-20090203</a></p>
</div>
<script type="text/javascript">

code_state=false;
//验证码获取焦点
$('#code2_').focus(function() {
	$(this).css('border','1px solid #f5b599');
});
//验证码失去焦点
$('#code2_').blur(function(){
	$(this).css('border','1px solid #d5d5d5');
	var vcode=$(this).val();
	if(vcode.length==0)
	{
		$('#codeTips2').text('验证码不能为空');
	}else{
			$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
			});
			$.get('/home/User/ajax',{vcode:vcode},function(data){

				if(data)
				{
					code_state=true;
					$('#codeTips2').empty();
				} else {
					 $('#codeTips2').text('验证码不正确');
				}

			},'json');
		}
})

//单击更换验证码
$('.changeImg').click(function()
{
	$('#img_switch')[0].src=$('#img_switch')[0].src+'?w';
});	

//手机获取验证码
$('#sendCodeBtn').click(function(){
	if(code_state==true)
	{
		var i=60
	into=setInterval(function()
		{
			i--;
			$('#sendTips').attr('class','righttips findPwd-tips mt2');
			$('#sendTips').text('验证码以发送至您的手机');
			$('#sendCodeBtn').attr('disabled','disabled');
			$('#sendCodeBtn').val(i);
			if(i==0)
			{
				$('#sendCodeBtn').removeAttr('disabled');
				$('#sendCodeBtn').val('再次获取');
				$('#sendTips').attr('class','findPwd-tips mt2');
				$('#sendTips').text('验证码以发送 30分钟内有效');
				clearInterval(into);
			}
		},1000);
	}
			
});
//手机验证码获取焦点
$('#code').focus(function() {
	$(this).css('border','1px solid #f5b599');
});
code2_state=false;
//失去焦点
$('#code').blur(function() {
	$(this).css('border','1px solid #d5d5d5');
	var value=$(this).val();
	if(value.length==0)
	{
		$('#code_msg').text('验证码不能为空');
	}else{

		$('#code_msg').empty();
		$('code_msg').removeAttr('errortips findPwd-tips mt8')
		code2_state=true;
	}

});
//禁止提交
$('#phonefrm').submit(function() {

	$('input').trigger('blur');
	if(code2_state==true&&code_state==true)
	{
		return true;
	}
	return false;
});

</script>
</body>
</html>