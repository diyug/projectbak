<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache,no-store, must-revalidate">
<meta http-equiv="expires" content="0">
<title>优购时尚商城_找回密码_密码修改成功</title>
<link href="/home/getback/base-2.css" type="text/css" rel="stylesheet">
<link href="/home/getback/validator.css" type="text/css" rel="stylesheet">
<link href="/home/getback/new_log_reg.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="/home/bootstrap/js/jquery-1.8.3.min.js">
</script>
</head>
<body>

<!-- reghead start-->
<!-- top nav bar created time: 2014-11-28 18:34:02-->
@include('/home_back/public_header')
<!--top_nav end -->

<div class="uc_hd">
	<div class="cen clearfix rel">
		<h2>找回密码</h2>
		<p class="link fl">
			<a href="/home/index" class="cblue">返回时尚商城</a>
			<!--
			|<a href="http://www.yougou.com/topics/1394617951051.html" class="cblue">OUTLETS 购划算</a>
			-->
		</p>
	</div>
</div>

<!--更换雅虎邮箱提示 start-->
<div class="uc_email_tip" id="uc_email_tip" style="display:none;">
	<i class="warn"></i><strong>由于雅虎邮箱即将停止服务</strong>，为了保障您以后能够通过邮箱找回密码、接收订单提醒等，建议尽快把账号完成绑定其他邮箱。<a class="Blue" href="javascript:void(0);" id="email_bind_modify">[立即绑定]</a><i class="close"></i>
</div>
<!--更换雅虎邮箱提示 end--><!-- reghead end-->
<div class="findPwd-box cen">
	<h2 class="findPwd-title">找回密码</h2>
    <ul class="findPwd-step findPwd-step3">

        <li class="step1">1.输入账号</li>
        <li>2.账户验证及密码重置</li>
        <li class="on">3.密码修改成功</li>
    </ul>
	<div class="findPwd-success">
		<p class="regSucess_box_suceIcon fl"></p>
		<div class="fl regSucess_box_rt">

			<h3 class="regSucess_box_title">密码修改成功</h3>
			<p>您现在可以立即 <strong class="f_blue"><a href="/home/User/login">登录</a></strong>，或返回<strong class="f_blue"><a href="/home/index">首页</a></strong></p>
			<p class="Gray"><span id="endTime">8</span>秒后，带您返回首页</p>
		</div>
	</div>	

</div>
<!--底部start-->
<div class="footer Gray">
	<p class="tright">Copyright © 2011-2014 Yougou Technology Co., Ltd. <a href="http://www.miibeian.gov.cn/" target="_blank">粤ICP备09070608号-4</a> 增值电信业务经营许可证：<a href="http://www.miibeian.gov.cn/" target="_blank" style="padding-left:0">粤 B2-20090203</a></p>
</div>
<!--底部end--> 
<script>
(function(){
var time=8;
var timer=setInterval(function(){
	$("#endTime").html(time);
	time-=1;
	if(time<=0)
	{
		//跳转
		location.href='/home/User/login';
		clearInterval(timer);
	}
},1000);
})();
</script>
<!--<script src="http://s1.ygimg.cn/template/common/js/mv.js?4.3.3" type="text/javascript"></script>-->
<script>
 var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23566531-1']);
  _gaq.push(['_setDomainName', '.yougou.com']);
  _gaq.push(['_addOrganic', 'baidu', 'word']);
  _gaq.push(['_addOrganic', 'soso', 'w']);
  _gaq.push(['_addOrganic', '3721', 'name']);
  _gaq.push(['_addOrganic', 'yodao', 'q']);
  _gaq.push(['_addOrganic', 'vnet', 'kw']);
  _gaq.push(['_addOrganic', 'sogou', 'query']);
  _gaq.push(['_trackPageview']);
  _gaq.push(['_trackPageLoadTime']);
(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://analytic' : 'http://analytic') + '.yougou.com/ga.js?4.3.3';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>


</body></html>