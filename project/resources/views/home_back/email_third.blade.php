<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache,no-store, must-revalidate">
<meta http-equiv="expires" content="0">
<title>优购网_找回密码_通过邮箱找回密码</title>
<link href="/home/getback/base-2.css" type="text/css" rel="stylesheet">
<link href="/home/getback/validator.css" type="text/css" rel="stylesheet">
<link href="/home/getback/new_log_reg.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="/home/bootstrap/js/jquery-1.8.3.min.js"></script>
</head>
<body>

<!-- reghead start-->
<!-- top nav bar created time: 2014-11-28 18:34:02-->
@include('/home_back/public_header')
<!--top_nav end -->

<div class="uc_hd">
	<div class="cen clearfix rel">
		<h2>找回密码</h2>
		<p class="link fl">
			<a href="/home/index/" class="cblue">返回时尚商城</a>
			<!--
			|<a href="http://www.yougou.com/topics/1394617951051.html" class="cblue">OUTLETS 购划算</a>
			-->
		</p>
	</div>
</div>

<!--更换雅虎邮箱提示 start-->
<div class="uc_email_tip" id="uc_email_tip" style="display:none;">
	<i class="warn"></i><strong>由于雅虎邮箱即将停止服务</strong>，为了保障您以后能够通过邮箱找回密码、接收订单提醒等，建议尽快把账号完成绑定其他邮箱。<a class="Blue" href="javascript:void(0);" id="email_bind_modify">[立即绑定]</a><i class="close"></i>
</div>
<!--更换雅虎邮箱提示 end--><!-- reghead end-->
<div class="findPwd-box cen">
	<h2 class="findPwd-title">找回密码</h2>
    <ul class="findPwd-step findPwd-step2">

        <li class="step1">1.输入账号</li>
        <li class="on">2.账户验证及密码重置</li>
        <li>3.密码修改成功</li>
    </ul>
	<div class="findPwd-form findPwd-step2-list2">
        <p class="findPwd-step2-t">您的邮箱是：<span class="blue b">
        {{$account}}</span>。我们已经发送一封确认信到您的邮箱中，请在收到确认信之后，点击信中的链接进行验证。</p>

        <div class="findPwd-sbt"><input class="findPwd-btn findPwd-goEmail" value="" onclick="toBindEmailBtn('{{$account}}')" type="button"></div>
	</div>
    <div class="seeProblem">
		<p>收不到验证邮件？</p>
        <ul>
        	<li>验证邮件可能在垃圾邮箱中，请仔细查找。</li>
            <li>由于网络原因，可能会延迟，如果没有收到邮件，请尝试重新操作。</li>
            <li>如果按照以上方法还是无法解决问题，请拨打客服热线：<span class="orange b">400 163 8888</span></li>

        </ul>
	</div>		
</div>
<!--底部start-->
<div class="footer Gray">
	<p class="tright">Copyright © 2011-2014 Yougou Technology Co., Ltd. <a href="http://www.miibeian.gov.cn/" target="_blank">粤ICP备09070608号-4</a> 增值电信业务经营许可证：<a href="http://www.miibeian.gov.cn/" target="_blank" style="padding-left:0">粤 B2-20090203</a></p>
</div>
<!--<script src="http://s1.ygimg.cn/template/common/js/mv.js?4.3.3" type="text/javascript"></script>-->
<script>
function toBindEmailBtn(email){	
	var dispacheUrl = getEmailLoginUrl(email);
	window.open(dispacheUrl);
}

function getEmailLoginUrl(email){
	var	suffUrl=[
		{id:"sina.com.cn",url:"http://mail.sina.com.cn/"},
		{id:"sina.com",url:"http://mail.sina.com.cn/"},
		{id:"sina.cn",url:"http://mail.sina.com.cn/"},
		{id:"vip.sina.com",url:"http://mail.sina.com.cn/"},
		{id:"2008.sina.com",url:"http://mail.sina.com.cn/"},
		{id:"163.com",url:"http://mail.163.com/"},
		{id:"126.com",url:"http://mail.126.com/"},
		{id:"popo.163.com",url:"http://popo.163.com/"},
		{id:"yeah.net",url:"http://email.163.com/"},
		{id:"vip.163.com",url:"http://vip.163.com/"},
		{id:"vip.126.com",url:"http://vip.126.com/"},
		{id:"188.com",url:"http://188.com/"},
		{id:"vip.188.com",url:"http://vip.188.com/"},
		{id:"tom.com",url:"http://mail.tom.com/"},
		{id:"yahoo.com",url:"http://mail.cn.yahoo.com/"},
		{id:"yahoo.com.cn",url:"http://mail.cn.yahoo.com/"},
		{id:"yahoo.cn",url:"http://mail.cn.yahoo.com/"},
		{id:"sohu.com",url:"http://mail.sohu.com/"},		
		{id:"hotmail.com",url:"https://login.live.com/"},
		{id:"139.com",url:"http://mail.10086.cn/"},
		{id:"gmail.com",url:"https://accounts.google.com"},
		{id:"msn.com",url:"https://login.live.com"},
		{id:"51.com",url:"http://passport.51.com/"},
		{id:"yougou.com",url:"http://mail.yougou.com/"},
		{id:"qq.com",url:"https://mail.qq.com"},
		{id:"foxmail.com",url:"http://mail.qq.com"},
		{id:"vip.qq.com",url:"http://mail.qq.com"}
	 ];
	var index = email.indexOf("@");
	var subStr = email.substring(index+1).replace(/\./g,"-");
	var suffIndext = subStr.indexOf(".");
	var exist = false;
	var loginUrl = "";	
	$.each(suffUrl,function(n,value) {
	  var emailId = value.id.replace(/\./g,"-");
	  if(subStr == emailId){
		loginUrl = value.url;
		exist = true;
	  }
	});
	 if(!exist){
		 loginUrl = "http://www."+ email.substring(index+1);
	 }
	
	 return loginUrl;
}
</script>
<script>
 var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23566531-1']);
  _gaq.push(['_setDomainName', '.yougou.com']);
  _gaq.push(['_addOrganic', 'baidu', 'word']);
  _gaq.push(['_addOrganic', 'soso', 'w']);
  _gaq.push(['_addOrganic', '3721', 'name']);
  _gaq.push(['_addOrganic', 'yodao', 'q']);
  _gaq.push(['_addOrganic', 'vnet', 'kw']);
  _gaq.push(['_addOrganic', 'sogou', 'query']);
  _gaq.push(['_trackPageview']);
  _gaq.push(['_trackPageLoadTime']);
(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://analytic' : 'http://analytic') + '.yougou.com/ga.js?4.3.3';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>
<!--底部end--> 

</body>
</html>