<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache,no-store, must-revalidate">
<meta http-equiv="expires" content="0">
<title>优购网_找回密码_选择找回密码方式</title>
<link href="/home/getback/base-2.css" type="text/css" rel="stylesheet">
<link href="/home/getback/validator.css" type="text/css" rel="stylesheet">
<link href="/home/getback/new_log_reg.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="/home/bootstrap/js/jquery-1.8.3.min.js"></script>
</head>
<body>

<!-- reghead start-->
<!-- top nav bar created time: 2014-11-28 18:34:02-->
@include('/home_back/public_header')
<!--top_nav end -->

<div class="uc_hd">
	<div class="cen clearfix rel">
		<h2>找回密码</h2>
		<p class="link fl">
			<a href="/home/index" class="cblue">返回时尚商城</a>
			<!--
			|<a href="http://www.yougou.com/topics/1394617951051.html" class="cblue">OUTLETS 购划算</a>
			-->
		</p>
	</div>
</div>

<!--更换雅虎邮箱提示 start-->
<div class="uc_email_tip" id="uc_email_tip" style="display:none;">
	<i class="warn"></i><strong>由于雅虎邮箱即将停止服务</strong>，为了保障您以后能够通过邮箱找回密码、接收订单提醒等，建议尽快把账号完成绑定其他邮箱。<a class="Blue" href="javascript:void(0);" id="email_bind_modify">[立即绑定]</a><i class="close"></i>
</div>
<!--更换雅虎邮箱提示 end--><!-- reghead end-->
<div class="findPwd-box cen">
	<h2 class="findPwd-title">找回密码</h2>
    <ul class="findPwd-step findPwd-step2">

        <li class="step1">1.输入账号</li>
        <li class="on">2.账户验证及密码重置</li>
        <li>3.密码修改成功</li>
    </ul>
	<div class="findPwd-form relative findPwd-step2-list1">
		<form name="frm" method="get" action="/home/User/checkout">
        	<p class="findPwd-step2-t">您可以选择以下方式找回密码</p>
        <div class="findPwd-style">
            <input  value="phone" class="findPwd-styleRadio" type="radio">
            {{ csrf_field() }}
            <div class="findPwd-t">
                <p class="styleTitle">通过绑定手机号码找回</p>
                <p>您的手机{{$account}}将收到验证码，通过绑定手机收到的验证码完成密码重置，本服务完全免费。</p>
            </div>
        	<div class="findPwd-sbt">
        		<input class="findPwd-btn findPwd-next" type="submit" value="">
        	</div>
		</form>

	</div>
    <div class="seeProblem">
		<p>遇到问题吗？</p>
        <ul>
            <li>若当前号码已不用/丢失，或无法收到验证码？请利用邮箱找回密码，或者确认是否被其他软件所拦截。</li>
            <li>如果按照以上方法还是无法解决问题，请拨打客服热线：<span class="orange b">400 163 8888</span>。</li>

        </ul>
	</div>		
</div>
<script type="text/javascript">
var isclick=false;
$('input[type=radio]').click(function()
{
	isclick=true;
})
$('input[type=submit]').click(function(event) {
	if(isclick)
	{

		return true;
	}
	return false;
});
</script>

<!--底部start-->
<div class="footer Gray">
	<p class="tright">Copyright © 2011-2014 Yougou Technology Co., Ltd. <a href="http://www.miibeian.gov.cn/" target="_blank">粤ICP备09070608号-4</a> 增值电信业务经营许可证：<a href="http://www.miibeian.gov.cn/" target="_blank" style="padding-left:0">粤 B2-20090203</a></p>
</div>

</body></html>