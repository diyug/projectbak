<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>优购网_找回密码_第一步</title>
<link href="/home/getback/base-2.css" type="text/css" rel="stylesheet">
<link href="/home/getback/validator.css" type="text/css" rel="stylesheet">
<link href="/home/getback/new_log_reg.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="/home/bootstrap/js/jquery-1.8.3.min.js"></script>

</head>
<body>

<!-- reghead start-->
<!-- top nav bar created time: 2014-11-28 18:34:02-->
@include('/home_back/public_header')
<!--top_nav end -->

<div class="uc_hd">
	<div class="cen clearfix rel">
		<h2>找回密码</h2>
		<p class="link fl">
			<a href="/home/index/" class="cblue">返回时尚商城</a>
			<!--
			|<a href="http://www.yougou.com/topics/1394617951051.html" class="cblue">OUTLETS 购划算</a>
			-->
		</p>
	</div>
</div>

<!--更换雅虎邮箱提示 start-->
<div class="uc_email_tip" id="uc_email_tip" style="display:none;">
	<i class="warn"></i><strong>由于雅虎邮箱即将停止服务</strong>，为了保障您以后能够通过邮箱找回密码、接收订单提醒等，建议尽快把账号完成绑定其他邮箱。<a class="Blue" href="javascript:void(0);" id="email_bind_modify">[立即绑定]</a><i class="close"></i>
</div>
<!--更换雅虎邮箱提示 end--><!-- reghead end-->
<form action="/home/User/vcode" method="POST">
<input id="memberName" name="memberName" type="hidden">
<div class="findPwd-box cen">
	<h2 class="findPwd-title">找回密码</h2>
    <ul class="findPwd-step findPwd-step1">

        <li class="step1 on">1.输入账号</li>
        <li>2.账户验证及密码重置</li>
        <li>3.密码修改成功</li>
    </ul>
	<div class="findPwd-form findPwd-step1-list">
        <div class="findPwd-item">
        	<label class="findPwd-label2 fl" style="width:90px;">账号：</label>

			<div class="findPwd-input relative fl">
				<input name="account" id="email_" class="nreg_input" type="text" AUTOCOMPLETE="off" >
				<span class="findPwd-ts">请正确输入您注册Email或绑定手机号</span>
			</div>
            <div class="findPwd-tips mt10" id="email_msg">
					@if(session("account_error"))
            		<script type="text/javascript">
            		$('#email_msg').attr('class','errortips findPwd-tips mt10');
            		$('#email_msg').text('账户名不存在');
            		</script>
					@endif
            </div>
            <div class="clear"></div>
        </div>
        <div class="findPwd-item">
        	<label class="findPwd-label2 fl" style="width:90px;">输入验证码：</label>
			<div class="findPwd-input fl">
			<input name="vcode" id="code_" class="nreg_sinput" maxlength="5" type="text"></div>
            <div class="valifyCode fl" style="margin-top:0px;">
            <img id="img_switch" src="{{URL('/admin/vcode')}}" 
             alt="" onclick="this.src=this.src+'?s'" style="display:inline" >
            </div>
            <div class="valifyCode fl">看不清？
            <a class="blue" id="switchcode" href="javascript:void(0);" >
            换一张
            </a>
            </div>
            <div class="findPwd-tips mt8" id="code_msg">
            	@if(session("vcode_error"))
            		<script type="text/javascript">
            		$('#code_msg').attr('class','errortips findPwd-tips mt10');
            		$('#code_msg').text('验证码错误');
            		</script>
				@endif
            </div>
            <div class="clear"></div>
        </div>
      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
      <input type="hidden" name="type" value="">
        <div class="findPwd-item">
        	<label class="findPwd-label2 fl" style="width:90px;">&nbsp;</label>
			<div class="findPwd-sbt fl">
			<input class="findPwd-btn findPwd-next" value="" type="submit">
			</div>
            <div class="clear"></div>

        </div> 
	</div>	
</div>
</form>

<script type="text/javascript" src="/home/getback/first.js"></script>



<!--底部start-->
<div class="footer Gray">
	<p class="tright">Copyright © 2011-2014 Yougou Technology Co., Ltd. <a href="http://www.miibeian.gov.cn/" target="_blank">粤ICP备09070608号-4</a> 增值电信业务经营许可证：<a href="http://www.miibeian.gov.cn/" target="_blank" style="padding-left:0">粤 B2-20090203</a></p>

</div>
</body>
</html>

