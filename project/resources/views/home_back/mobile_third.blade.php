<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache,no-store, must-revalidate">
<meta http-equiv="expires" content="0">
<title>优购时尚商城_绑定手机_找回密码_重设密码</title>
<script src="/home/getback/ga.js" async="" type="text/javascript"></script>
<script type="text/javascript" src="/home/getback/jquery-1.js"></script>
<link href="/home/getback/base-2.css" type="text/css" rel="stylesheet">
<link href="/home/getback/validator.css" type="text/css" rel="stylesheet">
<link href="/home/getback/new_log_reg.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="/home/getback/yg.js"></script>
<script type="text/javascript" src="/home/getback/findpwd.js"></script>
</head>
<body>

<!-- reghead start-->
<!-- top nav bar created time: 2014-11-28 18:34:02-->
@include('/home_back/public_header')
<!--top_nav end -->

<div class="uc_hd">
	<div class="cen clearfix rel">
		<h2>找回密码</h2>
		<p class="link fl">
			<a href="/home/index" class="cblue">返回时尚商城</a>
			<!--
			|<a href="http://www.yougou.com/topics/1394617951051.html" class="cblue">OUTLETS 购划算</a>
			-->
		</p>
	</div>
</div>

<!--更换雅虎邮箱提示 start-->
<div class="uc_email_tip" id="uc_email_tip" style="display:none;">
	<i class="warn"></i><strong>由于雅虎邮箱即将停止服务</strong>，为了保障您以后能够通过邮箱找回密码、接收订单提醒等，建议尽快把账号完成绑定其他邮箱。<a class="Blue" href="javascript:void(0);" id="email_bind_modify">[立即绑定]</a><i class="close"></i>
</div>
<!--更换雅虎邮箱提示 end--><!-- reghead end-->
<form action="/home/User/repassword" method="post" id="form2" name="form2">
<div class="findPwd-box cen">
	<h2 class="findPwd-title">找回密码</h2>
    <ul class="findPwd-step findPwd-step2">

        <li class="step1">1.输入账号</li>
        <li class="on">2.账户验证及密码重置</li>
        <li>3.密码修改成功</li>
    </ul>
	<div class="findPwd-form findPwd-step2-list4">
    	<p class="findPwd-step2-t">已通过验证，请设置您的新密码</p>
        <div class="findPwd-item" style="margin-bottom:10px;">
        	<label class="findPwd-label2 fl" style="width:70px;">设置密码：</label>
			<div class="findPwd-input relative fl"><input name="newPassword" id="findPwd_password" class="nreg_input" value="" type="password"><span class="findPwd-ts">字母、数字、符号均可，6-16-个字符以内</span></div>
            <div id="pwd1" class="righttips findPwd-tips mt10"></div>
            <div class="clear"></div>
        </div>
        <div style="padding-left:72px;margin-bottom:10px;">
        	<div id="pwdStrength" class="pwdStrength">
            	<em>低</em><em>中</em><em>高</em>
            </div>
        </div>
 		{{csrf_field()}}
        <div class="findPwd-item">
        	<label class="findPwd-label2 fl" style="width:70px;">确认密码：</label>
			<div class="findPwd-input fl">
			<input name="" id="findPwd_confirmPassword" class="nreg_input" value="" type="password">
			</div>
            <div class="findPwd-tips mt10" id="pass_msg"></div>
            <div class="clear"></div>
        </div>
        <div class="findPwd-item">
        	<label class="findPwd-label2 fl" style="width:70px;">&nbsp;</label>
			<div class="findPwd-sbt fl">
			<input class="findPwd-btn findPwd-ok" onclick="checkCodes('1')" type="button">
			</div>
            <div class="clear"></div>
        </div> 
	</div>	
</div>
</form>
<!--底部start-->
<div class="footer Gray">
	<p class="tright">Copyright © 2011-2014 Yougou Technology Co., Ltd. <a href="http://www.miibeian.gov.cn/" target="_blank">粤ICP备09070608号-4</a> 增值电信业务经营许可证：<a href="http://www.miibeian.gov.cn/" target="_blank" style="padding-left:0">粤 B2-20090203</a></p>

</div>
<!--底部end--> 
<script type="text/javascript">
$(function(){
	$(".findPwd-input").click(function(){
		$(this).children(".findPwd-ts").hide();
		$(this).children("input").focus();
	});
	$(".nreg_input,.nreg_sinput").focus(function(){
		$(this).addClass("nreg_yellowbor").siblings(".findPwd-ts").hide();
	}).focusout(function(){
		$(this).removeClass("nreg_yellowbor");$(this).val()==''?$(this).siblings(".findPwd-ts").show():$(this).siblings(".findPwd-ts").hide();
	});

	//密码强度验证
	$('#findPwd_password').keyup(function(){
		var that=$(this);
		var val=that.val();
		var em=$("#pwdStrength em");
		var score=0;
		// 有小写字母有数字
		if(val.match(/[a-z]/) && val.match(/\d+/)&&val.length>2 || val.length>12){score+=5}
		// 有大写字母有数字
		if(val.match(/[A-Z]/) && val.match(/\d+/)){score+=7}
		// 有小写字母和大写字母
		if(val.match(/[A-Z]/) && val.match(/[a-z]/)){score+=7}
		if(val.match(/[a-z]/) && val.match(/\d+/) && val.match(/[A-Z]/)){score+=10}
		// 有特殊字符
		if(val.match(/.[!,@,#,$,%,^,&,*,?,_,~]/)){score+=15}
		
		// 有小写字母或者大写字母有数字超过12位
		if(val.match(/[a-z]/) && val.match(/\d+/) && val.length>12){score+=15}
		if(val.match(/[A-Z]/) && val.match(/\d+/) && val.length>12){score+=15}
		//if(val.match(/[a-z]/) && val.match(/\d+/) && val.match(/[A-Z]/)){score+=10}
		
		if(val.length==0){
			$('#findPwd_password').siblings(".findPwd-ts").show();
			em.attr("class","");
			return false;
		}else{
			$('#findPwd_password').siblings(".findPwd-ts").hide();
		}
		if(score<5){
			em.eq(0).addClass("pwdLow").siblings().attr("class","");
		}else if(score>=5 && score<20&&val.length>2){
			em.eq(1).addClass("pwdMid").siblings().attr("class","");
		}else if(score>=20&&val.length>2){
			em.eq(2).addClass("pwdHeight").siblings().attr("class","");
		}
	});
	
	$("#findPwd_confirmPassword").blur(function(){
		if($(this).val()!=$("#findPwd_password").val()){
			$(this).parent().next(".findPwd-tips").removeClass("righttips").addClass("errortips").html("两次密码输入不一致");
		}else{
			$(this).parent().next(".findPwd-tips").removeClass("errortips").html("");
		}
	});
});
</script>
<!--<script src="http://s1.ygimg.cn/template/common/js/mv.js?4.3.3" type="text/javascript"></script>-->
<script>
 var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23566531-1']);
  _gaq.push(['_setDomainName', '.yougou.com']);
  _gaq.push(['_addOrganic', 'baidu', 'word']);
  _gaq.push(['_addOrganic', 'soso', 'w']);
  _gaq.push(['_addOrganic', '3721', 'name']);
  _gaq.push(['_addOrganic', 'yodao', 'q']);
  _gaq.push(['_addOrganic', 'vnet', 'kw']);
  _gaq.push(['_addOrganic', 'sogou', 'query']);
  _gaq.push(['_trackPageview']);
  _gaq.push(['_trackPageLoadTime']);
(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://analytic' : 'http://analytic') + '.yougou.com/ga.js?4.3.3';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>

</body></html>