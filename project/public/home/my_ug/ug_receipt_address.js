	//添加新的地址div 显示 隐藏
$('#newAddressBtn').click(function() {

      $('#newAddressBtn').css('display','none');
      $('#addressFormDiv').css('display','block');
      $('#tbl_address').css('display','none');
  });
  $('#closeFormBtn').click(function() {
      $('#newAddressBtn').css('display','block');
      $('#addressFormDiv').css('display','none');
        $('#tbl_address').css('display','block');
  });
  //单击取消隐藏div
  $('#cancelBtn').click(function() {
  	   $('#newAddressBtn').css('display','block');
  	   $('#addressFormDiv').css('display','none');
        $('#tbl_address').css('display','block');
  });

 // 为所有的input绑定onchangge事件
    isok=false;
 $(':input').change(function(){
 		//当提交的时候 判断是否选 择 省 市 县
  
  		//省
  		var s_province=$('#s_province').val();
  		if(s_province=='省份')
  		{
  			$('#area_tip').css('display','inline');
  			$('#area_tip').text('请选择省份');
  			return false;
  		}else{
  			$('#area_tip').text('　　　　　');
  		}
  		//市
  		var s_city=$('#s_city').val();
  		if(s_city=='地级市、')
  		{
  			$('#area_tip').css('display','inline');
  			$('#area_tip').text('请选择地级市');
  			return false;
  		}else{
  			$('#area_tip').text('　　　　　');
  		}
  		//县
  		var s_county=$('#s_county').val();
  		if(s_county=='县级市')
  		{
  			$('#area_tip').css('display','inline');
  			$('#area_tip').text('请选择县级市');
  			return false;
  		}else{
  			$('#area_tip').text('√　　　　');			
  		}



  		 /**
  		 *
  		 *判断详细地址是否符合要求
  		 *
  		 */
  		 var  receivingAddress=$('#receivingAddress').val();
  		 if(receivingAddress.length<5)
  		 {
  		 	$('#receivingAddress_tip').text('请输入收货人地址，要求5-120个字符');
  		 	$('#receivingAddress_tip').css('display','inline'); 
        $('#receivingAddress_tip').attr('class','myaddr_tip');
  		 	return false;
  		 }
  		 if(!isNaN(receivingAddress))
  		 {
  		 	
        $('#receivingAddress_tip').attr('class','myaddr_tip');
  		 	$('#receivingAddress_tip').text('请填写收货人地址，不能全部是数字/英文/包含特殊符号(括号、井号等)');
  		 	$('#receivingAddress_tip').css('display','inline');
  		 	return false;
  		 }		
  		 receivingAddress_reg=/[(\ )(\~)(\!)(\@)(\#) (\$)(\%)(\^)(\?)(\&)(\*)(\()(\))(\-)(\_)(\+)(\=) (\[)(\])(\{)(\})(\|)(\\)(\;)(\:)(\')(\")(\,)(\.)(\/) (\<)(\>)(\ )(\)(\`)]+/;

  		 receivingAddress_res=receivingAddress_reg.test(receivingAddress);
  	
  		 if(receivingAddress_res)
  		 {
  		 	$('#receivingAddress_tip').text('请填写收货人地址，不能全部是数字/英文/包含特殊符号(括号、井号等)');
        $('#receivingAddress_tip').attr('class','myaddr_tip');
  		 	$('#receivingAddress_tip').css('display','inline');
  		 	return false;
  		 }else{
  		 	$('#receivingAddress_tip').text('√');
  		 	$('#receivingAddress_tip').attr('class','myaddr_tiperr');
  		 	$('#receivingAddress_tip').css('display','inline'); 	
  		 	$('#receivingAddress_tip').css('color','#FF5000'); 	
  		 }
  		  /**
  		  *
  		  *	判断收货人姓名是否符合要求
  		  *
  		  */ 
  		 var  receivingName=$('#receivingName').val();
  		 if(receivingName.length==0)
  		 {
        $('#receivingName_tip').attr('class','myaddr_tip');
  		 	$('#receivingName_tip').text('请输入收货人姓名，要求5-30个字符');
  		 	$('#receivingName_tip').css('display','inline');

  		 	return false;
  		 }
  		 if(!isNaN(receivingName))
  		 {
  		 	$('#receivingName_tip').attr('class','myaddr_tip');
  		 	$('#receivingName_tip').text('请填写收货人姓名，不能全部是数字/英文/包含特殊符号(括号、井号等)');
  		 	$('#receivingName_tip').css('display','inline');
  		 	return false;
  		 }		
  		 receivingName_reg=/[(\ )(\~)(\!)(\@)(\#) (\$)(\%)(\^)(\?)(\&)(\*)(\()(\))(\-)(\_)(\+)(\=) (\[)(\])(\{)(\})(\|)(\\)(\;)(\:)(\')(\")(\,)(\.)(\/) (\<)(\>)(\ )(\)(\`)]+/;

  		 receivingName_res=receivingName_reg.test(receivingName);
  		 
  		 if(receivingName_res)
  		 {
          $('#receivingName_tip').attr('class','myaddr_tip');
  		 	$('#receivingName_tip').text('请填写收货人姓名，不能全部是数字/英文/包含特殊符号(括号、井号等)');
  		 	$('#receivingName').css('display','inline');
  		 	return false;
  		 }else{
  		 	$('#receivingName_tip').text('√');
  		 	$('#receivingName_tip').attr('class','myaddr_tiperr');
  		 	$('#receivingName_tip').css('display','inline'); 	
  		 	$('#receivingName_tip').css('color','#FF5000');	
  		 }
  		  /**
  		  *
  		  *	判断手机号码是否符合要求
  		  *
  		  */ 

  		 var receivingMobilePhone=$('#receivingMobilePhone').val();
  		 	receivingMobilePhone_reg=/^1[3|4|5|7|8]\d{9}$/;
  		 	receivingMobilePhone_res=receivingMobilePhone_reg.test(receivingMobilePhone);

  		 	if (receivingMobilePhone_res)
  		 	 {
  		 	 	$('#receivingMobilePhone_tip').text('√');
	  		 	$('#receivingMobilePhone_tip').attr('class','myaddr_tiperr');
	  		 	$('#receivingMobilePhone_tip').css('display','inline'); 	
	  		 	$('#receivingMobilePhone_tip').css('color','#FF5000'); 
	  		   isok=true;
  		 	 }else{

  		 	 	$('#receivingMobilePhone_tip').text('手机号不符合格式');
            $('#receivingMobilePhone_tip').attr('class','myaddr_tip');
  		 	 	$('#receivingMobilePhone_tip').css('display','block');
  		 	   return false;
  		 	 }
 });

  $('input[type=submit]').click(function()
  {
    if (isok == true)
    {

      return true;
    };

    return false;
  })

  

  
 